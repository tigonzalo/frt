﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Empresas
{
    public class Empresa
    {
   public long  codEmpresa          { get; set; }
   public string nombreEmp          { get; set; }
   public string rfcEmp             { get; set; }
   public string telefonoEmp        { get; set; }
   public string imgEmp             { get; set; }
   public string mailFromEmp        { get; set; }
   public string mailPasswordEmp    { get; set; }
   public string mailHostEmp        { get; set; }
   public int    mailPuertoEmp      { get; set; }
   public bool   mailSslEmp         { get; set; }
   public string calleEmp           { get; set; }
   public string noExteriorEmp      { get; set; }
   public string noInteriorEmp      { get; set; }
   public string coloniaEmp         { get; set; }
   public string localidadEmp       { get; set; }
   public long   codMunicipio       { get; set; }
   public string codEstado          { get; set; }
   public string paisEmp            { get; set; }
   public string cpEmp              { get; set; }
   public string contactoEmp        { get; set; }
   public string razonSocialEmp     { get; set; }
   public string rutaEmp            { get; set; }
   public string passwordCFDI       { get; set; }
   public string PathPlantillaPDF   { get; set; }
   public string PathArchivosCFDI   { get; set; }
   public string CodigoFactura      { get; set; }
   public string codConcepto        { get; set; }
   public string regimenFiscal      { get; set; }
   public string noCertificado      { get; set; }
    }
}