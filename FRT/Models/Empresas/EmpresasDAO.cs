﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;

namespace FRT.Models.Empresas
{
    public class EmpresasDAO
    {
        public long TotalRecords { get; set; }
        public List<Empresa> Listado(int? start, long? limit, long? codEmp,string valor, string campo)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TEMPRESAS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codEmp", codEmp);
            com.Parameters.AddWithValue("@busqvalor", valor);
            com.Parameters.AddWithValue("@busqcampo", campo);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Empresa> list = new List<Empresa>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Empresa
                    {
                        codEmpresa = (long)dr["codEmpresa"],
                        nombreEmp = (string)dr["nombreEmp"],
                        rfcEmp = (string)dr["rfcEmp"],
                        telefonoEmp = (string)dr["telefonoEmp"],
                        imgEmp =!Convert.IsDBNull(dr["imgEmp"])?(string)dr["imgEmp"]:"",
                        mailFromEmp = (string)dr["mailFromEmp"],
                        mailPasswordEmp = (string)dr["mailPasswordEmp"],
                        mailHostEmp = (string)dr["mailHostEmp"],
                        mailPuertoEmp = (int)dr["mailPuertoEmp"],
                        mailSslEmp = (bool)dr["mailSslEmp"],
                        calleEmp = (string)dr["calleEmp"],
                        noExteriorEmp = (string)dr["noExteriorEmp"],
                        noInteriorEmp =!Convert.IsDBNull(dr["noInteriorEmp"])?(string)dr["noInteriorEmp"]:"",
                        coloniaEmp = (string)dr["coloniaEmp"],
                        localidadEmp =!Convert.IsDBNull(dr["localidadEmp"])?(string)dr["localidadEmp"]:"",
                        codMunicipio = (long)dr["codMunicipio"],
                        codEstado = (string)dr["codEstado"],
                        paisEmp = (string)dr["paisEmp"],
                        cpEmp = (string)dr["cpEmp"],
                        contactoEmp = (string)dr["contactoEmp"],
                        razonSocialEmp = (string)dr["razonSocialEmp"],
                        rutaEmp = !Convert.IsDBNull(dr["rutaEmp"])?(string)dr["rutaEmp"]:"",
                        passwordCFDI =!Convert.IsDBNull(dr["passwordCFDI"])?(string)dr["passwordCFDI"]:"",
                        PathPlantillaPDF = !Convert.IsDBNull(dr["PathPlantillaPDF"]) ? (string)dr["PathPlantillaPDF"] : "",
                        PathArchivosCFDI =!Convert.IsDBNull(dr["PathArchivosCFDI"])?(string)dr["PathArchivosCFDI"]:"",
                        CodigoFactura =!Convert.IsDBNull(dr["CodigoFactura"])?(string)dr["CodigoFactura"]:"",
                        noCertificado = !Convert.IsDBNull(dr["noCertificado"]) ? (string)dr["noCertificado"] : ""
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

        public void Guardar(Empresa objEmpresa,string img)
        {
            //DataTable objTemTableArrayP = new DataTable();
            //objTemTableArrayP = Utils.TableDate<PerfilPermiso>(objPerfil.Perfilpermisos);

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TEMPRESAS_GUARDAR";
            com.Parameters.AddWithValue("@codEmp", objEmpresa.codEmpresa);
            com.Parameters.AddWithValue("@nombreEmp", objEmpresa.nombreEmp);
            com.Parameters.AddWithValue("@rfcEmp", objEmpresa.rfcEmp);
            com.Parameters.AddWithValue("@telefonoEmp", objEmpresa.telefonoEmp);
            com.Parameters.AddWithValue("@imgEmp", img);
            com.Parameters.AddWithValue("@mailFromEmp", objEmpresa.mailFromEmp);
            com.Parameters.AddWithValue("@mailPasswordEmp", objEmpresa.mailPasswordEmp);
            com.Parameters.AddWithValue("@mailHostEmp", objEmpresa.mailHostEmp);
            com.Parameters.AddWithValue("@mailPuertoEmp", objEmpresa.mailPuertoEmp);
            com.Parameters.AddWithValue("@mailSslEmp", objEmpresa.mailSslEmp);
            com.Parameters.AddWithValue("@calleEmp", objEmpresa.calleEmp);
            com.Parameters.AddWithValue("@noExteriorEmp", objEmpresa.noExteriorEmp);
            com.Parameters.AddWithValue("@noInteriorEmp", objEmpresa.noInteriorEmp);
            com.Parameters.AddWithValue("@coloniaEmp", objEmpresa.coloniaEmp);
            com.Parameters.AddWithValue("@localidadEmp", objEmpresa.localidadEmp);
            com.Parameters.AddWithValue("@codMunicipio", objEmpresa.codMunicipio);
            com.Parameters.AddWithValue("@codEstado", objEmpresa.codEstado);
            com.Parameters.AddWithValue("@paisEmp", objEmpresa.paisEmp);
            com.Parameters.AddWithValue("@cpEmp", objEmpresa.cpEmp);
            com.Parameters.AddWithValue("@contactoEmp", objEmpresa.contactoEmp);
            com.Parameters.AddWithValue("@razonSocialEmp", objEmpresa.razonSocialEmp);
            com.Parameters.AddWithValue("@rutaEmp", objEmpresa.rutaEmp);
            com.Parameters.AddWithValue("@passwordCFDI", objEmpresa.passwordCFDI);
            com.Parameters.AddWithValue("@PathPlantillaPDF", objEmpresa.PathPlantillaPDF);
            com.Parameters.AddWithValue("@PathArchivosCFDI", objEmpresa.PathArchivosCFDI);
            com.Parameters.AddWithValue("@CodigoFactura", objEmpresa.CodigoFactura);
            //com.Parameters.AddWithValue("@tbPermisos", objTemTableArrayP);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        public void Eliminar(long codEmp)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TEMPRESAS_ELIMINAR";
            com.Parameters.AddWithValue("@codEmp", codEmp);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        
    
    }

}