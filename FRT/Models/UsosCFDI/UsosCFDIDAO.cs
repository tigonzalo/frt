﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Admin;
using FRT.Models.Empresas;
using FRT.Models.Usuarios;

namespace FRT.Models.UsosCFDI
{
    public class UsosCFDIDAO
    {
        public long TotalRecords { get; set; }
        public List<UsoCFDI> Listado()
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TUSOS_CFDI_LISTADO";
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<UsoCFDI> list = new List<UsoCFDI>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new UsoCFDI
                    {
                        codUsoCFDI = (long)dr["codUsoCFDI"],
                        aplicaFisica = !Convert.IsDBNull(dr["aplicaFisica"]) ? (bool)dr["aplicaFisica"] : false,
                        aplicaMoral = !Convert.IsDBNull(dr["aplicaMoral"]) ? (bool)dr["aplicaMoral"] : false,
                        codigo = !Convert.IsDBNull(dr["codigo"]) ? (string)dr["codigo"] : "",
                        descripcion = !Convert.IsDBNull(dr["descripcion"]) ? (string)dr["descripcion"] : ""
                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}