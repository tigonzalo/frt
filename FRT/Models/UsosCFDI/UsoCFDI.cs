﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.UsosCFDI
{
    public class UsoCFDI
    {
        public long codUsoCFDI { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public bool aplicaFisica { get; set; }
        public bool aplicaMoral { get; set; }
    }
}