﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Admin;
using FRT.Models.Clientes;

namespace FRT.Models
{
     
    public class SistemasDAO
    {
        public long TotalRecords { get; set; }
        public List<Sistema> Listado(int? codSistema)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TSISTEMA_LISTADO";
            com.Parameters.AddWithValue("@codSistema", codSistema);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Sistema> list = new List<Sistema>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Sistema
                    {
                        
                        codSistema = (long)dr["codSistema"],
                        GLO_MAIL = !Convert.IsDBNull(dr["GLO_MAIL"]) ? (string)dr["GLO_MAIL"] : "",
                        GLO_MAIL_CONTRASENIA = !Convert.IsDBNull(dr["GLO_MAIL_CONTRASENIA"]) ? (string)dr["GLO_MAIL_CONTRASENIA"] : "",
                        GLO_MAIL_FROM = !Convert.IsDBNull(dr["GLO_MAIL_FROM"]) ? (string)dr["GLO_MAIL_FROM"] : "",
                        GLO_MAIL_HOST = !Convert.IsDBNull(dr["GLO_MAIL_HOST"]) ? (string)dr["GLO_MAIL_HOST"] : "",
                        GLO_MAIL_PUERTO = !Convert.IsDBNull(dr["GLO_MAIL_PUERTO"]) ? (int)dr["GLO_MAIL_PUERTO"] : 0,
                        GLO_MAIL_SSL = !Convert.IsDBNull(dr["GLO_MAIL_SSL"]) ? (bool)dr["GLO_MAIL_SSL"] : false,
                        DESCRIPCION = !Convert.IsDBNull(dr["DESCRIPCION"]) ? (string)dr["DESCRIPCION"] : ""

                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public void recuperarContrasenia(string correo) {
            List<Cliente> listCliente = new List<Cliente>();
            ClientesDAO objClientesDAO = new ClientesDAO();
            listCliente = objClientesDAO.Listado(null, null, correo, null, null);
            this.TotalRecords = listCliente.Count;
            if (listCliente.Count > 0) {
                Cliente objCliente = listCliente[0];
                List<Sistema> listSistema = new List<Sistema>();
                listSistema = this.Listado(1);
                Sistema objSistena = listSistema[0];
                Correo.enviar(objCliente, objSistena, "Contrasenia", "contrasenia");
            }
          



        }
    }
}