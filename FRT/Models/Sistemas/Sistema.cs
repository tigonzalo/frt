﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models
{
    public class Sistema
    {
        public long   codSistema           { get; set; }
        public int    GLO_MAIL_PUERTO      { get; set; }
        public string GLO_MAIL_HOST        { get; set; }
        public string GLO_MAIL_FROM        { get; set; }
        public string GLO_MAIL_CONTRASENIA { get; set; }
        public string GLO_MAIL             { get; set; }
        public bool   GLO_MAIL_SSL         { get; set; }
        public string DESCRIPCION          { get; set; }
    }
}