﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models
{
    public class StatusDocumento
    {
        public int    CodStatusDoc { get; set; }
        public string Descripcion  { get; set; }
    }
}