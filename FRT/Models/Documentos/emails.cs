﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//necesarios para los métodos
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace FRT.Models.Documentos
{
    public class emails
    {

        MailMessage message;
        SmtpClient clienteSmtp;
        Attachment at;
        int puerto;
        string user, password;

        public bool validarEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }

        public void ConfigurarMail(string from, string usuario, string contraseña, int puerto_salida, string smtp)
        {
            message = new MailMessage();

            message.From = new MailAddress(from);

            clienteSmtp = new SmtpClient(smtp);

            user = usuario;

            password = contraseña;

            clienteSmtp.Port = puerto_salida;

        }

        public bool EnviarMail(string to, string cc, string asunto, string mensaje, string ruta_archivo_xml, string ruta_archivo_pdf, bool ssl)
        {

            try
            {

                at = new Attachment(ruta_archivo_xml);

                message.Attachments.Add(at);

                at = new Attachment(ruta_archivo_pdf);

                message.Attachments.Add(at);


                message.To.Add(to);

                message.CC.Add(cc);

                message.Subject = asunto;

                message.IsBodyHtml = true; //el texto del mensaje lo pueden poner en HTML y darle formato

                message.Body = mensaje;

                //Establesco que usare seguridad (ssl = Secure Sockets Layer) 
                clienteSmtp.EnableSsl = ssl;

                clienteSmtp.UseDefaultCredentials = false;

                clienteSmtp.Credentials = new NetworkCredential(user, password);

                clienteSmtp.Send(message);

                return true;

            }
            catch
            {

                try
                {
                    //Establesco que no usare seguridad ssl (por si no pudo enviarlo con ssl habilitado)
                    clienteSmtp.EnableSsl = false;
                    clienteSmtp.Send(message);

                    return true;
                }
                catch
                {
                    return false;
                }

            }

        }
    }
}