﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Documentos;
using System.Text;
namespace FRT.Models.contpaqi
{
    public class Documento
    {
        public int CodDocumento { get; set; }
        public string CodConcepto { get; set; }
        public string Serie { get; set; }
        public double Folio { get; set; }
        public string FechaDocumento { get; set; }
        public string CodCliente { get; set; }
        public string CodAgente { get; set; }
        public int SistemaOrigen { get; set; }
        public int NumMoneda { get; set; }
        public double TipoCambio { get; set; }
        public double Importe { get; set; }
        public double Descuento1 { get; set; }
        public double Descuento2 { get; set; }
        public int Afecta { get; set; }
        public int Gasto1 { get; set; }
        public int Gasto2 { get; set; }
        public int Gasto3 { get; set; }
        public List<Movimiento> Movimientos { get; set; }

        public Documento() {
            Movimientos = new List<Movimiento>();
        }
        
        public void agregarMovimiento(  string codAlmacen  
          ,int    consecutivo  
          ,string codProducto  
          ,double unidades  
          ,double precio  
          ,double costo  
          ,string codClasificacion  
          ,string referencia  ) {

              Movimientos.Add(new Movimiento
              {
                  CodAlmacen = codAlmacen,
                  Consecutivo = consecutivo,
                  CodProducto = codProducto,
                  Unidades = unidades,
                  Precio = precio,
                  Costo = costo,
                  CodClasificacion = codClasificacion,
                  Referencia = referencia
              });
       
        }



        //public void inicializarDocumento(DateTime fecha,
        //    string codAgente,
        //    string codCliente,
        //    string conceptoDocumento) {

        //       // SDK.tDocumento documento = new SDK.tDocumento();
        //        double folio = 0;
        //        StringBuilder refSerie = new StringBuilder(12);
        //    //revisar sdk de lino en fSiguienteFolio parametro Serie
        //        NumError = SDK.fSiguienteFolio(CodConcepto, ref refSerie, ref folio);
        //        Folio = folio;
        //        Serie = refSerie.ToString();


        //    /*
        //        StringBuilder lSerieDocto = new StringBuilder(12);
        //        SDKTest.SDK.tDocumento ltDocumento = new SDKTest.SDK.tDocumento();
        //        string lFechaDocto = "";
        //        string lCodigoAgente = "(Ninguno)";
        //        int lIdDocumento = 0;
        //        string lCodigoCteProv = "U081";
        //        string lConceptoDocto = ObjVenta.VentaConcepto.CodigoOrigenConcepto;*/
        //}

    }
}