﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.contpaqi
{
    public class Movimiento
    {
        //public SDKTest.SDK.tMovimiento Movimiento { get; set; }
        public string CodAlmacen { get; set; }
        public int    Consecutivo { get; set; }
        public string CodProducto { get; set; }
        public double Unidades { get; set; }
        public double Precio { get; set; }
        public double Costo { get; set; }
        public string CodClasificacion { get; set; }
        public string Referencia { get; set; }

        
         
    }
}