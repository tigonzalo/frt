﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models;
using FRT.Models.Documentos;
using System.Text;
using FRT.Models.contpaqi;
using FRT.Models.Empresas;
using FRT.Models.Clientes;
using FRT.Models.Usuarios;
using FRT.Models.Admin;
namespace FRT.Models
{
    public class AdminPaq
    {
        public string DirectorioEmpresa { get; set; }
        public string Paquete { get; set; }
        public string DirectorioSDK { get; set; }
        public int NumError { get; set; }
        public string DescError { get; set; }
        SDK.tDocumento documento = new SDK.tDocumento();
        SDK.tCteProv cliente = new SDK.tCteProv();
        SDK.tDireccion direccion = new SDK.tDireccion();
        List<ClienteFacturacion> objClienteF = new List<ClienteFacturacion>();
        FacturacionDAO objFacturacionDAO = new FacturacionDAO();
        Empresa objEmpresa = new Empresa();
        Usuario objUsuario = new Usuario();
        emails objEmail = new emails();
        public int CodDocumento { get; set; }
        public double ExistenciaProducto { get; set; }
        public double folio { get; set; }
        public double Propina { get; set; }
        public int  ldCteProv=0;
        public int CIDCATAL01 = 0;
        public double IVA= 16;
        public AdminPaq(long? codEmpresa)
        {
            
            //string DirectorioSDK = AppDomain.CurrentDomain.BaseDirectory;
            //DirectorioSDK = DirectorioSDK + "\\AdminPaq\\AdminPAQ";
            documento = new SDK.tDocumento();
            cliente = new SDK.tCteProv();
            direccion = new SDK.tDireccion();

           List<Empresa> ListEmpresa= new List<Empresa>();
           EmpresasDAO objEmpresaDAO = new EmpresasDAO();
           FacturacionDAO objFacturacioDAO = new FacturacionDAO();
           ListEmpresa = objEmpresaDAO.Listado(null,null,codEmpresa,null,null);
           objEmpresa = ListEmpresa[0];
           this.DirectorioEmpresa = objEmpresa.rutaEmp;
           this.Paquete = "CONTPAQ I Facturacion";
           string strProcArchi = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            if (strProcArchi.IndexOf("64") > 0)
                // Console.WriteLine("Processor 64 bit");
                this.DirectorioSDK = "C:\\Program Files (x86)\\Compacw\\Facturacion";
            else
                this.DirectorioSDK = "C:\\Program Files (x86)\\Compacw\\Facturacion";

           


        }
        public void agregarCliente() { 
        //vefificar si existe el cliente

        
        }
        public void agregarDocumento(string codConcepto
          , string serie
          , double folio
          , string fechaDocumento
          , string codCliente
          , string codAgente
          , int sistemaOrigen
          , int numMoneda
          , double tipoCambio
          , double importe
          , double descuento1
          , double descuento2
          , int afecta
          , int gasto1
          , int gasto2
          , int gasto3
          , double propina)
        {
            documento.aCodConcepto = codConcepto;
            documento.aSerie = serie;
            documento.aFolio = folio;
            documento.aFecha = fechaDocumento;
            documento.aCodigoCteProv = codCliente;
            documento.aCodigoAgente = codAgente;
            documento.aSistemaOrigen = sistemaOrigen;
            documento.aNumMoneda = numMoneda;
            documento.aTipoCambio = tipoCambio;
            documento.aImporte = importe;
            documento.aDescuentoDoc1 = descuento1;
            documento.aDescuentoDoc2 = descuento2;
            documento.aReferencia = "";
            documento.aAfecta = afecta;
            documento.aGasto1 = gasto1;
            documento.aGasto2 = gasto2;
            documento.aGasto3 = gasto3;
            this.Propina = propina;
        }
        public void agregarDocumento(Documento ObjDocumento)
        {

            agregarDocumento(
                //Concepto de la venta
                ObjDocumento.codConcepto,
                //Serie de la Documento 
                ObjDocumento.folio,
                //Folio de la Documento
                Convert.ToSingle(ObjDocumento.folio),
                //fecha de la Documento
                //ObjDocumento.FechaOperacion.ToString("MM/dd/yyyy"),
                DateTime.Today.ToString("MM/dd/yyyy"),
                //codigo Cliente
                ObjDocumento.codCliente.ToString(),
                //codigo del Agente
                "(NINGUNO)",
                //Sistema Origen 205=COMERCIAL,0=AdminPAQ //int
                0,
                //Moneda
                1,
                //Tipo de Cambio
                1,
                //Total de la Documento
                (double)ObjDocumento.monto,
                //Descuento 1
                0,
                //Descuento 2
                0,
                //Afecta parametro para afectar o desafectar
                1,
                //gasto 1-2-3
                0, 0, 0,
                // Propina del documento
                ObjDocumento.propina
                );

        }
        public void iniciarAdminPaq()
        {
            //@"C:\Compacw\Empresas\SIAT"
            SDK.SetCurrentDirectory(DirectorioSDK);
            NumError = SDK.fSetNombrePAQ(Paquete);

            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void timbraXML(long codDocumento) {
            StringBuilder lUUID = new StringBuilder();

            string path=AppDomain.CurrentDomain.BaseDirectory + "Models\\DocumentosPac\\";
            string aRutaXML = "C:\\inetpub\\wwwroot\\FRT\\FRT\\Models\\DocumentosPac\\facturas\\0.xml",
                    aCodConcepto = "100", 
                    aRutaDDA = "",
                    aRutaResultado = "C:\\inetpub\\wwwroot\\FRT\\FRT\\Models\\DocumentosPac\\facturas2",
                    aPass = "051589ZO",
                    aRutaFormato = "C:\\Compacw\\Empresas\\Reportes\\Facturacion\\PLANTILLAS\\PLAZA DE TOROS\\Plantilla_Factura_cfdi_pampasB.htm";
           
            this.abrirEmpresa(true);
            
            NumError = SDK.fInicializaLicenseInfo(1);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }

            NumError = SDK.fTimbraXML(aRutaXML,aCodConcepto, lUUID,  aRutaDDA,  aRutaResultado,  aPass, aRutaFormato);
             
            if (NumError != 0)
             {
                 StringBuilder sMensaje = new StringBuilder(512);
                 string mensaje = "";
                 SDK.rError(NumError, ref mensaje);
                 DescError = mensaje;
                 return;
             }
        }

        public void abrirEmpresa(bool iniciarAdminPaq)
        {
            if (iniciarAdminPaq) this.iniciarAdminPaq();
            if(NumError==0)
               NumError = SDK.fAbreEmpresa(DirectorioEmpresa);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }

        }
        public void cerrarEmpresa(bool terminarAdminPaq)
        {
            SDK.SetCurrentDirectory(DirectorioSDK);
            SDK.fCierraEmpresa();
            if (terminarAdminPaq) this.terminarAdminPaq();
        }
        public void terminarAdminPaq()
        {
            SDK.fTerminaSDK();
        }

        public void generarDocumentoAdminPaq(Documento ObjDocumento)
        {
            
            this.verificarClienteFacturacion(ObjDocumento);
            this.agregarDocumento(ObjDocumento);
            this.obternerSiguenteFolio();
            if (NumError == 0)
                this.altaDocumento();
            if (NumError == 0)
            {
                NumError = SDK.fEditarDocumento();
                NumError = SDK.fSetDatoDocumento("cmetodopag", ObjDocumento.formaPago);
                NumError = SDK.fSetDatoDocumento("ctextoex01", "FechaConsumo; " + ObjDocumento.fechaConsumo.ToString("dd/MM/yyyy") + " Tikect:" + ObjDocumento.folio);
                NumError = SDK.fGuardaDocumento();

                this.altaMovimientos(ObjDocumento);
                if (NumError == 0)
                    this.afectarDocumento();
                if (NumError == 0)
                    this.timbrarfactura(ObjDocumento);
                if (NumError == 0)
                    this.enviocorreo(ObjDocumento, documento.aFolio);
                    //Actualiza Status de Documento a 3 de Facturado y enviado
                    objFacturacionDAO.ActualizarStatusDocumento(objEmpresa.codEmpresa, ObjDocumento.codDocumento, 3);
                    //this.cerrarEmpresa(true);

            }
            this.cerrarEmpresa(true);
        }
        private bool timbrarfactura(Documento ObjDocumento)
        {
            bool functionReturnValue = false;
            //long llError = 0;           
            string lPlantilla = null;
            int NoTikect = 0;
            StringBuilder lSerieDocto = new StringBuilder(12);

            // Revisa licencia
            //  byte[] lByte = System.Text.Encoding.ASCII.GetBytes(SISTEMA);
            NumError = SDK.fInicializaLicenseInfo(1); //' 0=AdminPAQ, 1=FACTURA ELECTRÓNICA
            if (NumError != 0)
            {
                //MessageBox.Show(rError(lError))
                SDK.fCierraEmpresa();
                //SDK.fTerminaSDK();
                return functionReturnValue;
            }

            // Emite el documento
            try
            {
                //CONTRASENAFACTURACFDI
                //lError = fEmitirDocumento("100", SerieFac, FolioFac, "051589ZO", "") 'Obtener Password de Sistema de Facturacion
                
                NumError = SDK.fEmitirDocumento(objEmpresa.CodigoFactura, documento.aSerie, documento.aFolio ,objEmpresa.passwordCFDI , "");//Obtener Password de Sistema de Facturacion // Falta Obtener el valor de la serie
                if (NumError != 0)
                {
                    //MessageBox.Show(rError(lError))
                    if (NumError == -1)
                    {
                        NumError = -2;
                    }
                    SDK.fCierraEmpresa();
                    //SDK.fTerminaSDK();
                    return functionReturnValue;
                }
                
                NoTikect = Int32.Parse(ObjDocumento.folio);
                objFacturacionDAO.ActualizarFolioFactura(objEmpresa.codEmpresa, NoTikect, documento.aFolio,ObjDocumento.codDocumento);
                //Actualiza Status de Documento a 3 de Facturado
                objFacturacionDAO.ActualizarStatusDocumento(objEmpresa.codEmpresa,  ObjDocumento.codDocumento, 2);
            }
            catch (Exception ex)
            {
                //MsgBox(ex.ToString)
                return false;
            }
            finally
            {
            }

            
            //lPlantilla = "C:\\Compacw\\Empresas\\Reportes\\Facturacion\\Plantilla_Factura_cfdi_pampasA.htm";
            lPlantilla = objEmpresa.PathPlantillaPDF;
            try
            {
                //1= pdf, 0 = xml

                //Genera Archivo XML
                NumError = SDK.fEntregEnDiscoXML(objEmpresa.CodigoFactura, documento.aSerie, documento.aFolio, 0, lPlantilla); // Falta Obtener el valor de la serie
                //Genera Archivo PDF
                NumError = SDK.fEntregEnDiscoXML(objEmpresa.CodigoFactura, documento.aSerie, documento.aFolio, 1, lPlantilla); // Falta Obtener el valor de la serie
                

                if (NumError != 0)
                {
                    //MessageBox.Show(rError(lError))
                    if (NumError == -1)
                    {
                        NumError = -3;
                    }

                    SDK.fCierraEmpresa();
                    SDK.fTerminaSDK();
                    return functionReturnValue;
                }
            }
            catch (Exception ex)
            {
                //MsgBox(ex.ToString)
                return false;
            }
            finally
            {
            }


            return true;
            //return functionReturnValue;
        }

        private bool enviocorreo(Documento ObjDocumento, Double aFactura)
        {
            try
            {
                var correoCliente = objUsuario.mailUsuario;
               
                UsuariosDAO objUsuarioDAO = new UsuariosDAO();
                List<Usuario> ListUsuario= new List<Usuario>();
                ListUsuario = objUsuarioDAO.Listado(null,null,null,null,ObjDocumento.codUsuario,null);
                objUsuario = ListUsuario[0];
                correoCliente = objUsuario.mailUsuario;

                List<Documento> listDocumentox = new List<Documento>();
                DocumentosDAO objDocumentosDAO = new DocumentosDAO();
                listDocumentox = objDocumentosDAO.Listado(null, null, ObjDocumento.codDocumento, objUsuario,null);
                Documento objDocumentol = new Documento();
                objDocumentol = listDocumentox[0];

                Correo.enviarCFDI(ObjDocumento, objEmpresa, correoCliente, "ENVIO CFDI", aFactura, objDocumentol.rutaPdf, objDocumentol.rutaXml);
                
                
            }
            catch (Exception ex)
            {
                //MsgBox(ex.ToString)
                return false;
            }
            finally
            {
            }

            return true;
        }

        
        public void afectarDocumento(){
            NumError = SDK.fAfectaDocto_Param(documento.aCodConcepto, documento.aSerie.ToString(), documento.aFolio, true);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void obternerSiguenteFolio()
        {
            string serie = "";
            double folio = 0;
            NumError = SDK.fSiguienteFolio(documento.aCodConcepto, serie, ref folio);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            else
            {
                documento.aFolio = folio;
                documento.aSerie = serie.ToString();
                this.folio = folio;
            }
            

        }
        public void altaDocumento()
        {
            int lIdDocumento = 0;
            NumError = SDK.fAltaDocumento(ref lIdDocumento, ref documento);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            else { CodDocumento = lIdDocumento; }
           
        }
        public void altaMovimientos(Documento ObjDocumento)
        {
            int consecutivo = 0;
            //foreach (DetalleVenta movimiento in ObjDocumento.listaDetalleVenta)
            //{
                SDK.tMovimiento ltMovimiento = new SDK.tMovimiento();
                //double precio = 0;
                //this.precioProducto(ObjDocumento.VentaConcepto.CodigoOrigenConcepto, ObjDocumento.ClienteAdminPaq.CodigoCliente, movimiento.CodigoOrigen, ref precio);
                //movimiento.Precio = Convert.ToDecimal(precio);
                int lIdMovimiento = 0;
                string lCodigoAlmacen = "";
                lCodigoAlmacen = "1";

                ltMovimiento.aCodAlmacen = lCodigoAlmacen;
                ltMovimiento.aConsecutivo = consecutivo+1;
                ltMovimiento.aCodProdSer = "CONSUMO";
                ltMovimiento.aUnidades = 1;
                //ltMovimiento.aPrecio = ((ObjDocumento.monto - ObjDocumento.propina) / (1 + (ObjDocumento.Iva / 100)));
                ltMovimiento.aPrecio = (ObjDocumento.monto / (1 + (ObjDocumento.Iva / 100))); 
                ltMovimiento.aCosto = 0;
                ltMovimiento.aCodClasificacion = "";
                ltMovimiento.aReferencia = ObjDocumento.folio.ToString();
                NumError = SDK.fAltaMovimiento(CodDocumento, ref lIdMovimiento, ref ltMovimiento);
                if (NumError != 0)
                {
                    StringBuilder sMensaje = new StringBuilder(512);
                    string mensaje = "";
                    SDK.rError(NumError, ref mensaje);
                    DescError = mensaje;
                    return;
                }
            
            
            if (ObjDocumento.propina > 0) {
                ltMovimiento = new SDK.tMovimiento();
                lIdMovimiento = 0;
                ltMovimiento.aConsecutivo = 2;
                //'unidades
                ltMovimiento.aUnidades = 1;
                //'Precio
                //ltMovimiento.aPrecio = (ObjDocumento.propina / (1 + (ObjDocumento.Iva / 100)));
                ltMovimiento.aPrecio = ObjDocumento.propina; 
                //'Costo
                ltMovimiento.aCosto = 0;  //'(monto / (1 + (IVA / 100)))
                //' Código de producto
                ltMovimiento.aCodProdSer = "PROPINA";
                ltMovimiento.aReferencia = ObjDocumento.folio.ToString();
                ltMovimiento.aCodAlmacen = "1";
                ltMovimiento.aCodClasificacion = "";
                //'lMovimiento.aCosto = datMovtos.Item(3, lContador - 1).Value
                NumError = SDK.fAltaMovimiento(CodDocumento, ref lIdMovimiento, ref ltMovimiento);
                       
            }

            }
        public void existenciaProuducto(string codProducto, string codAlmacen)
        {
          
            DateTime fecha = DateTime.Today;
            string anio = fecha.Year.ToString();
            string mes = fecha.Month.ToString();
            string dia = fecha.Day.ToString();
            double existencia = 0;

            NumError = SDK.fRegresaExistencia(codProducto, codAlmacen, anio, mes, dia, ref existencia);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            else { ExistenciaProducto = existencia; }
           
        }
        public void precioProducto(string codigoConcepto, string codigoCliente, string codigoProducto,  ref double precioVenta)
        {
            StringBuilder precioVentaP = new StringBuilder(512);
            //ObjAdminPaq.precioProducto("102", "U067", "KAR002", precio);
            NumError=SDK.fRegresaPrecioVenta(codigoConcepto, codigoCliente, codigoProducto,  precioVentaP);
            precioVenta = Convert.ToSingle(precioVentaP.ToString());
        }
    
    //funciones Cliente
        public void verificarClienteFacturacion(Documento objDocumento)
        {
            
            objClienteF = objFacturacionDAO.Listado(objDocumento);
            if (objClienteF.Count == 1)
                CIDCATAL01 = objClienteF[0].CIDCATAL01;
            this.cargarCliente(objDocumento);
            this.cargarDireccion(objDocumento.cliente);
            this.actualizarCliente(objDocumento.cliente);
            
            if (objClienteF.Count == 0)
                this.altaCliente(objDocumento);
            else
                this.actualizarDireccionCliente();


        }
        public void actualizarDireccionCliente()
        {
           
                 NumError = SDK.fActualizaDireccion(ref direccion);
                 if (NumError != 0)
                     this.altaDireccion(CIDCATAL01);
                     
                 
             
        }
        public void actualizarCliente(Cliente objCliente) {
            if (objClienteF.Count == 0)
                return;
            

            NumError = SDK.fBuscaCteProv(objCliente.codCliente.ToString().ToUpper().Trim());
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            else
            {
                this.iniciarEdicionCliente();
               // this.editarcampoCliente("CCODIGOC01", objCliente.codCliente.ToString().ToUpper().Trim());
                if (objCliente.razonSocialClie.Trim() != objClienteF[0].RazonSocial.Trim())
                    this.editarcampoCliente("CRAZONSO01", objCliente.razonSocialClie.ToString().ToUpper());
                if (objCliente.rfcClie.Trim() != objClienteF[0].RFC.Trim())
                    this.editarcampoCliente("CRFC", objCliente.rfcClie);

                this.finalizarEdicionCliente();
            }

        }
        public void editarcampoCliente(string campo, string valor) {
            NumError = SDK.fSetDatoCteProv(campo,valor);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        
        
        }

        public void iniciarEdicionCliente() {
            NumError = SDK.fEditaCteProv();
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void finalizarEdicionCliente()
        {
            NumError = SDK.fGuardaCteProv();
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void altaCliente(Documento objDocumento)
        {

           
            
            NumError = SDK.fAltaCteProv(ref ldCteProv, ref cliente);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            else
                this.altaDireccion(ldCteProv);
            
        }
        public void altaDireccion( int ldCteProv)
        {

            NumError = SDK.fAltaDireccion(ref ldCteProv, ref direccion);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void cargarDireccion(Cliente objCliente)
        {
            direccion.cCodCteProv = objCliente.codCliente.ToString();
            direccion.cTipoCatalogo = 1;                   // 1=Clientes y 2=Proveedores
            direccion.cTipoDireccion = 1;                  //1=Domicilio Fiscal, 2=Domicilio Envio
            direccion.cNombreCalle = objCliente.calleClie.ToString().ToUpper();
            direccion.cNumeroExterior = objCliente.numExtClie.ToString().ToUpper();
            direccion.cNumeroInterior = objCliente.numIntClie.ToString().ToUpper();
            direccion.cColonia = objCliente.coloniaClie.ToString().ToUpper();
            direccion.cCodigoPostal = objCliente.cpClie.ToString().ToUpper();
            direccion.cTelefono1 = objCliente.usuario.telefonoUsuario;
            //direccion.cTelefono3 = objCliente.rfcClie;
            //direccion.cTelefono4 = objCliente.rfcClie;
            direccion.cEmail = objCliente.usuario.mailUsuario;
            direccion.cCiudad = objCliente.Municipio.nombreMunicipio.ToString().ToUpper();
            direccion.cEstado = objCliente.Estado.nombreEstado.ToString().ToUpper();
            direccion.cPais = objCliente.paisClie.ToString().ToUpper();
        
        }
        public void cargarCliente(Documento objDocumento)
        {

            cliente.cCodigoCliente = objDocumento.cliente.codCliente.ToString();
            cliente.cRazonSocial = objDocumento.cliente.razonSocialClie.ToString().ToUpper();
            cliente.cFechaAlta = DateTime.Today.ToString("MM/dd/yyyy");
            cliente.cRFC = objDocumento.cliente.rfcClie.ToString().ToUpper();
            //cliente.cCURP = objDocumento.Cliente..ToString().ToUpper();
            //cliente.cNombreMoneda = "Peso Mexicano", kLongNombre) '[ kLongNombre + 1 ];
            cliente.cNombreMoneda = "Peso Mexicano";
            cliente.cBanVentaCredito = 1;  //0 = No se permite venta a crédito, 1 = Se permite venta a crédito
            cliente.cTipoCliente = 1;      //' 1 - Cliente, 2 - Cliente/Proveedor, 3 - Proveedor
            cliente.cEstatus = 1;          //' 0. Inactivo, 1. Activo
            cliente.cListaPreciosCliente = 0;
            cliente.cDescuentoMovto = 0.0;
            cliente.cLimiteCreditoCliente = 0.0;
            cliente.cDiasCreditoCliente = 0;
            cliente.cBanExcederCredito = 1;  // 0 = No se permite exceder crédito, 1 = Se permite exceder el crédito
            cliente.cDescuentoProntoPago = 0.0;
            cliente.cDiasProntoPago = 0;
            cliente.cDiaPago = 0;
            cliente.cDiasRevision = 0;
            cliente.cDiasEmbarqueCliente = 0;
            cliente.cRestriccionAgente = 0;
            cliente.cImpuesto1 = 0.0;
            cliente.cImpuesto2 = 0.0;
            cliente.cImpuesto3 = 0.0;
            cliente.cRetencionCliente1 = 0.0;
            cliente.cRetencionCliente2 = 0.0;
            cliente.cLimiteCreditoProveedor = 0.0;
            cliente.cDiasCreditoProveedor = 0;
            cliente.cTiempoEntrega = 0;
            cliente.cDiasEmbarqueProveedor = 0;
            cliente.cImpuestoProveedor1 = 0.0;
            cliente.cImpuestoProveedor2 = 0.0;
            cliente.cImpuestoProveedor3 = 0.0;
            cliente.cRetencionProveedor1 = 0.0;
            cliente.cRetencionProveedor2 = 0.0;
            cliente.cBanInteresMoratorio = 0;
        }
    }
}