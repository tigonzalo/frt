﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Documentos;
using System.Text;

namespace FRT.Models.Documentos
{
    public class ClienteFacturacion
    {

        public int CIDCATAL01 { get; set; }
        public    String     CodigoCliente                       { get; set; }
        public    String     RazonSocial                         { get; set; }
        public    String     FechaAlta                           { get; set; }
        public    String     RFC                                 { get; set; }
        public    String     CURP                                { get; set; }
        public    String     DenComercial                        { get; set; }
        public    String     RepLegal                            { get; set; }
        public    String     NombreMoneda                        { get; set; }
        public    int        cListaPreciosCliente                { get; set; }
        public    double     DescuentoMovto                      { get; set; }
        public    int        BanVentaCredito                     { get; set; } // 0 = No se permite venta a crédito, 1 = Se permite venta a crédito
        public    String     CodigoValorClasificacionCliente1    { get; set; }
        public    String     CodigoValorClasificacionCliente2    { get; set; }
        public    String     CodigoValorClasificacionCliente3    { get; set; }
        public    String     CodigoValorClasificacionCliente4    { get; set; }
        public    String     CodigoValorClasificacionCliente5    { get; set; }
        public    String     CodigoValorClasificacionCliente6    { get; set; }
        public    int        TipoCliente                         { get; set; } // 1 - Cliente, 2 - Cliente/Proveedor, 3 - Proveedor
        public    int        Estatus                             { get; set; } // 0. Inactivo, 1. Activo
        public    String     FechaBaja                           { get; set; }
        public    String     FechaUltimaRevision                 { get; set; }
        public    double     LimiteCreditoCliente                { get; set; }
        public    int        DiasCreditoCliente                  { get; set; }
        public    int        BanExcederCredito                   { get; set; }
        public    double     DescuentoProntoPago                 { get; set; } // 0 = No se permite exceder crédito, 1 = Se permite exceder el crédito
        public    int        DiasProntoPago                      { get; set; }
        public    double     InteresMoratorio                    { get; set; }
        public    int        DiaPago                             { get; set; }
        public    int        DiasRevision                        { get; set; }
        public    String     Mensajeria                          { get; set; }
        public    String     CuentaMensajeria                    { get; set; }
        public    int        DiasEmbarqueCliente                 { get; set; }
        public    String     CodigoAlmacen                       { get; set; }
        public    String     CodigoAgenteVenta                   { get; set; }
        public    String     CodigoAgenteCobro                   { get; set; }
        public    int        RestriccionAgente                   { get; set; }
        public    double     Impuesto1                           { get; set; }
        public    double     Impuesto2                           { get; set; }
        public    double     Impuesto3                           { get; set; }
        public    double     RetencionCliente1                   { get; set; }
        public    double     RetencionCliente2                   { get; set; }
        public    String     CodigoValorClasificacionProveedor1  { get; set; }
        public    String     CodigoValorClasificacionProveedor2  { get; set; }
        public    String     CodigoValorClasificacionProveedor3  { get; set; }
        public    String     CodigoValorClasificacionProveedor4  { get; set; }
        public    String     CodigoValorClasificacionProveedor5  { get; set; }
        public    String     CodigoValorClasificacionProveedor6  { get; set; }
        public    double     LimiteCreditoProveedor              { get; set; }
        public    int        DiasCreditoProveedor                { get; set; }
        public    int        TiempoEntrega                       { get; set; }
        public    int        DiasEmbarqueProveedor               { get; set; }
        public    double     ImpuestoProveedor1                  { get; set; }
        public    double     ImpuestoProveedor2                  { get; set; }
        public    double     ImpuestoProveedor3                  { get; set; }
        public    double     RetencionProveedor1                 { get; set; }
        public    double     RetencionProveedor2                 { get; set; }
        public    int        BanInteresMoratorio                 { get; set; } // 0 = No se le calculan intereses moratorios al cliente, 1 = Si se le calculan intereses moratorios al cliente.
        public    String     TextoExtra1                         { get; set; }
        public    String     TextoExtra2                         { get; set; }
        public    String     TextoExtra3                         { get; set; }
        public    String     FechaExtra                          { get; set; }
        public    double     ImporteExtra1                       { get; set; }
        public    double     ImporteExtra2                       { get; set; }
        public    double     ImporteExtra3                       { get; set; }
        public    double     ImporteExtra4                       { get; set; }
        public    Direccion  Direccion                           { get; set; }

        public void agregarDireccion(
          string   codigoCliente   
        , int      tipoCatalogo     // 1=Clientes y 2=Proveedores
        , int      tipoDireccion    // 1=Domicilio Fiscal, 2=Domicilio Envio
        , string   nombreCalle    
        , string   numeroExterior 
        , string   numeroInterior 
        , string   colonia        
        , string   codigoPostal   
        , string   telefono1      
        , string   telefono2      
        , string   telefono3      
        , string   telefono4      
        , string   email         
        , string   direccionWeb
        , string   ciudad
        , string   estado
        , string   pais
        , string   textoExtra) {

         this.Direccion.CodigoCliente=codigoCliente;  
 
        //, int      tipoCatalogo     // 1=Clientes y 2=Proveedores
        //, int      tipoDireccion    // 1=Domicilio Fiscal, 2=Domicilio Envio
        //, string   nombreCalle    
        //, string   numeroExterior 
        //, string   numeroInterior 
        //, string   colonia        
        //, string   codigoPostal   
        //, string   telefono1      
        //, string   telefono2      
        //, string   telefono3      
        //, string   telefono4      
        //, string   email         
        //, string   direccionWeb
        //, string   ciudad
        //, string   estado
        //, string   pais
        //, string   textoExtra
        }
    }
    
}

