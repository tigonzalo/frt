﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Usuarios;
namespace FRT.Models.Documentos
{
    public class FacturacionDAO
    {
        public long TotalRecords { get; set; }
        public List<ClienteFacturacion> Listado(Documento objDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_FACTURACION_CLIENTES_LISTADO";;
            com.Parameters.AddWithValue("@codigoCliente", objDocumento.cliente.codCliente.ToString());
            com.Parameters.AddWithValue("@codEmpresa", objDocumento.codEmpresa);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<ClienteFacturacion> list = new List<ClienteFacturacion>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new ClienteFacturacion
                    {
                        CIDCATAL01 = (int)dr["CIDCATAL01"],
                        CodigoCliente = !Convert.IsDBNull(dr["ccodigoc01"]) ? (string)dr["ccodigoc01"] : "",
                        RFC = !Convert.IsDBNull(dr["crfc"]) ? (string)dr["crfc"] : "",
                        RazonSocial = !Convert.IsDBNull(dr["crazonso01"]) ? (string)dr["crazonso01"] : "",
                        TipoCliente = !Convert.IsDBNull(dr["CTIPOCLI01"]) ? (int)dr["CTIPOCLI01"] : -1,
                        Estatus = !Convert.IsDBNull(dr["CESTATUS"]) ? (int)dr["CESTATUS"] : -1,
                        Direccion= new Direccion{
                                 
                                 CodigoCliente = !Convert.IsDBNull(dr["ccodigoc01"]) ? (string)dr["ccodigoc01"] : "",
                                 TipoCatalogo = !Convert.IsDBNull(dr["ctipocat01"]) ? (int)dr["ctipocat01"] : -1,
                                 TipoDireccion = !Convert.IsDBNull(dr["ctipodir01"]) ? (int)dr["ctipodir01"] : -1,
                                 NombreCalle = !Convert.IsDBNull(dr["cnombrec01"]) ? (string)dr["cnombrec01"] : "",
                                 NumeroExterior = !Convert.IsDBNull(dr["cnumeroe01"]) ? (string)dr["cnumeroe01"] : "",
                                 NumeroInterior = !Convert.IsDBNull(dr["cnumeroe01"]) ? (string)dr["cnumeroe01"] : "",
                                 Colonia = !Convert.IsDBNull(dr["ccolonia"]) ? (string)dr["ccolonia"] : "",
                                 CodigoPostal = !Convert.IsDBNull(dr["ccodigop01"]) ? (string)dr["ccodigop01"] : "",
                                 Telefono1 = !Convert.IsDBNull(dr["ctelefono1"]) ? (string)dr["ctelefono1"] : "",
                                 Telefono2 = !Convert.IsDBNull(dr["ctelefono2"]) ? (string)dr["ctelefono2"] : "",
                                 Telefono3 = !Convert.IsDBNull(dr["ctelefono3"]) ? (string)dr["ctelefono3"] : "",
                                 Telefono4 = !Convert.IsDBNull(dr["ctelefono4"]) ? (string)dr["ctelefono4"] : "",
                                 Email = !Convert.IsDBNull(dr["cemail"]) ? (string)dr["cemail"] : "",
                                 DireccionWeb = !Convert.IsDBNull(dr["cdirecci01"]) ? (string)dr["cdirecci01"] : "",
                                 Municipio = !Convert.IsDBNull(dr["cciudad"]) ? (string)dr["cciudad"] : "",
                                 Ciudad = !Convert.IsDBNull(dr["cmunicipio"]) ? (string)dr["cmunicipio"] : "",
                                 Estado = !Convert.IsDBNull(dr["cestado"]) ? (string)dr["cestado"] : "",
                                 Pais = !Convert.IsDBNull(dr["cpais"]) ? (string)dr["cpais"] : "",
                                 TextoExtra = !Convert.IsDBNull(dr["ctextoex01"]) ? (string)dr["ctextoex01"] : "",
                      
                        }
         
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

        public void ActualizarFolioFactura(long codEmp, int NoTikect, double NoFactura, long CodDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_UPDATE_NoFACTURA";
            com.Parameters.AddWithValue("@codEmp", codEmp);
            com.Parameters.AddWithValue("@Numero",  NoTikect);
            com.Parameters.AddWithValue("@NoFactura", (int) NoFactura);
            com.Parameters.AddWithValue("@CodDocumento",  CodDocumento);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        public void ActualizarStatusDocumento(long codEmp, long CodDocumento, int codStatus)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_UPDATE_StatusDocumento";
            com.Parameters.AddWithValue("@codEmp", codEmp);
            com.Parameters.AddWithValue("@CodDocumento", CodDocumento);
            com.Parameters.AddWithValue("@codStatus", (int)codStatus);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

    }
}