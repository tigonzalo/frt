﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Clientes;
using FRT.Models.Empresas;
using FRT.Models.Usuarios;
using FRT.Models.UsosCFDI;

namespace FRT.Models
{
    public class Documento
    {
        public long            codDocumento           { get; set; }
        public long            codEmpresa             { get; set; }
        public long            codUsuario             { get; set; }
        public DateTime        fecha                  { get; set; }
        public decimal         total                  { get; set; }
        public DateTime        fechaConsumo           { get; set; }
        public string          folio                  { get; set; }
        public StatusDocumento StatusDocumento        { get; set; }
        public string          rutaPdf                { get; set; }
        public string          rutaXml                { get; set; }
        public long            codCliente             { get; set; }
        public Cliente         cliente                { get; set; }
        public Usuario         usuario                { get; set; }
        public bool            eliminado              { get; set; }
        public string          codConcepto            { get; set; }
        public string          formaPago              { get; set; }
        public string          lugarDeExpedicion      { get; set; }
        public double          propina                { get; set; }
        public double          monto                  { get; set; }
        public double          Iva                    { get; set; }
        public string          folioFactura           { get; set; }
        public Empresa         empresa                { get; set; }
        public string          uuid                   { get; set; }
        public bool            hold                   { get; set; }
        public int             nError                 { get; set; }
        public string          serie                  { get; set; }
        public double          totalFraccion          { get; set; }
        public int             nFracciones            { get; set; }
        public bool            fraccionado            { get; set; }
        public long            codUsoCFDI             { get; set; }
        public UsoCFDI         usoCFDI                { get; set; }
        public string          codigoUsoCFDI          { get; set; }
        public string          rfcCliente             { get; set; }
        public string          razonSocialCliente     { get; set; }
        public bool            version33              { get; set; }
        public string          correo                 { get; set; }


        public void agregarDocumentoadminPaq(Documento objDocumento) {
            this.agregarCliente(objDocumento.codCliente);
        
        }
        public void agregarCliente(long codCliente) {
            List<Cliente> list = new List<Cliente>();
            ClientesDAO objClientesDAO = new ClientesDAO();
            list = objClientesDAO.Listado(null,null,null,codCliente,null);
            cliente = list[0];  
        
        }
   
    }
}