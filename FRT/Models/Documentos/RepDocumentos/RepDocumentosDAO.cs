﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Usuarios;
using System.Threading;

namespace FRT.Models.Documentos.RepDocumentos
{
    public class RepDocumentosDAO
    {
        public long TotalRecords { get; set; }
        public string DesError { get; set; }
        public int NumError { get; set; }
        public List<RepDocumento> Listado(Usuario objUsuario, string busqvalor)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_REP_LISTADO";
            com.Parameters.AddWithValue("@codUsuario", objUsuario.codUsuario);
            com.Parameters.AddWithValue("@codEmpresa", objUsuario.Empresa.codEmpresa);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<RepDocumento> list = new List<RepDocumento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new RepDocumento
                    {
                        fecha=!Convert.IsDBNull(dr["fecha"])?(DateTime)dr["fecha"]:default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        estatus = !Convert.IsDBNull(dr["estatus"]) ? (string)dr["estatus"] : "",
                        formaPago = !Convert.IsDBNull(dr["formaPago"]) ? (string)dr["formaPago"] : "",
                        propina = !Convert.IsDBNull(dr["propina"]) ? (double)dr["propina"] : 0,
                        restaurante = !Convert.IsDBNull(dr["restaurante"]) ? (string)dr["restaurante"] : "",
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        factura = !Convert.IsDBNull(dr["factura"]) ? (string)dr["factura"] : ""
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            //this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}