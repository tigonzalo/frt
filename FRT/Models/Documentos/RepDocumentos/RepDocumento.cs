﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;


namespace FRT.Models.Documentos.RepDocumentos
{
    public class RepDocumento
    {
        public DateTime fecha         { get; set; }
        public DateTime fechaConsumo { get; set; }
        public string folio         { get; set; }
        public string factura       { get; set; }
        public string restaurante   { get; set; }
        public string estatus       { get; set; }
        public string formaPago     { get; set; }
        public double propina      { get; set; }
        public decimal total        { get; set; }
        public List<RepDocumento> listado(string valorBusq,Usuario objUsuario) {
            RepDocumentosDAO objRepDocumentosDAO = new RepDocumentosDAO();
            List<RepDocumento> list =new List<RepDocumento>();
            list = objRepDocumentosDAO.Listado(objUsuario,valorBusq);
            return list;
 
        }
    }
}