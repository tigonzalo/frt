﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Usuarios;
using System.Threading;
using FRT.Models.DocumentosPac;
using FRT.Models.Admin;
using FRT.Models.Clientes;
using FRT.Models.UsosCFDI;

namespace FRT.Models
{
    public class DocumentosDAO
    {
        public long TotalRecords { get; set; }
        public string DesError { get; set; }
        public int NumError { get; set; }
        public List<Documento> Detalle(long codDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_DETALLE";
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        //codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        //codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"],
                            rfcEmp = (string)dr["rfcEmp"]
                        },
                        uuid = !Convert.IsDBNull(dr["uuid"]) ? (string)dr["uuid"] : "",
                        hold = !Convert.IsDBNull(dr["hold"]) ? (bool)dr["hold"] : false,
                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0,
                        cliente = new Cliente
                        {
                            //codCliente = (long)dr["codCliente"],
                            rfcClie = (string)dr["rfcCliente"],
                            ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""
                        },
                        usuario = new Usuario
                        {
                            //codUsuario = (long)dr["codUsuario"],
                            mailUsuario = (string)dr["correo"],
                        },
                        serie = (string)dr["serie"],
                        rfcCliente = !Convert.IsDBNull(dr["rfcCliente"]) ? (string)dr["rfcCliente"] : "",
                        correo = !Convert.IsDBNull(dr["correo"]) ? (string)dr["correo"] : "",
                        razonSocialCliente = !Convert.IsDBNull(dr["razonSocialCliente"]) ? (string)dr["razonSocialCliente"] : "",
                        usoCFDI = new UsoCFDI() {
                            codUsoCFDI = !Convert.IsDBNull(dr["codUsoCFDI"]) ? (long)dr["codUsoCFDI"] : 0,
                            codigo = !Convert.IsDBNull(dr["codigoUsoCFDI"]) ? (string)dr["codigoUsoCFDI"] : ""
                        }
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            //this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Documento> ListadoRecuperarCFDI(Documento objDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO_RECUPERAR_CFDI";
            com.Parameters.AddWithValue("@codEmpresa", objDocumento.codEmpresa);
            com.Parameters.AddWithValue("@folio", objDocumento.folio);
            com.Parameters.AddWithValue("@total", objDocumento.total);
            SqlParameter fechaConsumo = com.Parameters.Add("@fechaConsumo", System.Data.SqlDbType.DateTime);
            fechaConsumo.Value = objDocumento.fechaConsumo;
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        //codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        //codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"],
                            rfcEmp = (string)dr["rfcEmp"]
                        },
                        uuid = !Convert.IsDBNull(dr["uuid"]) ? (string)dr["uuid"] : "",
                        hold = !Convert.IsDBNull(dr["hold"]) ? (bool)dr["hold"] : false,
                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0,
                        cliente = new Cliente
                        {
                            //codCliente = (long)dr["codCliente"],
                            rfcClie = (string)dr["rfcCliente"],
                            ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""
                        },
                        usuario = new Usuario
                        {
                            //codUsuario = (long)dr["codUsuario"],
                            mailUsuario = (string)dr["correo"],
                        },
                        serie = (string)dr["serie"],
                        rfcCliente = !Convert.IsDBNull(dr["rfcCliente"]) ? (string)dr["rfcCliente"] : "",
                        correo = !Convert.IsDBNull(dr["correo"]) ? (string)dr["correo"] : "",
                        razonSocialCliente = !Convert.IsDBNull(dr["razonSocialCliente"]) ? (string)dr["razonSocialCliente"] : ""

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            //this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Documento> ListadoCFDI(int? start, long? limit, long? codDocumento, string busqvalor)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        //codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        //codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"],
                            rfcEmp = (string)dr["rfcEmp"]
                        },
                        uuid = !Convert.IsDBNull(dr["uuid"]) ? (string)dr["uuid"] : "",
                        hold = !Convert.IsDBNull(dr["hold"]) ? (bool)dr["hold"] : false,
                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0,
                        cliente = new Cliente
                        {
                            //codCliente = (long)dr["codCliente"],
                            rfcClie = (string)dr["rfcCliente"],
                            ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""
                        },
                        usuario = new Usuario
                        {
                            //codUsuario = (long)dr["codUsuario"],
                            mailUsuario = (string)dr["correo"],
                        },
                        serie = (string)dr["serie"],


                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Documento> Listado(int? start, long? limit, long? codDocumento, Usuario objUsuario, string busqvalor)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            com.Parameters.AddWithValue("@codUsuario", objUsuario.codUsuario);
            com.Parameters.AddWithValue("@codEmpresa", objUsuario.Empresa!=null ? objUsuario.Empresa.codEmpresa:0);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        //codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        //codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"],
                            rfcEmp = (string)dr["rfcEmp"]
                        },
                        uuid = !Convert.IsDBNull(dr["uuid"]) ? (string)dr["uuid"] : "",
                        hold = !Convert.IsDBNull(dr["hold"]) ? (bool)dr["hold"] : false,
                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0,
                        cliente = new Cliente {
                            //codCliente = (long)dr["codCliente"],
                            rfcClie = (string)dr["rfcCliente"],
                            ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""
                        },
                        usuario = new Usuario
                        {
                            //codUsuario = (long)dr["codUsuario"],
                            mailUsuario = (string)dr["correo"],
                        },
                        serie = (string)dr["serie"],
                        

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Documento> Buscar(int? start, long? limit, Documento ObjDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@total", ObjDocumento.total);
            com.Parameters.AddWithValue("@fechaConsumo", ObjDocumento.fechaConsumo);
            com.Parameters.AddWithValue("@folio", ObjDocumento.folio);
            com.Parameters.AddWithValue("@codEmpresa", ObjDocumento.codEmpresa);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa 
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"]
                        },
                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Documento> Buscar(int? start, long? limit, string valorBusqueda)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@valorBusqueda", valorBusqueda);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"]
                        },

                        nError = !Convert.IsDBNull(dr["nError"]) ? (int)dr["nError"] : 0
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public void Guardar(Documento objDocumento, Usuario objUsuario)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TIQUET_GUARDAR_V33";
            //com.Parameters.AddWithValue("@codDocumento", objDocumento.codDocumento);
            com.Parameters.AddWithValue("@codEmpresa", objDocumento.codEmpresa);
            com.Parameters.AddWithValue("@codClie", objDocumento.codCliente);
            com.Parameters.AddWithValue("@codUsuario", objDocumento.codUsuario);
            com.Parameters.AddWithValue("@total", objDocumento.total);
            com.Parameters.AddWithValue("@fechaConsumo", Convert.ToDateTime(objDocumento.fechaConsumo).ToString("yyyyMMdd"));
            com.Parameters.AddWithValue("@folio", objDocumento.folio);
            com.Parameters.AddWithValue("@fraccion", objDocumento.fraccionado);
            com.Parameters.AddWithValue("@totalFraccion", objDocumento.totalFraccion);
            com.Parameters.AddWithValue("@codUsoCFDI", objDocumento.usoCFDI.codUsoCFDI);
            com.Parameters.AddWithValue("@codigoUsoCFDI", objDocumento.usoCFDI.codigo);
            com.Parameters.AddWithValue("@version", "3.3");

            com.Parameters.AddWithValue("@rfcCliente", objDocumento.rfcCliente);
            com.Parameters.AddWithValue("@razonSocialCliente", objDocumento.razonSocialCliente);
            com.Parameters.AddWithValue("@version33", objDocumento.version33);
            com.Parameters.AddWithValue("@correo", objDocumento.correo);

            //retorna el id de la venta
            SqlParameter codDocumento = new SqlParameter("@codDocumento", 0);
            codDocumento.Direction = ParameterDirection.Output;
            com.Parameters.Add(codDocumento);

            //retorna el código del concepto del documento
            SqlParameter codConcepto = new SqlParameter("@codConcepto", SqlDbType.VarChar, 50);
            codConcepto.Direction = ParameterDirection.Output;
            com.Parameters.Add(codConcepto);

            //retorna la forma de pago del documento
            SqlParameter formaPago = new SqlParameter("@formaPago", SqlDbType.VarChar, 50);
            formaPago.Direction = ParameterDirection.Output;
            com.Parameters.Add(formaPago);

            //retorna el lugar de expedicion del documento
            SqlParameter lugarExpedicion = new SqlParameter("@lugarExpedicion", SqlDbType.VarChar, 250);
            lugarExpedicion.Direction = ParameterDirection.Output;
            com.Parameters.Add(lugarExpedicion);

            //retorna el lugar de expedicion del documento
            SqlParameter propina = new SqlParameter("@propina", SqlDbType.Float);
            propina.Direction = ParameterDirection.Output;
            com.Parameters.Add(propina);

            //retorna el lugar de expedicion del documento
            SqlParameter monto = new SqlParameter("@monto", SqlDbType.Float);
            monto.Direction = ParameterDirection.Output;
            com.Parameters.Add(monto);

            //retorna el lugar de expedicion del documento
            SqlParameter iva = new SqlParameter("@iva", SqlDbType.Float);
            iva.Direction = ParameterDirection.Output;
            com.Parameters.Add(iva);

            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
                objDocumento.codDocumento = !Convert.IsDBNull(codDocumento.Value) ? (long)codDocumento.Value : 0;
                objDocumento.codConcepto = !Convert.IsDBNull(codConcepto.Value) ? (string)codConcepto.Value : "(NINGUNO)";
                objDocumento.formaPago = !Convert.IsDBNull(formaPago.Value) ? (string)formaPago.Value : "NO IDENTIFICADO";
                objDocumento.lugarDeExpedicion = !Convert.IsDBNull(lugarExpedicion.Value) ? (string)lugarExpedicion.Value : "";
                objDocumento.propina = !Convert.IsDBNull(propina.Value) ? (double)propina.Value : 0;
                objDocumento.monto = !Convert.IsDBNull(monto.Value) ? (double)monto.Value : 0;
                objDocumento.Iva = !Convert.IsDBNull(iva.Value) ? (double)iva.Value : 16;

                this.generarXml(objDocumento);
               
               
            }
            catch (SqlException e)
            {
                //this.CambiaStatusProceso(0);
                throw (e);

            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            
            }
            
        }

        public void generarXml(Documento ObjDocumento) {

            bool procesar = ObjDocumento.hold;
            DocumentoPac documentoPac = new DocumentoPac(ObjDocumento);                   
            
             
        
        }
        public void insertarDocumentoAdminPaq(Documento ObjDocumento)
        {
            //szRegKeySistema = @"SOFTWARE\Wow6432Node\Computación en Acción, SA CV\AdminPAQ";
            try
            {
                AdminPaq objAdminPaq = new AdminPaq(ObjDocumento.codEmpresa);
                objAdminPaq.abrirEmpresa(true);
                if (objAdminPaq.NumError != 0)
                {
                    this.NumError = objAdminPaq.NumError;
                    this.DesError = objAdminPaq.DescError;
                    return;
                }
                this.generarXml(ObjDocumento);
                objAdminPaq.timbraXML(ObjDocumento.codDocumento);
                //ObjDocumento.agregarDocumentoadminPaq(ObjDocumento);
                //AdminPaq objAdminPaq = new AdminPaq(ObjDocumento.codEmpresa);

                //objAdminPaq.abrirEmpresa(true);
                //if (objAdminPaq.NumError != 0) {
                //    this.NumError = objAdminPaq.NumError;
                //    this.DesError = objAdminPaq.DescError;
                //    return;
                //}
                
                //objAdminPaq.generarDocumentoAdminPaq(ObjDocumento);
                //ObjDocumento.folio = objAdminPaq.folio.ToString();

                //Cambiar status de Proceso a Cero de que ya termino
                //this.CambiaStatusProceso(0);
            }
            catch (SqlException e)
            {
                //this.CambiaStatusProceso(0);
                throw (e);
            }

        }

        public void Eliminar(long codDocumento)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_ELIMINAR";
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        private int ValidaProceso()
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_OBTENER_StatusProceso";
            //com.Parameters.AddWithValue("@Status", 0);

            //retorna el Status del proceso
            SqlParameter Status = new SqlParameter("@Status", SqlDbType.Int);
            Status.Direction = ParameterDirection.Output;
            com.Parameters.Add(Status);

            SqlDataReader dr = null;
            int StatusPro ;
            StatusPro = 0;
            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
                StatusPro = !Convert.IsDBNull(Status.Value) ? (int)Status.Value : 0;

                return StatusPro;
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        private void CambiaStatusProceso(int valor)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_UPDATE_StatusProceso";
            com.Parameters.AddWithValue("@Status", valor);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        
        public void Copy(string pathx, string filex)
        {

            try
            {



                string fullPath_ini = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                //int firstCharacter = fullPath.IndexOf(@"C:");
                //int secondCharacter = fullPath.IndexOf(@"\bin");
                string FilePDF = filex + ".pdf";
                string FileXML = filex + ".xml";

                //string fullPath = fullPath_ini.Replace("\bin", @"");                
                string fullPathx = @"C:\inetpub\wwwroot\FRT\FRT\Documentos";//fullPath.Replace("file:\\", @"");

                // Delete a directory and all subdirectories with Directory static method...
                if (System.IO.Directory.Exists(fullPathx))
                {
                    try
                    {
                        //System.IO.Directory.Delete(fullPathx, true);
                        //DirectoryInfo direc = new DirectoryInfo(fullPathx);
                        //FileInfo[] file = direc.GetFiles(fullPathx);

                        string[] ficherosCarpeta = Directory.GetFiles(fullPathx);
                        foreach (string ficheroActual in ficherosCarpeta)
                            File.Delete(ficheroActual);

                    }


                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                    try
                    {

                        //string sourceFilePDF = pathx + @"\" + FilePDF;
                        //string sourceFileXML = pathx + @"\" + FileXML;
                        string sourceFilePDF = pathx + FilePDF;
                        string sourceFileXML = pathx + FileXML;

                        string destinationFilePDF = fullPathx + @"\" + FilePDF;
                        string destinationFileXML = fullPathx + @"\" + FileXML;

                        System.IO.File.Copy(sourceFilePDF, destinationFilePDF);
                        System.IO.File.Copy(sourceFileXML, destinationFileXML);


                        //// To Copiy a file or folder to a new location:
                        //if (System.IO.File.Exists(destinationFilePDF)==false)
                        //   {
                        //       System.IO.File.Copy(sourceFilePDF, destinationFilePDF);
                        //   }

                        //if (System.IO.File.Exists(destinationFileXML) == false)
                        //{
                        //    System.IO.File.Copy(sourceFileXML, destinationFileXML);
                        //}

                        

                    }


                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }

            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                //no Hace nada
            }

        }
        public void cancelarCFDI(Documento objDocumento,long codPerfil) {
            
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TCANCELAR_CFDI";
            com.Parameters.AddWithValue("@codDocumento", objDocumento.codDocumento);
            com.Parameters.AddWithValue("@codPerfil", codPerfil);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
        
        
        }
        //public bool holdDocumento (Documento objDocumento)
        //{

        //    SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = conn;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "SP_THOLD_DOCUMENTO";
        //    com.Parameters.AddWithValue("@codDocumento", objDocumento.codDocumento);
        //    SqlParameter hold = new SqlParameter("@hold", false);
        //    hold.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(hold);
        //    SqlDataReader dr = null;

        //    try
        //    {
        //        conn.Open();
        //        Object ok = com.ExecuteScalar();
                 
        //    }
        //    catch (SqlException e)
        //    {
        //        throw (e);
        //    }
        //    finally
        //    {
        //        if (dr != null)
        //            dr.Close();
        //        if (conn != null)
        //            conn.Close();
        //    }
        //    return (hold.Value != null ? (bool)hold.Value : false);

        //}
    
    }
}