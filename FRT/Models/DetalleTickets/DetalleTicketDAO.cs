﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Usuarios;

namespace FRT.Models.DetalleTickets
{
    public class DetalleTicketDAO
    {
        public long TotalRecords { get; set; }
        public List<DetalleTicket> Listado(string folio, long? codEmpresa, string serie)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDETALLE_TICKETS_LISTADO";
            com.Parameters.AddWithValue("@folio", folio);
            com.Parameters.AddWithValue("@codEmpresa", codEmpresa);
            //com.Parameters.AddWithValue("@serie", serie);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<DetalleTicket> list = new List<DetalleTicket>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new DetalleTicket
                    {
                        codDetalleTicket = (long)dr["codDetalleTicket"],
                        codTicket = (long)dr["codTicket"],
                        codigoArticulo = !Convert.IsDBNull(dr["codigoArticulo"]) ? (int)dr["codigoArticulo"] : 0,
                        iva = !Convert.IsDBNull(dr["iva"]) ? (double)dr["iva"] : 0,
                        precio = !Convert.IsDBNull(dr["precio"]) ? (double)dr["precio"] : 0,
                        producto = !Convert.IsDBNull(dr["producto"]) ? (string)dr["producto"] : "",
                        unidades = !Convert.IsDBNull(dr["unidades"]) ? (double)dr["unidades"] : 0,
                        importe = !Convert.IsDBNull(dr["importe"]) ? (double)dr["importe"] : 0

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}