﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DetalleTickets
{
    public class DetalleTicket
    {
        public long codDetalleTicket { get; set; }
        public long codTicket { get; set; }
        public double unidades { get; set; }
        public int codigoArticulo { get; set; }
        public string producto { get; set; }
        public double precio { get; set; }
        public double iva { get; set; }
        public double importe { get; set; }

    }
}