﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FRT.Models.Common
{
    public sealed class ExportReport
    {
        static ExportReport()
        {
        }
        /// <summary>
        /// Exporta un reporte de Crystal Report a un formato especifico.
        /// </summary>
        /// <param name="crReportDocument">Reporte a exportar, los datos ya deben estar cargados en el reporte.</param>
        /// <param name="Format">Formato al cual desea exportarse.</param>
        /// <returns>Devuelve un array de byte para poder enviarse a traves del objeto Response</returns>
        public static byte[] Export(ReportDocument crReportDocument, ExportFormatType format)
        {
            DiskFileDestinationOptions ExportDestOpc;
            ExportOptions opcExport;
            String TargetFieldName;
            FileStream fs;
            long FileSize;
            byte[] Buffer;
            ExportDestOpc = new DiskFileDestinationOptions();
            //Obtiene un archivo temporal dentro de la ruta temporal de aspnet.
            TargetFieldName = Path.GetTempFileName();
            ExportDestOpc.DiskFileName = TargetFieldName;
            opcExport = crReportDocument.ExportOptions;
            opcExport.ExportDestinationType = ExportDestinationType.DiskFile;
            opcExport.ExportFormatType = format;
            opcExport.DestinationOptions = ExportDestOpc;
            crReportDocument.Export();
            crReportDocument.Close();
            fs = new FileStream(TargetFieldName, FileMode.Open);
            FileSize = fs.Length;
            Buffer = new byte[(int)FileSize];
            fs.Read(Buffer, 0, (int)FileSize);
            fs.Close();
            File.Delete(TargetFieldName);
            return Buffer;
        }
    }
}