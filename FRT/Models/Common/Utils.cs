﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Web.Mvc;
using FRT.Models.Permisos;
using FRT.Models.Opciones;
using ZXing.Common;
using ZXing;
using ZXing.QrCode;
using System.Media;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace FRT.Models.Common
{
    public class Utils
    {
        public static DataTable TableDate<obj>(List<obj> ListObj)
        {
            if (ListObj == null)
                return null;
            DataTable dataTable = new DataTable();
            Assembly assembly = Assembly.GetExecutingAssembly();
            PropertyInfo[] Types = typeof(obj).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            int j = 0;
            Type typedata = null;
            string[] PropNoms = new string[Types.Length];
            foreach (PropertyDescriptor descpTypes in TypeDescriptor.GetProperties(typeof(obj)))
            {

                /// Propiedades con tipo de dato del Systema : Int, Long, String, DateTime, etc. Se obtiene su nombre y tipo de Dato.
                if (descpTypes.PropertyType.Namespace == "System")
                {
                    var nombre = descpTypes.Name;
                    PropNoms[j] = nombre;

                    if (descpTypes.PropertyType == typeof(long?))
                        typedata = typeof(long);
                    else if (descpTypes.PropertyType == typeof(decimal?))
                        typedata = typeof(decimal);
                    else if (descpTypes.PropertyType == typeof(DateTime?))
                        typedata = typeof(DateTime);
                    else if (descpTypes.PropertyType == typeof(int?))
                        typedata = typeof(int);
                    else
                        typedata = descpTypes.PropertyType;

                }
                ///Propiedades con Tipo de dato (webGPS_Clase). Se obtiene el nombre de su Identificador y su Tipo de Dato.
                else
                {
                    var subobj = AppDomain.CurrentDomain.CreateInstance(assembly.FullName, descpTypes.PropertyType.FullName);
                    PropertyInfo[] SubTypes = subobj.Unwrap().GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    int k = 0;
                    foreach (PropertyInfo subtype in SubTypes)
                    {
                        if (k == 0)
                        {
                            var nombre = subtype.Name;
                            var nC = 0;
                            foreach (string nom in PropNoms)
                            {
                                if (nom == nombre)
                                    nC++;
                            }
                            if (nC > 0)
                                nombre = nombre + nC.ToString();

                            PropNoms[j] = nombre;
                            if (subtype.PropertyType == typeof(long?))
                                typedata = typeof(long);
                            else if (subtype.PropertyType == typeof(int?))
                                typedata = typeof(int);
                            else
                                typedata = subtype.PropertyType;
                        }
                        k++;
                    }
                }


                dataTable.Columns.Add(PropNoms[j], typedata);
                j++;
            }
            foreach (obj item in ListObj)
            {
                var values = new object[PropNoms.Length];
                for (int i = 0; i < PropNoms.Length; i++)
                {
                    if (Types[i].PropertyType.Namespace == "System")
                        values[i] = Types[i].GetValue(item, null);
                    else
                    {
                        var subobj = AppDomain.CurrentDomain.CreateInstance(assembly.FullName, Types[i].PropertyType.FullName);
                        PropertyInfo[] SubTypes = subobj.Unwrap().GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        values[i] = SubTypes[0].GetValue(Types[i].GetValue(item, null), null);



                    }
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;


        }
    

    public IEnumerable<Type> getOpcion()
       {

           return from t in this.GetType().Assembly.GetTypes()
                  where t.IsAbstract == false
                  where typeof(Controller).IsAssignableFrom(t)
                  where t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)
                  && !t.Name.StartsWith("Home", StringComparison.OrdinalIgnoreCase)
                  && !t.Name.StartsWith("Estados", StringComparison.OrdinalIgnoreCase)
                  && !t.Name.StartsWith("Municipios", StringComparison.OrdinalIgnoreCase)
                  && !t.Name.StartsWith("PerfilPermisos", StringComparison.OrdinalIgnoreCase)
                  select t;
       }

       public List<Permiso> getPermisos(Type controller, Opcion Opcion)
       {
           int idPermiso = 0;

           MethodInfo[] methods = controller.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);

           List<Permiso> list = new List<Permiso>();

           foreach (MethodInfo method in methods)
           {
               idPermiso++;


               if (method.Name.IndexOf("Guardar") > -1 || method.Name.IndexOf("Actualizar") > -1 || method.Name.IndexOf("Eliminar") > -1 || method.Name.IndexOf("Reporte") > -1 || method.Name.IndexOf("Menu") > -1)
                   list.Add(new Permiso { codPermiso = ((int)Opcion.codOpcion * 100) + idPermiso, codOpcion = (int)Opcion.codOpcion, desPermiso = method.Name });

           }
           return list;
}


       public byte[] qrImage(string qrString)
       {

           QrCodeEncodingOptions options = new QrCodeEncodingOptions
           {
               DisableECI = true,
               CharacterSet = "UTF-8",
               Width = 230,
               Height = 230,
           };
           BarcodeWriter writer = new BarcodeWriter();
           writer.Format = BarcodeFormat.QR_CODE;
           writer.Options = options;
           ZXing.BarcodeWriter qr = new ZXing.BarcodeWriter();
           qr.Options = options;
           qr.Format = ZXing.BarcodeFormat.QR_CODE;

           var image = new Bitmap(qr.Write(qrString));
           //image.Save(@"D:QR.png", System.Drawing.Imaging.ImageFormat.Png);
           //byte[] imaB= this.Image2Bytes(image);

           //DocumentosDAO objDocumentosDAO = new DocumentosDAO();
           //objDocumentosDAO.guardarImagen(imaB, 1);

           //Image image = result;
           //string base64String = this.ImageToBase64String(image);
           //BarcodeReader reader = new BarcodeReader { AutoRotate = true, TryHarder = true };
           //Result result = reader.Decode(result1);
           //
           //pictureBox1.Image = result;

           //ZXing.BarcodeWriter qr = new ZXing.BarcodeWriter();
           //qr.Options = options;
           //qr.Format = ZXing.BarcodeFormat.QR_CODE;
           //var result = new Bitmap(qrString);


           return this.Image2Bytes(image);

       }
       public byte[] Image2Bytes(Image img)
       {
           string sTemp = Path.GetTempFileName();
           FileStream fs = new FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           img.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
           fs.Position = 0;
           //
           int imgLength = Convert.ToInt32(fs.Length);
           byte[] bytes = new byte[imgLength];
           fs.Read(bytes, 0, imgLength);
           fs.Close();
           return bytes;
       }
       public string ImageToBase64String(Image image)
       {
           using (MemoryStream stream = new MemoryStream())
           {
               image.Save(stream, image.RawFormat);
               return Convert.ToBase64String(stream.ToArray());
           }
       }
       public string ImageToBase64(Image image,
         System.Drawing.Imaging.ImageFormat format)
       {
           using (MemoryStream ms = new MemoryStream())
           {
               // Convert Image to byte[]
               image.Save(ms, format);
               byte[] imageBytes = ms.ToArray();

               // Convert byte[] to Base64 String
               string base64String = Convert.ToBase64String(imageBytes);
               return base64String;
           }
       }


    }
}