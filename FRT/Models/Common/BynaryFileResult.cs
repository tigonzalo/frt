﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FRT.Models.Common
{
    public class BynaryFileResult : ActionResult
    {
        private readonly byte[] _byteArray;
        private readonly String _contentType;
        private readonly String _contentDisposition;

        public BynaryFileResult(byte[] byteArray, String contentType, String name, bool attachment)
        {
            _byteArray = byteArray;
            _contentType = contentType;
            if (attachment)
                _contentDisposition = "attachment; filename=" + name;
            else
                _contentDisposition = "inline; filename=" + name;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.Clear();
            response.ContentType = _contentType;
            response.AppendHeader("content-disposition", _contentDisposition);
            response.BinaryWrite(_byteArray);
        }
    }

}