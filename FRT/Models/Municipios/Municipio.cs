﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Municipios
{
    public class Municipio
    {
        public long codMunicipio { get; set; }
        public string codEstado { get; set; }
        public string nombreMunicipio { get; set; }

        //public Estado Estado { get; set; }
    }
}