﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;

namespace FRT.Models.FormasPagos
{
    public class FormasPagosDAO
    {
        public long TotalRecords { get; set; }
        public List<FormaPago> Listado(string clave)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TFORMASPAGOS_LISTADO";
            com.Parameters.AddWithValue("@start", 0);
            com.Parameters.AddWithValue("@limit", 20);
            com.Parameters.AddWithValue("@clave", clave);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<FormaPago> list = new List<FormaPago>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new FormaPago()
                    {
                        codFormaPago = (long)dr["codFormaPago"],
                        alias = !Convert.IsDBNull(dr["alias"]) ? (string)dr["alias"] : "",
                        clave = !Convert.IsDBNull(dr["clave"]) ? (string)dr["clave"] : "",
                        descripcion = !Convert.IsDBNull(dr["descripcion"]) ? (string)dr["descripcion"] : "",

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<FormaPago> Listado_33()
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TFORMAS_PAGOS_LISTADO_33";
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<FormaPago> list = new List<FormaPago>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new FormaPago()
                    {
                        codFormaPago = (long)dr["codFormaPago"],
                        //alias = !Convert.IsDBNull(dr["alias"]) ? (string)dr["alias"] : "",
                        clave = !Convert.IsDBNull(dr["clave"]) ? (string)dr["clave"] : "",
                        descripcion = !Convert.IsDBNull(dr["descripcion"]) ? (string)dr["descripcion"] : "",

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}