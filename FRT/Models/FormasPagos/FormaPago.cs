﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.FormasPagos
{
    public class FormaPago
    {
        public long codFormaPago { get; set; }
        public string descripcion { get; set; }
        public string clave { get; set; }
        public string alias { get; set; }
    }
}