﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Usuarios;
using FRT.Models.Estados;
using FRT.Models.Municipios;


namespace FRT.Models.Clientes
{
    public class ClientesDAO
    {
        public long TotalRecords { get; set; }
        public List<Cliente> Listado(string correo)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TCLIENTES_LISTADO";
            com.Parameters.AddWithValue("@correo", correo);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Cliente> list = new List<Cliente>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Cliente
                   {
                       codCliente = (long)dr["codCliente"],
                       usuario = new Usuario
                       {
                           codUsuario = (long)dr["codUsuario"],
                           //nombreUsuario = (string)dr["nombreUsuario"],
                           //apePaterUsuario = (string)dr["apePaterUsuario"],
                           mailUsuario = (string)dr["mailUsuario"],
                           loginUsuario = (string)dr["loginUsuario"],
                           passwordUsuario = (string)dr["passwordUsuario"],
                           //apeMaterUsuario = !Convert.IsDBNull(dr["apeMaterUsuario"])?(string)dr["apeMaterUsuario"]:"",
                           //telefonoUsuario = !Convert.IsDBNull(dr["telefonoUsuario"])?(string)dr["telefonoUsuario"]:""

                       },
                       razonSocialClie = (string)dr["razonSocialClie"],
                       rfcClie = (string)dr["rfcClie"],
                       calleClie=(string)dr["calleClie"],
                       numExtClie = (string)dr["numExtClie"],
                       numIntClie = !Convert.IsDBNull(dr["numIntClie"]) ? (string)dr["numIntClie"] : "",
                       coloniaClie = (string)dr["coloniaClie"],
                       codMunicipio = (long)dr["codMunicipio"],
                       codEstado = (string)dr["codEstado"],
                       localidadClie = !Convert.IsDBNull(dr["localidadClie"]) ? (string)dr["localidadClie"] : "",
                       paisClie = (string)dr["paisClie"],
                       cpClie = (string)dr["cpClie"],
                       Estado= new Estado{
                           codEstado = (string)dr["codEstado"],
                           nombreEstado = !Convert.IsDBNull(dr["nombreEstado"]) ? (string)dr["nombreEstado"] : "",
                       },
                        Municipio= new Municipio{
                            codMunicipio = !Convert.IsDBNull(dr["codMunicipio"]) ? (long)dr["codMunicipio"] : 0,
                           nombreMunicipio = !Convert.IsDBNull(dr["nombreMunicipio"]) ? (string)dr["nombreMunicipio"] : "",
                       },
                       ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""

                   });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        // Obtiene la contraseña mediante el correo
        public List<Cliente> Listado(int? start, long? limit, string busqvalor, long? codClie, long? codUsuario)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TCLIENTES_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            com.Parameters.AddWithValue("@codClie", codClie);
            com.Parameters.AddWithValue("@codUsuario", codUsuario);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Cliente> list = new List<Cliente>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Cliente
                    {
                        codCliente = (long)dr["codCliente"],
                        usuario = new Usuario
                        {
                            codUsuario = (long)dr["codUsuario"],
                            //nombreUsuario = (string)dr["nombreUsuario"],
                            //apePaterUsuario = (string)dr["apePaterUsuario"],
                            mailUsuario = (string)dr["mailUsuario"],
                            loginUsuario = (string)dr["loginUsuario"],
                            passwordUsuario = (string)dr["passwordUsuario"],
                            //apeMaterUsuario = !Convert.IsDBNull(dr["apeMaterUsuario"])?(string)dr["apeMaterUsuario"]:"",
                            //telefonoUsuario = !Convert.IsDBNull(dr["telefonoUsuario"])?(string)dr["telefonoUsuario"]:""

                        },
                        razonSocialClie = (string)dr["razonSocialClie"],
                        rfcClie = (string)dr["rfcClie"],
                        calleClie = (string)dr["calleClie"],
                        numExtClie = (string)dr["numExtClie"],
                        numIntClie = !Convert.IsDBNull(dr["numIntClie"]) ? (string)dr["numIntClie"] : "",
                        coloniaClie = (string)dr["coloniaClie"],
                        codMunicipio = (long)dr["codMunicipio"],
                        codEstado = (string)dr["codEstado"],
                        localidadClie = !Convert.IsDBNull(dr["localidadClie"]) ? (string)dr["localidadClie"] : "",
                        paisClie = (string)dr["paisClie"],
                        cpClie = (string)dr["cpClie"],
                        Estado = new Estado
                        {
                            codEstado = (string)dr["codEstado"],
                            nombreEstado = !Convert.IsDBNull(dr["nombreEstado"]) ? (string)dr["nombreEstado"] : "",
                        },
                        Municipio = new Municipio
                        {
                            codMunicipio = !Convert.IsDBNull(dr["codMunicipio"]) ? (long)dr["codMunicipio"] : 0,
                            nombreMunicipio = !Convert.IsDBNull(dr["nombreMunicipio"]) ? (string)dr["nombreMunicipio"] : "",
                        },
                        ultimos_digitos = !Convert.IsDBNull(dr["ultimos_digitos"]) ? (string)dr["ultimos_digitos"] : ""

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public void Guardar(Cliente objCliente)
        {
           
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TCLIENTES_GUARDAR";
            com.Parameters.AddWithValue("@codClie", objCliente.codCliente);
            com.Parameters.AddWithValue("@codUsuario",objCliente.usuario.codUsuario);
            com.Parameters.AddWithValue("@loginUsuario", objCliente.usuario.loginUsuario);
            com.Parameters.AddWithValue("@passwordUsuario", objCliente.usuario.passwordUsuario);
            com.Parameters.AddWithValue("@nombreUsuario", objCliente.usuario.nombreUsuario);
            com.Parameters.AddWithValue("@apePaterUsuario", objCliente.usuario.apePaterUsuario);
            com.Parameters.AddWithValue("@apeMaterUsuario", objCliente.usuario.apeMaterUsuario);
            com.Parameters.AddWithValue("@telefonoUsuario", objCliente.usuario.telefonoUsuario);
            com.Parameters.AddWithValue("@mailUsuario", objCliente.usuario.mailUsuario);
            com.Parameters.AddWithValue("@razonSocialClie", objCliente.razonSocialClie);
            com.Parameters.AddWithValue("@rfcClie", objCliente.rfcClie);
            com.Parameters.AddWithValue("@calleClie", objCliente.calleClie);
            com.Parameters.AddWithValue("@numExtClie", objCliente.numExtClie);
            com.Parameters.AddWithValue("@numIntClie", objCliente.numIntClie);
            com.Parameters.AddWithValue("@paisClie", objCliente.paisClie);
            com.Parameters.AddWithValue("@codEstadoClie", objCliente.codEstado);
            //com.Parameters.AddWithValue("@estado", objCliente.estado);
            com.Parameters.AddWithValue("@municipioClie", objCliente.codMunicipio);
            com.Parameters.AddWithValue("@coloniaClie", objCliente.coloniaClie);
            com.Parameters.AddWithValue("@cpClie", objCliente.cpClie);
            com.Parameters.AddWithValue("@localidadClie", objCliente.localidadClie);
            com.Parameters.AddWithValue("@codPerfil", objCliente.usuario.Perfil.codPerfil);

            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }
        public void Eliminar(long codClie)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TCLIENTES_ELIMINAR";
            com.Parameters.AddWithValue("@codClie", codClie);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

    }
}