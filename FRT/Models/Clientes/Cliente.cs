﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using FRT.Models.Estados;
using FRT.Models.Municipios;

namespace FRT.Models.Clientes
{
    public class Cliente
    {
        public long codCliente { get; set; }
        public Usuario usuario { get; set; }
        public string razonSocialClie { get; set; }
        public string rfcClie { get; set; }
        public string calleClie { get; set; }
        public string numExtClie { get; set; }
        public string numIntClie { get; set; }
        public string coloniaClie { get; set; }
        public long codMunicipio { get; set; }
        public string codEstado { get; set; }
        //public string estado { get; set; }
        public string localidadClie { get; set; }
        public string paisClie { get; set;}
        public string cpClie { get; set; }
        public DateTime fechaModificacion { get; set; }
        public string codigoOrigen { get; set; }
        public Estado Estado      { get; set; }
        public Municipio Municipio { get; set; }
        public string ultimos_digitos { get; set; }

    }
}