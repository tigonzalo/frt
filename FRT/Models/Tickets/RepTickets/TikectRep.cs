﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Tickets.RepTickets
{
    public class TikectRep
    {
        public string folio            { get; set; }
        public double total            { get; set; }
        public double propina          { get; set; }
        public string fechaCosumo      { get; set; }
        public string statusDocumento  { get; set; }
        public string formaPago        { get; set; }
    }
}