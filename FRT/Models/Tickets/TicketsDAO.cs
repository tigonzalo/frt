﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Admin;
using FRT.Models.Empresas;
using FRT.Models.Usuarios;
using FRT.Models.Tickets.RepTickets;
using FRT.Models.FormasPagos;
namespace FRT.Models.Tickets
{
    public class TicketsDAO
    {
        public long TotalRecords { get; set; }
        public List<Ticket> Detalle(Ticket objTicket)
        {

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TTICKETS_DETALLE_33";
            com.Parameters.AddWithValue("@codEmpresa", objTicket.codEmpresa);
            com.Parameters.AddWithValue("@folio", objTicket.folio);
            SqlParameter fechaCosumo = com.Parameters.Add("@fechaCosumo", System.Data.SqlDbType.DateTime);
            fechaCosumo.Value = objTicket.fechaCosumo;
            com.Parameters.AddWithValue("@total", objTicket.total);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Ticket> list = new List<Ticket>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Ticket
                    {
                        codTicket = (long)dr["codTicket"],
                        codEmpresa = (long)dr["codEmpresa"],
                        CodStatusDoc = (int)dr["CodStatusDoc"],
                        empresa = new Empresa()
                        {
                            codEmpresa = (long)dr["codEmpresa"],
                            nombreEmp = (string)dr["EMPRESA_DOC"]
                        },
                        fechaCosumo = !Convert.IsDBNull(dr["fechaCosumo"]) ? (DateTime)dr["fechaCosumo"] : default(DateTime),
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        formaPago = new FormaPago()
                        {
                            codFormaPago = (long)dr["codFormaPago"],
                            clave = !Convert.IsDBNull(dr["claveFormaPago"]) ? (string)dr["claveFormaPago"] : "",
                            descripcion = !Convert.IsDBNull(dr["formaPago2"]) ? (string)dr["formaPago2"] : ""
                        },
                        propina = !Convert.IsDBNull(dr["propina"]) ? (double)dr["propina"] : 0,
                        statusDocumento = new StatusDocumento()
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        total = !Convert.IsDBNull(dr["total"]) ? (double)dr["total"] : 0,

                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<Ticket> Listado(int? start, long? limit, long? codTicket, Usuario objUsuario, string busqvalor,DateTime? fecha,bool? facturados)
        {
           
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TTICKETS_LISTADO_33";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codTicket", codTicket);
            com.Parameters.AddWithValue("@facturados", facturados);
            com.Parameters.AddWithValue("@fecha", fecha.HasValue ? fecha.Value.ToString("yyyyMMdd") : null);
            com.Parameters.AddWithValue("@codEmpresa", objUsuario.Empresa != null ? objUsuario.Empresa.codEmpresa : 0);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Ticket> list = new List<Ticket>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Ticket
                    {
                        codTicket = (long)dr["codTicket"],
                        codEmpresa = (long)dr["codEmpresa"],
                        CodStatusDoc = (int)dr["CodStatusDoc"],
                        empresa = new Empresa(){
                            codEmpresa = (long)dr["codEmpresa"],
                            nombreEmp = (string)dr["EMPRESA_DOC"]
                           },
                        fechaCosumo = !Convert.IsDBNull(dr["fechaCosumo"]) ? (DateTime)dr["fechaCosumo"] : default(DateTime),
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        formaPago = new FormaPago(){
                            codFormaPago = (long)dr["codFormaPago"],
                            clave = !Convert.IsDBNull(dr["claveFormaPago"]) ? (string)dr["claveFormaPago"] : "",
                            descripcion = !Convert.IsDBNull(dr["formaPago2"]) ? (string)dr["formaPago2"] : ""
                        },
                        propina = !Convert.IsDBNull(dr["propina"]) ? (double)dr["propina"] : 0,
                        statusDocumento = new StatusDocumento(){
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        total = !Convert.IsDBNull(dr["total"]) ? (double)dr["total"] : 0,

                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public List<TikectRep> ListadoRep( Usuario objUsuario, string busqvalor, bool? facturados)
        {

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TTICKETS_LISTADO";
            com.Parameters.AddWithValue("@facturados", facturados);
            com.Parameters.AddWithValue("@codEmpresa", objUsuario.Empresa != null ? objUsuario.Empresa.codEmpresa : 0);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<TikectRep> list = new List<TikectRep>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new TikectRep
                    {

                        fechaCosumo = !Convert.IsDBNull(dr["fechaCosumo"]) ? (string)dr["fechaCosumo"].ToString() : "",
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        formaPago = !Convert.IsDBNull(dr["formaPago"]) ? (string)dr["formaPago"] : "",
                        propina = !Convert.IsDBNull(dr["propina"]) ? (double)dr["propina"] : 0,
                        statusDocumento  = (string)dr["STATUS_DOC"],
                        total = !Convert.IsDBNull(dr["total"]) ? (double)dr["total"] : 0,

                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public void Guardar(Ticket objTicket)
        {

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_T_TICKETS_GUARDAR";
            com.Parameters.AddWithValue("@codTicket", objTicket.codTicket);
            com.Parameters.AddWithValue("@codEmpresa", objTicket.codEmpresa);
            com.Parameters.AddWithValue("@formaPago", objTicket.formaPago.descripcion);
            com.Parameters.AddWithValue("@codFormaPago", objTicket.formaPago.codFormaPago);
            com.Parameters.AddWithValue("@fechaCosumo", objTicket.fechaCosumo.ToString("yyyyMMdd"));
            com.Parameters.AddWithValue("@propina", objTicket.propina);
            com.Parameters.AddWithValue("@total", objTicket.total);
            SqlDataReader dr = null;
            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }
        public void Cancelar(long? codTicket)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TICKETS_CANCELAR";
            com.Parameters.AddWithValue("@codTicket", codTicket);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }
        public void ModificarFormaPago(long codTicket, long codFormaPago)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TICKETS_MODIFICAR_FORMAPAGO";
            com.Parameters.AddWithValue("@codTicket", codTicket);
            com.Parameters.AddWithValue("@codFormaPago", codFormaPago);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }
    }
}