﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Empresas;
using FRT.Models;
using FRT.Models.FormasPagos;

namespace FRT.Models.Tickets
{
    public class Ticket
    {
        public long? codTicket { get; set; }
        public long? codEmpresa { get; set; }
        public Empresa empresa { get; set; }
        public string folio { get; set; }
        public double total { get; set; }
        public double propina { get; set; }
        public DateTime fechaCosumo { get; set; }
        public int? CodStatusDoc { get; set; }
        public StatusDocumento statusDocumento { get; set; }
        public FormaPago formaPago { get; set; }
    }
}