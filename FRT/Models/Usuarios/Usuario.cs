﻿using FRT.Models.Empresas;
using FRT.Models.Perfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Usuarios
{
    public class Usuario
    {
        public long     codUsuario        { get; set; }
        public Empresa  Empresa           { get; set; }
        public Perfil   Perfil            { get; set; }
        public string   loginUsuario      { get; set; }
        public string   passwordUsuario   { get; set; }
        public string   nombreUsuario     { get; set; }
        public string   apePaterUsuario   { get; set; }
        public string   apeMaterUsuario   { get; set; }
        public string   mailUsuario       { get; set; }
        public string   telefonoUsuario   { get; set; }
        public DateTime fechaCreacion     { get; set; }
        public DateTime fechaModificacion { get; set; }
        public bool     eliminado         { get; set; }
    }
}