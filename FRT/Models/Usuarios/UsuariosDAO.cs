﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Perfiles;
using FRT.Models.Empresas;

namespace FRT.Models.Usuarios
{
    public class UsuariosDAO
    {

        public long TotalRecords { get; set; }

        public List<Usuario> Login(Usuario objUsuario)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TUSUARIOS_LOGIN";
            com.Parameters.AddWithValue("@loginUsuario", objUsuario.loginUsuario);
            com.Parameters.AddWithValue("@passwordUsuario", objUsuario.passwordUsuario);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Usuario> list = new List<Usuario>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Usuario
                    {

                        codUsuario = (long)dr["codUsuario"],
                        Empresa = new Empresa
                        {
                            codEmpresa = !Convert.IsDBNull(dr["codEmpresa"])?(long)dr["codEmpresa"]:0,
                            nombreEmp = !Convert.IsDBNull(dr["nombreEmp"])?(string)dr["nombreEmp"]:""
                        },
                        Perfil = new Perfil
                        {
                            codPerfil = !Convert.IsDBNull(dr["codPerfil"]) ? (long)dr["codPerfil"] : 0,
                            nombrePerfil = !Convert.IsDBNull(dr["nombrePerfil"]) ? (string)dr["nombrePerfil"] : ""
                        },
                        loginUsuario = (string)dr["loginUsuario"],
                        passwordUsuario = (string)dr["passwordUsuario"],
                        //nombreUsuario = (string)dr["nombreUsuario"],
                        //apePaterUsuario = (string)dr["apePaterUsuario"],
                        //apeMaterUsuario = !Convert.IsDBNull(dr["apeMaterUsuario"]) ? (string)dr["apeMaterUsuario"] : "",
                        mailUsuario = (string)dr["mailUsuario"],
                        //telefonoUsuario = !Convert.IsDBNull(dr["telefonoUsuario"]) ? (string)dr["telefonoUsuario"] : "",


                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
      
        public List<Usuario> Listado(int? start, long? limit, string busqcampo, string busqvalor, long? codUsuario, long? codPerfil)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TUSUARIOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@busqcampo", busqcampo);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            com.Parameters.AddWithValue("@codUsuario", codUsuario);
            com.Parameters.AddWithValue("@codPerfil", codPerfil);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Usuario> list = new List<Usuario>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Usuario
                    {
                        codUsuario = (long)dr["codUsuario"],
                        Empresa =new Empresa {
                           codEmpresa =!Convert.IsDBNull(dr["codEmpresa"]) ?  (long)dr["codEmpresa"] : 0,
                           nombreEmp= !Convert.IsDBNull(dr["nombreEmp"]) ? (string)dr["nombreEmp"]: ""
                    

                        },
                        Perfil=new Perfil {
                            codPerfil = (long)dr["codPerfil"],
                            nombrePerfil = (string)dr["nombrePerfil"],

                        },
                        loginUsuario = (string)dr["loginUsuario"],
                        passwordUsuario = (string)dr["passwordUsuario"],
                        //nombreUsuario = (string)dr["nombreUsuario"],
                        //apePaterUsuario = (string)dr["apePaterUsuario"],
                        //apeMaterUsuario =  !Convert.IsDBNull(dr["apeMaterUsuario"])?(string)dr["apeMaterUsuario"]:"",
                        mailUsuario = (string)dr["mailUsuario"],
                        //telefonoUsuario = !Convert.IsDBNull(dr["telefonoUsuario"])?(string)dr["telefonoUsuario"]:"",


                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

        public void Guardar(Usuario objUsuario)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TUSUARIOS_GUARDAR";
            com.Parameters.AddWithValue("@codUsuario", objUsuario.codUsuario);
            com.Parameters.AddWithValue("@codEmpresa", objUsuario.Empresa.codEmpresa);
            com.Parameters.AddWithValue("@codPerfil", objUsuario.Perfil.codPerfil);
            com.Parameters.AddWithValue("@loginUsuario", objUsuario.loginUsuario);
            com.Parameters.AddWithValue("@passwordUsuario", objUsuario.passwordUsuario);
            //com.Parameters.AddWithValue("@nombreUsuario", objUsuario.nombreUsuario);
            //com.Parameters.AddWithValue("@apePaterUsuario", objUsuario.apePaterUsuario);
            //com.Parameters.AddWithValue("@apeMaterUsuario", objUsuario.apeMaterUsuario);
            com.Parameters.AddWithValue("@mailUsuario", objUsuario.mailUsuario);
            //com.Parameters.AddWithValue("@telefonoUsuario", objUsuario.telefonoUsuario);

            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        public void Eliminar(long codUsuario)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TUSUARIOS_ELIMINAR";
            com.Parameters.AddWithValue("@codUsuario", codUsuario);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

    }
}