﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Permisos;
using FRT.Models.Admin;

namespace FRT.Models.Opciones
{
    public class PermisosDAO
    {
        public long TotalRecords { get; set; }

        public List<Permiso> Listado(int? start, long? limit, long? idPermiso)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TPERMISOS_LISTADO";
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Permiso> list = new List<Permiso>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Permiso
                    {
                        codPermiso = (int)dr["codPermiso"],
                        codOpcion = (int)dr["codOpcion"],
                        desPermiso = (string)dr["desPermiso"]



                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}