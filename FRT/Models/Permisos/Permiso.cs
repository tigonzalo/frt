﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Permisos
{
    public class Permiso
    {
        public int    codPermiso { get; set; }
        public int    codOpcion  { get; set; }
        public string desPermiso { get; set; }
    }
}