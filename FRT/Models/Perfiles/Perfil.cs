﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Permisos;

namespace FRT.Models.Perfiles
{
    public class Perfil
    {
        public long                codPerfil         { get; set; }
        public string              nombrePerfil      { get; set; }
        public List<PerfilPermiso> perfilPermisos    { get; set; }

        public DateTime            fechaCreacion     { get; set; }
        public DateTime            fechaModificacion { get; set; }
        public bool                eliminado         { get; set; }
        
    }
}