﻿using FRT.Models.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Perfiles
{
    public class PerfilPermiso
    {
        public long    codPerfilPermiso { get; set; }
        public long    codPerfil        { get; set; }
        public Permiso Permiso          { get; set; }
    }
}