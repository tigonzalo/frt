﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Permisos;

namespace FRT.Models.Perfiles
{
    public class PerfilPermisosDAO
    {
        public long TotalRecords { get; set; }

        public List<PerfilPermiso> Listado(int? start, long? limit, long? codPerfil)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TPERFILPERMISOS_LISTADO";
            com.Parameters.AddWithValue("@codPerfil", codPerfil);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<PerfilPermiso> list = new List<PerfilPermiso>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new PerfilPermiso
                    {

                        codPerfilPermiso = (long)dr["codPerfilPermiso"],
                        codPerfil = (long)dr["codPerfil"],
                        Permiso=new Permiso{
                            codPermiso = (int)dr["codPermiso"],
                            codOpcion = (int)dr["codOpcion"],
                            desPermiso = (string)dr["desPermiso"]
                            
                        }

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

       
    }
}