﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Permisos;

namespace FRT.Models.Perfiles
{
    public class PerfilesDAO
    {
        public long TotalRecords { get; set; }
        
        public List<Perfil> Listado(int? start, long? limit, long? codPerfil)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TPERFILES_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codPerfil", codPerfil);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Perfil> list = new List<Perfil>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Perfil
                    {
                        codPerfil = (long)dr["codPerfil"],
                        nombrePerfil = (string)dr["nombrePerfil"]

                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

        public void Guardar(Perfil objPerfil)
        {
            DataTable objTemTableArrayP = new DataTable();
            objTemTableArrayP = Utils.TableDate<PerfilPermiso>(objPerfil.perfilPermisos);

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TPERFILES_GUARDAR";
            com.Parameters.AddWithValue("@codPerfil", objPerfil.codPerfil);
            com.Parameters.AddWithValue("@nombrePerfil", objPerfil.nombrePerfil);
            com.Parameters.AddWithValue("@list", objTemTableArrayP);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

        public void Eliminar(long codPerfil)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TPERFILES_ELIMINAR";
            com.Parameters.AddWithValue("@codPerfil", codPerfil);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

    }
}