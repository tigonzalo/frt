﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Estados
{
    public class Estado
    {
        public string codEstado { get; set; }
        public string nombreEstado { get; set; }

    }
}