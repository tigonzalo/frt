﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;



namespace FRT.Models.Estados
{
    public class EstadosDAO
    {
        public long TotalRecords { get; set; }
        public List<Estado> Listado(int? start, long? limit, string codEstado)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TESTADOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codEstado", codEstado);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Estado> list = new List<Estado>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Estado
                    {
                        codEstado = (string)dr["codEstado"],
                        nombreEstado = (string)dr["nombreEstado"],
                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
    }
}