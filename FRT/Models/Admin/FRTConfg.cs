﻿using FRT.Models.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Admin
{
    public static class Config
    {
       

        public static Usuario sesionValida(HttpContextBase HttpContext)
        {
            Usuario objUsuario = (Usuario)HttpContext.Session["Usuario"];
            if (objUsuario == null)
                throw new SesionException();
            else
                return objUsuario;
        }
    }
}