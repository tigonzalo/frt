﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Configuration;
using FRT.Models.Clientes;
using System.IO.Ports;
using System.Threading;
using System.Net.Mail;
using FRT.Models.Empresas;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FRT.Models.Documentos;
using FRT.Models.Usuarios;
using FRT.Models.Admin;
using FRT.Models.DocumentosPac;
using FRT.Models.DocumentosPac.ReportesPac;
using FRT.Models.Documentos.RepDocumentos;
using FRT.Models;
using FRT.Models.Common;
//using System.Net.Mail.MailMessage;
//using System.Net.Mail.SmtpClient;
namespace FRT.Models.Admin
{
    public class Correo
    {
        public static string bodyCorreo = "";
        public static void enviar(Cliente objCliente, Sistema objSistema, string subject_,string plantilla)
        {
            correoBodyContrasenia(objCliente, objSistema, plantilla);
            MailMessage correo = new MailMessage();
            correo.From = new MailAddress(objSistema.GLO_MAIL_FROM);
            correo.To.Add(objCliente.usuario.mailUsuario);
            correo.Subject = subject_;
            correo.IsBodyHtml = true;
            correo.Body = bodyCorreo;
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = objSistema.GLO_MAIL_HOST;
            smtp.Port = objSistema.GLO_MAIL_PUERTO;
            smtp.EnableSsl = objSistema.GLO_MAIL_SSL;
            smtp.Timeout = 10000;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(objSistema.GLO_MAIL_FROM, objSistema.GLO_MAIL_CONTRASENIA);
            smtp.Send(correo);
       
            
        }
       

        public static void enviarCFDI_1(Documento objDocumento, string correoUsuario, string ArchivoPDF, string ArchivoXML,string dirTemp)
        {

            //if (!File.Exists(ArchivoPDF))
            //    crearPdfCFDI(objDocumento,ArchivoXML);
            correoBodyCfdi(objDocumento, objDocumento.empresa);
            string subject_ = objDocumento.empresa.razonSocialEmp + " Envio de Factura: " + objDocumento.folioFactura + " de Tikect: " + objDocumento.folio;
            MailMessage correo = new MailMessage();
            //Attachment PDF = new Attachment(ArchivoPDF);
            //Attachment XML = new Attachment(ArchivoXML);
            Attachment PDF = new Attachment(ArchivoPDF);
            Attachment XML = new Attachment(ArchivoXML);


            correo.From = new MailAddress(objDocumento.empresa.mailFromEmp);
            correo.To.Add(correoUsuario);
            correo.Subject = subject_;
            correo.IsBodyHtml = true;
            correo.Body = bodyCorreo;
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;
            correo.Attachments.Add(PDF);
            correo.Attachments.Add(XML);

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = objDocumento.empresa.mailHostEmp;
            smtp.Port = objDocumento.empresa.mailPuertoEmp;
            smtp.EnableSsl = objDocumento.empresa.mailSslEmp;
            smtp.Timeout = 20000;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(objDocumento.empresa.mailFromEmp, objDocumento.empresa.mailPasswordEmp);
            //smtp.Credentials = new System.Net.NetworkCredential("facturapampas@gmail.com", "Pampas2015");
            smtp.Send(correo);
            //Correo.borrarTemporal(dirTemp);
            

        }
        public static void borrarTemporal(string Dir) {
            string[] filePaths = Directory.GetFiles(Dir); 
                foreach (string filePath in filePaths)
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
        
        }
        public static void enviarCFDI(Documento objDocumento, Empresa objEmpresa, string correoUsuario, string subject_, Double aFactura, string ArchivoPDF, string ArchivoXML)
        {
            correoBodyCfdi(objDocumento, objEmpresa);
            String NoFactura = Convert.ToString(aFactura);
            subject_ = objEmpresa.razonSocialEmp + " Envio de Factura: " + NoFactura + " de Tikect: " + objDocumento.folio;
            MailMessage correo = new MailMessage();
            //Attachment PDF = new Attachment(ArchivoPDF);
            //Attachment XML = new Attachment(ArchivoXML);
            Attachment PDF = new Attachment(ArchivoPDF + ArchivoXML + ".pdf");
            Attachment XML = new Attachment(ArchivoPDF + ArchivoXML + ".xml");


            correo.From = new MailAddress(objEmpresa.mailFromEmp);
            correo.To.Add(correoUsuario);
            correo.Subject = subject_;
            correo.IsBodyHtml = true;
            correo.Body = bodyCorreo;
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;
            correo.Attachments.Add(PDF);
            correo.Attachments.Add(XML);

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = objEmpresa.mailHostEmp;
            smtp.Port = objEmpresa.mailPuertoEmp;
            smtp.EnableSsl = objEmpresa.mailSslEmp;
            smtp.Timeout = 20000;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(objEmpresa.mailFromEmp, objEmpresa.mailPasswordEmp);
            smtp.Send(correo);


        }
        public static void correoBodyContrasenia(Cliente objCliente, Sistema objSistema,string plantilla)
        {
            if (plantilla == "contrasenia")
            {
            bodyCorreo = "<div style=\"font: 15px Calibri, arial;\">";
            bodyCorreo += "<table width=100%>";
            bodyCorreo += "<tr><td bgcolor=#ffffff style=padding: 40px 30px 40px 30px; align=right> <strong> Sr(a)." + objCliente.usuario.nombreUsuario + " " + objCliente.usuario.apePaterUsuario + "</strong></td></tr>";
            bodyCorreo += "<tr><td align=center>" + objSistema.DESCRIPCION + "</td></tr>";
            bodyCorreo += "<tr><td><strong>Usuario:</strong>" + objCliente.usuario.loginUsuario + "</td></tr>";
            bodyCorreo += "<tr><td><strong>Contrasenia:</strong>" + objCliente.usuario.passwordUsuario + "</td></tr>";
            bodyCorreo += "<tr><td><strong>Favor de ingresar a la pagina <a href='http://www.facturaglobal.com.mx/FRT' target='_blank'>Facturacion Global!</a></strong></td></tr>";
            bodyCorreo += "</table></div>";
            }
        }


        public static void correoBodyCfdi(Documento objDocumento, Empresa objEmpresa)
        {
                bodyCorreo = "<div style=\"font: 15px Calibri, arial;\">";
                bodyCorreo += "<table width=100%>";
                //bodyCorreo += "<tr><td bgcolor=#ffffff style=padding: 40px 30px 40px 30px; align=right> <strong> Sr(a)." + objDocumento.Cliente.razonSocialClie + " " + objCliente.usuario.apePaterUsuario + "</strong></td></tr>";
                bodyCorreo += "<tr><td align=center>Envio de Archivos XML y PDF</td></tr>";
                bodyCorreo += "<tr><td><strong>Cliente:</strong>" + objDocumento.cliente.razonSocialClie + "</td></tr>";
                bodyCorreo += "<tr><td><strong>RFC:</strong>" + objDocumento.cliente.rfcClie + "</td></tr>";
                bodyCorreo += "</table></div>";
        }
        public static void correoBodyCfdi33(Documento objDocumento)
        {
            bodyCorreo = "<div style=\"font: 15px Calibri, arial;\">";
            bodyCorreo += "<table width=100%>";
            //bodyCorreo += "<tr><td bgcolor=#ffffff style=padding: 40px 30px 40px 30px; align=right> <strong> Sr(a)." + objDocumento.Cliente.razonSocialClie + " " + objCliente.usuario.apePaterUsuario + "</strong></td></tr>";
            bodyCorreo += "<tr><td align=center>Envio de Archivos XML y PDF</td></tr>";
            bodyCorreo += "<tr><td><strong>Cliente:</strong>" + objDocumento.razonSocialCliente + "</td></tr>";
            bodyCorreo += "<tr><td><strong>RFC:</strong>" + objDocumento.rfcCliente + "</td></tr>";
            bodyCorreo += "</table></div>";
        }
        public static void enviarCFDI33(Documento objDocumento, string ArchivoPDF, string ArchivoXML, string dirTemp,string correoC)
        {

            //if (!File.Exists(ArchivoPDF))
            //    crearPdfCFDI(objDocumento,ArchivoXML);
            correoBodyCfdi33(objDocumento);
            string subject_ = objDocumento.empresa.razonSocialEmp + " Envio de Factura: " + objDocumento.folioFactura + " de Tikect: " + objDocumento.folio;
            MailMessage correo = new MailMessage();
            //Attachment PDF = new Attachment(ArchivoPDF);
            //Attachment XML = new Attachment(ArchivoXML);
            Attachment PDF = new Attachment(ArchivoPDF);
            Attachment XML = new Attachment(ArchivoXML);


            correo.From = new MailAddress(objDocumento.empresa.mailFromEmp);
            correo.To.Add(correoC);
            correo.Subject = subject_;
            correo.IsBodyHtml = true;
            correo.Body = bodyCorreo;
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;
            correo.Attachments.Add(PDF);
            correo.Attachments.Add(XML);

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = objDocumento.empresa.mailHostEmp;
            smtp.Port = objDocumento.empresa.mailPuertoEmp;
            smtp.EnableSsl = objDocumento.empresa.mailSslEmp;
            smtp.Timeout = 20000;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(objDocumento.empresa.mailFromEmp, objDocumento.empresa.mailPasswordEmp);
            //smtp.Credentials = new System.Net.NetworkCredential("facturapampas@gmail.com", "Pampas2015");
            smtp.Send(correo);
            //Correo.borrarTemporal(dirTemp);


        }
        
    }
}