﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace FRT.Models.Admin
{
    public class AdminDB
    {
        public enum typeDBSystem
        {
            WEBFMCDB
        }

        public static SqlConnection getConnection(typeDBSystem type)
        {
            SqlConnection cnx;
            string scnx;
            switch (type)
            {
                case typeDBSystem.WEBFMCDB:
                    scnx = WebConfigurationManager.ConnectionStrings["WEBFMCDB"].ConnectionString;
                    break;
                default:
                    scnx = null;
                    break;
            }
            if (scnx != null)
                cnx = new SqlConnection(scnx);
            else
                return null;
            return cnx;
        }
    }
}