﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMC.Models.Contpaqi
{
    public class Direccion
    {
        public string CodigoCliente { get; set; }
        public int TipoCatalogo { get; set; }  // 1=Clientes y 2=Proveedores
        public int TipoDireccion { get; set; }  // 1=Domicilio Fiscal, 2=Domicilio Envio
        public string NombreCalle { get; set; }
        public string NumeroExterior { get; set; }
        public string NumeroInterior { get; set; }
        public string Colonia { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Telefono3 { get; set; }
        public string Telefono4 { get; set; }
        public string Email { get; set; }
        public string DireccionWeb { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string TextoExtra { get; set; }
    }
}