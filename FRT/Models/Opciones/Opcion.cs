﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.Opciones
{
    public class Opcion
    {
        public int    codOpcion { get; set; }
        public string desOpcion { get; set; }
    }
}