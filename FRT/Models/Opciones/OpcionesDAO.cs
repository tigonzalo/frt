﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using FRT.Models.Common;
using FRT.Models.Permisos;
using FRT.Models.Admin;

namespace FRT.Models.Opciones
{
    public class OpcionesDAO
    {
        public long TotalRecords { get; set; }

        public List<Opcion> Listado(int? start, long? limit, long? idOpcion)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TOPCIONES_LISTADO";
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Opcion> list = new List<Opcion>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Opcion
                    {
                        codOpcion = (int)dr["codOpcion"],
                        desOpcion = (string)dr["desOpcion"]



                    });
                }
            }

            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }

        public void Guardar(List<Opcion> listOpciones, List<Permiso> listPermisos)
        {
            DataTable objTemTableArrayO = new DataTable();
            DataTable objTemTableArrayP = new DataTable();
            objTemTableArrayO = Utils.TableDate<Opcion>(listOpciones);
            objTemTableArrayP = Utils.TableDate<Permiso>(listPermisos);

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_OPCIONPERMISOS_GUARDAR";
            com.Parameters.AddWithValue("@tbOpciones", objTemTableArrayO);
            com.Parameters.AddWithValue("@tbPermisos", objTemTableArrayP);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }

    }
}