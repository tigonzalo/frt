﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac.ReportesPac
{
    public class ConceptosCFDI
    {
        public string claveProdServ { get; set; }
        public string claveUnidad { get; set; }
        public string cantidad { get; set; }
        public string descripcion { get; set; }
        public string valorUnitario { get; set; }
        public string importe { get; set; }
        public List<Traslado> traslados { get; set; }
    }
}