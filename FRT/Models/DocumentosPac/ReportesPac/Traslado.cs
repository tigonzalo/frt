﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac.ReportesPac
{
    public class Traslado
    {
        public string Base { get; set; }
        public string Impuesto { get; set; }
        public string TipoFactor { get; set; }
        public string TasaOCuota { get; set; }
        public string Importe { get; set; }
    }
}