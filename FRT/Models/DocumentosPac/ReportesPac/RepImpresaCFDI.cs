﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac.ReportesPac
{
    public class RepImpresaCFDI
    {
        //cfdi:Comprobante
        public string sello { get; set; }
        public string folio { get; set; }
        public string fecha { get; set; }
        public string formaDePago { get; set; }
        public string noCertificado { get; set; }
        public string certificado { get; set; }
        public string subTotal { get; set; }
        public string TipoCambio { get; set; }
        public string Moneda { get; set; }
        public string total { get; set; }
        public string tipoDeComprobante { get; set; }
        public string metodoDePago { get; set; }
        public string LugarExpedicion { get; set; }
        public string versionCFDI { get; set; }
        //cfdi:Emisor 
        public string eRfc { get; set; }
        public string eNombre { get; set; }
        public string eDFCalle { get; set; }
        public string eDNoExterior { get; set; }
        public string eDFLocalidad { get; set; }
        public string eDFMunicipio { get; set; }
        public string eDFEstado { get; set; }
        public string eDFPais { get; set; }
        public string eDFCodigoPostal { get; set; }
        public string eRegimenFiscal { get; set; }
        //ExpedidoEn                               
        public string eEnCalle { get; set; }
        public string eNoExterior { get; set; }
        public string eEnColonia { get; set; }
        public string eEnLocalidad { get; set; }
        public string eEnMunicipio { get; set; }
        public string eEnEstado { get; set; }
        public string eEnPais { get; set; }
        public string eEnCodigoPostal { get; set; }

        //RegimenFiscal
        public string rFRegimen { get; set; }
        //Receptor
        public string rRfc { get; set; }
        public string rNombre { get; set; }
        public string rCalle { get; set; }
        public string rNoExterior { get; set; }
        public string rNoInt { get; set; }
        public string rColonia { get; set; }
        public string rLocalidad { get; set; }
        public string rMunicipio { get; set; }
        public string rEstado { get; set; }
        public string rPais { get; set; }
        public string rCodigoPostal { get; set; }
        public string rUsoCFDI { get; set; }
        //<cfdi:Impuestos
        public string totalImpuestosTrasladados { get; set; }
        //<cfdi:Traslado
        public string impuesto { get; set; }
        public string tipoFactor { get; set; }
        public string tasaOCuota { get; set; }
        public string importe { get; set; }

        //<tfd:TimbreFiscalDigital 
        public string selloCFD { get; set; }
        public string FechaTimbrado { get; set; }
        public string UUID { get; set; }
        public string noCertificadoSAT { get; set; }
        public string selloSAT { get; set; }
        public string version { get; set; }
        public string cadenaDelSat { get; set; }
        public string rfcProvCertif { get; set; }

        public byte[] qrImage { get; set; }
        public string cbb { get; set; }
        public string logo { get; set; }
        public string fechaConsumo { get; set; }
        public List<ConceptosCFDI> conceptosCFDI { get; set; }
    }
}