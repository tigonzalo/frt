﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.Data;
using FRT.Models.Common;
using System.Threading;
using FRT.Models.DocumentosPac;
using FRT.Models.DocumentosPac.ReportesPac;
using System.Text;
using FRT.Models.DetalleTickets;


namespace FRT.Models.DocumentosPac
{
    public class DocumentosPacDAO
    {
        
        public long TotalRecords { get; set; }
        public string DesError { get; set; }
        public int NumError { get; set; }
        public string file_name { get; set; }
        public List<Documento> Listado(int? start, long? limit, long? codDocumento, Usuario objUsuario, string busqcampo, string busqvalor)
        {
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_TDOCUMENTOS_LISTADO";
            com.Parameters.AddWithValue("@start", start);
            com.Parameters.AddWithValue("@limit", limit);
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            com.Parameters.AddWithValue("@codUsuario", objUsuario.codUsuario);
            com.Parameters.AddWithValue("@busqcampo", busqcampo);
            com.Parameters.AddWithValue("@busqvalor", busqvalor);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
            List<Documento> list = new List<Documento>();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(new Documento
                    {
                        codDocumento = (long)dr["codDocumento"],
                        codUsuario = (long)dr["codUsuario"],
                        codEmpresa = (long)dr["codEmpresa"],
                        codCliente = (long)dr["codCliente"],
                        fecha = !Convert.IsDBNull(dr["fecha"]) ? (DateTime)dr["fecha"] : default(DateTime),
                        fechaConsumo = !Convert.IsDBNull(dr["fechaConsumo"]) ? (DateTime)dr["fechaConsumo"] : default(DateTime),
                        total = !Convert.IsDBNull(dr["total"]) ? (decimal)dr["total"] : 0,
                        folio = !Convert.IsDBNull(dr["folio"]) ? (string)dr["folio"] : "",
                        StatusDocumento = new StatusDocumento
                        {
                            CodStatusDoc = (int)dr["CodStatusDoc"],
                            Descripcion = (string)dr["STATUS_DOC"]
                        },
                        rutaPdf = !Convert.IsDBNull(dr["rutaPdf"]) ? (string)dr["rutaPdf"] : "",
                        rutaXml = !Convert.IsDBNull(dr["rutaXml"]) ? (string)dr["rutaXml"] : "",
                        folioFactura = !Convert.IsDBNull(dr["folioFactura"]) ? (string)dr["folioFactura"] : "",
                        empresa = new Empresas.Empresa
                        {
                            nombreEmp = (string)dr["EMPRESA_DOC"]
                        },


                    });
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
            this.TotalRecords = (totalRecords.Value != null) ? (long)totalRecords.Value : 0;
            return list;
        }
        public void obtenerDatosFacturaCFDI(DocumentoPac documentoPac)
        {
            
            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_OBTENER_DATOS_FACTURA_CFDI_33";
            com.Parameters.AddWithValue("@codDocumento", documentoPac.documento.codDocumento);
            com.Parameters.AddWithValue("@hold", !documentoPac.documento.hold);
            SqlParameter totalRecords = new SqlParameter("@totalRecords", 0);
            totalRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalRecords);
            SqlDataReader dr = null;
          //  DocumentoPac documentoPac = new DocumentoPac();
            try
            {
                conn.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    //datos generales 3.3 
                   
                    documentoPac.claveProdServ = !Convert.IsDBNull(dr["claveProdServ"]) ? (string)dr["claveProdServ"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.claveUnidad = !Convert.IsDBNull(dr["claveUnidad"]) ? (string)dr["claveUnidad"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.claveImpuesto = !Convert.IsDBNull(dr["claveImpuesto"]) ? (string)dr["claveImpuesto"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.tipoFactorImpuesto = !Convert.IsDBNull(dr["tipoFactorImpuesto"]) ? (string)dr["tipoFactorImpuesto"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.tasaOCuotaImpuesto = !Convert.IsDBNull(dr["tasaOCuotaImpuesto"]) ? (decimal)dr["tasaOCuotaImpuesto"] : 0;
// stcDocumentInfo
                    documentoPac.documento.hold = !Convert.IsDBNull(dr["hold"]) ? (bool)dr["hold"] : false;
                    Empresas.Empresa empresa = new Empresas.Empresa();
                    empresa.rutaEmp  = !Convert.IsDBNull(dr["rutaEmp"]) ? (string)dr["rutaEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.codConcepto = !Convert.IsDBNull(dr["codConcepto"]) ? (string)dr["codConcepto"].ToString().Trim().Replace("  ", "") : "";
                    empresa.PathPlantillaPDF   = !Convert.IsDBNull(dr["PathPlantillaPDF"]) ? (string)dr["PathPlantillaPDF"].ToString().Trim().Replace("  ", "") : "";
                    empresa.codEmpresa = !Convert.IsDBNull(dr["codEmpresa"]) ? (long)dr["codEmpresa"]: 0;
                    empresa.nombreEmp = !Convert.IsDBNull(dr["nombreEmp"]) ? (string)dr["nombreEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.telefonoEmp = !Convert.IsDBNull(dr["telefonoEmp"]) ? (string)dr["telefonoEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.mailFromEmp = !Convert.IsDBNull(dr["mailFromEmp"]) ? (string)dr["mailFromEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.mailPasswordEmp = !Convert.IsDBNull(dr["mailPasswordEmp"]) ? (string)dr["mailPasswordEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.mailHostEmp = !Convert.IsDBNull(dr["mailHostEmp"]) ? (string)dr["mailHostEmp"].ToString().Trim().Replace("  ", "") : "";
                    empresa.mailPuertoEmp = !Convert.IsDBNull(dr["mailPuertoEmp"]) ? (int)dr["mailPuertoEmp"] : 0;
                    empresa.mailSslEmp = !Convert.IsDBNull(dr["mailSslEmp"]) ? (bool)dr["mailSslEmp"] : false;
                    empresa.contactoEmp = !Convert.IsDBNull(dr["contactoEmp"]) ? (string)dr["contactoEmp"].ToString().Trim().Replace("  ", "") : "";
                   
               
                    
                    documentoPac.documento.empresa = empresa;
                    //documentoPac.documento.total = !Convert.IsDBNull(dr["dmlTotal"]) ? (decimal)dr["dmlTotal"] : 0;
                    documentoPac.documento.propina = !Convert.IsDBNull(dr["propina"]) ? (double)dr["propina"] : 0;
                    
                    documentoPac.documento.total = !Convert.IsDBNull(dr["dmlTotal"]) ? Convert.ToDecimal((double)dr["dmlTotal"]) : 0;
                    documentoPac.stcDocument.dmlTotal = !Convert.IsDBNull(dr["dmlTotal"]) ? Convert.ToDecimal((double)dr["dmlTotal"]) : 0;
                    // subtotal
                    documentoPac.stcDocument.dmlSubtotal = !Convert.IsDBNull(dr["dmlSubtotal"]) ? Convert.ToDecimal((double)dr["dmlSubtotal"]) : 0;
                    documentoPac.stcDocument.dmlSubtotal += Convert.ToDecimal(documentoPac.documento.propina);
                    documentoPac.stcDocument.dmlTotal += Convert.ToDecimal(documentoPac.documento.propina);
                    //iva 
                    documentoPac.stcDocument.dmlIVATax = !Convert.IsDBNull(dr["dmlIVATax"]) ? Convert.ToDecimal((double)dr["dmlIVATax"]) : 0;
                    
                    StatusDocumento statusDocumento=new StatusDocumento();
                    statusDocumento.CodStatusDoc = (int)dr["CodStatusDoc"];
                    //statusDocumento.Descripcion = (string)dr["STATUS_DOC"];

                    documentoPac.documento.StatusDocumento = statusDocumento;
                    
                    //archivo llave
                   // documentoPac.strKeyFilePath = !Convert.IsDBNull(dr["strKeyFilePath"]) ? (string)dr["strKeyFilePath"].ToString().Trim().Replace("  ", "") : "";
                    //contraseña archivo
                    documentoPac.strKeyFilePassword = !Convert.IsDBNull(dr["strKeyFilePassword"]) ? (string)dr["strKeyFilePassword"].ToString().Trim().Replace("  ", "") : "";
                    //archivo certificado
                   // documentoPac.strCertificateFilePath = !Convert.IsDBNull(dr["strCertificateFilePath"]) ? (string)dr["strCertificateFilePath"].ToString().Trim().Replace("  ", "") : "";
                    // usario Pac
                    documentoPac.stcDocument.strUserPAC = !Convert.IsDBNull(dr["rfcEmp"]) ? (string)dr["rfcEmp"].ToString().Trim().Replace("  ", "") : "";
                    // pasword pac
                    documentoPac.stcDocument.strPasswordPAC = !Convert.IsDBNull(dr["strPasswordPAC"]) ? (string)dr["strPasswordPAC"].ToString().Trim().Replace("  ", "") : "";
                    //documentoPac.stcDocument.strStatusPAC = !Convert.IsDBNull(dr["passwordCFDI"]) ? (string)dr["passwordCFDI"].ToString().Trim().Replace("  ", "") : "";

                   
                    documentoPac.stcDocument.noCertificado = !Convert.IsDBNull(dr["noCertificado"]) ? (string)dr["noCertificado"].ToString().Trim().Replace("  ", "") : "";
                    //serie del documento
                    documentoPac.stcDocument.strSerie = !Convert.IsDBNull(dr["strSerie"]) ? (string)dr["strSerie"].ToString().Trim().Replace("  ", "") : "";
                   //folio del documento
                    documentoPac.stcDocument.strFolio = !Convert.IsDBNull(dr["strFolio"]) ? (string)dr["strFolio"].ToString().Trim().Replace("  ", "") : "";
                   //metodo de pago
                    documentoPac.stcDocument.strVoucherType = !Convert.IsDBNull(dr["strVoucherType"]) ? (string)dr["strVoucherType"].ToString().Trim().Replace("  ", "") : "";
                    // forma de pago
                    documentoPac.stcDocument.strPaymentForm = !Convert.IsDBNull(dr["strPaymentForm"]) ? (string)dr["strPaymentForm"].ToString().Trim().Replace("  ", "") : "";
                    // condisiones de pago
                    documentoPac.stcDocument.strPaymentConditions = !Convert.IsDBNull(dr["strPaymentConditions"]) ? (string)dr["strPaymentConditions"].ToString().Trim().Replace("  ", "") : "";
                    //descuento del documento
                    documentoPac.stcDocument.dmlDiscount = !Convert.IsDBNull(dr["dmlDiscount"]) ? (decimal)dr["dmlDiscount"] : 0;
                    //total del documento
                    
                    //metodo de pago
                    documentoPac.stcDocument.strPaymentMethod = !Convert.IsDBNull(dr["strPaymentMethod"]) ? (string)dr["strPaymentMethod"].ToString().Trim().Replace("  ", "") : "";
                    //
                   // documentoPac.stcDocument.strCertificateNumber = !Convert.IsDBNull(dr["strCertificateNumber"]) ? (string)dr["strCertificateNumber"].ToString().Trim().Replace("  ", "") : "";
                    //iva tarifa
                    documentoPac.stcDocument.dmlIVATaxRate = !Convert.IsDBNull(dr["dmlIVATaxRate"]) ? Convert.ToDecimal((double)dr["dmlIVATaxRate"]) : 0;
                    
                   //moneda 'PESOS'
                    documentoPac.stcDocument.strMoneda = !Convert.IsDBNull(dr["strMoneda"]) ? (string)dr["strMoneda"].ToString().Trim().Replace("  ", "") : "";
                    // 1 PARA PASOS
                    documentoPac.stcDocument.strTipoCambio = !Convert.IsDBNull(dr["strTipoCambio"]) ? (decimal)dr["strTipoCambio"] : 0;
                    //NUMERO DE CUAENTA
                    documentoPac.stcDocument.strNumCtaPago = !Convert.IsDBNull(dr["strNumCtaPago"]) ? (string)dr["strNumCtaPago"].ToString().Trim().Replace("  ", "") : "";
                    
// stcEmitterInfo_General

                    documentoPac.stcEmitter.strRFC = !Convert.IsDBNull(dr["rfcEmp"]) ? (string)dr["rfcEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strName = !Convert.IsDBNull(dr["razonSocialEmp"]) ? (string)dr["razonSocialEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strStreet = !Convert.IsDBNull(dr["calleEmp"]) ? (string)dr["calleEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strOutsideNumber = !Convert.IsDBNull(dr["noExteriorEmp"]) ? (string)dr["noExteriorEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strInsideNumber = !Convert.IsDBNull(dr["noInteriorEmp"]) ? (string)dr["noInteriorEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strColony = !Convert.IsDBNull(dr["coloniaEmp"]) ? (string)dr["coloniaEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strLocation = !Convert.IsDBNull(dr["localidadEmp"]) ? (string)dr["localidadEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strReference = !Convert.IsDBNull(dr["strReferenceEmp"]) ? (string)dr["strReferenceEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strTown = !Convert.IsDBNull(dr["strTownEmp"]) ? (string)dr["strTownEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strState = !Convert.IsDBNull(dr["strStateEmp"]) ? (string)dr["strStateEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strCountry = !Convert.IsDBNull(dr["paisEmp"]) ? (string)dr["paisEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strPostalCode = !Convert.IsDBNull(dr["cpEmp"]) ? (string)dr["cpEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmitter.strRegimenFiscal = !Convert.IsDBNull(dr["regimenFiscalEmp"]) ? (string)dr["regimenFiscalEmp"].ToString().Trim().Replace("  ", "") : "";
// stcEmittedInInfo_General
                    
                    documentoPac.stcEmitter.lugarExpedicion = !Convert.IsDBNull(dr["lugarExpedicion"]) ? (string)dr["lugarExpedicion"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strStreet = !Convert.IsDBNull(dr["calleEmp"]) ? (string)dr["calleEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strOutsideNumber = !Convert.IsDBNull(dr["noExteriorEmp"]) ? (string)dr["noExteriorEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strInsideNumber = !Convert.IsDBNull(dr["noInteriorEmp"]) ? (string)dr["noInteriorEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strColony = !Convert.IsDBNull(dr["coloniaEmp"]) ? (string)dr["coloniaEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strLocation = !Convert.IsDBNull(dr["localidadEmp"]) ? (string)dr["localidadEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strReference = !Convert.IsDBNull(dr["strReferenceEmp"]) ? (string)dr["strReferenceEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strTown = !Convert.IsDBNull(dr["strTownEmp"]) ? (string)dr["strTownEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strState = !Convert.IsDBNull(dr["strStateEmp"]) ? (string)dr["strStateEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strCountry = !Convert.IsDBNull(dr["paisEmp"]) ? (string)dr["paisEmp"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcEmittedIn.strPostalCode = !Convert.IsDBNull(dr["cpEmp"]) ? (string)dr["cpEmp"].ToString().Trim().Replace("  ", "") : "";

//stcReceiverInfo_General

                    documentoPac.stcReceiver.strRFC = !Convert.IsDBNull(dr["rfcClie"]) ? (string)dr["rfcClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strName = !Convert.IsDBNull(dr["razonSocialClie"]) ? (string)dr["razonSocialClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strStreet = !Convert.IsDBNull(dr["calleClie"]) ? (string)dr["calleClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strOutsideNumber = !Convert.IsDBNull(dr["numExtClie"]) ? (string)dr["numExtClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strInsideNumber = !Convert.IsDBNull(dr["numIntClie"]) ? (string)dr["numIntClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strColony = !Convert.IsDBNull(dr["coloniaClie"]) ? (string)dr["coloniaClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strLocation = !Convert.IsDBNull(dr["localidadClie"]) ? (string)dr["localidadClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strReference = !Convert.IsDBNull(dr["strReferenceClie"]) ? (string)dr["strReferenceClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strTown = !Convert.IsDBNull(dr["strTownClie"]) ? (string)dr["strTownClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strState = !Convert.IsDBNull(dr["strStateClie"]) ? (string)dr["strStateClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strCountry = !Convert.IsDBNull(dr["paisClie"]) ? (string)dr["paisClie"].ToString().Trim().Replace("  ", "") : "";
                    documentoPac.stcReceiver.strPostalCode = !Convert.IsDBNull(dr["cpClie"]) ? (string)dr["cpClie"].ToString().Trim().Replace("  ", "") : "";
                }
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }
           
        }
        public void obtenerDatosFacturaCFDIDetalle(DocumentoPac documentoPac)
        {
            StcConcepts stcConcepts = new StcConcepts();
            DetalleTicketDAO objDetalleTicketDAO = new DetalleTicketDAO();
            List<DetalleTicket> listD = new List<DetalleTicket>();
            listD = objDetalleTicketDAO.Listado(documentoPac.documento.folio, documentoPac.documento.codEmpresa, documentoPac.documento.serie);
            if (listD.Count == 0)
            {
                stcConcepts.intQuantity = 1;
                stcConcepts.strDescription = "CONSUMO RESTAURANTE";

                stcConcepts.dmlPrice = Convert.ToDecimal(documentoPac.stcDocument.dmlSubtotal) - Convert.ToDecimal(documentoPac.documento.propina);
                stcConcepts.dmlTotal = stcConcepts.dmlPrice * Convert.ToDecimal(stcConcepts.intQuantity);


                //stcConcepts.dmlPrice = Convert.ToDecimal(documentoPac.stcDocument.dmlSubtotal);
                //stcConcepts.dmlTotal = stcConcepts.dmlPrice * Convert.ToDecimal(stcConcepts.intQuantity);
                
                stcConcepts.claveProdServ = documentoPac.claveProdServ;
                stcConcepts.claveUnidad = documentoPac.claveUnidad;
                stcConcepts.claveImpuesto= documentoPac.claveImpuesto;
                stcConcepts.tipoFactor = documentoPac.tipoFactorImpuesto;
                stcConcepts.tasaOCuota = documentoPac.tasaOCuotaImpuesto;

                stcConcepts.baseImpuesto = stcConcepts.dmlTotal;
                stcConcepts.importeImpuesto = stcConcepts.dmlPrice * documentoPac.tasaOCuotaImpuesto; 

                documentoPac.totalImpuestos = stcConcepts.dmlPrice * documentoPac.tasaOCuotaImpuesto;
                documentoPac.stcConcepts.Add(stcConcepts);


                if (documentoPac.documento.propina > 0)
                {
                    stcConcepts = new StcConcepts();
                    stcConcepts.intQuantity = 1;
                    stcConcepts.strDescription = "PROPINA";
                    stcConcepts.dmlPrice = Convert.ToDecimal(documentoPac.documento.propina);
                    stcConcepts.dmlTotal = Convert.ToDecimal(documentoPac.documento.propina);
                    stcConcepts.claveProdServ = "84101603";
                    stcConcepts.claveUnidad = documentoPac.claveUnidad;
                    stcConcepts.claveImpuesto = documentoPac.claveImpuesto;
                    stcConcepts.tipoFactor = "Exento";
                    stcConcepts.baseImpuesto = stcConcepts.dmlTotal;
          
                    documentoPac.stcConcepts.Add(stcConcepts);
                }
            }
            else
            {
                documentoPac.stcDocument.dmlIVATax = 0;
                documentoPac.stcDocument.dmlSubtotal = 0;
                foreach (DetalleTicket row in listD)
                {
                    
                    stcConcepts = new StcConcepts();
                    stcConcepts.intQuantity = row.unidades;
                    stcConcepts.strDescription = row.producto;
                    stcConcepts.dmlPrice = Convert.ToDecimal(row.precio);
                    stcConcepts.dmlTotal = Convert.ToDecimal(row.importe);
                    documentoPac.stcDocument.dmlSubtotal += (Convert.ToDecimal(row.unidades) * Convert.ToDecimal(row.precio));
                    documentoPac.stcDocument.dmlIVATax += (Convert.ToDecimal(row.unidades) * Convert.ToDecimal(row.precio)) * (Convert.ToDecimal(row.iva) / 100);
                    documentoPac.stcConcepts.Add(stcConcepts);
                }

            }   
              

        }
        public void actualizarUUID(long codDocumento, string UUID, int nError, string estatus)
        {
            //DataTable objTemTableArrayP = new DataTable();
            //objTemTableArrayP = Utils.TableDate<PerfilPermiso>(objPerfil.Perfilpermisos);

            SqlConnection conn = AdminDB.getConnection(AdminDB.typeDBSystem.WEBFMCDB);
            SqlCommand com = new SqlCommand();
            com.Connection = conn;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "SP_T DOCUMENTOS_ACTUALIZAR_UUID";
            com.Parameters.AddWithValue("@codDocumento", codDocumento);
            com.Parameters.AddWithValue("@nError", nError);
            com.Parameters.AddWithValue("@UUID", UUID);
            com.Parameters.AddWithValue("@estatus", estatus);
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                Object ok = com.ExecuteScalar();
            }
            catch (SqlException e)
            {
                throw (e);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (conn != null)
                    conn.Close();
            }

        }
        public List<RepImpresaCFDI> reportePdf(Documento objDocumento) 
        {
            string strOpenSSLPath = AppDomain.CurrentDomain.BaseDirectory + "Models\\DocumentosPac\\";
            string xmlPath = strOpenSSLPath + "33" + objDocumento.empresa.rfcEmp + "Resultado\\" + objDocumento.uuid + ".xml";
            string cbb = strOpenSSLPath + "33" + objDocumento.empresa.rfcEmp + "Resultado\\" + objDocumento.uuid + "_Cbb.bmp";
            string logo = strOpenSSLPath + "33" + objDocumento.empresa.rfcEmp + "Resultado\\logo" + objDocumento.codEmpresa.ToString() + ".png";
            file_name = strOpenSSLPath + "temp\\" + objDocumento.empresa.rfcEmp + DateTime.Now.ToString("yyyyMMddHHmmss")+".xml";
            
            File.Copy(xmlPath,file_name);
            List<RepImpresaCFDI> list = new List<RepImpresaCFDI>();
            List<ConceptosCFDI> listConceptos = new List<ConceptosCFDI>();
            RepImpresaCFDI repImpresaCFDI = new RepImpresaCFDI();
            Utils objUtils = new Utils();
          
            System.Xml.XmlDataDocument xmldoc = new System.Xml.XmlDataDocument();
            System.Xml.XmlNodeList xmlnode;
            int i = 0;
            string str = null;
            //FileStream fs = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
            FileStream fs = new FileStream(file_name, FileMode.Open, FileAccess.Read);
            xmldoc.Load(fs);

            repImpresaCFDI.fechaConsumo = objDocumento.fechaConsumo.ToString("yyyy'/'MM'/'dd");
            //.ToString("dd'/'MM'/'yyyy")
            //cfdi:Comprobante
            //((xmlnode[0].ChildNodes.Item(j).Attributes["cantida"]!=null) ? (xmlnode[0].ChildNodes.Item(j).Attributes["cantida"].Value :""

            xmlnode = xmldoc.GetElementsByTagName("cfdi:Comprobante");
            //repImpresaCFDI.version = (xmlnode[0].Attributes["Version"] != null) ? xmlnode[0].Attributes["Version"].Value : "";
            repImpresaCFDI.folio = (xmlnode[0].Attributes["Folio"] != null) ? xmlnode[0].Attributes["Folio"].Value : "";
            repImpresaCFDI.sello = (xmlnode[0].Attributes["Sello"] != null) ? xmlnode[0].Attributes["Sello"].Value : "";
            repImpresaCFDI.fecha = (xmlnode[0].Attributes["Fecha"] != null) ? xmlnode[0].Attributes["Fecha"].Value : "";
            repImpresaCFDI.formaDePago = (xmlnode[0].Attributes["FormaPago"] != null) ? xmlnode[0].Attributes["FormaPago"].Value : "";
            repImpresaCFDI.noCertificado = (xmlnode[0].Attributes["NoCertificado"] != null) ? xmlnode[0].Attributes["NoCertificado"].Value : "";
            repImpresaCFDI.certificado = (xmlnode[0].Attributes["Certificado"] != null) ? xmlnode[0].Attributes["Certificado"].Value : "";
            repImpresaCFDI.subTotal = (xmlnode[0].Attributes["SubTotal"] != null) ? xmlnode[0].Attributes["SubTotal"].Value : "";
            repImpresaCFDI.TipoCambio = (xmlnode[0].Attributes["TipoCambio"] != null) ? xmlnode[0].Attributes["TipoCambio"].Value : "";
            repImpresaCFDI.Moneda = (xmlnode[0].Attributes["Moneda"] != null) ? xmlnode[0].Attributes["Moneda"].Value : "";
            repImpresaCFDI.total = (xmlnode[0].Attributes["Total"] != null) ? xmlnode[0].Attributes["Total"].Value : "";
            repImpresaCFDI.tipoDeComprobante = (xmlnode[0].Attributes["TipoDeComprobante"] != null) ? xmlnode[0].Attributes["TipoDeComprobante"].Value : "";
            repImpresaCFDI.metodoDePago = (xmlnode[0].Attributes["MetodoPago"] != null) ? xmlnode[0].Attributes["MetodoPago"].Value : "";
            repImpresaCFDI.LugarExpedicion = (xmlnode[0].Attributes["LugarExpedicion"] != null) ? xmlnode[0].Attributes["LugarExpedicion"].Value : "";

            //cfdi:Emisor
            xmlnode = xmldoc.GetElementsByTagName("cfdi:Emisor");
            repImpresaCFDI.eRfc = xmlnode[0].Attributes["Rfc"].Value;
            repImpresaCFDI.eNombre = xmlnode[0].Attributes["Nombre"].Value;
            repImpresaCFDI.eRegimenFiscal = xmlnode[0].Attributes["RegimenFiscal"].Value;
            //for (int j = 0; j < xmlnode[0].ChildNodes.Count; j++)
            //{
            //    switch (xmlnode[0].ChildNodes.Item(j).Name)
            //    {
            //        case "cfdi:DomicilioFiscal":
            //            repImpresaCFDI.eDFCalle = (xmlnode[0].ChildNodes.Item(j).Attributes["calle"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["calle"].Value : "";
            //            repImpresaCFDI.eDNoExterior = (xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"].Value : "";
            //            repImpresaCFDI.eDFLocalidad = (xmlnode[0].ChildNodes.Item(j).Attributes["localidad"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["localidad"].Value : "";
            //            repImpresaCFDI.eDFMunicipio = (xmlnode[0].ChildNodes.Item(j).Attributes["municipio"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["municipio"].Value : "";
            //            repImpresaCFDI.eDFEstado = (xmlnode[0].ChildNodes.Item(j).Attributes["estado"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["municipio"].Value : "";
            //            repImpresaCFDI.eDFPais = (xmlnode[0].ChildNodes.Item(j).Attributes["pais"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["pais"].Value : "";
            //            repImpresaCFDI.eDFCodigoPostal = (xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"].Value : "";
            //            break;
            //        case "cfdi:ExpedidoEn":
            //            repImpresaCFDI.eEnCalle = (xmlnode[0].ChildNodes.Item(j).Attributes["calle"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["calle"].Value : "";
            //            repImpresaCFDI.eNoExterior = (xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"].Value : "";
            //            repImpresaCFDI.eEnColonia = (xmlnode[0].ChildNodes.Item(j).Attributes["colonia"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["colonia"].Value : "";
            //            repImpresaCFDI.eEnLocalidad = (xmlnode[0].ChildNodes.Item(j).Attributes["localidad"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["localidad"].Value : "";
            //            repImpresaCFDI.eEnMunicipio = (xmlnode[0].ChildNodes.Item(j).Attributes["municipio"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["municipio"].Value : "";
            //            repImpresaCFDI.eEnEstado = (xmlnode[0].ChildNodes.Item(j).Attributes["estado"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["estado"].Value : "";
            //            repImpresaCFDI.eEnPais = (xmlnode[0].ChildNodes.Item(j).Attributes["pais"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["pais"].Value : "Mexico";
            //            repImpresaCFDI.eEnCodigoPostal = (xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"].Value : "";
            //            break;
            //        case "cfdi:RegimenFiscal":
            //            repImpresaCFDI.rFRegimen = (xmlnode[0].ChildNodes.Item(j).Attributes["Regimen"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["Regimen"].Value : "";
            //            break;

            //    }
            //}

            //cfdi:Receptor
            xmlnode = xmldoc.GetElementsByTagName("cfdi:Receptor");
            repImpresaCFDI.rRfc = xmlnode[0].Attributes["Rfc"].Value;
            repImpresaCFDI.rNombre = xmlnode[0].Attributes["Nombre"] == null ? "" : xmlnode[0].Attributes["Nombre"].Value;
            repImpresaCFDI.rUsoCFDI = xmlnode[0].Attributes["UsoCFDI"].Value;
            //for (int j = 0; j < xmlnode[0].ChildNodes.Count; j++)
            //{
            //    switch (xmlnode[0].ChildNodes.Item(j).Name)
            //    {
            //        case "cfdi:Domicilio":
            //            repImpresaCFDI.rCalle = (xmlnode[0].ChildNodes.Item(j).Attributes["calle"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["calle"].Value : "";
            //            repImpresaCFDI.rNoExterior = (xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["noExterior"].Value : "";
            //            repImpresaCFDI.rColonia = (xmlnode[0].ChildNodes.Item(j).Attributes["colonia"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["colonia"].Value : "";
            //            repImpresaCFDI.rLocalidad = (xmlnode[0].ChildNodes.Item(j).Attributes["localidad"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["localidad"].Value : "";
            //            repImpresaCFDI.rMunicipio = (xmlnode[0].ChildNodes.Item(j).Attributes["municipio"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["municipio"].Value : "";
            //            repImpresaCFDI.rEstado = (xmlnode[0].ChildNodes.Item(j).Attributes["estado"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["estado"].Value : "";
            //            repImpresaCFDI.rPais = (xmlnode[0].ChildNodes.Item(j).Attributes["pais"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["pais"].Value : "";
            //            repImpresaCFDI.rCodigoPostal = (xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["codigoPostal"].Value : "";
            //            repImpresaCFDI.rNoInt = (xmlnode[0].ChildNodes.Item(j).Attributes["noInterior"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["noInterior"].Value : "";
            //            break;
            //    }
            //}

            //cfdi: Impuestos

            xmlnode = xmldoc.GetElementsByTagName("cfdi:Impuestos");
            //repImpresaCFDI.totalImpuestosTrasladados = xmlnode[1].Attributes["TotalImpuestosTrasladados"].Value;
            for (int j = 0; j < xmlnode.Count; j++)
            {
                if (xmlnode[j].Attributes["TotalImpuestosTrasladados"] != null)
                {
                    repImpresaCFDI.totalImpuestosTrasladados = xmlnode[j].Attributes["TotalImpuestosTrasladados"].Value;
                    repImpresaCFDI.importe = xmlnode[j].Attributes["TotalImpuestosTrasladados"].Value;
                }


                //switch (xmlnode[1].ChildNodes.Item(j).Name)
                //{
                //    case "cfdi:Traslados":
                //        repImpresaCFDI.impuesto = xmlnode[1].ChildNodes[0].ChildNodes.Item(j).Attributes["Impuesto"].Value;
                //        repImpresaCFDI.tipoFactor = xmlnode[1].ChildNodes[0].ChildNodes.Item(j).Attributes["TipoFactor"].Value;
                //        repImpresaCFDI.tasaOCuota = xmlnode[1].ChildNodes[0].ChildNodes.Item(j).Attributes["TasaOCuota"].Value;
                //        repImpresaCFDI.importe = xmlnode[1].ChildNodes[0].ChildNodes.Item(j).Attributes["Importe"].Value;
                //        break;
                //}
            }



            //cfdi:Complemento

            xmlnode = xmldoc.GetElementsByTagName("cfdi:Complemento");
            for (int j = 0; j < xmlnode[0].ChildNodes.Count; j++)
            {
                switch (xmlnode[0].ChildNodes.Item(j).Name)
                {
                    case "tfd:TimbreFiscalDigital":
                        repImpresaCFDI.selloCFD = (xmlnode[0].ChildNodes.Item(j).Attributes["SelloCFD"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["SelloCFD"].Value : "";
                        repImpresaCFDI.FechaTimbrado = (xmlnode[0].ChildNodes.Item(j).Attributes["FechaTimbrado"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["FechaTimbrado"].Value : "";
                        repImpresaCFDI.UUID = (xmlnode[0].ChildNodes.Item(j).Attributes["UUID"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["UUID"].Value : "";
                        repImpresaCFDI.noCertificadoSAT = (xmlnode[0].ChildNodes.Item(j).Attributes["NoCertificadoSAT"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["NoCertificadoSAT"].Value : "";
                        repImpresaCFDI.selloSAT = (xmlnode[0].ChildNodes.Item(j).Attributes["SelloSAT"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["SelloSAT"].Value : "";
                        repImpresaCFDI.version = (xmlnode[0].ChildNodes.Item(j).Attributes["Version"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["Version"].Value : "";
                        repImpresaCFDI.rfcProvCertif = (xmlnode[0].ChildNodes.Item(j).Attributes["RfcProvCertif"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["RfcProvCertif"].Value : "";
                        break;
                }
            }

            //cfdi:Conceptos

            xmlnode = xmldoc.GetElementsByTagName("cfdi:Conceptos");
            for (int j = 0; j < xmlnode[0].ChildNodes.Count; j++)
            {
                switch (xmlnode[0].ChildNodes.Item(j).Name)
                {
                    case "cfdi:Concepto":
                        ConceptosCFDI ConceptoCFDI = new ConceptosCFDI();
                        ConceptoCFDI.claveProdServ = (xmlnode[0].ChildNodes.Item(j).Attributes["ClaveProdServ"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["ClaveProdServ"].Value : "";
                        ConceptoCFDI.claveUnidad = (xmlnode[0].ChildNodes.Item(j).Attributes["ClaveUnidad"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["ClaveUnidad"].Value : "";
                        ConceptoCFDI.cantidad = (xmlnode[0].ChildNodes.Item(j).Attributes["Cantidad"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["Cantidad"].Value : "";
                        ConceptoCFDI.descripcion = (xmlnode[0].ChildNodes.Item(j).Attributes["Descripcion"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["Descripcion"].Value : "";
                        ConceptoCFDI.valorUnitario = (xmlnode[0].ChildNodes.Item(j).Attributes["ValorUnitario"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["ValorUnitario"].Value : "";
                        ConceptoCFDI.importe = (xmlnode[0].ChildNodes.Item(j).Attributes["Importe"] != null) ? xmlnode[0].ChildNodes.Item(j).Attributes["Importe"].Value : "";

                        System.Xml.XmlNodeList xmlnodeTraslados;
                        xmlnodeTraslados = xmldoc.GetElementsByTagName("cfdi:Traslados");
                        List<Traslado> List = new List<Traslado>();

                        for (int k = 0; k < xmlnodeTraslados[0].ChildNodes.Count; k++)
                        {
                            Traslado objTraslado = new Traslado();
                            switch (xmlnodeTraslados[0].ChildNodes.Item(k).Name)
                            {
                                case "cfdi:Traslado":
                                    objTraslado.Base = (xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Base"] != null) ? xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Base"].Value : "";
                                    objTraslado.Impuesto = (xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Impuesto"] != null) ? xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Impuesto"].Value : "";
                                    objTraslado.TipoFactor = (xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["TipoFactor"] != null) ? xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["TipoFactor"].Value : "";
                                    objTraslado.TasaOCuota = (xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["TasaOCuota"] != null) ? xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["TasaOCuota"].Value : "";
                                    objTraslado.Importe = (xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Importe"] != null) ? xmlnodeTraslados[0].ChildNodes.Item(k).Attributes["Importe"].Value : "";
                                    break;
                            }
                            List.Add(objTraslado);
                        }
                        ConceptoCFDI.traslados = List;
                        listConceptos.Add(ConceptoCFDI);
                        break;
                }
            }

            repImpresaCFDI.cadenaDelSat = "||" + repImpresaCFDI.version + "|" + repImpresaCFDI.UUID + "|" + repImpresaCFDI.FechaTimbrado + "|" + repImpresaCFDI.selloCFD + "|" + repImpresaCFDI.noCertificadoSAT + "||";
            repImpresaCFDI.conceptosCFDI = listConceptos;
            //repImpresaCFDI.cbb = cbb;
            string cbbString = "?re=" + repImpresaCFDI.eRfc + "&rr=" + repImpresaCFDI.rRfc + "&tt=" + this.cbbString(repImpresaCFDI.total.ToString()) + "&id=" + repImpresaCFDI.UUID;
            repImpresaCFDI.qrImage = objUtils.qrImage(cbbString);
            repImpresaCFDI.logo = logo;
            list.Add(repImpresaCFDI);


            return list;
        }
        public string cbbString(string cbb)
        {
            // 10 caractres para los enteros
            // 1  caracter para el punto
            // 6  caracteres para los decimales

            // declaracion de variables
            int punto = 0;
            int diferencia = 0;
            // se ubica la posición del caracter .
            punto = cbb.IndexOf(".");
            // si la cadena no contiene el caracter . el resultado será -1
            if (punto == -1)
            {
                // se agrega el caracter "." al final de la cadena
                cbb = cbb + ".";
                // se asigna la posición del caracter "." a la variable punto.
                punto = cbb.IndexOf(".");
            }
            // se asignan a la cadena las posciones faltantes ala izquierda de los enteres para completar las 10 posiciones
            for (int i = punto; i < 10; i++)
            {
                cbb = "0" + cbb;
            }
            // se obtiene la diferencia de entre la longitud de la cadena y el caracter "." 
            diferencia = cbb.Length - cbb.IndexOf(".");
            // se agregan a la cadena las posiciones faltantes a la derecha para completar las 6 pisiciones
            for (int i = diferencia; i < 7; i++)
            {
                cbb = cbb + "0";
            }
            //se retorna la cadena con longitud de 17 caracteres en total.
            //if(cbb.Length==17)
            //    true
            return cbb;
        }
        public string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
    }

}