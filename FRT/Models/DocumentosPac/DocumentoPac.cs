﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FRT.ServicePAx;
using System.Xml;
using System.IO;
using System.Data;
using System.Text;
using System.Diagnostics;
using System.Data.Odbc;
using FRT.Models.Documentos;
using System.Threading;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FRT.Models.Documentos;
using FRT.Models.Usuarios;
using FRT.Models.Admin;
using FRT.Models.DocumentosPac;
using FRT.Models.DocumentosPac.ReportesPac;
using FRT.Models.Documentos.RepDocumentos;
using FRT.Models;
using FRT.Models.Common;
using FRT.Models.Clientes;
using FRT.Models.FormasPagos;

namespace FRT.Models.DocumentosPac
{
    public class DocumentoPac
    {
    public static byte bLicencia;
    public int               NumError                        { get; set; }
    public string            estutusDocumento                { get; set; }
    public bool              enProceso                       { get; set; }
    public string            DescError                       { get; set; }
    public string            Paquete                         { get; set; }
    public string            DirectorioSDK                   { get; set; }
    public string            strXMLFilePath                  { get; set; }                        
    public string            strKeyFilePassword              { get; set; }
    public string            strOpenSSLPath                  { get; set; }
    public string            strGetCertificateCommand        { get; set; }
    public Cliente           cliente                         { get; set; }


    public Documento         documento                       { get; set; }
    public string            certificado                     { get; set; }
    public StringBuilder     lUUID                           { get; set; }
    public StcDocument       stcDocument         = new StcDocument();
    public StcReceiver       stcReceiver         = new StcReceiver();
    public List<StcConcepts> stcConcepts         = new List<StcConcepts>();
    public StcEmitter        stcEmitter          = new StcEmitter();
    public StcEmittedIn      stcEmittedIn        = new StcEmittedIn();
    DocumentosPacDAO         objDocumentosPacDAO = new DocumentosPacDAO();
    private static Object lockObj = new Object();
    private Object thisLock = new Object();
    public string dirTemp { get; set; }

    //actualizacion 3.3
    public string claveProdServ { get; set; }
    public string claveUnidad { get; set; }
    public string claveImpuesto { get; set; }
    public string tipoFactorImpuesto { get; set; }
    public decimal tasaOCuotaImpuesto { get; set; }
    public decimal totalImpuestos { get; set; }
    

        public DocumentoPac(Documento objDocumento)
        {
            
            this.Paquete = "CONTPAQ I Facturacion";
            DocumentoPac.bLicencia = 1;
            this.DirectorioSDK = "C:\\Program Files (x86)\\Compacw\\Facturacion";
            this.strOpenSSLPath = AppDomain.CurrentDomain.BaseDirectory + "Models\\DocumentosPac\\";
            this.stcDocument.strDate = DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00");
            this.stcDocument.strTime = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
            dirTemp = this.strOpenSSLPath + "temp\\";
            this.documento = objDocumento;
            this.obtenerDatosFacturaCFDI();
            this.obtenerDatosFacturaCFDIDetalle();
            this.createXMLFileCFDI33();
        }
        
        public void abrirEmpresa(bool iniciarAdminPaq)
        {
            if (iniciarAdminPaq) this.iniciarAdminPaq();
            if (NumError == 0)
                NumError = SDK.fAbreEmpresa(this.documento.empresa.rutaEmp);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                //SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }

        }
        public void iniciarAdminPaq()
        {
            
            SDK.SetCurrentDirectory(this.DirectorioSDK);
            NumError = SDK.fSetNombrePAQ(this.Paquete);

            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                //SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
        public void iniciaizarLicencia ()
        {
            NumError = SDK.fInicializaLicenseInfo(DocumentoPac.bLicencia);
            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                //SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
        }
       
        public void obtenerDatosFacturaCFDI()
        {
            this.objDocumentosPacDAO.obtenerDatosFacturaCFDI(this); 
        }
        public void obtenerDatosFacturaCFDIDetalle()
        {
            this.objDocumentosPacDAO.obtenerDatosFacturaCFDIDetalle(this);
        }
        public void cancelarCFDI()
        {
            Thread thread = Thread.CurrentThread;
            lock (thisLock)
            {
               
                    
                    this.abrirEmpresa(true);
                    this.iniciaizarLicencia();
                    this.cancelarUUID();
                    this.actualizarUUID();
                    this.cerrarEmpresa(false);
               
                
            }


        }
        public void cancelarUUID (){

            //NumError = SDK.fCancelaUUID(this.documento.uuid,
            //                            "3006",
            //                            this.strKeyFilePassword);
            NumError = SDK.fCancelaUUID("A2DC3425-503C-4FC5-923C-F0154FB6D040",
                                        "3006",
                                        "PTP110816P15");

            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                //SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            this.estutusDocumento = "Cancelado";
        }
        public void procesar()
        {
            Thread thread = Thread.CurrentThread;
            String msg = null;
            if (this.documento.hold != true && this.documento.StatusDocumento.CodStatusDoc == 1)
            {
                lock (lockObj)
                {
                    //try
                    //{
                    
                        this.abrirEmpresa(true);
                        this.iniciaizarLicencia();
                        this.timbrarXML();
                        this.actualizarUUID();
                        this.cerrarEmpresa(false);
                        this.crearPdfCFDI();
                        this.enviarCorreoCFDI();
                   
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine("ThreadProc: {0}", ex.ToString());
                    //}
                }
            }
          

        }
        public void crearPdfCFDI()
        {
            Usuario objUsuario = new Usuario();
            ReportDocument rptDoc = new ReportDocument();
            DocumentosPacDAO objDocumentosPacDAO = new DocumentosPacDAO();
            List<RepImpresaCFDI> list = new List<RepImpresaCFDI>();
            List<Documento> listD = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            ReportDocument cryRpt = new ReportDocument();
            try
            {
                this.obtenerdatosCliente();
                this.documento.cliente = this.cliente;
                this.documento.empresa = this.documento.empresa;
                string rutaPDF = this.strOpenSSLPath + this.stcEmitter.strRFC + "Resultado\\" + this.documento.uuid;
                listD = objDocumentosDAO.Listado(0, 1, this.documento.codDocumento, objUsuario, null);
                list = objDocumentosPacDAO.reportePdf(listD[0]);
                cryRpt.Load(this.strOpenSSLPath + "ReportesPac\\RepImpresaCFDI.rpt");
                cryRpt.Database.Tables["RepImpresaCFDI"].SetDataSource(list);
                cryRpt.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
                cryRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaPDF + ".pdf");
                this.documento.rutaPdf = rutaPDF + ".pdf";
                this.documento.rutaXml = rutaPDF + ".xml";



            }
            catch (SesionException ex)
            {
                ex.ToString();
            }

            catch (Exception e)
            {
                e.ToString();
            }
            finally
            {
                cryRpt.Close();
            }
        }
        public void enviarCorreoCFDI() {
            try
            {
            Correo.enviarCFDI_1(this.documento, this.documento.cliente.usuario.mailUsuario,
                      this.documento.rutaPdf,
                      this.documento.rutaXml,
                      this.dirTemp);
            }
         
            catch (Exception ex)
            {
                ex.ToString();

            }
        }
        public void crearPdfCFDI(Documento objDocumento)
        {
            Usuario objUsuario = new Usuario();
            ReportDocument rptDoc = new ReportDocument();
            DocumentosPacDAO objDocumentosPacDAO = new DocumentosPacDAO();
            List<RepImpresaCFDI> list = new List<RepImpresaCFDI>();
            List<Documento> listD = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            ReportDocument cryRpt = new ReportDocument();
            FormasPagosDAO objFormasPagosDAO = new FormasPagosDAO();
            List<FormaPago> listFP = new List<FormaPago>();

            try
            {
                this.obtenerdatosCliente();
                objDocumento.cliente = this.cliente;
                objDocumento.empresa = this.documento.empresa;
                //string rutaPDF = this.strOpenSSLPath + this.stcEmitter.strRFC + "Resultado\\" + objDocumento.uuid;
                string rutaPDF = this.strOpenSSLPath + "temp\\" + objDocumento.empresa.rfcEmp + DateTime.Now.ToString("yyyyMMddHHmmss");
                
                listD = objDocumentosDAO.Listado(0, 1, objDocumento.codDocumento, objUsuario, null);
                list = objDocumentosPacDAO.reportePdf(listD[0]);

                int v;
                if (Int32.TryParse(list[0].metodoDePago.Trim(), out v))
                {
                    listFP = objFormasPagosDAO.Listado(list[0].metodoDePago);
                    list[0].metodoDePago = listFP[0].descripcion;
                }

                cryRpt.Load(this.strOpenSSLPath + "ReportesPac\\RepImpresaCFDI.rpt");
                cryRpt.Database.Tables["RepImpresaCFDI"].SetDataSource(list);
                cryRpt.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
                cryRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaPDF+".pdf");
                this.documento.rutaPdf = rutaPDF + ".pdf";
                this.documento.rutaXml = objDocumentosPacDAO.file_name;
                objDocumento.rutaPdf = rutaPDF + ".pdf";
                objDocumento.rutaXml = objDocumentosPacDAO.file_name;
               
               

            }
            catch (SesionException ex)
            {
                ex.ToString();
            }

            catch (Exception e)
            {
                e.ToString();
            }
            finally
            {
                cryRpt.Close();
            }
        }
        public void recuperarPdfCFDI(Documento objDocumento)
        {
            
            ReportDocument rptDoc = new ReportDocument();
            DocumentosPacDAO objDocumentosPacDAO = new DocumentosPacDAO();
            List<RepImpresaCFDI> list = new List<RepImpresaCFDI>();
            List<Documento> listD = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            ReportDocument cryRpt = new ReportDocument();
            FormasPagosDAO objFormasPagosDAO = new FormasPagosDAO();
            List<FormaPago> listFP = new List<FormaPago>();

            try
            {
                
                objDocumento.empresa = this.documento.empresa;
                //string rutaPDF = this.strOpenSSLPath + this.stcEmitter.strRFC + "Resultado\\" + objDocumento.uuid;
                string rutaPDF = this.strOpenSSLPath + "temp\\" + objDocumento.empresa.rfcEmp + DateTime.Now.ToString("yyyyMMddHHmmss");

                listD = objDocumentosDAO.ListadoCFDI(0, 1, objDocumento.codDocumento, null);
                list = objDocumentosPacDAO.reportePdf(listD[0]);

                cryRpt.Load(this.strOpenSSLPath + "ReportesPac\\RepImpresaCFDI.rpt");
                cryRpt.Database.Tables["RepImpresaCFDI"].SetDataSource(list);
                cryRpt.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
                cryRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaPDF + ".pdf");
                this.documento.rutaPdf = rutaPDF + ".pdf";
                this.documento.rutaXml = objDocumentosPacDAO.file_name;
                objDocumento.rutaPdf = rutaPDF + ".pdf";
                objDocumento.rutaXml = objDocumentosPacDAO.file_name;



            }
            catch (SesionException ex)
            {
                ex.ToString();
            }

            catch (Exception e)
            {
                e.ToString();
            }
            finally
            {
                cryRpt.Close();
            }
        }
        public void createXMLFileCFDI33()
        {
            string[] strFunctionResult = new string[2];
            //[CREACIÓN DEL ARCHIVO XML]

            XmlTextWriter W = new XmlTextWriter(this.strOpenSSLPath + "33" + this.stcEmitter.strRFC + "\\" + this.documento.codDocumento.ToString() + ".xml", Encoding.UTF8);
            try
            {
                //[INICIO DEL DOCUMENTO]
                W.WriteStartDocument();

                //[ENCABEZADO]
                W.WriteStartElement("cfdi:Comprobante");
                W.WriteStartAttribute("xmlns:cfdi");
                W.WriteValue("http://www.sat.gob.mx/cfd/3");
                W.WriteEndAttribute();

                W.WriteStartAttribute("xmlns:xsi");
                W.WriteValue("http://www.w3.org/2001/XMLSchema-instance");
                W.WriteEndAttribute();
                //1 se realiza el cambio version 3.3 en url
                W.WriteStartAttribute("xsi:schemaLocation");
                W.WriteValue("http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd");
                W.WriteEndAttribute();
                //2 cambio 3.3
                W.WriteStartAttribute("Version");
                W.WriteValue("3.3");
                W.WriteEndAttribute();
                
                //3 forma pago a la clave correspondiente 01/04 etc
                if (string.IsNullOrEmpty(this.stcDocument.strPaymentForm))
                {
                    this.stcDocument.strPaymentForm = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strPaymentForm.Trim()))
                {
                    W.WriteStartAttribute("FormaPago");
                    W.WriteValue(this.stcDocument.strPaymentForm.Trim());
                    W.WriteEndAttribute();
                }
                //4 Moneda
                if (string.IsNullOrEmpty(this.stcDocument.strTipoCambio.ToString()))
                {
                    W.WriteStartAttribute("TipoCambio");
                    W.WriteValue("1");
                    W.WriteEndAttribute();
                }
                else if (this.stcDocument.strTipoCambio != 0)
                {
                    W.WriteStartAttribute("TipoCambio");
                    W.WriteValue(this.stcDocument.strTipoCambio);
                    W.WriteEndAttribute();

                    if (string.IsNullOrEmpty(this.stcDocument.strMoneda))
                    {
                        W.WriteStartAttribute("Moneda");
                        W.WriteValue("MXN");
                        W.WriteEndAttribute();
                    }
                    else if (!string.IsNullOrEmpty(this.stcDocument.strMoneda.Trim()))
                    {
                        W.WriteStartAttribute("Moneda");
                        W.WriteValue(this.stcDocument.strMoneda.Trim());
                        W.WriteEndAttribute();
                    }

                }
                //5 metodo de pago "PUE"
                if (string.IsNullOrEmpty(this.stcDocument.strPaymentMethod))
                {
                    this.stcDocument.strPaymentMethod = "PUE";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strPaymentMethod.Trim()))
                {
                    W.WriteStartAttribute("MetodoPago");
                    W.WriteValue(this.stcDocument.strPaymentMethod);
                    W.WriteEndAttribute();
                }

                // 6 tipo comprobante
                if (string.IsNullOrEmpty(this.stcDocument.strVoucherType))
                {
                    this.stcDocument.strVoucherType = "I";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strVoucherType.Trim()))
                {
                    W.WriteStartAttribute("TipoDeComprobante");
                    W.WriteValue(this.stcDocument.strVoucherType);
                    W.WriteEndAttribute();
                }
                //7 Lugar de expedicion
                W.WriteStartAttribute("LugarExpedicion");
                W.WriteValue(this.stcEmitter.strPostalCode);
                W.WriteEndAttribute();
               
                W.WriteStartAttribute("Folio");
                W.WriteValue(this.stcDocument.strFolio.Trim());
                W.WriteEndAttribute();

                if (string.IsNullOrEmpty(this.stcDocument.strDate))
                {
                    this.stcDocument.strDate = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strDate.Trim()))
                {
                    W.WriteStartAttribute("Fecha");
                    W.WriteValue(this.stcDocument.strDate + "T" + this.stcDocument.strTime);
                    W.WriteEndAttribute();
                }
                W.WriteStartAttribute("Sello");
                W.WriteValue("");
                W.WriteEndAttribute();

                //W.WriteStartAttribute("NoCertificado");
                //W.WriteValue("");
                //W.WriteEndAttribute();
                
                //noCertificado
                if (string.IsNullOrEmpty(this.stcDocument.noCertificado))
                {
                    this.stcDocument.noCertificado = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.noCertificado.Trim()))
                {
                    W.WriteStartAttribute("NoCertificado");
                    W.WriteValue(this.stcDocument.noCertificado.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteStartAttribute("Certificado");
                W.WriteValue("");
                W.WriteEndAttribute();

                if (string.IsNullOrEmpty(this.stcDocument.dmlSubtotal.ToString("#0.00")))
                {
                    this.stcDocument.dmlSubtotal = 0;
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.dmlSubtotal.ToString("#0.00").Trim()))
                {
                    W.WriteStartAttribute("SubTotal");
                    W.WriteValue(this.stcDocument.dmlSubtotal.ToString("#0.00"));
                    W.WriteEndAttribute();
                }


                if (string.IsNullOrEmpty(this.stcDocument.dmlTotal.ToString("#0.00")))
                {
                    this.stcDocument.dmlTotal = 0;
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.dmlTotal.ToString("#0.00").Trim()))
                {
                    W.WriteStartAttribute("Total");
                    W.WriteValue(this.stcDocument.dmlTotal.ToString("#0.00"));
                    W.WriteEndAttribute();
                }

                //W.WriteEndAttribute();

                // verificar si el nodo del numero de cuenta aplica para 3.3 ya no es vigente

               

                //[EMISOR]
                W.WriteStartElement("cfdi:Emisor");


                // 8 cambio regimen fiscal 
                if (string.IsNullOrEmpty(this.stcEmitter.strRegimenFiscal))
                {
                    this.stcEmitter.strRegimenFiscal = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strRegimenFiscal.Trim()))
                {
                    W.WriteStartAttribute("RegimenFiscal");
                    W.WriteValue(this.stcEmitter.strRegimenFiscal.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strRFC))
                {
                    this.stcEmitter.strRFC = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strRFC.Trim()))
                {
                    W.WriteStartAttribute("Rfc");
                    W.WriteValue(this.stcEmitter.strRFC.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strName))
                {
                    this.stcEmitter.strName = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strName.Trim()))
                {
                    W.WriteStartAttribute("Nombre");
                    W.WriteValue(this.stcEmitter.strName.Trim());
                    W.WriteEndAttribute();
                }

                //9 se elimina DomicilioFiscal y expedido en             


                W.WriteEndElement();
                //Fin de Emisor


                //[RECEPTOR]
                W.WriteStartElement("cfdi:Receptor");
                //10 se agrega el usoCFDI

                if (string.IsNullOrEmpty(this.documento.usoCFDI.codigo))
                {
                    this.documento.usoCFDI.codigo = "P01";
                }
                else if (!string.IsNullOrEmpty(this.documento.usoCFDI.codigo.Trim()))
                {
                    W.WriteStartAttribute("UsoCFDI");
                    W.WriteValue(this.documento.usoCFDI.codigo.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strRFC))
                {
                    this.stcReceiver.strRFC = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strRFC.Trim()))
                {
                    W.WriteStartAttribute("Rfc");
                    W.WriteValue(this.stcReceiver.strRFC.Trim());
                    W.WriteEndAttribute();
                }


                if (string.IsNullOrEmpty(this.stcReceiver.strName))
                {
                    this.stcReceiver.strName = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strName.Trim()))
                {
                    W.WriteStartAttribute("Nombre");
                    W.WriteValue(this.stcReceiver.strName.Trim());
                    W.WriteEndAttribute();
                }
                //11 se elimina domicilio del receptor
                W.WriteEndElement();
                //Fin de Receptor

                //[CONCEPTOS]
                W.WriteStartElement("cfdi:Conceptos");

                foreach (StcConcepts stcSingleConcept in this.stcConcepts)
                {
                    if (stcSingleConcept.intQuantity > 0)
                    {
                        //strFunctionResult(0) = 1;
                        W.WriteStartElement("cfdi:Concepto");

                     //12 ClaveProdServ
                        if (string.IsNullOrEmpty(stcSingleConcept.claveProdServ))
                        {
                            W.WriteStartAttribute("ClaveProdServ");
                            W.WriteValue("90101501");
                            W.WriteEndAttribute();
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.claveProdServ.Trim()))
                        {
                            W.WriteStartAttribute("ClaveProdServ");
                            W.WriteValue(stcSingleConcept.claveProdServ.Trim());
                            W.WriteEndAttribute();
                        }
                    //13 clave unidad
                        if (string.IsNullOrEmpty(stcSingleConcept.claveUnidad))
                        {
                            W.WriteStartAttribute("ClaveUnidad");
                            W.WriteValue("E48");
                            W.WriteEndAttribute();
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.claveUnidad.Trim()))
                        {
                            W.WriteStartAttribute("ClaveUnidad");
                            W.WriteValue(stcSingleConcept.claveUnidad.Trim());
                            W.WriteEndAttribute();
                        }
                    // 14 se elimina clave unidad
                        
                        if (string.IsNullOrEmpty(stcSingleConcept.intQuantity.ToString()))
                        {
                            stcSingleConcept.intQuantity = 1;
                        }
                        else if (stcSingleConcept.intQuantity != 0)
                        {
                            W.WriteStartAttribute("Cantidad");
                            W.WriteValue(stcSingleConcept.intQuantity.ToString("#0.00"));
                            W.WriteEndAttribute();
                        }

                        

                        if (string.IsNullOrEmpty(stcSingleConcept.strDescription))
                        {
                            stcSingleConcept.strDescription = "";
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.strDescription.Trim()))
                        {
                            W.WriteStartAttribute("Descripcion");
                            W.WriteValue(stcSingleConcept.strDescription.Trim());
                            W.WriteEndAttribute();
                        }

                       

                        if (string.IsNullOrEmpty(stcSingleConcept.dmlPrice.ToString()))
                        {
                            stcSingleConcept.dmlPrice = 0;
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.dmlPrice.ToString("#0.00").Trim()))
                        {
                            W.WriteStartAttribute("ValorUnitario");
                            W.WriteValue(stcSingleConcept.dmlPrice.ToString("#0.00"));
                            W.WriteEndAttribute();
                        }

                      
                        if (string.IsNullOrEmpty(stcSingleConcept.dmlTotal.ToString()))
                        {
                            stcSingleConcept.dmlTotal = 0;
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.dmlTotal.ToString("#0.00").Trim()))
                        {
                            W.WriteStartAttribute("Importe");
                            W.WriteValue(stcSingleConcept.dmlTotal.ToString("#0.00"));
                            W.WriteEndAttribute();
                        }

                        //[IMPUESTOS] por concepto
            //            <cfdi:Impuestos>
            //    <cfdi:Traslados>
            //        <cfdi:Traslado 
            //            Base="100.00" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="16.00"/>
            //        </cfdi:Traslados>
            //</cfdi:Impuestos>
                        
                W.WriteStartElement("cfdi:Impuestos");
                    W.WriteStartElement("cfdi:Traslados");
                        W.WriteStartElement("cfdi:Traslado");
                        // Base
                            if (string.IsNullOrEmpty(stcSingleConcept.baseImpuesto.ToString()))
                            {
                                stcSingleConcept.baseImpuesto = (decimal)0.00;
                            }
                            else if (!string.IsNullOrEmpty(stcSingleConcept.baseImpuesto.ToString("#0.00").Trim()))
                            {
                                W.WriteStartAttribute("Base");
                                W.WriteValue(stcSingleConcept.baseImpuesto.ToString("#0.00"));
                                W.WriteEndAttribute();
                            }
                            // Impuesto
                            if (string.IsNullOrEmpty(stcSingleConcept.claveImpuesto))
                            {
                                stcSingleConcept.claveImpuesto = "002";
                            }
                            else if (!string.IsNullOrEmpty(stcSingleConcept.claveImpuesto))
                            {
                                W.WriteStartAttribute("Impuesto");
                                W.WriteValue(stcSingleConcept.claveImpuesto);
                                W.WriteEndAttribute();
                            }
                            // TipoFactor
                            if (string.IsNullOrEmpty(stcSingleConcept.tipoFactor))
                            {
                                stcSingleConcept.tipoFactor = "Tasa";
                            }
                            else if (!string.IsNullOrEmpty(stcSingleConcept.tipoFactor))
                            {
                                W.WriteStartAttribute("TipoFactor");
                                W.WriteValue(stcSingleConcept.tipoFactor);
                                W.WriteEndAttribute();
                            }

                            // TasaOCuota
                            if (string.IsNullOrEmpty(stcSingleConcept.tasaOCuota.ToString()))
                            {
                                stcSingleConcept.tasaOCuota = (decimal)0.160000;
                            }
                            else if (!string.IsNullOrEmpty(stcSingleConcept.tasaOCuota.ToString("#0.000000").Trim()))
                            {
                                W.WriteStartAttribute("TasaOCuota");
                                W.WriteValue(stcSingleConcept.tasaOCuota.ToString("#0.000000"));
                                W.WriteEndAttribute();
                            }
                            // Importe
                            if (string.IsNullOrEmpty(stcSingleConcept.importeImpuesto.ToString()))
                            {
                                stcSingleConcept.importeImpuesto = 0;
                            }
                            else if (!string.IsNullOrEmpty(stcSingleConcept.importeImpuesto.ToString("#0.00").Trim()))
                            {
                                W.WriteStartAttribute("Importe");
                                W.WriteValue(stcSingleConcept.importeImpuesto.ToString("#0.00"));
                                W.WriteEndAttribute();
                            }
  

                        //fin nodo traslado
                        W.WriteEndElement();
                    //fin nodo traslados
                    W.WriteEndElement();
                //fin de impuestos por concepto
                W.WriteEndElement();
                        //Fin de Concepto
                        W.WriteEndElement();
                        
                    }
                }
                W.WriteEndElement();
                //Fin de Conceptos

                //[IMPUESTOS]
                W.WriteStartElement("cfdi:Impuestos");
                    W.WriteStartAttribute("TotalImpuestosTrasladados");
                    W.WriteValue(this.stcDocument.dmlIVATax.ToString("#0.00").Trim());
                            W.WriteEndAttribute();
                                W.WriteStartElement("cfdi:Traslados");
                                    W.WriteStartElement("cfdi:Traslado");

                                    // Impuesto

                                    if (string.IsNullOrEmpty( this.claveImpuesto))
                                    {
                                        this.claveImpuesto = "002";
                                    }
                                    else if (!string.IsNullOrEmpty(this.claveImpuesto))
                                    {
                                        W.WriteStartAttribute("Impuesto");
                                        W.WriteValue(this.claveImpuesto);
                                        W.WriteEndAttribute();
                                    }
                                    // TipoFactor
                                    if (string.IsNullOrEmpty(this.tipoFactorImpuesto))
                                    {
                                        this.tipoFactorImpuesto = "Tasa";
                                    }
                                    else if (!string.IsNullOrEmpty(this.tipoFactorImpuesto))
                                    {
                                        W.WriteStartAttribute("TipoFactor");
                                        W.WriteValue(this.tipoFactorImpuesto);
                                        W.WriteEndAttribute();
                                    }

                                    // TasaOCuota
                                    if (string.IsNullOrEmpty(this.tasaOCuotaImpuesto.ToString()))
                                    {
                                        this.tasaOCuotaImpuesto = (decimal)0.160000;
                                    }
                                    else if (!string.IsNullOrEmpty(this.tasaOCuotaImpuesto.ToString("#0.000000").Trim()))
                                    {
                                        W.WriteStartAttribute("TasaOCuota");
                                        W.WriteValue(this.tasaOCuotaImpuesto.ToString("#0.000000"));
                                        W.WriteEndAttribute();
                                    }
                                    // Importe
                                   
                                    W.WriteStartAttribute("Importe");
                                    W.WriteValue(this.totalImpuestos.ToString("#0.00"));
                                    W.WriteEndAttribute();
                                    

                W.WriteEndElement();
                //Fin de traslado
                W.WriteEndElement();
                //Fin de traslados
                W.WriteEndElement();
                //Fin de Impuestos
                //[FIN DEL XML]
                W.WriteEndDocument();
                //Fin de Comprobante

            }
            catch (Exception ex)
            {
                //strFunctionResult(0) = -1;
                //strFunctionResult(1) = ex.Message;
            }
            finally
            {
                W.Flush();
                W.Close();
            }
            //return strFunctionResult;
        }
        public void createXMLFileCFDI()
        {
            string[] strFunctionResult = new string[2];
            //[CREACIÓN DEL ARCHIVO XML]

            XmlTextWriter W = new XmlTextWriter(this.strOpenSSLPath + this.stcEmitter.strRFC + "\\" + this.documento.codDocumento.ToString() + ".xml", Encoding.UTF8);
            try
            {
                //[INICIO DEL DOCUMENTO]
                W.WriteStartDocument();

                //[ENCABEZADO]
                W.WriteStartElement("cfdi:Comprobante");
                W.WriteStartAttribute("xmlns:cfdi");
                W.WriteValue("http://www.sat.gob.mx/cfd/3");
                W.WriteEndAttribute();

                W.WriteStartAttribute("xmlns:xsi");
                W.WriteValue("http://www.w3.org/2001/XMLSchema-instance");
                W.WriteEndAttribute();

                W.WriteStartAttribute("xsi:schemaLocation");
                W.WriteValue("http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
                W.WriteEndAttribute();

                W.WriteStartAttribute("version");
                W.WriteValue("3.2");
                W.WriteEndAttribute();

                //If Not String.IsNullOrEmpty(this.stcDocument.strSerie) Then
                //    W.WriteStartAttribute("serie")
                //    W.WriteValue(this.stcDocument.strSerie.Trim())
                //    W.WriteEndAttribute()
                //End If


                W.WriteStartAttribute("folio");
                W.WriteValue(this.stcDocument.strFolio.Trim());
                W.WriteEndAttribute();


                //W.WriteStartAttribute("fecha")
                //W.WriteValue(this.stcDocument.strDate & "T" & this.stcDocument.strTime)
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcDocument.strDate))
                {
                    this.stcDocument.strDate = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strDate.Trim()))
                {
                    W.WriteStartAttribute("fecha");
                    W.WriteValue(this.stcDocument.strDate + "T" + this.stcDocument.strTime);
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("sello")
                //W.WriteValue(this.stcDocument.strDigitalSeal.Trim())
                //W.WriteEndAttribute()

                // if (string.IsNullOrEmpty(this.stcDocument.strDigitalSeal)) {
                W.WriteStartAttribute("sello");
                W.WriteValue("");
                W.WriteEndAttribute();
                //this.stcDocument.strDigitalSeal = "";
                // }
                //else if (!string.IsNullOrEmpty(this.stcDocument.strDigitalSeal.Trim()))
                //{
                //    W.WriteStartAttribute("sello");
                //    W.WriteValue(this.stcDocument.strDigitalSeal.Trim());
                //    W.WriteEndAttribute();
                //}

                //W.WriteStartAttribute("noAprobacion")
                //W.WriteValue(this.stcDocument.strApprovalNumber.Trim())
                //W.WriteEndAttribute()

                //W.WriteStartAttribute("anoAprobacion")
                //W.WriteValue(this.stcDocument.strApprovalYear.Trim())
                //W.WriteEndAttribute()

                //W.WriteStartAttribute("formaDePago")
                //W.WriteValue(this.stcDocument.strPaymentForm.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcDocument.strPaymentForm))
                {
                    this.stcDocument.strPaymentForm = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strPaymentForm.Trim()))
                {
                    W.WriteStartAttribute("formaDePago");
                    W.WriteValue(this.stcDocument.strPaymentForm.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("noCertificado")
                //W.WriteValue(this.stcDocument.strCertificateNumber.Trim())
                //W.WriteEndAttribute()

                //if (string.IsNullOrEmpty(this.stcDocument.strCertificateNumber)) {
                //    this.stcDocument.strCertificateNumber = "";
                //} else if (!string.IsNullOrEmpty(this.stcDocument.strCertificateNumber.Trim())) {
                W.WriteStartAttribute("noCertificado");
                W.WriteValue("");
                W.WriteEndAttribute();
                //}

                //if (string.IsNullOrEmpty(this.stcDocument.strCertificate)) {
                W.WriteStartAttribute("certificado");
                W.WriteValue("");
                W.WriteEndAttribute();
                // this.stcDocument.strCertificate = "";
                //}
                //else if (!string.IsNullOrEmpty(this.stcDocument.strCertificate.Trim()))
                //{
                //    W.WriteStartAttribute("certificado");
                //    W.WriteValue(this.stcDocument.strCertificate.Trim());
                //    W.WriteEndAttribute();
                //}

                if (string.IsNullOrEmpty(this.stcDocument.strPaymentConditions))
                {
                    this.stcDocument.strPaymentConditions = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strPaymentConditions.Trim()))
                {
                    W.WriteStartAttribute("condicionesDePago");
                    W.WriteValue(this.stcDocument.strPaymentConditions.Trim());

                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("subTotal")
                //W.WriteValue(Format(this.stcDocument.dmlSubtotal.ToString("#0.00"))
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcDocument.dmlSubtotal.ToString("#0.00")))
                {
                    this.stcDocument.dmlSubtotal = 0;
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.dmlSubtotal.ToString("#0.00").Trim()))
                {
                    W.WriteStartAttribute("subTotal");
                    W.WriteValue(this.stcDocument.dmlSubtotal.ToString("#0.00"));
                    W.WriteEndAttribute();
                }

                if (this.stcDocument.dmlDiscount > 0)
                {
                    W.WriteStartAttribute("descuento");
                    W.WriteValue(this.stcDocument.dmlDiscount.ToString("#0.00"));
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcDocument.strDiscountMotive))
                {
                    this.stcDocument.strDiscountMotive = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strDiscountMotive.Trim()))
                {
                    W.WriteStartAttribute("motivoDescuento");
                    W.WriteValue(this.stcDocument.strDiscountMotive.Trim());
                    W.WriteEndAttribute();
                }
                //TipoCambio

                if (string.IsNullOrEmpty(this.stcDocument.strTipoCambio.ToString()))
                {
                    W.WriteStartAttribute("TipoCambio");
                    W.WriteValue("1");
                    W.WriteEndAttribute();
                }
                else if (this.stcDocument.strTipoCambio != 0)
                {
                    W.WriteStartAttribute("TipoCambio");
                    W.WriteValue(this.stcDocument.strTipoCambio);
                    W.WriteEndAttribute();

                    if (string.IsNullOrEmpty(this.stcDocument.strMoneda))
                    {
                        W.WriteStartAttribute("Moneda");
                        W.WriteValue("Peso Mexicano");
                        W.WriteEndAttribute();
                    }
                    else if (!string.IsNullOrEmpty(this.stcDocument.strMoneda.Trim()))
                    {
                        W.WriteStartAttribute("Moneda");
                        W.WriteValue(this.stcDocument.strMoneda.Trim());
                        W.WriteEndAttribute();
                    }

                }

                //Moneda


                //W.WriteStartAttribute("total")
                //W.WriteValue(Format(this.stcDocument.dmlTotal.ToString("#0.00"))
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcDocument.dmlTotal.ToString("#0.00")))
                {
                    this.stcDocument.dmlTotal = 0;
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.dmlTotal.ToString("#0.00").Trim()))
                {
                    W.WriteStartAttribute("total");
                    W.WriteValue(this.stcDocument.dmlTotal.ToString("#0.00"));
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcDocument.strPaymentMethod))
                {
                    this.stcDocument.strPaymentMethod = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strPaymentMethod.Trim()))
                {
                    W.WriteStartAttribute("metodoDePago");
                    W.WriteValue(this.stcDocument.strPaymentMethod);
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("tipoDeComprobante")
                //W.WriteValue(this.stcDocument.strVoucherType.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcDocument.strVoucherType))
                {
                    this.stcDocument.strVoucherType = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strVoucherType.Trim()))
                {
                    W.WriteStartAttribute("tipoDeComprobante");
                    W.WriteValue(this.stcDocument.strVoucherType);
                    W.WriteEndAttribute();
                }

                string LugarExpedicion = (this.stcEmittedIn.strStreet.ToString().Trim() + " " + this.stcEmittedIn.strOutsideNumber.ToString().Trim() + " " + this.stcEmittedIn.strInsideNumber.ToString().Trim() + ", " + this.stcEmittedIn.strPostalCode.ToString().Trim() + ", " + this.stcEmittedIn.strTown.ToString().Trim() + ", " + this.stcEmittedIn.strState.ToString().Trim() + ", " + this.stcEmittedIn.strCountry.ToString().Trim());
                W.WriteStartAttribute("LugarExpedicion");
                LugarExpedicion = LugarExpedicion.Replace(" , ", ", ");
                //LugarExpedicion = Strings.Replace(LugarExpedicion, " , ", ", ");
                W.WriteValue(LugarExpedicion);

                //W.WriteValue(this.stcEmittedIn.strStreet.ToString().Trim() _
                //             + " " + this.stcEmittedIn.strOutsideNumber.ToString().Trim() _
                //             + " " + this.stcEmittedIn.strInsideNumber.ToString().Trim() _
                //             + ", " + this.stcEmittedIn.strPostalCode.ToString().Trim() _
                //             + ", " + this.stcEmittedIn.strTown.ToString().Trim() _
                //             + ", " + this.stcEmittedIn.strState.ToString().Trim() _
                //             + ", " + this.stcEmittedIn.strCountry.ToString().Trim())
                W.WriteEndAttribute();


                if (string.IsNullOrEmpty(this.stcDocument.strNumCtaPago))
                {
                    this.stcDocument.strNumCtaPago = "";
                }
                else if (!string.IsNullOrEmpty(this.stcDocument.strNumCtaPago.Trim()))
                {
                    W.WriteStartAttribute("NumCtaPago");
                    W.WriteValue(this.stcDocument.strNumCtaPago);
                    W.WriteEndAttribute();
                }

                //[EMISOR]
                W.WriteStartElement("cfdi:Emisor");

                //W.WriteStartAttribute("rfc")
                //W.WriteValue(this.stcEmitter.strRFC.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strRFC))
                {
                    this.stcEmitter.strRFC = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strRFC.Trim()))
                {
                    W.WriteStartAttribute("rfc");
                    W.WriteValue(this.stcEmitter.strRFC.Trim());
                    W.WriteEndAttribute();
                }
                //W.WriteStartAttribute("nombre")
                //W.WriteValue(this.stcEmitter.strName.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strName))
                {
                    this.stcEmitter.strName = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strName.Trim()))
                {
                    W.WriteStartAttribute("nombre");
                    W.WriteValue(this.stcEmitter.strName.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteStartElement("cfdi:DomicilioFiscal");

                //W.WriteStartAttribute("calle")
                //W.WriteValue(this.stcEmitter.strStreet.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strStreet))
                {
                    this.stcEmitter.strStreet = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strName.Trim()))
                {
                    W.WriteStartAttribute("calle");
                    W.WriteValue(this.stcEmitter.strStreet.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strOutsideNumber))
                {
                    this.stcEmitter.strOutsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strOutsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noExterior");
                    W.WriteValue(this.stcEmitter.strOutsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strInsideNumber))
                {
                    this.stcEmitter.strInsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strInsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noInterior");
                    W.WriteValue(this.stcEmitter.strInsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strColony))
                {
                    this.stcEmitter.strColony = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strColony.Trim()))
                {
                    W.WriteStartAttribute("colonia");
                    W.WriteValue(this.stcEmitter.strColony.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strLocation))
                {
                    this.stcEmitter.strLocation = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strLocation.Trim()))
                {
                    W.WriteStartAttribute("localidad");
                    W.WriteValue(this.stcEmitter.strLocation.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmitter.strReference))
                {
                    this.stcEmitter.strReference = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strReference.Trim()))
                {
                    W.WriteStartAttribute("referencia");
                    W.WriteValue(this.stcEmitter.strReference.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("municipio")
                //W.WriteValue(this.stcEmitter.strTown.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strTown))
                {
                    this.stcEmitter.strTown = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strTown.Trim()))
                {
                    W.WriteStartAttribute("municipio");
                    W.WriteValue(this.stcEmitter.strTown.Trim());
                    W.WriteEndAttribute();

                }

                //W.WriteStartAttribute("estado")
                //W.WriteValue(this.stcEmitter.strState.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strState))
                {
                    this.stcEmitter.strState = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strState.Trim()))
                {
                    W.WriteStartAttribute("estado");
                    W.WriteValue(this.stcEmitter.strState.Trim());
                    W.WriteEndAttribute();

                }

                //W.WriteStartAttribute("pais")
                //W.WriteValue(this.stcEmitter.strCountry.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strCountry))
                {
                    this.stcEmitter.strCountry = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strCountry.Trim()))
                {
                    W.WriteStartAttribute("pais");
                    W.WriteValue(this.stcEmitter.strCountry.Trim());
                    W.WriteEndAttribute();

                }

                //W.WriteStartAttribute("codigoPostal")
                //W.WriteValue(this.stcEmitter.strPostalCode.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmitter.strPostalCode))
                {
                    this.stcEmitter.strPostalCode = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmitter.strPostalCode.Trim()))
                {
                    W.WriteStartAttribute("codigoPostal");
                    W.WriteValue(this.stcEmitter.strPostalCode.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteEndElement();
                //fin de DomicilioFiscal

                //Expedido En
                W.WriteStartElement("cfdi:ExpedidoEn");

                //W.WriteStartAttribute("calle")
                //W.WriteValue(this.stcEmittedIn.strStreet.Trim())
                //W.WriteEndAttribute()
                if (string.IsNullOrEmpty(this.stcEmittedIn.strStreet))
                {
                    this.stcEmittedIn.strStreet = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strStreet.Trim()))
                {
                    W.WriteStartAttribute("calle");
                    W.WriteValue(this.stcEmittedIn.strStreet.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmittedIn.strOutsideNumber))
                {
                    this.stcEmittedIn.strOutsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strOutsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noExterior");
                    W.WriteValue(this.stcEmittedIn.strOutsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmittedIn.strInsideNumber))
                {
                    this.stcEmittedIn.strInsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strInsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noInterior");
                    W.WriteValue(this.stcEmittedIn.strInsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmittedIn.strColony))
                {
                    this.stcEmittedIn.strColony = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strColony.Trim()))
                {
                    W.WriteStartAttribute("colonia");
                    W.WriteValue(this.stcEmittedIn.strColony.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmittedIn.strLocation))
                {
                    this.stcEmittedIn.strLocation = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strLocation.Trim()))
                {
                    W.WriteStartAttribute("localidad");
                    W.WriteValue(this.stcEmittedIn.strLocation.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcEmittedIn.strReference))
                {
                    this.stcEmittedIn.strReference = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strReference.Trim()))
                {
                    W.WriteStartAttribute("referencia");
                    W.WriteValue(this.stcEmittedIn.strReference.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("municipio")
                //W.WriteValue(this.stcEmittedIn.strTown.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmittedIn.strTown))
                {
                    this.stcEmittedIn.strTown = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strTown.Trim()))
                {
                    W.WriteStartAttribute("municipio");
                    W.WriteValue(this.stcEmittedIn.strTown.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("estado")
                //W.WriteValue(this.stcEmittedIn.strState.Trim())
                //W.WriteEndAttribute()
                if (string.IsNullOrEmpty(this.stcEmittedIn.strState))
                {
                    this.stcEmittedIn.strState = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strState.Trim()))
                {
                    W.WriteStartAttribute("estado");
                    W.WriteValue(this.stcEmittedIn.strState.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("pais")
                //W.WriteValue(this.stcEmittedIn.strCountry.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmittedIn.strCountry))
                {
                    this.stcEmittedIn.strCountry = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strCountry.Trim()))
                {
                    W.WriteStartAttribute("pais");
                    W.WriteValue(this.stcEmittedIn.strCountry.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("codigoPostal")
                //W.WriteValue(this.stcEmittedIn.strPostalCode.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcEmittedIn.strPostalCode))
                {
                    this.stcEmittedIn.strPostalCode = "";
                }
                else if (!string.IsNullOrEmpty(this.stcEmittedIn.strPostalCode.Trim()))
                {
                    W.WriteStartAttribute("codigoPostal");
                    W.WriteValue(this.stcEmittedIn.strPostalCode.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteEndElement();
                //Fin de ExpedidoEn

                W.WriteStartElement("cfdi:RegimenFiscal");
                W.WriteStartAttribute("Regimen");
                W.WriteValue(this.stcEmitter.strRegimenFiscal.Trim());
                W.WriteEndAttribute();
                W.WriteEndElement();
                // Fin de RegimenFiscal

                W.WriteEndElement();
                //Fin de Emisor


                //[RECEPTOR]
                W.WriteStartElement("cfdi:Receptor");

                //W.WriteStartAttribute("rfc")
                //W.WriteValue(this.stcReceiver.strRFC.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcReceiver.strRFC))
                {
                    this.stcReceiver.strRFC = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strRFC.Trim()))
                {
                    W.WriteStartAttribute("rfc");
                    W.WriteValue(this.stcReceiver.strRFC.Trim());
                    W.WriteEndAttribute();
                }


                if (string.IsNullOrEmpty(this.stcReceiver.strName))
                {
                    this.stcReceiver.strName = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strName.Trim()))
                {
                    W.WriteStartAttribute("nombre");
                    W.WriteValue(this.stcReceiver.strName.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteStartElement("cfdi:Domicilio");

                if (string.IsNullOrEmpty(this.stcReceiver.strStreet))
                {
                    this.stcReceiver.strStreet = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strStreet.Trim()))
                {
                    W.WriteStartAttribute("calle");
                    W.WriteValue(this.stcReceiver.strStreet.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strOutsideNumber))
                {
                    this.stcReceiver.strOutsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strOutsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noExterior");
                    W.WriteValue(this.stcReceiver.strOutsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strInsideNumber))
                {
                    this.stcReceiver.strInsideNumber = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strInsideNumber.Trim()))
                {
                    W.WriteStartAttribute("noInterior");
                    W.WriteValue(this.stcReceiver.strInsideNumber.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strColony))
                {
                    this.stcReceiver.strColony = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strColony.Trim()))
                {
                    W.WriteStartAttribute("colonia");
                    W.WriteValue(this.stcReceiver.strColony.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strLocation))
                {
                    this.stcReceiver.strLocation = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strLocation.Trim()))
                {
                    W.WriteStartAttribute("localidad");
                    W.WriteValue(this.stcReceiver.strLocation.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strReference))
                {
                    this.stcReceiver.strReference = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strReference.Trim()))
                {
                    W.WriteStartAttribute("referencia");
                    W.WriteValue(this.stcReceiver.strReference.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strTown))
                {
                    this.stcReceiver.strTown = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strTown.Trim()))
                {
                    W.WriteStartAttribute("municipio");
                    W.WriteValue(this.stcReceiver.strTown.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strState))
                {
                    this.stcReceiver.strState = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strState.Trim()))
                {
                    W.WriteStartAttribute("estado");
                    W.WriteValue(this.stcReceiver.strState.Trim());
                    W.WriteEndAttribute();
                }

                //W.WriteStartAttribute("pais")
                //W.WriteValue(this.stcReceiver.strCountry.Trim())
                //W.WriteEndAttribute()

                if (string.IsNullOrEmpty(this.stcReceiver.strCountry))
                {
                    this.stcReceiver.strCountry = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strCountry.Trim()))
                {
                    W.WriteStartAttribute("pais");
                    W.WriteValue(this.stcReceiver.strCountry.Trim());
                    W.WriteEndAttribute();
                }

                if (string.IsNullOrEmpty(this.stcReceiver.strPostalCode))
                {
                    this.stcReceiver.strPostalCode = "";
                }
                else if (!string.IsNullOrEmpty(this.stcReceiver.strPostalCode.Trim()))
                {
                    W.WriteStartAttribute("codigoPostal");
                    W.WriteValue(this.stcReceiver.strPostalCode.Trim());
                    W.WriteEndAttribute();
                }

                W.WriteEndElement();
                //Fin de domicilio

                W.WriteEndElement();
                //Fin de Receptor

                //[CONCEPTOS]
                W.WriteStartElement("cfdi:Conceptos");

                foreach (StcConcepts stcSingleConcept in this.stcConcepts)
                {
                    if (stcSingleConcept.intQuantity > 0)
                    {
                        //strFunctionResult(0) = 1;
                        W.WriteStartElement("cfdi:Concepto");

                        //W.WriteStartAttribute("cantidad")
                        //W.WriteValue(stcSingleConcept.intQuantity)
                        //W.WriteEndAttribute()

                        if (string.IsNullOrEmpty(stcSingleConcept.intQuantity.ToString()))
                        {
                            stcSingleConcept.intQuantity = 0;
                        }
                        else if (stcSingleConcept.intQuantity != 0)
                        {
                            W.WriteStartAttribute("cantidad");
                            W.WriteValue(stcSingleConcept.intQuantity);
                            W.WriteEndAttribute();
                        }

                        //W.WriteStartAttribute("unidad")
                        //W.WriteValue(stcSingleConcept.strUnity.Trim())
                        //W.WriteEndAttribute()

                        if (string.IsNullOrEmpty(stcSingleConcept.strUnity))
                        {
                            W.WriteStartAttribute("unidad");
                            W.WriteValue("NO APLICA");
                            W.WriteEndAttribute();
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.strUnity.Trim()))
                        {
                            W.WriteStartAttribute("unidad");
                            W.WriteValue(stcSingleConcept.strUnity.Trim());
                            W.WriteEndAttribute();
                        }

                        if (string.IsNullOrEmpty(stcSingleConcept.strSerialNumber))
                        {
                            stcSingleConcept.strSerialNumber = "";
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.strSerialNumber.Trim()))
                        {
                            W.WriteStartAttribute("noIdentificacion");
                            W.WriteValue(stcSingleConcept.strSerialNumber.Trim());
                            W.WriteEndAttribute();
                        }

                        //W.WriteStartAttribute("descripcion")
                        //W.WriteValue(stcSingleConcept.strDescription.Trim())
                        //W.WriteEndAttribute()

                        if (string.IsNullOrEmpty(stcSingleConcept.strDescription))
                        {
                            stcSingleConcept.strDescription = "";
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.strDescription.Trim()))
                        {
                            W.WriteStartAttribute("descripcion");
                            W.WriteValue(stcSingleConcept.strDescription.Trim());
                            W.WriteEndAttribute();
                        }

                        //W.WriteStartAttribute("valorUnitario")
                        //W.WriteValue(Format(stcSingleConcept.dmlPrice.ToString("#0.00"))
                        //W.WriteEndAttribute()

                        if (string.IsNullOrEmpty(stcSingleConcept.dmlPrice.ToString()))
                        {
                            stcSingleConcept.dmlPrice = 0;
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.dmlPrice.ToString("#0.00").Trim()))
                        {
                            W.WriteStartAttribute("valorUnitario");
                            W.WriteValue(stcSingleConcept.dmlPrice.ToString("#0.00"));
                            W.WriteEndAttribute();
                        }

                        //W.WriteStartAttribute("importe")
                        //W.WriteValue(Format(stcSingleConcept.dmlTotal.ToString("#0.00"))
                        //W.WriteEndAttribute()

                        if (string.IsNullOrEmpty(stcSingleConcept.dmlTotal.ToString()))
                        {
                            stcSingleConcept.dmlTotal = 0;
                        }
                        else if (!string.IsNullOrEmpty(stcSingleConcept.dmlTotal.ToString("#0.00").Trim()))
                        {
                            W.WriteStartAttribute("importe");
                            W.WriteValue(stcSingleConcept.dmlTotal.ToString("#0.00"));
                            W.WriteEndAttribute();
                        }

                        W.WriteEndElement();
                        //Fin de Concepto
                    }
                }
                W.WriteEndElement();
                //Fin de Conceptos

                //[IMPUESTOS]
                W.WriteStartElement("cfdi:Impuestos");

                W.WriteStartAttribute("totalImpuestosTrasladados");
                W.WriteValue(this.stcDocument.dmlIVATax.ToString("#0.00").Trim());
                W.WriteEndAttribute();
                W.WriteStartElement("cfdi:Traslados");
                W.WriteStartElement("cfdi:Traslado");
                W.WriteStartAttribute("impuesto");
                W.WriteValue("IVA");
                W.WriteEndAttribute();
                //Fin de traslado

                W.WriteStartAttribute("tasa");
                W.WriteValue(this.stcDocument.dmlIVATaxRate.ToString("#0.00").Trim());
                W.WriteEndAttribute();

                W.WriteStartAttribute("importe");
                W.WriteValue(this.stcDocument.dmlIVATax.ToString("#0.00").Trim());
                W.WriteEndAttribute();

                W.WriteEndElement();
                //Fin de traslado
                W.WriteEndElement();
                //Fin de traslados
                W.WriteEndElement();
                //Fin de Impuestos
                //[FIN DEL XML]
                W.WriteEndDocument();
                //Fin de Comprobante

            }
            catch (Exception ex)
            {
                //strFunctionResult(0) = -1;
                //strFunctionResult(1) = ex.Message;
            }
            finally
            {
                W.Flush();
                W.Close();
            }
            //return strFunctionResult;
        }
        public void timbrarXML()
        {
            this.lUUID = new StringBuilder();
            string xmlP=this.strOpenSSLPath + this.stcEmitter.strRFC + "\\" + this.documento.codDocumento.ToString() + ".xml";
            NumError = SDK.fTimbraXML(  xmlP,
                                        this.documento.empresa.codConcepto.Trim(),
                                        this.lUUID,
                                        "",
                                        this.strOpenSSLPath + this.stcEmitter.strRFC+"Resultado",
                                        this.strKeyFilePassword,
                                        this.documento.empresa.PathPlantillaPDF);

            if (NumError != 0)
            {
                StringBuilder sMensaje = new StringBuilder(512);
                string mensaje = "";
                //SDK.rError(NumError, ref mensaje);
                DescError = mensaje;
                return;
            }
            this.documento.uuid = this.lUUID.ToString();
            this.estutusDocumento = "Facturado";
        }
        public void actualizarUUID() {

            this.objDocumentosPacDAO.actualizarUUID(this.documento.codDocumento, this.lUUID.ToString(), this.NumError, this.estutusDocumento);
        
        }
 
        public void cerrarEmpresa(bool terminarAdminPaq)
        {
            //SDK.SetCurrentDirectory(DirectorioSDK);
            SDK.fCierraEmpresa();
            if (terminarAdminPaq) this.terminarAdminPaq();
        }
        public void terminarAdminPaq()
        {
            SDK.fTerminaSDK();
        }
        public void obtenerdatosCliente() {
            
            ClientesDAO objClientesDAO = new ClientesDAO();
            List<Cliente> clientes = new List<Cliente>();
            clientes = objClientesDAO.Listado(null, null, null, this.documento.codCliente,null);
            this.cliente = clientes[0];
        }
    }
}