﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac
{
    public class StcConcepts
    {
        
        //Cantidad.
        public double intQuantity { get; set; } 
        //Unidad.
        public string strUnity { get; set; } 
        //# de Identificación (Serie).
        public string strSerialNumber { get; set; } 
        //# de parte.
        public string strPartNumber { get; set; } 
        //Descripción.
        public string strDescription { get; set; } 
        //Valor Unitario.
        public decimal dmlPrice { get; set; } 
        //Importe.
        public decimal dmlTotal { get; set; } 
        //....................................................................................
        //Contador inicial.
        public Int32 detfacIni { get; set; } 
        //Contador final.
        public Int32 detfacFin { get; set; }
        // conceptos para version 3.3
        public string claveProdServ { get; set; }
        public string claveUnidad { get; set; }
        // impuestos
        public decimal baseImpuesto { get; set; }
        public string claveImpuesto { get; set; }
        public string tipoFactor { get; set; }
        public decimal tasaOCuota { get; set; }
        public decimal importeImpuesto { get; set; }
    }
}