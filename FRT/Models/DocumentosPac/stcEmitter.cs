﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac
{
    public class StcEmitter
    {
        //Tipo de documento (identificador).
        public Int32 intDocumentTypeID { get; set; } 
        //RFC.
        public string strRFC { get; set; } 
        //Nombre.
        public string strName { get; set; } 
        //Calle.
        public string strStreet { get; set; } 
        //# Exterior.
        public string strOutsideNumber { get; set; } 
        //# Interior.
        public string strInsideNumber { get; set; } 
        //Colonia.
        public string strColony { get; set; } 
        //Localidad.
        public string strLocation { get; set; } 
        //Referencia.
        public string strReference { get; set; } 
        //Municipio.
        public string strTown { get; set; } 
        //Estado.
        public string strState { get; set; } 
        //País.
        public string strCountry { get; set; } 
        //Código Postal.
        public string strPostalCode { get; set; } 
        //Régimen Fiscal
        public string strRegimenFiscal { get; set; }
        public string lugarExpedicion { get; set; }
    }
}