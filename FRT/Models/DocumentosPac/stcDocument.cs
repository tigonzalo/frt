﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac
{
    public class StcDocument
    {
        public string noCertificado { get; set; }
         public string strUserPAC { get; set; }
       //Password PAC
        public string strPasswordPAC { get; set; }
        //pascertificado
        public string strPasscer { get; set; } 
        //Status.
        public string strStatusPAC { get; set; } 


        //Version
        public string strVersion   { get; set; } 
        //Serie
        public string strSerie { get; set; } 
        //Folio
        public string strFolio { get; set; } 
        //Date
        public string strDate { get; set; } 
        //Time
        public string strTime { get; set; } 
        //DigitalSeal
        public string strDigitalSeal { get; set; } 
        //ApprovalNumber
        public string strApprovalNumber { get; set; } 
        //ApprovalYear
        public string strApprovalYear { get; set; } 
        //PaymentForm
        public string strPaymentForm { get; set; } 
        //CertificateNumber
        public string strCertificateNumber { get; set; } 
        //Certificate
        public string strCertificate { get; set; } 
        //PaymentConditions
        public string strPaymentConditions { get; set; } 
        //Subtotal
        public decimal dmlSubtotal { get; set; } 
        //Discount
        public decimal dmlDiscount { get; set; } 
        //DiscountMotive
        public string strDiscountMotive { get; set; } 
        //Total
        public decimal dmlTotal { get; set; } 
        //Descuento
        public decimal dmlDescuento { get; set; } 
        //PaymentMethod
        public string strPaymentMethod { get; set; } 
        //VoucherType
        public string strVoucherType { get; set; } 
        //Moneda
        public string strMoneda { get; set; } 
        //Tipo de Cambio
        public decimal strTipoCambio { get; set; } 
        //Cuenta de pago
        public string strNumCtaPago { get; set; } 

        //....................................................................................
        //Detailsstring
        public string strDetailsstring { get; set; } 
        //IVATax
        public decimal dmlIVATax { get; set; } 
        //Porcentaje de IVA.
        public decimal dmlIVATaxRate { get; set; } 
        //ISRTax
        public decimal dmlISRTax { get; set; }
        //IEPSTax
        public decimal dmlIEPSTax { get; set; } 
        //Status.
        public bool blnStatus { get; set; } 
        //Observaciones.
        public string strGeneralNotes { get; set; } 
        //Nombre del agente.
        public string strAgentName { get; set; } 
        //Folio del sistema Integra Copy.
        public string strIntegraCopyFolio { get; set; } 
        //....................................................................................
        //Tipo de factura.
        //facPro
        public Int32 facPro { get; set; } 
        //Copias mínimas mensuales.
        //facCopMesMin
        public Int32 facCopMesMin { get; set; } 
        //Precios por copia (excedente).
        //facPreCop
        public decimal facPreCop { get; set; } 
        //facPreCopExc
        public decimal facPreCopExc { get; set; } 
        //facPreCopExc2
        public decimal facPreCopExc2 { get; set; } 
        //facPreCopExc3
        public decimal facPreCopExc3 { get; set; } 
        //Renta mensual.
        public decimal facRenMen { get; set; } 
        //....................................................................................
        //Periodos.
        //Inicial.
        public string facPer { get; set; } 
        //Final.
        public string facPer2 { get; set; } 
    }
}