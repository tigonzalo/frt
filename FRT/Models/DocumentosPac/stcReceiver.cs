﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRT.Models.DocumentosPac
{
    public class StcReceiver
    {
        
         //ReceiverID
        public Int32 intReceiverID { get; set; } 
        //ReceiverRFC
        public string strRFC { get; set; } 
        //ReceiverName
        public string strName { get; set; } 
        //ReceiverStreet
        public string strStreet { get; set; } 
        //ReceiverOutsideNumber
        public string strOutsideNumber { get; set; } 
        //ReceiverInsideNumber
        public string strInsideNumber { get; set; } 
        //ReceiverColony
        public string strColony { get; set; } 
        //ReceiverLocation
        public string strLocation { get; set; } 
        //ReceiverReference
        public string strReference { get; set; } 
        //ReceiverTown
        public string strTown { get; set; } 
        //ReceiverState
        public string strState { get; set; } 
        //ReceiverCountry
        public string strCountry { get; set; } 
        //ReceiverPostalCode
        public string strPostalCode { get; set; } 
    }
}