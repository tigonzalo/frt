﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace FRT.Models.DocumentosPac
{
    public class StcEmittedIn
    {
           //Calle.
        public string strStreet { get; set; } 
        //# Exterior.
        public string strOutsideNumber { get; set; } 
        //# Interior.
        public string strInsideNumber { get; set; } 
        //Colonia.
        public string strColony { get; set; } 
        //Localidad.
        public string strLocation { get; set; } 
        //Referencia.
        public string strReference { get; set; } 
        //Municipio.
        public string strTown { get; set; } 
        //Estado.
        public string strState { get; set; } 
        //País.
        public string strCountry { get; set; } 
        //Cósigo Postal.
        public string strPostalCode { get; set; } 
    }
}