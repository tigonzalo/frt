﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Opciones;
using FRT.Models.Admin;
using FRT.Models;

namespace FRT.Controllers
{
    public class SistemasController : Controller
    {
        //
        // GET: /Sistemas/

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("OptenerContrasenia")]
        public JsonResult OptenerContrasenia(Usuario objUsuario)
        {
            List<Sistema> list = new List<Sistema>();
            SistemasDAO objSistemasDAO = new SistemasDAO();
            try
            {
                objSistemasDAO.recuperarContrasenia(objUsuario.mailUsuario);
                if(objSistemasDAO.TotalRecords==0)
                    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Correo no registrado en el Sistema.<br/>Favor de Verificarlo" } }, JsonRequestBehavior.AllowGet);

               
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            //if (list.Count() > 0)
            //    return Json(new { success = session, user = objUsuario, menu = listM }, JsonRequestBehavior.AllowGet);

            //else
                return Json(new { success = true, responseMessage = new { nError = "AppFMC004", msgError = "La contraseña se ha enviado a su correo" } }, JsonRequestBehavior.AllowGet);
        }

    }
}
