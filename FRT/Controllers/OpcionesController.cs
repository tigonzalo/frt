﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Opciones;
using FRT.Models.Common;
using FRT.Models.Permisos;
using System.Collections;
using System.Data.SqlClient;
using FRT.Models.Usuarios;
using FRT.Models.Admin;

namespace FRT.Controllers
{
    public class OpcionesController : Controller
    {
        //
        // GET: /Opciones/

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(long? start, long? limit)
        {
            List<Opcion> list = new List<Opcion>();
            OpcionesDAO objOpcionesDAO = new OpcionesDAO();
            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);
                list = objOpcionesDAO.Listado(null, null, null);
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError =mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Sistema.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objOpcionesDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);
        }
      
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("ListadoPermisos")]
        public JsonResult ListadoPermisos(long? start, long? limit)
        {
            List<Permiso> list = new List<Permiso>();
            PermisosDAO objPermisosDAO = new PermisosDAO();
            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);
                list = objPermisosDAO.Listado(null, null, null);
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Sistema.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objPermisosDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);
        }

        
    
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Guardar")]
        public JsonResult Guardar()
        {
            OpcionesDAO objOpcionesDAO = new OpcionesDAO();

            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);
                Utils utils = new Utils();

                IList opc = utils.getOpcion().ToList();
                List<Opcion> listop = new List<Opcion>();
                List<Permiso> listPermisos = new List<Permiso>();

                foreach (Type controller in opc)
                {
                    Opcion c = new Opcion();

                    c.codOpcion = listop.Count + 1;
                    c.desOpcion = ((string)controller.GetType().GetProperty("Name").GetValue(controller, null)).Replace("Controller","");

                    listop.Add(c);
                    listPermisos.InsertRange(listPermisos.Count, utils.getPermisos(controller, c));
                }

                objOpcionesDAO.Guardar(listop, listPermisos);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


    }
}
