﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Estados;
using System.Data.SqlClient;
using FRT.Models.Admin;
using FRT.Models.FormasPagos;
using FRT.Models.Usuarios;

namespace FRT.Controllers
{
    public class FormasPagosController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado_33")]
        public JsonResult Listado_33()
        {
            List<FormaPago> list = new List<FormaPago>();
            FormasPagosDAO objFormasPagosDAO = new FormasPagosDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                list = objFormasPagosDAO.Listado_33();
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objFormasPagosDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
    }
}