﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using FRT.Models;
using System.IO;
using FRT.Models.Usuarios;
using FRT.Models.Admin;
using FRT.Models.DocumentosPac;
using FRT.Models.Common;
using FRT.Models.DocumentosPac.ReportesPac;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FRT.Models.Documentos.RepDocumentos;
using FRT.Models.FormasPagos;

namespace FRT.Controllers
{
    public class DocumentosController : Controller
    {
        ReportDocument cryRpt;

        //[AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        //[ActionName("CancelacionCFDI")]
        //public void CancelacionCFDI()
        //{

        //}

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuConsultarFacturas")]
        public void MenuConsultarFacturas()
        {

        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuGenerarFacturas")]
        public void MenuGenerarFacturas()
        {

        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, long? codDocumento, string busqvalor)
        {
            List<Documento> list = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                list = objDocumentosDAO.Listado(start, limit, codDocumento, objUsuario, busqvalor);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
                
                //Linea Modificada porque para quitar el mensaje de error al generar la consulta cuando iniciaba el sistema
                return Json(new { success = false,  }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objDocumentosDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Busqueda")]
        public JsonResult Busqueda(int? start, long? limit, string valorBusqueda)
        {
            List<Documento> list = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            //Usuario objUsuario = new Usuario();
            try
            {
                //objUsuario = Config.sesionValida(HttpContext);
                list = objDocumentosDAO.Buscar(start, limit, valorBusqueda);
            }
            //catch (SesionException ex)
            //{
            //    return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            //}
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objDocumentosDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Guardar")]
        public JsonResult Guardar(Documento objDocumento)
        {
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();

            Usuario objUsuario = new Usuario();
            try
            {
                //objUsuario = Config.sesionValida(HttpContext);
                DocumentosPacDAO objDocumentosPacDAO = new DocumentosPacDAO();
                //objDocumentosPacDAO.reportePdf();

               // objDocumentosDAO.insertarDocumentoAdminPaq(objDocumento);
                objDocumentosDAO.Guardar(objDocumento, objUsuario);
                if (objDocumentosDAO.NumError != 0)
                    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al Facturar folio, Favor de Reportarlo a la administración" } }, JsonRequestBehavior.AllowGet);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
                               
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]

        [ActionName("RetimbrarCFDI")]
        public JsonResult RetimbrarCFDI(Documento objDocumento)
        {

            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                DocumentosDAO objDocumentosDAO = new DocumentosDAO();
                DocumentoPac documentoPac = new DocumentoPac(objDocumento);
                //if (!objDocumentosDAO.holdDocumento(objDocumento)) {

                    lock (documentoPac)
                    {
                        documentoPac.procesar();
                    }
                
                //}
                    if (documentoPac.documento.hold==true)
                        return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Su Documento se encuentra en Proceso de Facturación <br> el sistema le enviara a su correo su Factura. " } }, JsonRequestBehavior.AllowGet);
                

                if (documentoPac.NumError != 0)
                    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Su Documento se encuentra en Proceso de Facturación <br> el sistema le enviara a su correo su Factura." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Su Documento se encuentra en Proceso de Facturación <br> el sistema le enviara a su correo su Factura." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("CancelarCFDI")]
        public JsonResult CancelarCFDI(Documento objDocumento)
        {

            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                DocumentosDAO objDocumentosDAO = new DocumentosDAO();
                objDocumentosDAO.cancelarCFDI(objDocumento,objUsuario.Perfil.codPerfil);
                
                //DocumentoPac documentoPac = new DocumentoPac(objDocumento);
                //documentoPac.cancelarCFDI();
                
                //if (documentoPac.NumError != 0)
                //    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al Cancelar el CDFI, Favor de Reportarlo a la administración" } }, JsonRequestBehavior.AllowGet);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[1] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error al Cancelar el Documento Intentelo mas tarde." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Eliminar")]
        public JsonResult Eliminar(long codPerfil)
        {
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();

            //Usuario objUsuario = new Usuario();
            try
            {

                //objUsuario = Config.sesionValida(HttpContext);

                objDocumentosDAO.Eliminar(codPerfil);
            }
            //catch (SesionException ex)
            //{
            //    return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            //}
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Copiar")]
        public JsonResult Copiar(Documento objDocumento)
        {
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();

            try
            {


                objDocumentosDAO.Copy(objDocumento.rutaPdf, objDocumento.rutaXml);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("RepImpresaCFDI")]
        public BynaryFileResult RepImpresaCFDI(long? codDocumento)
        {

          
           // user = DBManager.sesionValida(HttpContext, "Ventas", "ReporteVenta");
          //  string nombreUsuario = user.Persona.Nombre + " " + user.Persona.ApellidoP + " " + user.Persona.ApellidoM;
            Usuario objUsuario = new Usuario();
            ReportDocument rptDoc = new ReportDocument();
            DocumentosPacDAO objDocumentosPacDAO = new DocumentosPacDAO();
            List<RepImpresaCFDI> list = new List<RepImpresaCFDI>();
            List<Documento> listD = new List<Documento>();
            DocumentosDAO objDocumentosDAO = new DocumentosDAO();
            FormasPagosDAO objFormasPagosDAO = new FormasPagosDAO();
            List<FormaPago> listFP = new List<FormaPago>();
            try
            {
                //objUsuario = Config.sesionValida(HttpContext);
                listD = objDocumentosDAO.Listado(0, 1, codDocumento, objUsuario, null);
                list = objDocumentosPacDAO.reportePdf(listD[0]);
                //int v;
                //if (Int32.TryParse(list[0].metodoDePago.Trim(), out v))
                //{
                //    listFP = objFormasPagosDAO.Listado(list[0].metodoDePago);
                //    list[0].metodoDePago = listFP[0].descripcion;
                //    if( listD[0].cliente.ultimos_digitos.Length>1)
                //        list[0].metodoDePago = list[0].metodoDePago + " Cuenta:" + listD[0].cliente.ultimos_digitos;
                //}

                    
                rptDoc.Load(Request.PhysicalApplicationPath + "\\Models\\DocumentosPac\\ReportesPac\\RepImpresaCFDI.rpt");
                rptDoc.Database.Tables["RepImpresaCFDI"].SetDataSource(list);
                rptDoc.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
               // rptDoc.Database.Tables[1].SetDataSource(list[0].conceptosCFDI);
               // rptDoc.SetDataSource(objDocumentosPacDAO.reportePdf());
                //rptDoc.SetParameterValue("pNombreUsuario", nombreUsuario);
                //rptDoc.SetParameterValue("pFechaInicio", fechaInicio);
                //rptDoc.SetParameterValue("pFechaFin", fechaFin);

                //type = ExportType.Pdf;
                //switch (type)
                //{
                //    case ExportType.Pdf:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
                //    case ExportType.Excel:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
                //    default:
                //        return null;
                //}

                return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);

            }

            catch (SesionException ex)
            {
                return null;
            }

            catch (Exception)
            {
                return null;
            }
            finally
            {
                rptDoc.Close();
            }
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        
        [ActionName("RepDocumentos")]
        public BynaryFileResult RepDocumentos(string busqvalor)
        {


            // user = DBManager.sesionValida(HttpContext, "Ventas", "ReporteVenta");
            //  string nombreUsuario = user.Persona.Nombre + " " + user.Persona.ApellidoP + " " + user.Persona.ApellidoM;
            Usuario objUsuario = new Usuario();
            ReportDocument rptDoc = new ReportDocument();
            RepDocumento objRepDocumento = new RepDocumento();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                //list = objDocumentosPacDAO.reportePdf(listD[0]);
                rptDoc.Load(Request.PhysicalApplicationPath + "\\Models\\Documentos\\RepDocumentos\\RepDocumentos.rpt");
                rptDoc.SetDataSource(objRepDocumento.listado(busqvalor, objUsuario));
                //rptDoc.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
                // rptDoc.Database.Tables[1].SetDataSource(list[0].conceptosCFDI);
                // rptDoc.SetDataSource(objDocumentosPacDAO.reportePdf());
                //rptDoc.SetParameterValue("pNombreUsuario", nombreUsuario);
                //rptDoc.SetParameterValue("pFechaInicio", fechaInicio);
                //rptDoc.SetParameterValue("pFechaFin", fechaFin);

                //type = ExportType.Pdf;
                //switch (type)
                //{
                //    case ExportType.Pdf:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
                //    case ExportType.Excel:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
                //    default:
                //        return null;
                //}

               // return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
                return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
            }
            catch (SesionException ex)
            {
                return null;
            }

            catch (Exception e)
            {
                return null;
            }
            finally
            {
                rptDoc.Close();
            }
        }
        [ActionName("enviarCorreoCFDI")]
        public JsonResult enviarCorreoCFDI(Documento objDocumento)
        {

           
            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);
                DocumentoPac documentoPac = new DocumentoPac(objDocumento);
                documentoPac.crearPdfCFDI(objDocumento);
               // string correoUsuario, string ArchivoPDF, string ArchivoXML
                Correo.enviarCFDI_1(objDocumento, objDocumento.cliente.usuario.mailUsuario,
                    objDocumento.rutaPdf,
                    objDocumento.rutaXml,
                    documentoPac.dirTemp);
                if (documentoPac.NumError != 0)
                    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al Enviar el CDFI, Favor de Reportarlo a la administración" } }, JsonRequestBehavior.AllowGet);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error al Enviar el CDFI, favor de intentarlo mas tarde." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        //public BynaryFileResult RepDocumentos(string busqvalor)
        //{


        //    // user = DBManager.sesionValida(HttpContext, "Ventas", "ReporteVenta");
        //    //  string nombreUsuario = user.Persona.Nombre + " " + user.Persona.ApellidoP + " " + user.Persona.ApellidoM;
        //    Usuario objUsuario = new Usuario();
        //    ReportDocument rptDoc = new ReportDocument();
        //    RepDocumento objRepDocumento = new RepDocumento();
        //    try
        //    {
        //        objUsuario = Config.sesionValida(HttpContext);
        //        //list = objDocumentosPacDAO.reportePdf(listD[0]);
        //        rptDoc.Load(Request.PhysicalApplicationPath + "\\Models\\Documentos\\RepDocumentos\\RepDocumentos.rpt");
        //        rptDoc.SetDataSource(objRepDocumento.listado(busqvalor, objUsuario));
        //        //rptDoc.Database.Tables["Conceptos"].SetDataSource(list[0].conceptosCFDI);
        //        // rptDoc.Database.Tables[1].SetDataSource(list[0].conceptosCFDI);
        //        // rptDoc.SetDataSource(objDocumentosPacDAO.reportePdf());
        //        //rptDoc.SetParameterValue("pNombreUsuario", nombreUsuario);
        //        //rptDoc.SetParameterValue("pFechaInicio", fechaInicio);
        //        //rptDoc.SetParameterValue("pFechaFin", fechaFin);

        //        //type = ExportType.Pdf;
        //        //switch (type)
        //        //{
        //        //    case ExportType.Pdf:
        //        //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
        //        //    case ExportType.Excel:
        //        //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
        //        //    default:
        //        //        return null;
        //        //}

        //        // return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
        //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
        //    }
        //    catch (SesionException ex)
        //    {
        //        return null;
        //    }

        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        rptDoc.Close();
        //    }
        //}
        [ActionName("RecuperarCFDI")]
        public JsonResult RecuperarCFDI(Documento objDocumento)
        {

            try
            {
                List<Documento> list = new List<Documento>();
                DocumentosDAO objDocumentosDAO = new DocumentosDAO();
                string correo = objDocumento.correo;
                list = objDocumentosDAO.ListadoRecuperarCFDI(objDocumento);
                if (list.Count > 0) {
                    if (correo == null)
                        correo = list[0].correo;

                    DocumentoPac documentoPac = new DocumentoPac(list[0]);
                    documentoPac.recuperarPdfCFDI(list[0]);
                    // nuevo estructura
                    Correo.enviarCFDI33(list[0],
                        list[0].rutaPdf,
                        list[0].rutaXml,
                        documentoPac.dirTemp,
                        correo);
                    if (documentoPac.NumError != 0)
                        return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al Enviar el CDFI, Favor de Reportarlo a la administración" } }, JsonRequestBehavior.AllowGet);
                
                }else
                    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "CFDI no encontrado con los datos proporcionados." } }, JsonRequestBehavior.AllowGet);
                
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error al Enviar el CDFI, favor de intentarlo mas tarde." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("ReHacerXML")]
        public JsonResult ReHacerXML(Documento objDocumento)
        {

            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                List<Documento> list = new List<Documento>();
                DocumentosDAO objDocumentosDAO = new DocumentosDAO();
                list = objDocumentosDAO.Detalle(objDocumento.codDocumento);
                if (list.Count>0)
                    objDocumentosDAO.generarXml(list[0]);

                //DocumentoPac documentoPac = new DocumentoPac(objDocumento);
                //documentoPac.cancelarCFDI();

                //if (documentoPac.NumError != 0)
                //    return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al Cancelar el CDFI, Favor de Reportarlo a la administración" } }, JsonRequestBehavior.AllowGet);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[1] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error al Cancelar el Documento Intentelo mas tarde." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }

}
