﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Clientes;
using System.Data.SqlClient;
using FRT.Models.Admin;
using FRT.Models.Usuarios;
using FRT.ServicePAx;
namespace FRT.Controllers
{
    
    public class ClientesController : Controller
    {
        // GET: /Clientes/
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, string busqvalor, long? codCliente, long? codUsuario)
        {
            List<Cliente> list = new List<Cliente>();
            ClientesDAO objClientesDAO = new ClientesDAO();
            Usuario objUsuario = new Usuario();
          try
            {
             // ServicioFEL As New ServiceReference1.wcfRecepcionASMXSoapClient
                objUsuario = Config.sesionValida(HttpContext);
                list = objClientesDAO.Listado(start, limit, busqvalor, codCliente, codUsuario);
               
            }

          catch (SesionException ex)
          {
              return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
          }
          catch (SqlException ex)
          {
              var mnj = ex.Message.Split('|');
              return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
          }
          catch (Exception ex)
          {
              return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
          }
            return Json(new { success = true, total = objClientesDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuModificarClientes")]
        public JsonResult MenuModificarClientes()
        {
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuListadoClientes")]
        public JsonResult MenuListadoClientes()
        {
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Guardar")]
        public JsonResult Guardar(Cliente objCliente)
        {
            ClientesDAO objClientesDAO = new ClientesDAO();

           try
            {
                objClientesDAO.Guardar(objCliente);
               
            }
               
           catch (SesionException ex)
           {
               return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
           }
           catch (SqlException ex)
           {
               var mnj = ex.Message.Split('|');
               return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
           }
           catch (Exception ex)
           {
               return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
           }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);


        }
        
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Eliminar")]
        public JsonResult Eliminar(long codCliente)
        {
            ClientesDAO objClientesDAO = new ClientesDAO();

            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);

                objClientesDAO.Eliminar(codCliente);
            }
            catch (SesionException ex)
            {

                return Json(new { success = false, responseMessage = new { nError = "AppCABS001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppCABS002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppCABS003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
               
    }
}
