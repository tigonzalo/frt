﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using FRT.Models.Tickets;
using FRT.Models.Common;
using FRT.Models.Usuarios;
using FRT.Models.Admin;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FRT.Models.Tickets.RepTickets;
namespace FRT.Controllers
{
    public class TicketsController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Detalle")]
        public JsonResult Detalle(Ticket objTicket)
        {
            List<Ticket> list = new List<Ticket>();
            TicketsDAO objTicketsDAO = new TicketsDAO();
            try
            {
                list= objTicketsDAO.Detalle(objTicket);

            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, List = list }, JsonRequestBehavior.AllowGet);


        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, long? codTicket, string busqvalor,DateTime? fecha,bool? facturados)
        {
            List<Ticket> list = new List<Ticket>();
            TicketsDAO objTicketsDAO = new TicketsDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                list = objTicketsDAO.Listado(start, limit, codTicket, objUsuario, busqvalor,fecha,facturados);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);

                //Linea Modificada porque para quitar el mensaje de error al generar la consulta cuando iniciaba el sistema
                return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objTicketsDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
        
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Guardar")]
        public JsonResult Guardar(Ticket objTicket)
        {
            TicketsDAO objTicketsDAO = new TicketsDAO();

            try
            {
                objTicketsDAO.Guardar(objTicket);

            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);


        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Cancelar")]
        public JsonResult Cancelar(Ticket objTicket)
        {

            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                TicketsDAO objTicketsDAO = new TicketsDAO();
                objTicketsDAO.Cancelar(objTicket.codTicket);

             }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error al Cancelar el Ticket Intentelo mas tarde." } }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        [ActionName("RepTickets")]
        public BynaryFileResult RepTickets(string busqvalor,bool facturados)
        {


            // user = DBManager.sesionValida(HttpContext, "Ventas", "ReporteVenta");
            //  string nombreUsuario = user.Persona.Nombre + " " + user.Persona.ApellidoP + " " + user.Persona.ApellidoM;
            Usuario objUsuario = new Usuario();
            ReportDocument rptDoc = new ReportDocument();
            TicketsDAO objTicketsDAO = new TicketsDAO();
            List<TikectRep> list = new List<TikectRep>();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                string Estatus = facturados ? "Facturados" : "No Facturados";
                list = objTicketsDAO.ListadoRep( objUsuario, busqvalor, facturados);
                
                rptDoc.Load(Request.PhysicalApplicationPath + "\\Models\\Tickets\\RepTickets\\TicketsRep.rpt");
                //rptDoc.SetDataSource(list);
                rptDoc.Database.Tables["DataTable1"].SetDataSource(list);
                rptDoc.SetParameterValue("estatus", Estatus);
                // rptDoc.Database.Tables[1].SetDataSource(list[0].conceptosCFDI);
                // rptDoc.SetDataSource(objDocumentosPacDAO.reportePdf());
                //rptDoc.SetParameterValue("pNombreUsuario", nombreUsuario);
                //rptDoc.SetParameterValue("pFechaInicio", fechaInicio);
                //rptDoc.SetParameterValue("pFechaFin", fechaFin);

                //type = ExportType.Pdf;
                //switch (type)
                //{
                //    case ExportType.Pdf:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
                //    case ExportType.Excel:
                //        return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
                //    default:
                //        return null;
                //}

                // return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.PortableDocFormat), "application/pdf", "Reporte.pdf", false);
                return new BynaryFileResult(ExportReport.Export(rptDoc, ExportFormatType.Excel), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte.xls", false);
            }
            catch (SesionException ex)
            {
                return null;
            }

            catch (Exception e)
            {
                return null;
            }
            finally
            {
                rptDoc.Close();
            }
        }
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("ModificarFormaPago")]
        public JsonResult ModificarFormaPago(long codFormaPago, long codTicket)
        {
            TicketsDAO objTicketsDAO = new TicketsDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                objTicketsDAO.ModificarFormaPago(codTicket, codFormaPago);
            }
            //catch (SesionException ex)
            //{
            //    return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            //}
            catch (SqlException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error Grl." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
