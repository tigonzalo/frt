﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Estados;
using System.Data.SqlClient;
using FRT.Models.Admin;
using FRT.Models.Usuarios;

namespace FRT.Controllers
{
    public class EstadosController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, string codEstado,bool? session )

        {
            List<Estado> list = new List<Estado>();
            EstadosDAO objEstadosDAO = new EstadosDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                //if (session==null)
                //objUsuario = Config.sesionValida(HttpContext);
                list = objEstadosDAO.Listado(start, limit, codEstado);
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objEstadosDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }
    }
}
