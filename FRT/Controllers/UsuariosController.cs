﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Usuarios;
using System.Data.SqlClient;
using FRT.Models.Opciones;
using FRT.Models.Admin;
using FRT.Models.Perfiles;

namespace FRT.Controllers
{
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuUsuarios")]
        public JsonResult MenuUsuarios()
        {
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Session")]
        public JsonResult Session()
        {

            var session = false;
            Usuario objUsuario = new Usuario();
            OpcionesDAO objOpcionesDAO = new OpcionesDAO();
            PerfilPermisosDAO objPerfilPermisosDAO = new PerfilPermisosDAO();
            List<Opcion> listM = new List<Opcion>();
            try
            {
                listM = objOpcionesDAO.Listado(null, null, null);
                objUsuario = Config.sesionValida(HttpContext);
                objUsuario.Perfil.perfilPermisos = objPerfilPermisosDAO.Listado(null, null, objUsuario.Perfil.codPerfil);
                HttpContext.Session["Usuario"] = objUsuario;

            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            session = true;
            return Json(new { success = session, user = objUsuario, menu = listM }, JsonRequestBehavior.AllowGet);

        }

        [ActionName("Logout")]
        public JsonResult Logout()
        {
            Usuario objUsuario = new Usuario();
            try
            {
                HttpContext.Session["Usuario"] = null;
                objUsuario = Config.sesionValida(HttpContext);
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Login")]
        public JsonResult Login(Usuario objUsuario)
        {
            List<Usuario> list = new List<Usuario>();
            List<Opcion> listM = new List<Opcion>();
            UsuariosDAO objUsuariosDAO = new UsuariosDAO();
            OpcionesDAO objOpcionesDAO = new OpcionesDAO();
            PerfilPermisosDAO objPerfilPermisosDAO = new PerfilPermisosDAO();
            var session = false;
            try
            {
                list = objUsuariosDAO.Login(objUsuario);
                listM = objOpcionesDAO.Listado(null, null, null);
                if (list.Count() > 0)
                {
                    objUsuario = list[0];
                    session = true;
                    objUsuario.Perfil.perfilPermisos = objPerfilPermisosDAO.Listado(null, null, objUsuario.Perfil.codPerfil);
                    HttpContext.Session["Usuario"] = objUsuario;

                }
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            if (list.Count() > 0)
                return Json(new { success = session, user = objUsuario, menu = listM }, JsonRequestBehavior.AllowGet);

            else
                return Json(new { success = false, responseMessage = new { nError = "AppFMC004", msgError = "Error al ingresar usuario o contraseña. <br>Favor de verificar sus datos." } }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, string busqcampo, string busqvalor, long? codUsuario, long? codPerfil)
        {
            List<Usuario> list = new List<Usuario>();
            UsuariosDAO objUsuarioesDAO = new UsuariosDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                list = objUsuarioesDAO.Listado(start, limit, busqcampo, busqvalor, codUsuario, codPerfil);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objUsuarioesDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Guardar")]
        public JsonResult Guardar(Usuario objUsuario)
        {
            UsuariosDAO objUsuarioesDAO = new UsuariosDAO();

            Usuario objUsuario1 = new Usuario();
            try
            {
                objUsuario1 = Config.sesionValida(HttpContext);
                objUsuarioesDAO.Guardar(objUsuario);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj=ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Eliminar")]
        public JsonResult Eliminar(long codUsuario)
        {
            UsuariosDAO objUsuarioesDAO = new UsuariosDAO();

            Usuario objUsuario = new Usuario();
            try
            {

                objUsuario = Config.sesionValida(HttpContext);

                objUsuarioesDAO.Eliminar(codUsuario);
            }
            catch (SesionException ex)
            {

                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError =mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
 

    }
}
