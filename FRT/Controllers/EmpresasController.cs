﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Empresas;
using System.Data.SqlClient;
using FRT.Models.Admin;
using System.IO;
using FRT.Models.Usuarios;

namespace FRT.Controllers
{
    public class EmpresasController : Controller
    {

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("MenuEmpresas")]
        public JsonResult MenuEmpresas()
        {
            return null;
        }

         [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
         [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, long? codEmp, string busqvalor, string busqcampo)
        {
            List<Empresa> list = new List<Empresa>();
            EmpresasDAO objEmpresasDAO = new EmpresasDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                
                //objUsuario = Config.sesionValida(HttpContext);
                list = objEmpresasDAO.Listado(start, limit, codEmp, busqvalor, busqcampo);
            }

            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objEmpresasDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }

         [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
         [ActionName("Guardar")]
         public JsonResult Guardar(Empresa objEmpresa, HttpPostedFileBase imgEmp)
         {
             EmpresasDAO objEmpresaDAO = new EmpresasDAO();
             Usuario objUsuario = new Usuario();
             try
             {
                 objUsuario = Config.sesionValida(HttpContext);
                 if (imgEmp != null)
                 {
                     var fileName = Path.GetFileName(imgEmp.FileName);
                     var nuevoNombre = ("C:/Documentos/" + fileName);
                     imgEmp.SaveAs(nuevoNombre);
                     objEmpresaDAO.Guardar(objEmpresa, nuevoNombre);
                 }
                 else if(imgEmp == null) { 
                     objEmpresaDAO.Guardar(objEmpresa, "");
                 }
             }
             catch (SesionException ex)
             {
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
             }
             catch (SqlException ex)
             {
                 var mnj = ex.Message.Split('|');
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
             }
             catch (Exception ex)
             {
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
             }
             return Json(new { success = true }, JsonRequestBehavior.AllowGet);
         }

         [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
         [ActionName("Eliminar")]
         public JsonResult Eliminar(long codEmpresa)
         {
             EmpresasDAO objEmpresaDAO = new EmpresasDAO();
             Usuario objUsuario = new Usuario();
             try
             {
                 objUsuario = Config.sesionValida(HttpContext);
                 objEmpresaDAO.Eliminar(codEmpresa);
             }
             catch (SesionException ex)
             {
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
             }
             catch (SqlException ex)
             {
                 var mnj = ex.Message.Split('|');
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
             }
             catch (Exception ex)
             {
                 return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
             }
             return Json(new { success = true }, JsonRequestBehavior.AllowGet);
         }

         

    }
}
