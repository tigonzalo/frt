﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FRT.Models.Clientes;
using System.Data.SqlClient;
using FRT.Models.Admin;
using FRT.Models.Usuarios;
using FRT.Models.UsosCFDI;

namespace FRT.Controllers
{
    public class UsosCFDIController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado()
        {
            List<UsoCFDI> list = new List<UsoCFDI>();
            UsosCFDIDAO objUsosCFDIDAO = new UsosCFDIDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                //objUsuario = Config.sesionValida(HttpContext);
                list = objUsosCFDIDAO.Listado();

            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objUsosCFDIDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }

    }
}
