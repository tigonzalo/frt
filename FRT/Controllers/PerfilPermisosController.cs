﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using FRT.Models.Perfiles;
using FRT.Models.Usuarios;
using FRT.Models.Admin;

namespace FRT.Controllers
{
    public class PerfilPermisosController : Controller
    {
        //
        // GET: / PerfilPermisos/

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ActionName("Listado")]
        public JsonResult Listado(int? start, long? limit, long? codPerfil)
        {
            List<PerfilPermiso> list = new List<PerfilPermiso>();
            PerfilPermisosDAO objPerfilPermisoDAO = new PerfilPermisosDAO();
            Usuario objUsuario = new Usuario();
            try
            {
                objUsuario = Config.sesionValida(HttpContext);
                list = objPerfilPermisoDAO.Listado(null, null, codPerfil);
            }
            catch (SesionException ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC001", msgError = "Tiempo de Session finalizado." } }, JsonRequestBehavior.AllowGet);
            }
            catch (SqlException ex)
            {
                var mnj = ex.Message.Split('|');
                return Json(new { success = false, responseMessage = new { nError = "AppFMC002", msgError = mnj[0] } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = new { nError = "AppFMC003", msgError = "Error no tratado en el Servidor.<br>Favor de reportarlo con el administrador del Sistema." } }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, total = objPerfilPermisoDAO.TotalRecords, List = list }, JsonRequestBehavior.AllowGet);

        }

      


 


    }
}
