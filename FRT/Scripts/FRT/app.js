Ext.define('FRT.Routes', { statics: { root: document.FMCUrl } });
Ext.define('FRT.Session', { statics: { usuario: null, menu: null } });

Ext.require(['FRT.common.FnConnSeg']);


Ext.application({
    name: 'FRT',
    extend: 'FRT.Application',
    models: [  
        'perfiles.Perfil',
        'perfiles.PerfilPermiso',
        'usuarios.Usuario',
        'opciones.Opcion',
        'opciones.Permiso',
        'documentos.Documento',
        'statusDocumentos.StatusDocumento',
        'clientes.Cliente',
        'estados.Estado',
        'municipios.Municipio',
        'empresas.Empresa',
        'tickets.Ticket',
        'usosCFDI.UsoCFDI',
        'formasPagos.FormaPago'
    ],
    stores: [
        'perfiles.Perfiles',
        'perfiles.PerfilPermisos',
        'usuarios.Usuarios',
        'opciones.Opciones',
        'opciones.Permisos',
        'empresas.Empresas',
        'estados.Estados',
        'opciones.Permisos',
        'documentos.Documentos',
        'empresas.Empresas',
        'clientes.Clientes',
        'municipios.Municipios',
        'tickets.Tickets',
        'usosCFDI.UsosCFDI',
        'formasPagos.FormasPagos'
    ],
    views: [
        'main.Main',
        'perfiles.PerfilesMain',
        'usuarios.UsuariosMain',
        'login.Login',
        'login.registro',
        'documentos.DocumentosMain',
        'clientes.ClientesMain',
        'clientes.ClientesListadoMain',
        'clientes.ClienteDetalle',
        'clientes.ClienteInfo',
        'clientes.DocumentoInfo',
        'empresas.EmpresasMain',
        'documentos.DocumentosConsultaMain',
        'documentos.DocumentosMain',
        'tickets.TicketsMain',
        'tickets.TicketDetalle',
        'documentos.DocumentosCliente'
    ],
    launch: function () {
        document.FMCUrl = {};
        //FRT.common.FnConnSeg.getSession();
        FRT.common.FnConnSeg.getMain();
        if (Ext.get('page-loader')) {
            Ext.get('page-loader').remove();
        }
    }
});
