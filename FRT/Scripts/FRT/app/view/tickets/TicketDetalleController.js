Ext.define('FRT.view.tickets.TicketDetalleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ticketdetalle',
    //editar: true,
    objTicket: null,
    objUsuario: null,
    cargarStores: function (me, eOpts) {
        this.cargarFormasPagos();
    },
    cargarFormasPagos: function () {
        var view = this.getView();
        var store = Ext.getStore('FormasPagos');
        store.load();
    },

    guardarDocumento: function (me, e, eOpts) {
         
        var view = this.getView();
        form = view.down('#FormDocumento'),
        cmbFormasPagos = view.down('#cmbFormasPagos'),
        data = form.getForm().getValues();
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando Ticket...');
        var objTicket = Ext.create('FRT.model.tickets.Ticket', data);
        objTicket.set('codEmpresa', view.down('#CmbEmpresa').getValue());
        objTicket.setFormaPago(cmbFormasPagos.getSelection())
        
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Tickets/Guardar',
            encode: true,
            params: Ext.encode(objTicket.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.mnj('Aviso', 'Su ticket se ha Generado.')
                    //this.consultarFacturas();
                }

                //this.limpiarForm();
                view.unmask();
                //this.cargarStores();
            },
            failure: function (o, r, n) {
                me.mnj('Error', 'El registro no se registro', Ext.MessageBox.ERROR)
                
                view.unmask();
            }
        });

    },
    cargarEmpresasCmb: function (inicial) {
        
        
        var me = this;
        var view = this.getView();
        var comboEmpresa = view.down('#CmbEmpresa');
        var proxy = comboEmpresa.getStore().getProxy();
        comboEmpresa.getStore().pageSize = 10;
        proxy.setExtraParam('start', 0);
        proxy.setExtraParam('limit', 10);
        comboEmpresa.getStore().load({
            params: { campo: 'Nombre', valor: comboEmpresa.getRawValue() },
            scope: this,
            url: FRT.Routes.root + 'Empresas/Listado',
            pageSize: 10,
            callback: function (records, operation, success) {
                if (success == true) {
                    if (!inicial == true) {
                         
                        // TUA.common.Common.prepareExpadPickerEvent(me.getCmbPuestos());
                    }
                }
            }
        });
    },
    

    cargarEstados: function (me, eOpts) {
         
        var view = this.getView();
         
        var storeEstados = view.down('#cmbEstados').getStore();
        storeEstados.load({
            params: !this.editar ? { start:null,limit:null,session: false } : { start: null, limit: null },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    
 
    guardarTicket: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormTicket'),
        cmbFormasPagos = view.down('#cmbFormasPagos'),
        data = form.getForm().getValues();
       
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando...');
        
        var viewT = Ext.ComponentQuery.query('ticketsMain')[0];
        var objTicket = Ext.create('FRT.model.tickets.Ticket', data);
        objTicket.set('codEmpresa', FRT.Session.usuario.Empresa.codEmpresa);
        objTicket.set('codTicket', 0);
        objTicket.setFormaPago(cmbFormasPagos.getSelection())
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Tickets/Guardar',
            encode: true,
            params: Ext.encode(objTicket.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                view.unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    viewT.getController().cargarTicketsConsulta();
                    this.mnj('Info', 'Datos del Ticket Guardados.', Ext.MessageBox.INFO);
                    if (!Ext.isNumber(objTicket.get('codTicket')))
                        this.getView().destroy();
                }
                
                
               
            },
            failure: function (o, r, n) {
                view.unmask();
            }
           
        });
        
    },
    limpiarForm: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormTicket');
        form.reset();
    },
    
    cancelarRegistro: function (item, e, eOpts) {
        Ext.ComponentQuery.query('ticketsdetalle')[0].destroy();
    },

  
  
    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    mostrarDetalleTicket: function (record) {
          
            var view = this.getView();
            view.mask('Procesando...');
            this.cargarEstados();
            var objTicket = Ext.create('FRT.model.tickets.Ticket', { codTicket: record.getData().codTicket })
            Ext.Ajax.request({
                url: FRT.Routes.root + 'Tickets/Listado',
                encode: true,
                params: Ext.encode(objTicket.getData(true)),
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                scope: this,
                success: function (response) {
                    var resp = Ext.JSON.decode(response.responseText);
                    if (resp.success == true) {
                        this.objTicket = Ext.create('FRT.model.tickets.Ticket', resp.List[0]).getData(true);
                        this.objUsuario = Ext.create('FRT.model.usuarios.Usuario', resp.List[0].usuario).getData(true);
                        view.down('ticketinfo #cmbEstados').setValue(this.objTicket.Estado.codEstado);
                        view.down('#FormTicket').getForm().setValues(this.objTicket);
                        view.down('#FormTicket').getForm().setValues(this.objUsuario);
                    }

                    view.unmask();
                },
                failure: function (o, r, n) {
                    view.unmask();
                }
            });
       
    },
    cargarMunicipios: function (me, e, eOpts) {
        
        var view = this.getView();
        var storeMunicipios = view.down('#cmbMunicipios').getStore();
        view.mask('Procesando...');
        storeMunicipios.load({
            params: !this.editar ? { start: null, limit: null, codEstado: me.getValue(), session: false } : { start: null, limit: null, codEstado: (Ext.isNumber(me * 1) ? me : me.getValue()) },
            scope: this,
            callback: function (records, operation, success) {
                if (this.editar == true) {
                    view.down('ticketinfo #codMunicipio').setValue(this.objTicket.Municipio.codMunicipio);
                }
                view.unmask();
            },
            failure: function (o, r, n) {
                view.unmask();
            }
        });
    }
});

