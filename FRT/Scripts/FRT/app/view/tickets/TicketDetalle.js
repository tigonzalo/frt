﻿Ext.define('FRT.view.tickets.TicketDetalle', {
    alias: 'widget.ticketsdetalle'
    , controller: 'ticketdetalle'
    , extend: 'Ext.window.Window'
    , requires: [
        'Ext.toolbar.Spacer',
        'Ext.grid.column.RowNumberer',
        'Ext.grid.column.Action',
        'Ext.grid.column.Check',
        'FRT.view.tickets.TicketDetalleController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.grid.column.Boolean',
        'Ext.grid.View'
    ],
    config: {
    buttons: [
       {
           text: 'Cancelar',
           width: 100,
           itemId: 'BtnLimpiar',
           listeners: {
               click: 'cancelarRegistro'
           }
       },
       {
           text: 'Guardar',
           width: 100,
           itemId: 'BtnGuardar',
           listeners: {
               click: 'guardarTicket'
           }
       }
    ]
    },
     titleAlign: 'center',
     height: 480,
     width: 580,
     title: 'Detalle del Ticket',
     modal: true,
     layout: {
         type: 'vbox',
         align: 'stretch'
     },
     initComponent: function () {
         var me = this,
         storeFormasPagos = Ext.create('FRT.store.formasPagos.FormasPagos');
        Ext.applyIf(me, {
            items: [
        {
            xtype: 'form',
            itemId: 'FormTicket',
            flex: 1,
            padding: '10,10,10,10',
            defaults: {
                margin: '35,10,10,35'
            },
            bodyPadding: 10,
            items: [
                {
                    xtype: 'hiddenfield',
                    anchor: '100%',
                    fieldLabel: 'Label',
                    name: 'codTicket'
                },
                {
                    xtype: 'hiddenfield',
                    anchor: '100%',
                    fieldLabel: 'Label',
                    name: 'codEmpresa'
                },
                {
                    xtype: 'textfield',
                    anchor: '100%',
                    disabled: true,
                    itemId: 'folio',
                    fieldLabel: 'Folio',
                    name: 'folio'
                },
                {
                    xtype: 'datefield',
                    anchor: '100%',
                    itemId: 'fechaCosumo',
                    fieldLabel: 'Fecha Consumo',
                    name: 'fechaCosumo',
                    allowBlank: false,
                    format: 'd/m/Y'
                },
                {
                    xtype: 'combobox',
                    name: 'codFormaPago',
                    forceSelection: true,
                    anchor: '100%',
                    fieldLabel: 'FormaPago',
                    itemId: 'cmbFormasPagos',
                    displayField: 'descripcionClave',
                    queryMode: 'local',
                    store: storeFormasPagos,
                    enableKeyEvents: true,
                    valueField: 'codFormaPago',
                    allowBlank: false,
                    autoSelect: false,
                    anyMatch: true
                },
                //{
                //    xtype: 'textfield',
                //    anchor: '100%',
                //    itemId: 'formaPago',
                //    fieldLabel: 'FormaPago',
                //    name: 'formaPago',
                //    allowBlank: false
                //},
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    itemId: 'propina',
                    fieldLabel: 'Propina',
                    name: 'propina',
                    minValue: 0,
                    allowBlank: false
                },
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    itemId: 'total',
                    fieldLabel: 'Total Neto',
                    name: 'total',
                    minValue: 0,
                    allowBlank: false
                }
            ]
        }
            ]

        });

        me.callParent(arguments);
    },
    listeners: {
    afterrender: 'cargarStores'

    }
  

});