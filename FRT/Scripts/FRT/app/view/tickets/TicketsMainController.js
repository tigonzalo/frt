

Ext.define('FRT.view.tickets.ticketsMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ticketsmain',
    facturados: false,
    cargarStores: function (me, eOpts) {
        this.cargarFormasPagos();
        this.cargarGridTickets();
    },
    cargarFormasPagos: function () {
        var view = this.getView();
        var storeEstatus = Ext.getStore('FormasPagos');
        storeEstatus.load();
    },
    modificarFormaPago: function (editor, context, eOpts, otra) {
        var view = this.getView();
        view.mask('Procesando...');
        
        var columna = context.field,
            rec = context.record,
            codTicket = rec.get('codTicket'),
            codFormaPago = rec.get('formaPago');
        
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Tickets/ModificarFormaPago',
            encode: true,
            params: Ext.encode({
                codFormaPago: codFormaPago,
                codTicket: codTicket
            }),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                view.unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    //this.mnj('INFO', 'El Estatus del Documento.<br> Fue Actualizado', Ext.MessageBox.INFO)
                    this.cargarGridTickets();
                }
            },
            failure: function (o, r, n) {
                this.mnj('Error', 'El registro no fue modificado', Ext.MessageBox.ERROR)
                view.unmask();
            }
        });
    },
    agregarTicket: function () {
        var vista = Ext.create('FRT.view.tickets.TicketDetalle');
        vista.show();
        // vista.getController().cargarEstados();
    },
    eliminarTickets: function (me, record, item, index, e, eOpts) {
        if (item === "actionCancelar") {
            this.cancelarTicket(me, record, item, index, e, eOpts);

        }
    },
    cancelarTicket: function (me, record, item, index, e, eOpts) {
        me = this;
        Ext.MessageBox.show({
            title:'Cancelar',
            msg: 'Desea Cancelar el Ticket?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn == "no"){
                    return;
                }
                if (btn == "yes") {

                    var objTicket = Ext.create('FRT.model.tickets.Ticket', record.getData());
                    me.getView().mask("Procesando..");
                    Ext.Ajax.request({
                        url: FRT.Routes.root + 'Tickets/Cancelar',
                        encode: true,
                        params: Ext.encode(objTicket.getData(true)),
                        headers: { 'Content-Type': 'application/json' },
                        method: 'post',
                        scope: this,
                        success: function (response) {

                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success == true) {
                                me.cargarGridTickets();
                            }

                            me.getView().unmask();

                        },
                        failure: function (o, r, n) {
                            me.getView().unmask();
                        }

                    });
                }
            }                
    });
        
    },
    cargarTicketsConsulta: function (me, eOpts) {
        this.cargarGridTickets();
    },
//     cargarStores: function (me, eOpts) {
//         var storeTickets = Ext.getStore('Tickets.Tickets');
//         storeTickets.load({
//             //  params: params
//             scope: this,
//             callback: function (records, operation, success) {
//             }
//         });
//     }
    //     ,
    crearReporteTickets: function () {
        
        //this.open('POST', url, record.get('codDocumento'), '_blank');
        var view = this.getView();
        var form = document.createElement("form");
        form.action = FRT.Routes.root + 'Tickets/RepTickets';
        form.method = 'POST';
        form.style = "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400"
        form.target = '_blank' || "_self";
        
            
            var input = document.createElement("INPUT");
            input.name = 'busqvalor';
            input.value = view.down('#txtBuscar').getValue();
            form.appendChild(input);

            var input2 = document.createElement("INPUT");
            input2.name = 'facturados';
            input2.value = this.facturados;
            form.appendChild(input2);
           
        
        form.style.display = 'none';
        document.body.appendChild(form);
        form.submit();
    },
    cargarGridTickets: function (url) {
       
        var view = this.getView()
        var gridTickets = view.down('#gridTickets');
         if (view.down('#paginadorTickets').getStore().currentPage > 1)
             view.down('#paginadorTickets').moveFirst();
        
        gridTickets.getStore().load({
            url: url,
            params: {
                busqvalor: view.down('#txtBuscar').getValue(),
                fecha: view.down('#txtFecha').getValue() ,
                facturados: this.facturados
            },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    buscarTickets: function (me, e, eOpts) {
         
        if (e.getCharCode() == Ext.EventObject.ENTER) 
            this.cargarGridTickets();
        
    },
    cargarTicketsToolBar: function (me, page, eOpts) {
         
        var view = this.getView()
        var p = me.getStore().getProxy();
        p.setExtraParam('busqvalor', view.down('#txtBuscar').getValue());
        p.setExtraParam('fecha', view.down('#txtFecha').getValue());
        this.facturados ? p.setExtraParam('facturados', true) : p.setExtraParam('facturados', false);
    },
    changeBuscarTickets: function (me, newValue, oldValue, eOpts) {
        if (newValue.length === 0) 
            this.cargarGridTickets();
    },
    guardarDocumento: function (me, e, eOpts) {
        
        var view = this.getView();
        form = view.down('#FormDocumento');
        data = form.getForm().getValues();
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando Factura...');
        var objTickets = Ext.create('FRT.model.documentos.Documento', data);
        objTickets.set('codEmpresa', view.down('#CmbEmpresa').getValue());
        //var objEmpresa = Ext.create('TUA.model.Departamentos.Departamento', this.getDepartamentoInfo().getForm().getValues());
         //= view.down('#CmbEmpresa');

        Ext.Ajax.request({
            url: FRT.Routes.root + 'Tickets/Guardar',
            encode: true,
            params: Ext.encode(objTickets.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.mnj('Aviso', 'Su ticket se ha facturado, en breve le enviaremos su factura y documento XML.')
                    this.consultarFacturas();
                }

                //this.limpiarForm();
                view.unmask();
                //this.cargarStores();
            },
            failure: function (o, r, n) {
                me.mnj('Error', 'El registro no se registro', Ext.MessageBox.ERROR)
                this.limpiarForm();
               view.unmask();
            }
        });

    }
    , mostrarDetalleCliente: function () {
         
        var view = this.getView();
        var vistaCliente = view.down('#datosCliente');
        vistaCliente.disable();
       // var objUsuario = Ext.create('FRT.model.usuarios.Usuario', FRT.Session.usuario);
        //Ext.Ajax.request({
        //                        url: FRT.Routes.root + 'Clientes/Listado',
        //                        encode: true,
        //                        params: Ext.encode(objUsuario.getData(true)),
        //                        headers: { 'Content-Type': 'application/json' },
        //                        method: 'post',
        //                        scope: this,
        //                        success: function (response) {
        //                            var resp = Ext.JSON.decode(response.responseText);
        //                            if (resp.success == true) {
        //                                 
        //                               // me.mnj('Aviso', 'El registro fue eliminado')

        //                            }
        //                            me.cargarStores();
        //                            me.getView().unmask();
        //                        },
        //                        failure: function (o, r, n) {
        //                            me.mnj('Error', 'El registro no fue eliminado', Ext.MessageBox.ERROR)
        //                            me.getView().unmask();
        //                        }
        //                    });


    }
    , cargarEmpresasCmb: function (inicial) {
        
        this.informacionCliente();
        var me = this;
        var view = this.getView();
        var comboEmpresa = view.down('#CmbEmpresa');
        var proxy = comboEmpresa.getStore().getProxy();
        comboEmpresa.getStore().pageSize = 10;
        proxy.setExtraParam('start', 0);
        proxy.setExtraParam('limit', 10);
        comboEmpresa.getStore().load({
            params: { campo: 'Nombre', valor: comboEmpresa.getRawValue() },
            scope: this,
            url: FRT.Routes.root + 'Empresas/Listado',
            pageSize: 10,
            callback: function (records, operation, success) {
                if (success == true) {
                    if (!inicial == true) {
                         
                       // TUA.common.Common.prepareExpadPickerEvent(me.getCmbPuestos());
                    }
                }
            }
        });
    }
   , informacionCliente: function () {
        
        var view = this.getView();
        var botonGuardar = view.down('#FormDocumento #clienteMain #BtnGuardar');
        var botonCancelar = view.down('#FormDocumento #clienteMain #BtnLimpiar');
        var vistaCliente = view.down('#FormDocumento #clienteMain #FormCliente');
        //vistaCliente.setTitle('Datos de Facturación del Cliente')
        botonGuardar.setVisible(false);
        botonCancelar.setVisible(false);
       // this.setReadOnlyForAll(true, vistaCliente)

    }
   , setReadOnlyForAll: function (readOnly, vista) {
        Ext.suspendLayouts();
        vista.getForm().getFields().each(function (field) {
            field.setReadOnly(readOnly);
        });
        Ext.resumeLayouts();
   }
   , siguiente: function () {
       
       var view = this.getView();
       var vistaCliente = view.down('#FormDocumento #clienteMain #FormCliente');
       this.guardarCliente(vistaCliente)
   }
  , guardarCliente: function (vistaCliente) {
      
    var view  = this.getView();
    form = vistaCliente;
    //btnGuardar = view.down('#BtnGuardar'),
    data = form.getForm().getValues();
      
    if (!form.getForm().isValid()) {
        this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
        return;
    }
    if (data.passwordUsuario !== data.cpasswordUsuario) {
        this.mnj('Error', 'Las contraseñas no coinciden.', Ext.MessageBox.ERROR);
        return;
    }
    view.mask('Procesando...');
      //        me = this,
    
    var codEstado = view.down('#cmbEstados').getValue();
    var objClientes = Ext.create('FRT.model.clientes.Cliente', {
        codCliente: data.codCliente, razonSocialClie: data.razonSocialClie, rfcClie: data.rfcClie,
        calleClie: data.calleClie, numExtClie: data.numExtClie, numIntClie: data.numIntClie, coloniaClie: data.coloniaClie, codMunicipio: data.codMunicipio,
        codEstado: codEstado, localidadClie: data.localidadClie, paisClie: data.paisClie, cpClie: data.cpClie
    });
    objClientes.setUsuario(Ext.create('FRT.model.usuarios.Usuario', {
        codUsuario: data.codUsuario, loginUsuario: data.loginUsuario, passwordUsuario: data.passwordUsuario, nombreUsuario: data.nombreUsuario, apePaterUsuario: data.apePaterUsuario,
        apeMaterUsuario: data.apeMaterUsuario, mailUsuario: data.mailUsuario, telefonoUsuario: data.telefonoUsuario
    }));
    objClientes.getUsuario().setPerfil(Ext.create('FRT.model.perfiles.Perfil', { codPerfil: 1 }))

    Ext.Ajax.request({
        url: FRT.Routes.root + 'Clientes/Guardar',
        encode: true,
        params: Ext.encode(objClientes.getData(true)),
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        scope: this,
        success: function (response) {
            view.unmask();
            var resp = Ext.JSON.decode(response.responseText);
            if (resp.success == true) {
                if (this.editar == false) {
                    this.iniciarSession()
                    return;
                }
                var view2 = this.getView();
                var vistaCliente = view2.down('#FormDocumento #datosCliente');
                vistaCliente.setVisible(false);
                var vistaFactura = view2.down('#FormDocumento #facturaFielset');
                var documentoDescarga = view2.down('#FormDocumento #documentoDescarga');
                var botonliente = view2.down('#FormDocumento #BtnDatosCliente');
                //var botonFacturar = view2.down('#FormDocumento #BtnGuardarF');
                //var BtnConsultarF = view2.down('#FormDocumento #BtnConsultarF');
                vistaFactura.setVisible(true);
                documentoDescarga.setVisible(true);
                botonliente.setVisible(false);
                Ext.ComponentQuery.query('#FormDocumento #botonestb')[0].setVisible(true)
                //botonFacturar.setVisible(true);
                //BtnConsultarF.setVisible(true); 
                
              
            }
                
               
        },
        failure: function (o, r, n) {
               
            this.limpiarForm();
            view.unmask();
        }
           
    });
        
}
  , consultarFacturas: function () {
      Ext.ComponentQuery.query('main #contentPanel')[0].removeAll();
      var documentosConsultaMain = Ext.create('FRT.view.documentos.TicketsConsultaMain');
      Ext.ComponentQuery.query('main #contentPanel')[0].add(documentosConsultaMain);
  }
  , visualizarPdf: function (me, record, item, index, e, eOpts) {
      //this.descargarXML(34)
      var view = this.getView();
      var codDocumento = view.down('#FormDocumento #codDocumento');
      codDocumento.setValue(34)
      var file = codDocumento.getValue() + '.pdf';
    
      if (file) {
          var dir = FRT.Routes.root + 'Tickets/pdf/' + file;
          window.open(dir, '_blank', 'left=10, top=10,width=600,height=600');
      }
      else
          this.mnj('Aviso', 'No se puede mostar documento PDF! debe generar una Factura.');
  },
  retimbrarCFDI: function (me, record, item, index, e, eOpts) { 
      var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
      this.getView().mask();
      Ext.Ajax.request({
          url: FRT.Routes.root + 'Tickets/RetimbrarCFDI',
          encode: true,
          params: Ext.encode(objDocumento.getData(true)),
          headers: { 'Content-Type': 'application/json' },
          method: 'post',
          scope: this,
          success: function (response) {

              var resp = Ext.JSON.decode(response.responseText);
              if (resp.success == true) {
                  this.cargarGridTickets();
                  url = FRT.Routes.root + 'Tickets/RepImpresaCFDI';
                  this.open('POST', url, record.get('codDocumento'), '_blank');
              } else {
                  this.mnj('Error', 'Folio no Facturado, </br> Favor de reportarlo al administrador', Ext.MessageBox.ERROR);
                  this.getView().unmask();
                  return;

              }

              this.getView().unmask();

          },
          failure: function (o, r, n) {
              this.getView().unmask();
          }

      });
  },

 descargarArchivoMain: function (me, record, item, index, e, eOpts) {
    if (item === "actionCancelarCFDI") {
        this.cancelarCFDI(me, record, item, index, e, eOpts);

    }
    var uuid = record.get("uuid");
    if (uuid.length !== 36) {
        this.retimbrarCFDI(me, record, item, index, e, eOpts);
        return;
        
        //this.mnj('Error', 'Folio no Facturado, </br> Favor de reportarlo al administrador', Ext.MessageBox.ERROR);
        //return;
    }
    if (item ==="actionPdf") {
        
        url = FRT.Routes.root + 'Tickets/RepImpresaCFDI';
        this.open('POST', url, record.get('codDocumento'), '_blank');
       
    }
    if (item === "actionXml") {
        this.descargarXML(me, record, item, index, e, eOpts);

    }
    if (item === "actionEnviarCorreo") {
        this.enviarCorreoCFDI(me, record, item, index, e, eOpts);

    }

 },
 enviarCorreoCFDI: function (me, record, item, index, e, eOpts) {
 
     var objDocumento=Ext.create('FRT.model.documentos.Documento',record.getData());
     this.getView().mask();
     Ext.Ajax.request({
         url: FRT.Routes.root + 'Tickets/enviarCorreoCFDI',
         encode: true,
         params: Ext.encode(objDocumento.getData(true)),
         headers: { 'Content-Type': 'application/json' },
         method: 'post',
         scope: this,
         success: function (response) {

             var resp = Ext.JSON.decode(response.responseText);
             if (resp.success == true) {
                 this.mnj('Aviso', 'Los Archivos se han enviado al correo Registrado.');
             }

             this.getView().unmask();

         },
         failure: function (o, r, n) {
             this.getView().unmask();
         }

     });
 },
cancelarCFDI: function (me, record, item, index, e, eOpts) {
   
    var objDocumento=Ext.create('FRT.model.documentos.Documento',record.getData());
    this.getView().mask();
    Ext.Ajax.request({
        url: FRT.Routes.root + 'Tickets/CancelarCFDI',
        encode: true,
        params: Ext.encode(objDocumento.getData(true)),
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        scope: this,
        success: function (response) {

            var resp = Ext.JSON.decode(response.responseText);
            if (resp.success == true) {
                this.cargarGridTickets();
            }

            this.getView().unmask();

        },
        failure: function (o, r, n) {

            this.limpiarForm();
            this.getView().unmask();
        }

    });
    }
    , descargarXML: function (me, record, item, index, e, eOpts) {
        var view = this.getView();
        var codDocumento = record.get('codDocumento')

        var empresa = record.getEmpresa();
        var fileXML = empresa.get('rfcEmp') + 'Resultado/' + record.get('uuid') + '.xml';// codDocumento + '.xml';

        var Path = record.get('rutaPdf');

        if ( fileXML) {
            //si es esa ruta? bueno lo dejamos ahi
            var dir = FRT.Routes.root + 'Models/' + 'TicketsPac/' + fileXML;
            var link = document.createElement("a");
            link.download = fileXML;
            link.href = dir;
            link.click();

        } else
            this.mnj('Aviso', 'No se puede realizar la descarga! debe generar una Factura.');
    }
,  open: function (verb, url, data, target) {
         
    var form = document.createElement("form");
    form.action = url;
    form.method = verb;
    form.style = "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400"
    form.target = target || "_self";
    if (data) {
        // for (var key in data) {
        var input = document.createElement("INPUT");
        input.name = 'codDocumento';
        // input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
        input.value = data
        form.appendChild(input);
        //}
    }
    form.style.display = 'none';
    document.body.appendChild(form);
    form.submit();
}
 , descargarArchivos: function (me, record, item, index, e, eOpts)
 {
     var view = this.getView();
     var codDocumento = record.get('codDocumento')

     var filePDF = record.get('rutaXml') + '.pdf';// codDocumento + '.pdf';
     var fileXML = record.get('rutaXml') + '.xml';// codDocumento + '.xml';

     var Path = record.get('rutaPdf');

    if (filePDF && fileXML) {
        //si es esa ruta? bueno lo dejamos ahi
        var dir = FRT.Routes.root + 'Tickets/' + fileXML;
            var link = document.createElement("a");
            link.download = fileXML;
            link.href = dir;
            link.click();

            var dir2 = FRT.Routes.root + 'Tickets/' + filePDF;
            var link2 = document.createElement("a");
            link2.download = filePDF;
            link2.href = dir2;
            link2.click();

     }else
         this.mnj('Aviso', 'No se puede realizar la descarga! debe generar una Factura.');
    }

    ,ObtenerPdfXml: function (me, record, item, index, e, eOpts) {
        var view = this.getView();
        var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
        // no hace la peticion al servidor params: Ext.encode(objClientes.getData(true)),
         Ext.Ajax.request({
             url: FRT.Routes.root + 'Tickets/Copiar',
            encode: true,
            params: Ext.encode(objDocumento.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,

                 
            success: function (response) {

                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.descargarArchivos(me, record, item, index, e, eOpts);
                    //this.mnj('Mensaje', ' Proceso Exitoso', Ext.MessageBox.INFO);
                    //no hace nada
                }
            },
            failure: function (o, r, n) {
                this.mnj('Error', 'No se copiaron los archivos PDF y XML', Ext.MessageBox.ERROR);
            }
        });

}    

    ,mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    buscarDoc: function (me, e, eOpts) {
         var busqcampo = this.getView().down('#Busqcampo').getValue();
         var busqvalor = this.getView().down('#Busqvalor').getValue();

         if ((e.keyCode === Ext.EventObject.ENTER) || ((e.keyCode === Ext.EventObject.BACKSPACE || e.keyCode === Ext.EventObject.DELETE) && me.getValue() == "")) {

             this.cargarDoc({ busqcampo: busqcampo, busqvalor: busqvalor });
         } else if (me.getItemId() === 'Busqcampo') {
             this.cargarDoc({ busqcampo: busqcampo, busqvalor: busqvalor });
         }
     },

     cargarDoc: function (params) {
         var store = Ext.getStore('Tickets.Tickets');
         store.load({
             params: params ? params : {},
             scope: this,
             callback: function (records, operation, success) {
             }
         });
     },
    
});
