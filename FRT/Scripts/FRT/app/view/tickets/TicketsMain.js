

Ext.define('FRT.view.tickets.TicketsMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.ticketsMain',
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    requires: [
    //        'FRT.view.documentos.documentosMainViewModel',
        'FRT.view.tickets.ticketsMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Spacer'

    ],
    controller: 'ticketsmain',
    initComponent: function () {
        var me = this,
        storeFormasPagos = Ext.create('FRT.store.formasPagos.FormasPagos'),
        storeTickets = Ext.create('FRT.store.tickets.Tickets'),
        cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            autoCancel: false,
            listeners: {
                edit: 'modificarFormaPago'

            }
        });
        Ext.applyIf(me, {
            dockedItems: [
              {
                  xtype: 'toolbar',
                  dock: 'top',
                  items: [
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Label'
                      }
                  ]
              }
            ],
            items: [
                   {
                       xtype: 'gridpanel',
                       store: storeTickets,
                       itemId: 'gridTickets',
                       style: {
                           'background-color': 'transparent',
                           'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                           'background-size': '100% 100%',
                           'background-repeat': 'no-repeat',
                           'border-radius': '7px 7px 0px 0px',
                           'color': 'white'
                       },
                       flex: 1,
                       dockedItems: [
                           {
                               dock: 'top',
                               height: 70,
                               xtype: 'toolbar',
                               style: {
                                   'background-color': 'transparent'
                               },
                               items: [
                                   {
                                       xtype: 'tbspacer',
                                       width: 30,
                                   },
                                  {
                                      xtype: 'textfield',
                                      cls: 'rounded',
                                      itemId: 'txtBuscar',
                                      width: 400,
                                      fieldLabel: 'Buscar',
                                      labelAlign: 'top',
                                      labelWidth: 50,
                                      emptyText: 'Criterio de Busqueda',
                                      enableKeyEvents: true,
                                      listeners: {
                                          keypress: 'buscarTickets',
                                          change: 'changeBuscarTickets'
                                      }
                                  },
                                   
                                   {
                                       xtype: 'tbspacer',
                                       width: 30,
                                   },
                                   {
                                       xtype: 'datefield',
                                       cls: 'rounded',
                                       labelAlign: 'top',
                                       width: 400,
                                       fieldLabel: 'Fecha',
                                       itemId: 'txtFecha',
                                       name: 'txtFecha',
                                       //value: new Date(),
                                       maxValue: new Date(),
                                       enableKeyEvents: true,
                                       listeners: {
                                           keypress: 'buscarTickets',
                                           change: 'changeBuscarTickets'
                                       }
                                   },
                                   
                                   {
                                       xtype: 'button',
                                       text: 'Reporte',
                                       itemId: 'btnReporte',
                                       //hidden: !FRT.common.FnConnSeg.isVisible('Tickets', 'RepTickets'),
                                       listeners: {
                                           click: 'crearReporteTickets'
                                       }

                                   },
                                    {
                                        xtype: 'tbspacer',
                                        flex: 1
                                    },
                                     {
                                         xtype: 'button',
                                         text: 'Agregar',
                                         itemId: 'btnNuevo',
                                         icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/file-add.png',
                                         hidden: !FRT.common.FnConnSeg.isVisible('Tickets', 'Guardar'),
                                         listeners: {
                                             click: 'agregarTicket'
                                         }

                                     },
                                    {
                                        xtype: 'tbspacer',
                                        flex: 1
                                    }
                               ]
                           },
                           {
                               xtype: 'pagingtoolbar',
                               itemId: 'paginadorTickets',
                               cls: 'go-paging-tb',
                               dock: 'bottom',
                               displayInfo: true,
                               //style: {
                               //    'background-color': '#174360',
                               //    //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                               //    'background-size': '100% 100%',
                               //    'background-repeat': 'no-repeat',
                               //   // 'border-radius': '0px 0px 7px 7px',
                               //    'color': 'red',

                               //},
                               store: storeTickets,
                               listeners: {
                                   beforechange: 'cargarTicketsToolBar'
                               }
                           }
                       ],
                       
                       columns: [
                           {
                               xtype: 'rownumberer',
                               width: 35
                           },
                              {
                                  xtype: 'actioncolumn',
                                  width: 40,
                                  align: "center",
                                  itemId:"colCancelar",
                                  hidden: !FRT.common.FnConnSeg.isVisible('Tickets', 'Eliminar'),
                                  items: [
                                      {
                                          icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Delete.png',
                                          tooltip: 'Cancelar',
                                          handler: function (grid, rowIndex, colIndex) {
                                              var rec = grid.getStore().getAt(rowIndex);
                                              this.up('grid').fireEvent('itemclick', grid, rec, 'actionCancelar', rowIndex, colIndex);

                                          }

                                      }
                                  ]
                              },
                           {
                               xtype: 'actioncolumn',
                               width: 40,
                               align: "center",
                               hidden: !FRT.common.FnConnSeg.isVisible('Tickets', 'CancelacionCFDI'),
                               items: [
                                   {
                                       icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Delete.png',
                                       tooltip: 'Cancelar CFDI',
                                       handler: function (grid, rowIndex, colIndex) {
                                           var rec = grid.getStore().getAt(rowIndex);
                                           this.up('grid').fireEvent('itemclick', grid, rec, 'actionCancelarCFDI', rowIndex, colIndex);

                                       }

                                   }
                               ]
                           },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'empresa',
                                text: 'Restaurante',
                                flex: 2,
                                renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                    var empresa = record.getEmpresa();
                                    return empresa.get('nombreEmp');
                                }
                            },
                           {
                               xtype: 'gridcolumn',
                               width: 138,
                               dataIndex: 'fechaCosumo',
                               text: 'Fecha Consumo',
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   return Ext.Date.format(value, 'd/m/Y');
                               }

                           },
                           {
                               xtype: 'gridcolumn',
                               flex: 1,
                               dataIndex: 'folio',
                               text: 'Folio'
                           },
                           {
                               xtype: 'gridcolumn',
                               dataIndex: 'StatusDocumento',
                               text: 'Estatus',
                               flex: 1,
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   s = record.getStatusDocumento();
                                   return s.get('Descripcion');
                               }
                               
                           },
                           {
                               xtype: 'gridcolumn',
                               flex: 1,
                               dataIndex: 'formaPago',
                               text: 'Forma Pago',
                               editor:
                                    {
                                        xtype: 'combobox',
                                        name: 'codFormaPago',
                                        forceSelection: true,
                                        itemId: 'cmbFormasPagos',
                                        displayField: 'descripcionClave',
                                        queryMode: 'local',
                                        store: storeFormasPagos,
                                        enableKeyEvents: true,
                                        valueField: 'codFormaPago',
                                        allowBlank: false,
                                        autoSelect: false,
                                        anyMatch: true
                                    },
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   var formaPago = record.getFormaPago();
                                   return (value != null ? value : formaPago.get('clave'))
                               }
                           },
                           {
                               xtype: 'gridcolumn',
                               width: 127,
                               align: 'right',
                               dataIndex: 'propina',
                               text: 'Propina',
                               flex: 1,
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   return Ext.util.Format.usMoney(value);
                               }
                           },
                           {
                               xtype: 'gridcolumn',
                               width: 127,
                               align: 'right',
                               dataIndex: 'total',
                               text: 'Total',
                               flex: 1,
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   return Ext.util.Format.usMoney(value);
                               }
                           }
                       ],

                       plugins: [cellEditing],
                       listeners: {
                           itemclick: 'eliminarTickets'

                       }
                   }
            ]
            //    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }

});