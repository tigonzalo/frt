﻿Ext.define('FRT.view.main.Main', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.main',

    requires: [
        'FRT.view.main.MainController',
        'Ext.menu.Menu',
        'Ext.menu.Item'
    ],

    layout: 'border',
    controller: 'main',
    style: {
        'background-color': 'transparent',
        'text-align': 'center',
        'padding': '15px'
    },
    initComponent: function () {
        var me = this;
        storeUsosCFDI = Ext.create('FRT.store.usosCFDI.UsosCFDI'),
        storeEmpresas = Ext.create('FRT.store.empresas.Empresas');
        //var nombreUsuario = FRT.Session.usuario ? FRT.Session.usuario.nombreUsuario + ' ' + FRT.Session.usuario.apePaterUsuario + ' ' + FRT.Session.usuario.apeMaterUsuario + ' (' + FRT.Session.usuario.loginUsuario + ')' : "";
        var nombreUsuario = FRT.Session.usuario ?  ' (' + FRT.Session.usuario.loginUsuario + ')' : "";

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    region: 'north',
                    height: 110,
                    itemId: 'headerPanel',
                    //style: {
                    //    'background-color': 'transparent',
                    //},
                    //title: '<a href="#"style="text-decoration: none; color:rgb(255, 255, 255);">FACTURA REST</a>',
                    html: '<img src="' + FRT.Routes.root + 'Scripts/FRT/Imagenes/facturaRest.png" align="middle">',
                    listeners: {
                        render: function (p) {
                            p.getEl().on('click', 'mostrarLogin');
                            // FRT.common.FnConnSeg.toolTip('Podrás obtener tu Usuario y contraseña.', p, '63 0 0 60');
                        }
                    },
					items: [
                        {
                            xtype: 'container',
                            html: '<div style="text-align: right; padding-top: 10px; padding-right: 10px;">soportefacturarest@gmail.com</div>'
                    }
                    ]
                },
                {
                    xtype: 'panel',
                    region: 'west',
                    split: true,
                    itemId: 'menuPanel',
                    padding: '10,10,10,10',
                    width: 250,
                    layout: 'accordion',
                    collapseDirection: 'left',
                    title: 'Menu',
                    collapseDirection: 'left',
                    collapsible: true,
                    hidden: true,
                    style: {
                        'background-color': 'transparent',
                    },
                    items: [

                        {
                            xtype: 'panel',
                            //title: 'Gestión',
                            //icon: FRT.Routes.root + 'Scripts/FRT/resources/imagenes/16x16/content.png',
                            items: [
                                {
                                    xtype: 'menu',
                                    floating: false,
                                    itemId: 'menuGestion',
                                    items: [
                                        {
                                            xtype: 'menuitem',
                                            text: 'Realizar Factura',
                                            ////hidden: !FRT.common.FnConnSeg.isVisible('Productos', 'Guardar'),
                                            icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/arrow-1-right.png',
                                            focusable: true,
                                            listeners: {
                                                click: 'mostrarMain'

                                            }
                                        },
                                         {
                                             xtype: 'menuitem',
                                             text: 'Consultar Facturas',
                                             ////hidden: !FRT.common.FnConnSeg.isVisible('Productos', 'Guardar'),
                                             icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/files.png',
                                             focusable: true,
                                             listeners: {
                                                 click: 'mostrarConsultarDocumentosMain'

                                             }
                                         },
                                        {
                                            xtype: 'menuitem',
                                            text: 'Consultar Tickets',
                                            //hidden: !FRT.common.FnConnSeg.isVisible('Clientes', 'Consulta'),
                                            icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/file-edit.png',
                                            focusable: true,
                                            listeners: {
                                                click: 'mostrarTickestMain'

                                            }
                                        }                                           

                                    ]
                                }
                            ]
                        },
                        //{
                        //    xtype: 'panel',
                        //    title: 'CONFIGURACION',
                        //    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/settings.png',
                        //    //hidden: menuConfiguracion,
                        //    items: [
                        //        {
                        //            xtype: 'menu',
                        //            floating: false,
                        //            itemId: 'menu1',
                        //            items: [
                        //                {
                        //                    xtype: 'menuitem',
                        //                    text: 'Empresas',
                        //                    ////hidden: !FRT.common.FnConnSeg.isVisible('Empresas', 'Consulta'),
                        //                    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/home.png',
                        //                    focusable: true,
                        //                    listeners: {
                        //                        click: 'mostrarEmpresasMain'

                        //                    }
                        //                },
                        //                {
                        //                    xtype: 'menuitem',
                        //                    text: 'Sucursales',
                        //                    ////hidden: !FRT.common.FnConnSeg.isVisible('Sucursales', 'Consulta'),
                        //                    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/bank.png',
                        //                    focusable: true,
                        //                    listeners: {
                        //                        click: 'mostrarSucursalesMain'

                        //                    }
                        //                },
                        //                {
                        //                    xtype: 'menuitem',
                        //                    text: 'Usuarios',
                        //                    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/Users.png',
                        //                    ////hidden: !FRT.common.FnConnSeg.isVisible('Usuarios', 'Consulta'),
                        //                    focusable: true,
                        //                    listeners: {
                        //                        click: 'show_users_main'

                        //                    }
                        //                },
                        //                {
                        //                    xtype: 'menuitem',
                        //                    text: 'Perfiles',
                        //                    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/user-alt-1.png',
                        //                    ////hidden: !FRT.common.FnConnSeg.isVisible('Perfiles', 'Consulta'),
                        //                    focusable: true,
                        //                    listeners: {
                        //                        click: 'mostrarPerfilesMain'

                        //                    }

                        //                },
                        //                //{
                        //                //    xtype: 'menuitem',
                        //                //    text: 'TiposDocumentos',
                        //                //    icon: FRT.Routes.root + 'Scripts/FRT/resources/Imagenes/16x16/user-alt-1.png',
                        //                //    ////hidden: !FRT.common.FnConnSeg.isVisible('Perfiles', 'Consulta'),
                        //                //    focusable: true,
                        //                //    listeners: {
                        //                //        click: 'mostrarTiposDocumentosMain'

                        //                //    }
                        //                //}
                        //            ]
                        //        }
                        //    ]
                        //}
                    ]
                },
                {
                    xtype: 'tabpanel',
                    region: 'center',
                    itemId: 'contentPanel',
                    style: {
                        'background-color': 'transparent',
                    },
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            itemId: 'tab1',
                            style: {
                                'background-color': 'transparent'
                            },
                            layout: {
                                type: 'fit'
                            },
                            title: 'Datos del Ticket',
                            items: [
                                {
                                    xtype: 'form',
                                    itemId:'formTicket',
                                    margin: 10,
                                    autoScroll: true,
                                    defaults: {
                                        labelWidth: 180,
                                        maxWidth: 300,
                                        minWidth: 500,
                                        width: 500,

                                    },
                                    layout: {
                                        align: 'center',
                                        padding: 10,
                                        type: 'vbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            maxWidth: 300,
                                            queryMode: 'local',
                                            store: storeEmpresas,
                                            valueField: 'codEmpresa',
                                            name: 'codEmpresa',
                                            itemId: 'cmbEmpresas',
                                            displayField: 'nombreEmp',
                                            forceSelection: true,
                                            allowBlank: false,
                                            autoSelect: false,
                                            minWidth: 500,
                                            width: 500,
                                            fieldLabel: '*Retaurant/empresa',
                                            labelWidth: 180
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Número de folio del Tikect',
                                            name: 'folio',
                                            allowBlank: false,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '*Fecha de consumo',
                                            name: 'fechaCosumo',
                                            format: 'd/m/Y',
                                            allowBlank: false,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'numberfield',
                                            fieldLabel: '*Total consumo',
                                            name: 'total',
                                            allowBlank: false,
                                            msgTarget: 'side',
                                            listeners: {
                                                blur: 'setCantidadFactura'

                                            }
                                        },
                                        {
                                            xtype: 'numberfield',
                                            fieldLabel: '*Cantidad a facturar',
                                            itemId: 'cantidadFactura',
                                            name: 'cantidadFactura',
                                            allowBlank: false,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 0.2
                                        },
                                        {
                                            xtype: 'button',
                                            flex: 1,
                                            height: 40,
                                            maxHeight: 40,
                                            maxWidth: 200,
                                            minHeight: 40,
                                            minWidth: 100,
                                            width: 200,
                                            text: 'Siguiente'
                                            , listeners: {
                                                click: 'btSiguiente'

                                            }
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 0.2
                                        }
                                    ]
                                    
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            itemId: 'tab2',
                            title: 'Datos del cliente',
                            items: [
                                {
                                    xtype: 'form',
                                    itemId: 'formCliente',
                                    margin: 10,
                                    autoScroll: true,
                                    defaults: {
                                        labelWidth: 180,
                                        maxWidth: 300,
                                        minWidth: 500,
                                        width: 500,

                                    },
                                    layout: {
                                        align: 'center',
                                        padding: 10,
                                        type: 'vbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Rfc del cliente',
                                            maxLength: 14,
                                            minLength: 12,
                                            maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                            anchor: '100%',
                                            regex: /^[A-Z&]{3,4}\d{6}\w{3}/,
                                            regexText: 'Se requiere un RFC válido',
                                            allowBlank: false,
                                            allowOnlyWhitespace: false,
                                            msgTarget: 'under',
                                            name: 'rfcClie',
                                            listeners: {
                                                change: function (field, newValue, oldValue) {
                                                    field.setValue(newValue.toUpperCase());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Razon Social',
                                            name: 'razonSocialClie',
                                            listeners: {
                                                change: function (field, newValue, oldValue) {
                                                    field.setValue(newValue.toUpperCase());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            maxWidth: 300,
                                            queryMode: 'local',
                                            store: storeUsosCFDI,
                                            valueField: 'codUsoCFDI',
                                            name: 'codUsoCFDI',
                                            itemId: 'cmbUsosCFDI',
                                            displayField: 'claveDescripcion',
                                            forceSelection: true,
                                            allowBlank: false,
                                            autoSelect: false,
                                            minWidth: 500,
                                            width: 500,
                                            fieldLabel: '*Uso del CFDI'
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Correo del cliente',
                                            allowBlank: false,
                                            allowOnlyWhitespace: false,
                                            maxLength: 50,
                                            maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                            emptyText: 'usuario@mail.com',
                                            name: 'correo',
                                            vtype: 'email',
                                            listeners: {
                                                change: function (field, newValue, oldValue) {
                                                    field.setValue(newValue.toUpperCase());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'button',
                                            flex: 1,
                                            height: 40,
                                            maxHeight: 40,
                                            maxWidth: 200,
                                            minHeight: 40,
                                            minWidth: 100,
                                            width: 200,
                                            text: 'Realizar factura'
                                            , listeners: {
                                                click: 'btFacturar'
                                            }
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        }
                                    
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            itemId: 'tab3',
                            layout: {
                                type: 'fit'
                            },
                            title: 'Recuperar CDFI',
                            disable:true,
                            items: [
                                {
                                    xtype: 'form',
                                    itemId: 'formRecuperarCFDI',
                                    margin: 10,
                                    autoScroll: true,
                                    defaults: {
                                        labelWidth: 180,
                                        maxWidth: 300,
                                        minWidth: 500,
                                        width: 500,

                                    },
                                    layout: {
                                        align: 'center',
                                        padding: 10,
                                        type: 'vbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            maxWidth: 300,
                                            queryMode: 'local',
                                            store: storeEmpresas,
                                            valueField: 'codEmpresa',
                                            name: 'codEmpresa',
                                            itemId: 'cmbEmpresas',
                                            displayField: 'nombreEmp',
                                            forceSelection: true,
                                            allowBlank: false,
                                            autoSelect: false,
                                            minWidth: 500,
                                            width: 500,
                                            fieldLabel: '*Retaurant/empresa',
                                            labelWidth: 180
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Número de folio del Tikect',
                                            name: 'folio',
                                            allowBlank: false,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '*Fecha de consumo',
                                            name: 'fechaCosumo',
                                            format: 'd/m/Y',
                                            allowBlank: false,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'numberfield',
                                            fieldLabel: '*Total consumo',
                                            name: 'total',
                                            allowBlank: false,
                                            msgTarget: 'side',
                                            listeners: {
                                                blur: 'setCantidadFactura'

                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Correo del cliente',
                                            allowBlank: false,
                                            allowOnlyWhitespace: false,
                                            maxLength: 50,
                                            maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                            emptyText: 'usuario@mail.com',
                                            name: 'correo',
                                            vtype: 'email',
                                            listeners: {
                                                change: function (field, newValue, oldValue) {
                                                    field.setValue(newValue.toUpperCase());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 0.2
                                        },
                                        {
                                            xtype: 'button',
                                            flex: 1,
                                            height: 40,
                                            maxHeight: 40,
                                            maxWidth: 200,
                                            minHeight: 40,
                                            minWidth: 100,
                                            width: 200,
                                            text: 'Recuperar CFDI'
                                            , listeners: {
                                                click: 'btRecuperarCFDI'

                                            }
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 0.2
                                        }
                                    ]

                                }
                            ]
                        },
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
    , listeners: {
        afterrender: 'cargarStores'

    }

});