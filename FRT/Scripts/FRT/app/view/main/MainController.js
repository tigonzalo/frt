Ext.define('FRT.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',
    ajaxTicket: null,
    btRecuperarCFDI: function (me, e, eOpts) {
        
        var view = this.getView(),
            //contiene los tabs del main para gestionar la facturacion
            contentPanel = view.down('#contentPanel'),
            formRecuperarCFDI = view.down('#formRecuperarCFDI'),
            data = formRecuperarCFDI.getForm().getValues();

        if (!formRecuperarCFDI.getForm().isValid()) {
                this.mnj('Error', 'Datos no validos en el formulario del ticket.<BR> Favor de verificarlos', Ext.MessageBox.ERROR);
                return;
            }

            view.mask('Procesando Recuperar CFDI...');
            var objDocumentos = Ext.create('FRT.model.documentos.Documento', {
                codEmpresa: data.codEmpresa,
                total: data.total,
                fechaConsumo: data.fechaCosumo,
                folio: data.folio,
                correo: data.correo,
                version33:1
            });
        
            Ext.Ajax.request({
                url: FRT.Routes.root + 'Documentos/RecuperarCFDI',
                encode: true,
                params: Ext.encode(objDocumentos.getData(true)),
                headers: { 'Content-Type': 'application/json' },
                method: 'post',
                scope: this,
                success: function (response) {
                    view.unmask();
                    var resp = Ext.JSON.decode(response.responseText);
                    if (resp.success == true) {
                        this.mnj('Aviso', 'El Sistema ha enviado el CFDI al correo indicado');
                        contentPanel.setActiveTab(0);
                        formRecuperarCFDI.reset();
                    }
                },
                failure: function (o, r, n) {
                    me.mnj('Error', 'En el envio de la información', Ext.MessageBox.ERROR);
                    view.unmask();
                }
            });

        },
    mostrarLogin: function (item, e, eOpts) {
        //FRT.common.FnConnSeg.mostrarLogin();
        v = Ext.create('FRT.view.login.Login');
        v.show();
    },
    resetForm: function () {
        var view = this.getView(),
                formTicket = view.down('#formTicket'),
                formCliente = view.down('#formCliente');
        formTicket.reset();
        formCliente.reset();

    },
    btFacturar: function (me, e, eOpts) {

        var view = this.getView(),
            totalFraccion = 0,
            fraccionado   = false,
        cmbUsosCFDI = view.down('#cmbUsosCFDI'),
        cmbEmpresa = view.down('#cmbEmpresas'),
        contentPanel = view.down('#contentPanel'),
        formTicket = view.down('#formTicket'),
        formCliente = view.down('#formCliente'),
        dataTicket = formTicket.getForm().getValues();
        dataCliente = formCliente.getForm().getValues();

        if (!formTicket.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario del ticket.<BR> Favor de verificarlos', Ext.MessageBox.ERROR);
            contentPanel.setActiveTab(0);
            return;
        }
        if (!formCliente.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR);
            return;
        }

        if (dataTicket.total > dataTicket.cantidadFactura) {
            totalFraccion = dataTicket.cantidadFactura;
            fraccionado = true;
        }

    view.mask('Procesando Factura...');
    var objDocumentos = Ext.create('FRT.model.documentos.Documento', {
        codEmpresa: dataTicket.codEmpresa,
        total:dataTicket.total,
        fechaConsumo: dataTicket.fechaCosumo,
        folio:dataTicket.folio,
        fraccionado: fraccionado,
        totalFraccion: totalFraccion,
        rfcCliente: dataCliente.rfcClie,
        razonSocialCliente: dataCliente.razonSocialClie,
        correo: dataCliente.correo,
        version33:1
    });
    objDocumentos.setUsoCFDI(cmbUsosCFDI.getSelection());
    objDocumentos.setEmpresa(cmbEmpresa.getSelection());
        
    Ext.Ajax.request({
        url: FRT.Routes.root + 'Documentos/Guardar',
        encode: true,
        params: Ext.encode(objDocumentos.getData(true)),
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        scope: this,
        success: function (response) {
            view.unmask();
            var resp = Ext.JSON.decode(response.responseText);
            if (resp.success == true) {
                this.mnj('Aviso', 'El Sistema le enviara su factura y documento XML al correo registrado.');
                contentPanel.setActiveTab(0);
                this.resetForm();
            }

        },
        failure: function (o, r, n) {
            me.mnj('Error', 'En el envio de la información', Ext.MessageBox.ERROR)
            this.limpiarForm();
            view.unmask();
        }
    });

    },
    getTicket: function () {
        var view = this.getView(),
            contentPanel = view.down('#contentPanel'),
            form = view.down('#formTicket'),
            data = form.getValues(),
            
        objTicket = Ext.create('FRT.model.tickets.Ticket', data);
        view.mask('Procesando...');
        

    Ext.Ajax.request({
        url: FRT.Routes.root + 'Tickets/Detalle',
        encode: true,
        params: Ext.encode(objTicket.getData(true)),
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        scope: this,
        success: function (response) {
            
            var resp = Ext.JSON.decode(response.responseText);
            if (resp.success == true) {
                if (resp.List.length > 0) {
                    this.ajaxTicket = resp.List[0];
                    contentPanel.setActiveTab(1);
                }
                else
                    this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR);
            }
            view.unmask();
                
        },
        failure: function (o, r, n) {
            view.unmask();
        }
    });
},
    setCantidadFactura: function (me, event, eOpts) {
        var view = this.getView(),
         cantidadFactura = view.down('#cantidadFactura');
        cantidadFactura.setValue(me.getValue());
    },
    btSiguiente: function (me, e, eOpts) {
        var view = this.getView(),
         tab2 = view.down('#tab2'),
         contentPanel = view.down('#contentPanel'),
        form = view.down('#formTicket');
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        this.getTicket();
        

    },
    cargarStores: function () {
        this.cargarEmpresas();
        this.cargarUsosCFDI();
    },
    cargarUsosCFDI: function () {
        var view = this.getView(),
        cmbUsosCFDI = view.down('#cmbUsosCFDI');
        cmbUsosCFDI.getStore().load();
    },
    cargarEmpresas: function () {
        var view = this.getView(),
        cmbEmpresas = view.down('#cmbEmpresas');
        cmbEmpresas.getStore().load();
    },
    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },
    cerrarSesion: function (me, e, eOpts) {
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Usuarios/Logout',
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {

                //var usuarioLg = Ext.ComponentQuery.query('main #nombreUsuario')[0];
                //usuarioLg.setValue('');
                //this.getView().down('#contentPanel').removeAll();
                //var logo = Ext.create('Ext.Img', {
                //    src: FRT.Routes.root + 'Scripts/FRT/Imagenes/body.png',
                //    //id: 'logo',
                //    width: 372,
                //    height: 140
                //});
                //this.getView().down("#contentPanel").add(logo);
                //var menu = Ext.ComponentQuery.query('main #menuPanel')[0];
                //menu.setVisible(false);
            },
            failure: function (o, r, n) {
                this.limpiarForm();
                view.unmask();

            }

        });

    },
    //mostrarUsuariosMain: function (item, e, eOpts) {
    //    this.getView().down("#contentPanel").removeAll();
    //    var usuariosMain = Ext.create('FRT.view.usuarios.UsuariosMain');
    //    this.getView().down("#contentPanel").add(usuariosMain);
    //},

    cargarOpcionPermisos: function (item, e, eOpts) {
        var me = this;
        me.getView().down('#contentPanel').removeAll();
        this.getView().down('#contentPanel').add({
            html: '<img src="' + FRT.Routes.root + 'Scripts/FRT/Imagenes/body.png" >'
        });
        Ext.MessageBox.confirm('Aviso', 'Se actualizaran las  Opciones y los Permisos. ¿Está Seguro de realizarlo?', function (button) {

            if (button === 'yes') {

                me.getView().mask('Procesando...');

                Ext.Ajax.request({
                    url: FRT.Routes.root + 'Opciones/Guardar',
                    encode: true,
                    //params: null,
                    headers: { 'Content-Type': 'application/json' },
                    method: 'post',
                    scope: this,
                    success: function (response) {
                        var resp = Ext.JSON.decode(response.responseText);
                        if (resp.success == true) {
                            Ext.MessageBox.show({
                                title: 'Aviso',
                                msg: 'La actualización se ha realizado correctamente.',
                                buttons: Ext.MessageBox.OK,
                                animateTarget: 'mb9',
                                icon: Ext.MessageBox.INFO
                            });
                        }

                        me.getView().unmask();
                    },
                    failure: function (o, r, n) {
                        me.getView().unmask();
                    }
                });
            }
        });
    },

    //mostrarPerfilesMain: function (item, e, eOpts) {
    //    this.getView().down('#contentPanel').removeAll();
    //    var perfilesMain = Ext.create('FRT.view.perfiles.PerfilesMain');
    //    this.getView().down('#contentPanel').add(perfilesMain);
    //},

    //mostrarEmpresasMain: function (item, e, eOpts) {
    //    this.getView().down('#contentPanel').removeAll();
    //    var empresasMain = Ext.create('FRT.view.empresas.EmpresasMain');
    //    this.getView().down('#contentPanel').add(empresasMain);
    //}

    //, mostrarGenerarFacturasMain: function (item, e, eOpts) {
    //    var icon = Ext.ComponentQuery.query('#tituloImage')[0];
    //    icon.getEl().dom.src = FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/GenerarFactura.png';
    //    this.getView().down('#contentPanel').removeAll();
    //    var documentosMain = Ext.create('FRT.view.documentos.DocumentosMain');
    //    this.getView().down('#contentPanel').add(documentosMain);
    //    var btnSiguiente = Ext.ComponentQuery.query('clientesmain #BtnDatosCliente')[0];
    //    btnSiguiente.setVisible(true);
    //}
    mostrarConsultarDocumentosMain: function (item, e, eOpts) {
       //var icon = Ext.ComponentQuery.query('#tituloImage')[0];
       //icon.getEl().dom.src = FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/Consultar.png';
       this.getView().down('#contentPanel').removeAll();
       var documentosConsultaMain = Ext.create('FRT.view.documentos.DocumentosConsultaMain');
       this.getView().down('#contentPanel').add(documentosConsultaMain);
    },
    mostrarMain: function (item, e, eOpts) {
        var main = Ext.ComponentQuery.query('main')[0];
        main.destroy();
        Ext.create('FRT.view.main.Main');
    },
    mostrarTickestMain: function (item, e, eOpts) {
       //var icon = Ext.ComponentQuery.query('#tituloImage')[0];
       //icon.getEl().dom.src = FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/Consultar.png';
       this.getView().down('#contentPanel').removeAll();
       var ticketsMain = Ext.create('FRT.view.tickets.TicketsMain');
       this.getView().down('#contentPanel').add(ticketsMain);
       //ticketsMain.getController().facturados = true;
       //ticketsMain.getController().cargarTicketsConsulta();
       //var colCancelar = ticketsMain.down('#colCancelar');
       //var btnNuevo = ticketsMain.down('#btnNuevo');
       //colCancelar.setVisible(false);
       //btnNuevo.setVisible(false);
       
    },
    mostrarTickestMainNF: function (item, e, eOpts) {
        //var icon = Ext.ComponentQuery.query('#tituloImage')[0];
        //icon.getEl().dom.src = FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/Consultar.png';
        
        this.getView().down('#contentPanel').removeAll();
        var ticketsMain = Ext.create('FRT.view.tickets.TicketsMain');
        var colCancelar = ticketsMain.down('#colCancelar');
        var btnNuevo = ticketsMain.down('#btnNuevo');
        colCancelar.setVisible(true);
        btnNuevo.setVisible(true);
        this.getView().down('#contentPanel').add(ticketsMain);
        ticketsMain.getController().facturados = false;
        ticketsMain.getController().cargarTicketsConsulta();
    },


 



});
