

Ext.define('FRT.view.perfiles.PerfilesMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.perfilesmain',

    requires: [
        'FRT.view.perfiles.PerfilesMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View'
    ],


    controller: 'perfilesmain',
    layout: 'border',
    //se crea funcion para 
     initComponent: function () {
       var me = this,
           storePerfiles = Ext.create('FRT.store.perfiles.Perfiles');
       Ext.applyIf(me, {
    items: [
        {
            xtype: 'form',
            height: 300,
            region: 'north',
            collapsible: true,
            collapseMode: 'mini',
            bodyPadding: 10,
            autoScroll: true,
            itemId:'FormPerfil',
            //title: 'Perfiles',
            titleAlign: 'center',
            buttons: [
                {
                    text: 'Agregar',
                    itemId: 'BtnGuardar',
                    listeners: {
                        click: 'guardarPerfil'
                    }
                },
                {
                    text: 'Limpiar',
                    itemId: 'BtnLimpiar',
                    listeners: {
                        click: 'limpiarForm'
                    }
                }
            ],
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Datos del Perfil',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            anchor: '100%',
                            fieldLabel: 'Label',
                            name:'codPerfil'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'Perfil:',
                            maxLength: 100,
                            maxLengthText: 'Se super� la cantidad maxima de caracteres {0}.',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            name:'nombrePerfil'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Opciones-Permisos',
                    items: [
                      {
                          xtype: 'treepanel',
                          itemId: 'TreeOpPermisos',
                          viewConfig: {
                          },
                          listeners: {
                              checkchange: 'checkNode'
                          }
                      }
                    ]
                }
            ]
        },
        {
            xtype: 'gridpanel',
            itemId: 'gridPerfiles',
            region: 'center',
            //title: 'Listado de  Perfiles',
            titleAlign: 'center',
            store: storePerfiles,
            columns: [
                {
                    xtype: 'rownumberer',
                    width: 35
                },
                {
                    xtype: 'actioncolumn',
                    width: 35,
                    items: [
                        {
                          
                           icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Edit.png',
                           tooltip: 'Editar registro',
                           handler: function (grid, rowIndex, colIndex) {
                               var rec = grid.getStore().getAt(rowIndex);
                               this.up('grid').fireEvent('itemclick', grid, rec, 'actionEditar', rowIndex, colIndex);
                           
                       }

                        }
                    ]
                },
                {
                    xtype: 'actioncolumn',
                    width: 35,
                    items: [
                        {
                        
                           icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Delete.png',
                           tooltip: 'Eliminar registro',
                           handler: function (grid, rowIndex, colIndex) {
                               var rec = grid.getStore().getAt(rowIndex);
                               this.up('grid').fireEvent('itemclick', grid, rec, 'actionEliminar', rowIndex, colIndex);
                           
                       }
 
                        }
                    ]
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'nombrePerfil',
                    text: 'Perfil',
                    flex: 1
                }
            ],
            dockedItems: [
                {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: storePerfiles
                }
            ],

            listeners: {
               itemclick: 'accionPerfil'

           }
        }
    ]
//    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }
 
});