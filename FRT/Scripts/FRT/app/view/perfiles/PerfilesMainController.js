

Ext.define('FRT.view.perfiles.PerfilesMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.perfilesmain',

    cargarStores: function (me, eOpts) {
        this.cargarOpcionesPermisos();
        this.cargarPerfiles();

    },

    cargarPerfiles: function () {
        var storePerfiles = Ext.getStore('Perfiles.Perfiles');
        storePerfiles.load({
            //  params: params
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },

    cargarOpcionesPermisos: function () {
        var storeM = Ext.create('FRT.store.opciones.Opciones'),
             view=this.getView();
        storeM.load({
            //params: params
            scope: this,
            callback: function (recordsM, operation, success) {

                var menu = new Array();
                Ext.Array.each(recordsM, function (recordMenu, indx) {

                    objMenu = {
                        text: recordMenu.get('desOpcion'),
                        checked: false,
                        id: recordMenu.get('codOpcion')
                    }
                    menu.push(objMenu);
                });

                var storeA = Ext.create('FRT.store.opciones.Permisos');
                storeA.load({
                    //params: params,
                    scope: this,
                    callback: function (records, operation, success) {

                        Ext.Array.each(menu, function (objMenu, indx) {
                            var acciones = new Array();
                            storeA.clearFilter();
                            storeA.filter(function (record, id) {
                                if (record.get('codOpcion') == objMenu.id)
                                    return true;
                            }

                            );
                            storeA.each(function (accion, indx) {
                                objAccion = {
                                    text: accion.get('desPermiso'),
                                    leaf: true,
                                    checked: false,
                                    codPerfilPermiso: null,
                                    id: accion.get('codPermiso')
                                }
                                acciones.push(objAccion);
                            });


                            objMenu.children = acciones;


                        });
                        var treeStore = Ext.create('Ext.data.TreeStore', {
                            root: {
                                text: 'Menu',
                                expanded: true,
                                id: 'Menu',
                                children: menu
                            }
                        });
                        treeStore.sort('text', 'ASC');
                        view.down('#TreeOpPermisos').setStore(treeStore);
                    }
                });
            }
        });
    },

    accionPerfil: function (me, record, item, index, e, eOpts) {
      
        var view = this.getView(),
        form = view.down('#FormPerfil'),
        btnGuardar = view.down('#BtnGuardar'),
        me = this,
        codPerfil = record.get('codPerfil');

        if (item === 'actionEditar') {
            btnGuardar.show();
            btnGuardar.setText('Guardar');
            form.setCollapsed(false);
            form.getForm().loadRecord(record);
            var fields = form.getForm().getFields().items;
            Ext.Array.each(fields, function (field, indx) {
                field.setReadOnly(false);
            });
            this.asignarRolesAcciones(codPerfil);

        } else if ((item === 'actionEliminar')) {

            Ext.MessageBox.confirm('Aviso', 'Confirmar eliminación', function (button) {

                if (button === 'yes') {

                    me.getView().mask('Procesando...');
                    me.limpiarForm();

                    Ext.Ajax.request({
                        url: FRT.Routes.root + '/Perfiles/Eliminar',
                        encode: true,
                        params: Ext.encode({ codPerfil: codPerfil }),
                        headers: { 'Content-Type': 'application/json' },
                        method: 'post',
                        scope: this,
                        success: function (response) {
                            me.getView().unmask();
                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success == true) {
                                this.limpiarForm();
                                if (Ext.getStore('Perfiles.Perfiles').getCount() == 1)
                                    this.cargarPerfiles();

                                Ext.getStore('Perfiles.Perfiles').remove(record);
                                view.down('#GridPerfiles').getView().refresh();
                               

                            }
                           
                        },
                        failure: function (o, r, n) {
                            me.mnj('Error', 'El registro no fue eliminado', Ext.MessageBox.ERROR)
                            me.getView().unmask();
                        }
                    });
                }
            });

        } else {
            btnGuardar.hide();
            form.setCollapsed(false);
            form.getForm().loadRecord(record);
            var fields = form.getForm().getFields().items;
            Ext.Array.each(fields, function (field, indx) {
                field.setReadOnly(true);
            });
            this.asignarRolesAcciones(record.get('codPerfil'));

        }

    },

    asignarRolesAcciones: function (codPerfil) {
        var view = this.getView(),
            PerfilesPermisos = Ext.create('FRT.store.perfiles.PerfilPermisos');

        storeOpcionesPermisos = view.down('#TreeOpPermisos').getStore();
        storeOpcionesPermisos.each(function (record, indx) {

            if (record.get('parentId') == "Menu") {
                Ext.Array.each(record.childNodes, function (accion, indx) {
                    accion.set('checked', false);
                    accion.set('codPerfilPermiso', null);
                    accion.parentNode.set('checked', false);
                });
            }
        });
        PerfilesPermisos.load({
            params: { codPerfil: codPerfil },
            scope: this,
            callback: function (records, operation, success) {

                Ext.Array.each(records, function (recd, idx) {
                    storeOpcionesPermisos.each(function (record, indx) {
                        if (record.get('parentId') == "Menu") {
                            Ext.Array.each(record.childNodes, function (permiso, indx) {
                              
                                if (permiso.id == recd.get('Permiso').codPermiso) {
                                    permiso.set('checked', true);
                                    permiso.set('codPerfilPermiso', recd.get('codPerfilPermiso'));
                                    permiso.parentNode.set('checked', true)
                                }
                            });
                        }

                    });

                });

            }
        });
    },

    guardarPerfil: function (me, e, eOpts) {

        var view = this.getView(),
        form = view.down('#FormPerfil'),
        data = form.getForm().getValues();

        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando...');
        var objPerfil = Ext.create('FRT.model.perfiles.Perfil', data),
           storeOpcionesPermisos = view.down('#TreeOpPermisos').getStore();
            codPerfil = objPerfil.get('codPerfil');
      
            storeOpcionesPermisos.each(function (record, indx) {

            if (record.get('parentId') == "Menu") {
                Ext.Array.each(record.childNodes, function (permiso, indx) {
                    if (permiso.get('checked') == true && permiso.get('codPerfilPermiso') * 1 == 0) {
                        objPerfil.perfilPermisos().add(Ext.create('FRT.model.perfiles.PerfilPermiso', { codPerfil: codPerfil, Permiso: { codPermiso: permiso.id } }));
                    }
                        
                    else if (permiso.get('checked') == false && permiso.get('codPerfilPermiso') * 1 > 0) {
                     
                        objPerfil.perfilPermisos().add(Ext.create('FRT.model.perfiles.PerfilPermiso', { codPerfilPermiso: (permiso.get('codPerfilPermiso') * -1), codPerfil: codPerfil, Permiso: { codPermiso: permiso.id } }));
                    }
                });
            }


        });
        Ext.Ajax.request({
            url: FRT.Routes.root + '/Perfiles/Guardar',
            encode: true,
            params: Ext.encode(objPerfil.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                view.unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.limpiarForm();
                    this.cargarStores();
                }

               
            },
            failure: function (o, r, n) {
             
                this.limpiarForm();
               view.unmask();
            }
        });

    },

    limpiarForm: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormPerfil').getForm();
        form.reset();
        var fields = form.getFields().items;
        Ext.Array.each(fields, function (field, indx) {
            field.setReadOnly(false);
        });
        view.down('#BtnGuardar').show();
        view.down('#BtnGuardar').setText('Agregar');

        storeOpcionesPermisos = view.down('#TreeOpPermisos').getStore();
        storeOpcionesPermisos.each(function (record, indx) {

            if (record.get('parentId') == "Menu") {
                Ext.Array.each(record.childNodes, function (accion, indx) {
                    accion.set('checked', false);
                    accion.set('codPerfilPermiso', null);
                    accion.parentNode.set('checked', false);
                });
            }
        });
    },

    checkNode: function (node, state) {
        if (node.get('parentId') == "Menu") {
            node.cascadeBy(function (node) {
                node.set('checked', state);
                return true;
            });
        }
        else if (node.get('leaf') == true) {
            if (state)
                node.parentNode.set('checked', true);
        }
        else if (node.get('text').indexOf('Menu') > -1) {
            //node.set('checked', false);
        }
    },

    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    }


});
