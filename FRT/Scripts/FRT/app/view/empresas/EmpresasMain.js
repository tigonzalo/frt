

Ext.define('FRT.view.empresas.EmpresasMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.empresasmain',

    requires: [
       'FRT.view.empresas.EmpresasMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.ComboBox',
        'Ext.form.Label',
        'Ext.form.field.File',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.toolbar.Paging'
    ],

    controller: 'empresasMain',
    initComponent: function () {
        var me = this,
            storeEmpresas = Ext.create('FRT.store.empresas.Empresas'),
            storeEstados = Ext.create('FRT.store.estados.Estados'),
            storeMunicipios = Ext.create('FRT.store.municipios.Municipios');
       Ext.applyIf(me, {
    items: [
        {
            xtype: 'form',
            itemId: 'FormEmpresa',
             layout: 'fit',
            buttons: [
                {
                    text: 'Agregar',
                    itemId: 'BtnGuardar',
                    disabled: !FRT.common.FnConnSeg.isVisible('Empresas', 'Guardar'),
                    listeners: {
                        click: 'guardarEmpresa'
                    }
                },
                {
                    text: 'Limpiar',
                    itemId: 'BtnLimpiar',
                    listeners: {
                        click: 'limpiarForm'
                    }
                }
            ],

                bodyPadding: 10,
                collapsible: true,
                collapseMode: 'mini',
            title: 'Empresas',
            titleAlign: 'center',
            items: [
                {
                    xtype: 'fieldset',
                    width: 828,
                    title: 'Datos de la Empresa',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            width: 380,
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    fieldLabel: 'Label',
                                    name:'codEmpresa'
                                },
                                {
                                    xtype: 'textfield',
                                    width: 330,
                                    fieldLabel: 'Empresa',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'nombreEmp',
                                    maxLength: 60,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Razon Social',
                                    width: 330,
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'razonSocialEmp',
                                    maxLength: 60,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'RFC:',
                                    width: 330,
                                    maxLength: 14,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    regex: /[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?/,
                                    regexText: 'Se requiere un RFC válido',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'rfcEmp',
                                    listeners: {
                                        change: function (field, newValue, oldValue) {
                                            field.setValue(newValue.toUpperCase());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Calle',
                                    width: 330,
                                    maxLength: 60,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name:'calleEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    width: 330,
                                    fieldLabel: 'No. Exterior',
                                    allowBlank: false,
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name:'noExteriorEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'No. Interior',
                                    width: 330,
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    name:'noInteriorEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Colonia',
                                    width: 330,
                                    maxLength: 50,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name:'coloniaEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Localidad',
                                    width: 330,
                                    maxLength: 50,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    name:'localidadEmp'
                                }
                            ]
                                },
                                {
                            xtype: 'container',
                            width: 380,
                            items: [
                            {
                                    xtype: 'combobox',
                                    fieldLabel: 'Estado:',
                                    width: 330,
                                    name: 'codEstado',
                                    store:storeEstados,
                                    displayField: 'nombreEstado',
                                    valueField: 'codEstado',
                                    queryMode: 'local',
                                    allowBlank: false,
                                    forceSelection: true,
                                    allowOnlyWhitespace: false,
                                    listeners: {
                                    select: 'cargarMunicipios',
                                    }
                                    
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Municipio',
                                    width: 330,
                                    name: 'codMunicipio',
                                    store:storeMunicipios,
                                    displayField: 'nombreMunicipio',
                                    valueField: 'codMunicipio',
                                    allowBlank: false,
                                    forceSelection: true,
                                    allowOnlyWhitespace: false,
                                    queryMode: 'local'
                                    
                                },
                                
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'CP',
                                    width: 330,
                                    maxLength: 5,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    regex: /^\d+(\.\d{1,2})?$/,
                                    regexText: 'Se requiere un CP válido',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name:'cpEmp',

                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Pais',
                                    width: 330,
                                    maxLength: 30,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    value:'Mexico',
                                    name:'paisEmp'
                        },
                        {
                                    xtype: 'textfield',
                                    fieldLabel: 'Contacto:',
                                    width: 330,
                                    maxLength: 50,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'contactoEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Telefono',
                                    width: 330,
                                    regex: /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/,
                                    regexText: 'Se requiere un teléfono válido ejemplo: 123-456-7890.',
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'telefonoEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Ruta Contpaq:',
                                    width: 330,
                                    maxLength: 200,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'rutaEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Contraseña CFDI',
                                    width: 330,
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'passwordCFDI'
                                },
                                ]
                         },
                        {
                            xtype: 'container',
                            width: 380,
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Configuración para el Envio de Correos',
                                    items: [
                                       {
                                           xtype: 'textfield',
                                           width: 250,
                                           fieldLabel: 'Mail:',
                                           allowBlank: false,
                                          
                                           maxLength: 50,
                                           maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                           vtype: 'email',
                                           allowOnlyWhitespace: false,
                                           name: 'mailFromEmp'
                                       },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Contraseña',
                                    width: 250,
                                    inputType: 'password',
                                    allowBlank: false,
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name: 'mailPasswordEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Host',
                                    width: 250,
                                    maxLength: 50,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'mailHostEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Puerto',
                                    width: 250,
                                    allowBlank: false,
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name: 'mailPuertoEmp'
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'SSL:',
                                    width: 250,
                                    allowBlank: false,
                                     editable: false,
                                     store: [['True', 'Si'], ['False', 'No']],
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name: 'mailSslEmp'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Ruta XML:',
                                    width: 250,
                                    allowBlank: false,
                                    maxLength: 400,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name: 'PathArchivosCFDI'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Ruta PDF:',
                                    width: 250,
                                    allowBlank: false,
                                    maxLength: 400,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    allowOnlyWhitespace: false,
                                    name: 'PathPlantillaPDF'
                                },

                                {
                                    xtype: 'filefield',
                                    fieldLabel: 'Logo:',
                                    itemId: 'imgEmp',
                                    width: 250,
                                    accept: 'image',
                                    name: 'imgEmp'
                                }
                                    ]
                                }
                                
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'gridpanel',
            itemId: 'gridEmpresas',
            title: 'Listado de Empresas',
            titleAlign: 'center',
            store: storeEmpresas,
            columns: [
                {
                    xtype: 'rownumberer',
                    text: ''
                },
                {
                    xtype: 'actioncolumn',
                    hidden: !FRT.common.FnConnSeg.isVisible('Empresas', 'Guardar'),
                    width: 35,
                    items: [
                        {
                            icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Edit.png',
                            tooltip: 'Editar registro',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                this.up('grid').fireEvent('itemclick', grid, rec, 'actionEditar', rowIndex, colIndex);

                            }
                        }
                    ]
                },
                {
                    xtype: 'actioncolumn',
                    hidden: !FRT.common.FnConnSeg.isVisible('Empresas', 'Eliminar'),
                    width: 35,
                    items: [
                        {


                            icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Delete.png',
                            tooltip: 'Eliminar registro',
                            handler: function (grid, rowIndex, colIndex) {
                                var rec = grid.getStore().getAt(rowIndex);
                                this.up('grid').fireEvent('itemclick', grid, rec, 'actionEliminar', rowIndex, colIndex);

                            }
                        }
                    ]
                },
                {
                    xtype: 'gridcolumn',
                    width: 300,
                    dataIndex: 'nombreEmp',
                    text: 'Nombre'
                },
                {
                    xtype: 'gridcolumn',
                    width: 300,
                    dataIndex: 'razonSocialEmp',
                    text: 'Razon Social'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'rfcEmp',
                    width: 150,
                    text: 'RFC'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contactoEmp',
                    width: 250,
                    text: 'Contacto'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'telefonoEmp',
                    text: 'Telefono'
                }
                
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                         {
                             xtype: 'textfield',
                             fieldLabel: '',
                             itemId: 'Busqvalor',
                             enableKeyEvents: true,
                             listeners: {
                                 keyup: 'buscarEmpresa',
                             }
                         },
                        {
                            xtype: 'combobox',
                            fieldLabel: '',
                            itemId: 'Busqcampo',
                            editable: false,
                            value: 'Nombre',
                            store: [['RFC', 'RFC'], ['Nombre', 'Nombre']],
                            enableKeyEvents: true,
                            listeners: {
                                change: 'buscarEmpresa',
                            }
                        },
                       
                        
                        {
                            xtype: 'image',
                            src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Search.png',
                            height: 20,
                            width: 20
                        }
                    ]
                },
                {
                    xtype: 'pagingtoolbar',
                    itemId:'paginadorEmp',
                    dock: 'bottom',
                    width: 360,
                    displayInfo: true,
                    store: storeEmpresas
                }
            ],
            listeners: {
               itemclick: 'accionEmpresa'

           }
        }
    ]
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }

});