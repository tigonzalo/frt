
Ext.define('FRT.view.empresas.EmpresasMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.empresasMain',

      cargarStores: function (me, eOpts) {
        this.cargarEmpresas();
        this.cargarEstados();
       
    },

    cargarEmpresas: function (me, eOpts) {
        var storeEmpresas = Ext.getStore('empresas.Empresas');
        storeEmpresas.load({
            //  params: params
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },

    cargarEstados: function (me, eOpts) {
        var storeEstados = Ext.getStore('estados.Estados');
        storeEstados.load({
            params: {
                start: null,
                limit:null
            },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    cargarMunicipios: function (me, eOpts) {
       var storeMunicipio = Ext.getStore('municipios.Municipios');
        storeMunicipio.load({
            params: {
                start: null,
                limit:null,
                codEstado: me.getValue()
            },
            scope: this,
            callback: function (records, operation, success) {
            
            }
        });
    },
    cargarMun: function (me, eOpts) {
        var storeMunicipio = Ext.getStore('municipios.Municipios');
        storeMunicipio.load({
            //  params: params
            params: {
                start: null,
                limit: null,
                codEstado: me
            },
            scope: this,
            callback: function (records, operation, success) {

            }
        });
    },
    accionEmpresa: function (me, record, item, index, e, eOpts) {
        
        var view = this.getView();
        form = view.down('#FormEmpresa');
        btnGuardar = view.down('#BtnGuardar');
        data = record.getData(true);
        me = this;
        codEmpresa = record.get('codEmpresa');
        codEstado = record.get('codEstado');
        this.cargarMun(codEstado);
     
        if (item === 'actionEditar') {
            btnGuardar.show();
            btnGuardar.setText('Guardar');
            
            form.getForm().setValues(data);
            var view = this.getView()
            form.setCollapsed(false);
            form.loadRecord(record);
           

        } else if ((item === 'actionEliminar')) {

            Ext.MessageBox.confirm('Aviso', 'Esta seguro de eliminar elregistro?', function (button) {

                if (button === 'yes') {
                    me.getView().mask('Procesando...');
                    me.limpiarForm();

                    Ext.Ajax.request({
                        url: FRT.Routes.root + 'Empresas/Eliminar',
                        encode: true,
                        params: Ext.encode({ codEmpresa: codEmpresa }),
                        headers: { 'Content-Type': 'application/json' },
                        method: 'post',
                        scope: this,

                        success: function (response) {
                            me.getView().unmask();
                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success == true) {
                                me.limpiarForm();

                                if (Ext.getStore('Empresas.Empresas').getCount() == 1)
                                    this.cargarEmpresas();

                                Ext.getStore('Empresas.Empresas').remove(record);
                                view.down('#GridEmpresas').getView().refresh();
                            }
                        },
                        failure: function (o, r, n) {
                            me.mnj('Error', 'El registro no fue eliminado', Ext.MessageBox.ERROR)
                            me.limpiarForm();
                        }
                    });
                }
            }, this );

        } else {
            btnGuardar.hide();
            form.setCollapsed(false);
            form.loadRecord(record);

        }

    },

    guardarEmpresa: function (me, e, eOpts) {
     
        var view = this.getView(),
        form = view.down('#FormEmpresa'),
        data = form.getForm().getValues();

        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando...');
        var objEmpresa = Ext.create('FRT.model.empresas.Empresa', data);
        var file = view.down('#imgEmp').fileInputEl.dom.files[0];
        var nombreFile = file ? file.name : '';
        objEmpresa.set("imgEmp", nombreFile);
        codEmpresa = objEmpresa.get('codEmpresa');
        

        form.getForm().submit({
            url: FRT.Routes.root + '/Empresas/Guardar',
            params: Ext.encode(objEmpresa.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (fp, o) {
                this.limpiarForm();
                view.unmask();
                this.cargarStores();
            },
            failure: function (o, r, n) {
                this.limpiarForm();
                view.unmask();
            }
        });
    },

    limpiarForm: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormEmpresa').getForm();
        form.reset();
        view.down('#BtnGuardar').setText('Agregar');
    },

    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    buscarEmpresa: function (me, e, eOpts) {
        var busqcampo = this.getView().down('#Busqcampo').getValue();
        var busqvalor = this.getView().down('#Busqvalor').getValue();

        if ((e.keyCode === Ext.EventObject.ENTER) || ((e.keyCode === Ext.EventObject.BACKSPACE || e.keyCode === Ext.EventObject.DELETE) && me.getValue() == "")) {

            this.cargarGridEmpresas({ busqcampo: busqcampo, busqvalor: busqvalor });
        } else if (me.getItemId() === 'Busqcampo') {
            this.cargarGridEmpresas({ busqcampo: busqcampo, busqvalor: busqvalor});
        }
    },

    cargarGridEmpresas: function (params) {
        var storeEmpresas = Ext.getStore('empresas.Empresas');
        storeEmpresas.load({
            params: params?params:'',
            scope: this,
            callback: function (records, operation, success) {
            }
        });
},
});

