Ext.define('FRT.view.clientes.ClienteDetalleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.clientedetalle',
    //editar: true,
    objCliente: null,
    objUsuario: null,
    calcularMontoFraccion: function (me, newValue, oldValue, eOpts) {
        var view = this.getView();
        var montoTicket = view.down('#total').getValue();
        var nFracciones = view.down('#nFracciones').getValue();
        var totalFraccion = montoTicket / nFracciones;
        view.down('#totalFraccion').setValue(totalFraccion.toFixed(2));
    },
    crearFraccionTicket: function (me, newValue, oldValue, eOpts) {
        var view = this.getView();
        this.expandirFraccion(newValue);
    },
    expandirFraccion: function (value) {
        var view = this.getView(),
            fielFraccion = view.down('#fielFraccion'),
            totalFraccion = view.down('#totalFraccion'),
            nFracciones = view.down('#nFracciones');

        if (value) {
            fielFraccion.setExpanded(true);
            nFracciones.setValue(2);
            //totalFraccion.setValue(0);
        } else {
            fielFraccion.setExpanded(false);
            //fielFraccion.setDisabled(true);
        }

    },
    guardarDocumento: function (me, e, eOpts) {
        
        var view = this.getView();
        form = view.down('#FormDocumento');
        cmbUsosCFDI = view.down('#cmbUsosCFDI'),
        cmbEmpresa = view.down('#CmbEmpresa'),
        data = form.getForm().getValues();
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        view.mask('Procesando Factura...');
        var objCliente=Ext.create('FRT.model.clientes.Cliente',Ext.ComponentQuery.query('#FormCliente')[0].getForm().getValues());
        var objDocumentos = Ext.create('FRT.model.documentos.Documento', data);
        objDocumentos.setUsoCFDI(cmbUsosCFDI.getSelection());
        objDocumentos.setEmpresa(cmbEmpresa.getSelection());
        objDocumentos.set('codCliente', objCliente.get('codCliente'));
        objDocumentos.set('codUsuario', objCliente.get('codUsuario'));
        
        Ext.Ajax.request({
            url: FRT.Routes.root + '/Documentos/Guardar',
            encode: true,
            params: Ext.encode(objDocumentos.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.mnj('Aviso', 'Su ticket se ha facturado, en breve le enviaremos su factura y documento XML.')
                    //this.consultarFacturas();
                }

                //this.limpiarForm();
                view.unmask();
                //this.cargarStores();
            },
            failure: function (o, r, n) {
                me.mnj('Error', 'El registro no se registro', Ext.MessageBox.ERROR)
                this.limpiarForm();
                view.unmask();
            }
        });

    },
    cargarEmpresasCmb: function (inicial) {
        
        this.cargarUsosCFDI();
        var me = this;
        var view = this.getView();
        var comboEmpresa = view.down('#CmbEmpresa');
        var proxy = comboEmpresa.getStore().getProxy();
        comboEmpresa.getStore().pageSize = 10;
        proxy.setExtraParam('start', 0);
        proxy.setExtraParam('limit', 10);
        comboEmpresa.getStore().load({
            params: { campo: 'Nombre', valor: comboEmpresa.getRawValue() },
            scope: this,
            url: FRT.Routes.root + 'Empresas/Listado',
            pageSize: 10,
            callback: function (records, operation, success) {
                if (success == true) {
                    if (!inicial == true) {
                         
                        // TUA.common.Common.prepareExpadPickerEvent(me.getCmbPuestos());
                    }
                }
            }
        });
    },
    cargarStores: function (me, eOpts) {
        this.cargarEstados();
        this.cargarUsosCFDI();
    },
    //cargar los ususos  de los CFDI
    cargarUsosCFDI: function () {
        var view = this.getView(),
        cmbUsosCFDI = view.down('#cmbUsosCFDI');
        cmbUsosCFDI.getStore().load();
    },
    cargarEstados: function (me, eOpts) {

        var view = this.getView();

        var storeEstados = view.down('#cmbEstados').getStore();
        storeEstados.load({
            params: !this.editar ? { start: null, limit: null, session: false } : { start: null, limit: null },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    cargarEstados: function (me, eOpts) {
         
        var view = this.getView();
         
        var storeEstados = view.down('#cmbEstados').getStore();
        storeEstados.load({
            params: !this.editar ? { start:null,limit:null,session: false } : { start: null, limit: null },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    
 
    guardarCliente: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormCliente'),
        data = form.getForm().getValues();
       
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        if (data.passwordUsuario !== data.cpasswordUsuario) {
            this.mnj('Error', 'Las contraseņas no coinciden.', Ext.MessageBox.ERROR);
            return;
        }
        view.mask('Procesando...');
        //         
        //        me = this,
        var objClientes = Ext.create('FRT.model.clientes.Cliente', data);
        objClientes.setUsuario(Ext.create('FRT.model.usuarios.Usuario', data));
        objClientes.getUsuario().setPerfil(Ext.create('FRT.model.perfiles.Perfil', { codPerfil: 1 }))

        Ext.Ajax.request({
            url: FRT.Routes.root + '/Clientes/Guardar',
            encode: true,
            params: Ext.encode(objClientes.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                view.unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    
                    var viewC = Ext.ComponentQuery.query('clienteslistadomain')[0];
                    viewC.getController().cargarGridClientes();
                    this.mnj('Info', 'Datos del Cliente Guardados.', Ext.MessageBox.INFO);
                   
                     
                    if (!Ext.isNumber(objClientes.get('codCliente')))
                        this.getView().destroy();
                }
                
                
               
            },
            failure: function (o, r, n) {
                view.unmask();
            }
           
        });
        
    },
    limpiarForm: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormCliente');
        form.reset();
    },
    
    cancelarRegistro: function (item, e, eOpts) {
        Ext.ComponentQuery.query('clientesdetalle')[0].destroy();
    },

  
  
    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    mostrarDetalleCliente: function (record) {
          
            var view = this.getView();
            view.mask('Procesando...');
            this.cargarEstados();
            var objCliente = Ext.create('FRT.model.clientes.Cliente', { codCliente: record.getData().codCliente })
            Ext.Ajax.request({
                url: FRT.Routes.root + 'Clientes/Listado',
                encode: true,
                params: Ext.encode(objCliente.getData(true)),
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                scope: this,
                success: function (response) {
                    var resp = Ext.JSON.decode(response.responseText);
                    if (resp.success == true) {
                        this.objCliente = Ext.create('FRT.model.clientes.Cliente', resp.List[0]).getData(true);
                        this.objUsuario = Ext.create('FRT.model.usuarios.Usuario', resp.List[0].usuario).getData(true);
                        view.down('clienteinfo #cmbEstados').setValue(this.objCliente.Estado.codEstado);
                        view.down('#FormCliente').getForm().setValues(this.objCliente);
                        view.down('#FormCliente').getForm().setValues(this.objUsuario);
                    }

                    view.unmask();
                },
                failure: function (o, r, n) {
                    view.unmask();
                }
            });
       
    },
    cargarMunicipios: function (me, e, eOpts) {
        
        var view = this.getView();
        var storeMunicipios = view.down('#cmbMunicipios').getStore();
        view.mask('Procesando...');
        storeMunicipios.load({
            params: !this.editar ? { start: null, limit: null, codEstado: me.getValue(), session: false } : { start: null, limit: null, codEstado: (Ext.isNumber(me * 1) ? me : me.getValue()) },
            scope: this,
            callback: function (records, operation, success) {
                if (this.editar == true) {
                    view.down('clienteinfo #codMunicipio').setValue(this.objCliente.Municipio.codMunicipio);
                }
                view.unmask();
            },
            failure: function (o, r, n) {
                view.unmask();
            }
        });
    }
});

