Ext.define('FRT.view.clientes.ClientesMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.clientesmain',
       
    requires: [
        'FRT.view.clientes.ClientesMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.ComboBox',
        'Ext.form.*',
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.toolbar.Spacer'
    ],

    controller: 'clientesmain',
    constrain: true,
    frame: true,
 
      initComponent: function () {
          var me = this,
             storePerfiles = Ext.create('FRT.store.clientes.Clientes'),
             storeEstados = Ext.create('FRT.store.estados.Estados'),
             storeMunicipios = Ext.create('FRT.store.municipios.Municipios');
        Ext.applyIf(me, {

            items: [
                 {

                     xtype: 'form',
                     cls: 'pMain',
                     itemId: 'FormCliente',
                     height: 400,
                     autoScroll: true,
                     layout: {
                         type: 'hbox',
                         align: 'stretch'
                     },
                     padding: '5 0 5 0',
                     items: [
                             {
                                 xtype: 'panel',
                                 flex:1
                             },
                            {
                                xtype: 'panel',
                                title: 'DATOS DEL CLIENTE',
                                style: {
                                    'background-color': 'transparent',
                                    'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                    'background-size': '100% 100%',
                                    'background-repeat': 'no-repeat'
                                },
                                width: 300,
                                //height: 100,
                                autoScroll: true,
                                defaults: {
                                    margin:'5 0 0 60',
                                },
                                items: [
                                     {
                                         xtype: 'hiddenfield',
                                         fieldLabel: 'codUsuario',
                                         name: 'codUsuario'
                                     },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Usuario',
                                        labelAlign: 'top',
                                        anchor: '100%',
                                        allowBlank: false,
                                        allowOnlyWhitespace: false,
                                        name: 'loginUsuario',
                                        maxLength: 20,
                                        maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                        msgTarget: 'under'
                    
                                    },
                                    {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
                                        anchor: '100%',
                                        fieldLabel: 'Contraseña',
                                        inputType: 'password',
                                        allowBlank: false,
                                        allowOnlyWhitespace: false,
                                        msgTarget: 'under',
                                        name: 'passwordUsuario'
                                    },
                                    {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
                                        anchor: '100%',
                                        fieldLabel: 'Confirmar Contraseña',
                                        inputType: 'password',
                                        allowBlank: false,
                                        allowOnlyWhitespace: false,
                                        msgTarget: 'under',
                                        name: 'cpasswordUsuario'

                                     },
                                    {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
                                        anchor: '100%',
                                        fieldLabel: 'Correo Electronico',
                                        allowBlank: false,
                                        allowOnlyWhitespace: false,
                                        maxLength: 50,
                                        maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                        emptyText: 'usuario@mail.com',
                                        msgTarget: 'under',
                                        name: 'mailUsuario',
                                        vtype: 'email'
                                    },
                                     {
                                         xtype: 'hiddenfield',
                                         fieldLabel: ' ',
                                         name: 'codCliente'
                                     },
                                    //{
                                    //    xtype: 'textfield',
                                    //    labelAlign: 'top',
                                    //    anchor: '100%',
                                    //    fieldLabel: 'Nombre',
                                    //    allowBlank: false,
                                    //    allowOnlyWhitespace: false,
                                    //    name: 'nombreUsuario',
                                    //    maxLength: 20,
                                    //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'

                                    //},

                                    //{
                                    //    xtype: 'textfield',
                                    //    labelAlign: 'top',
                                    //    fieldLabel: 'Apellido Paterno',
                                    //    anchor: '100%',
                                    //    allowBlank: false,
                                    //    allowOnlyWhitespace: false,
                                    //    name: 'apePaterUsuario',
                                    //    maxLength: 20,
                                    //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',

                                    //},
                                    //{
                                    //    xtype: 'textfield',
                                    //    labelAlign: 'top',
                                    //    fieldLabel: 'Apellido Materno',
                                    //    anchor: '100%',
                                    //    name: 'apeMaterUsuario',
                                    //    maxLength: 20,
                                    //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',


                                    //},
                                    //{
                                    //    xtype: 'textfield',
                                    //    labelAlign: 'top',
                                    //    anchor: '100%',
                                    //    fieldLabel: 'Telefono',
                                    //    name: 'telefonoUsuario',
                                    //    regex: /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/,
                                    //    regexText: 'Se requiere un teléfono válido ejemplo: 123-456-7890.',
                                    //    maxLength: 20,
                                    //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'


                                    //},
                                ]
                            },
                            {
                                xtype: 'tbspacer',
                                width: 40,
                            },
                            {
                                xtype: 'panel',
                                title: 'DATOS FISCALES',
                                autoScroll: true,
                                style: {
                                    'background-color': 'transparent',
                                    'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                    'background-size': '100% 100%',
                                    'background-repeat': 'no-repeat'
                                },
                                width: 600,
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                defaults: {
                                    margin: '10 10 10 40',
                                },
          
                                items: [
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                maxLength: 200,
                                                fieldLabel: 'Razón Social',
                                                anchor: '100%',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                msgTarget: 'under',
                                                name: 'razonSocialClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'RFC',
                                                labelAlign: 'top',
                                                maxLength: 14,
                                                minLength: 12,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                anchor: '100%',
                                                regex: /^[A-Z&]{3,4}\d{6}\w{3}/,
                                                regexText: 'Se requiere un RFC válido',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                msgTarget: 'under',
                                                name: 'rfcClie',
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        field.setValue(newValue.toUpperCase());
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Calle',
                                                maxLength: 70,
                                                anchor: '100%',
                                                allowBlank: false,
                                                msgTarget: 'under',
                                                allowOnlyWhitespace: false,
                                                name: 'calleClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Número Exterior',
                                                maxLength: 12,
                                                anchor: '100%',
                                                allowBlank: false,
                                                msgTarget: 'under',
                                                allowOnlyWhitespace: false,
                                                name: 'numExtClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Número Interior',
                                                maxLength: 12,
                                                msgTarget: 'under',
                                                anchor: '100%',
                                                name: 'numIntClie'
                                            },

                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                anchor: '100%',
                                                msgTarget: 'under',
                                                fieldLabel: 'Código Postal',
                                                maxLength: 6,
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'cpClie',
                                                regex: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/,
                                                regexText: 'Se requiere un código postal válido',

                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                       
                                        items: [
                                          {
                                              xtype: 'textfield',
                                              fieldLabel: 'Colonia',
                                              labelAlign: 'top',
                                              maxLength: 70,
                                              anchor: '100%',
                                              allowBlank: false,
                                              allowOnlyWhitespace: false,
                                              name:'coloniaClie'
                                          },
                                        {
                                            xtype: 'combobox',
                                            labelAlign: 'top',
                                            labelAlign: 'top',
                                            fieldLabel: 'Estado',
                                            anchor: '100%',
                                            name: 'codEstado',
                                            itemId: 'cmbEstados',
                                            store: storeEstados,
                                            displayField: 'nombreEstado',
                                            valueField: 'codEstado',
                                            queryMode: 'local',
                                            emptyText: 'Seleccionar Estado',
                                            allowBlank: false,
                                            forceSelection: true,
                                            allowOnlyWhitespace: false,
                                            msgTarget: 'under',
                                            listeners: {
                                                select: 'cargarMunicipios'
                                            },
                                        },
                                        {
                                            xtype: 'combobox',
                                            labelAlign: 'top',
                                            fieldLabel: 'Municipio',
                                            anchor: '100%',
                                            name: 'codMunicipio',
                                            store: storeMunicipios,
                                            displayField: 'nombreMunicipio',
                                            valueField: 'codMunicipio',
                                            queryMode: 'local',
                                            emptyText: 'Seleccionar Municipio',
                                            forceSelection: true,
                                            msgTarget: 'under',
                                            allowBlank: false,
                                            allowOnlyWhitespace: false,
                                        },
                                        {
                                            xtype: 'textfield',
                                            labelAlign: 'top',
                                            anchor: '100%',
                                            fieldLabel: 'Pais',
                                            maxLength: 50,
                                            name: 'paisClie',
                                            value: 'México',
                                            editable:false,
                                            allowBlank: false,
                                            msgTarget: 'under',
                                            allowOnlyWhitespace: false

                                        },
                                        {
                                            xtype: 'textfield',
                                            labelAlign: 'top',
                                            anchor: '100%',
                                            fieldLabel: 'Localidad',
                                            maxLength: 150,
                                            msgTarget: 'under',
                                            name:'localidadClie'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            height: 30
                                        },
                                          {
                                              xtype: 'container',
                                              layout: {
                                                  type: 'hbox',
                                                  align: 'stretch'
                                              },
                                              padding: '10 10 10 10',
                                              //style: { 'background-color': 'transparent' },
                                              items: [
                                                  
                                                  {
                                                      xtype: 'button',
                                                      flex: 1,
                                                      text: 'Guardar',
                                                      itemId: 'BtnGuardar',
                                                      listeners: {
                                                          click: 'guardarCliente'
                                                      }
                                                  },
                                                   {
                                                       xtype: 'tbspacer',
                                                       width: 10
                                                   },
                                                  {
                                                      xtype: 'button',
                                                      text: 'Cancelar',
                                                      flex: 1,
                                                      itemId: 'BtnLimpiar',
                                                      listeners: {
                                                          click: 'cancelarRegistro'
                                                      }
                                                  },
                                                   {
                                                       text: 'Siguiente',
                                                       hidden: true,
                                                       width: 150,
                                                       xtype: 'button',
                                                        itemId: 'BtnDatosCliente',
                                                            listeners: {
                                                                click: 'siguiente'
                                                            }
                                                        }
                                              ]
                                          }
                                        ]
                                    }
                 
                                ]
                            },
                            {
                                xtype: 'panel',
                                flex: 1
                            }
                     
                     ]
                 }]

        });



        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }
 
});