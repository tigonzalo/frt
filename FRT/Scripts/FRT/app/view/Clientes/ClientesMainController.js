Ext.define('FRT.view.clientes.ClientesMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.clientesmain',
    editar: true,
    objCliente: null,

    siguiente: function () {
        var view = Ext.ComponentQuery.query('documentosmain')[0],
        contoller = view.getController();
        contoller.siguiente();

    },
    cargarStores: function (me, eOpts) {
         
        this.cargarEstados();
        if (this.editar == true) {
            var view = this.getView(),
            btnCancelar = view.down('#BtnLimpiar');
            btnCancelar.hide();
            
        }
 },

    cargarEstados: function (me, eOpts) {
         
        var view = this.getView();
        var storeEstados = view.down('#cmbEstados').getStore();
        storeEstados.load({
            params: !this.editar ? { start:null,limit:null,session: false } : { start: null, limit: null },
            scope: this,
            callback: function (records, operation, success) {
                if (this.editar) {
                    var codUsuario = FRT.Session.usuario.codUsuario;
                    this.cargarCliente({ codUsuario: codUsuario });
                }
            }
        });
    },

    cargarMunicipios: function (me, e, eOpts) {
     
        var storeMunicipios = Ext.getStore('municipios.Municipios');
        storeMunicipios.load({
            params: !this.editar ? {start:null,limit:null,codEstado:me.getValue(), session: false }:{start:null,limit:null,codEstado:(Ext.isNumber(me*1)?me:me.getValue())},
            scope: this,
            callback: function (records, operation, success) {
                if (this.editar == true) {
                    var view = this.getView();
                    form = view.down('#FormCliente');
                    var codMunicipio = this.objCliente.codMunicipio;
                    form.getForm().setValues({ codMunicipio: codMunicipio });
                }
                
            }
        });
    },

    cargarCliente: function (params) {
        var view = this.getView();
        view.mask('Procesando...');
        var storeClientes = Ext.getStore('clientes.Clientes');
        storeClientes.load({
            params: params ? params : {},
            scope: this,
            callback: function (records, operation, success) {
                var view = this.getView();
                form = view.down('#FormCliente');
                this.objCliente = records[0].getData(true);
                var codEstado = this.objCliente.codEstado;
                view.down('#cmbEstados').setValue(this.objCliente.Estado.codEstado);
                this.cargarMunicipios(codEstado);
                form.getForm().setValues(this.objCliente);
                form.getForm().setValues(this.objCliente.usuario);
                view.unmask();
            }
        });
       
      
    },

    guardarCliente: function (me, e, eOpts) {
         
        var view = this.getView(),
        form = view.down('#FormCliente'),
        btnGuardar = view.down('#BtnGuardar'),
        data = form.getForm().getValues();
      
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        if (data.passwordUsuario !== data.cpasswordUsuario) {
            this.mnj('Error', 'Las contraseņas no coinciden.', Ext.MessageBox.ERROR);
            return;
        }
        view.mask('Procesando...');
        //         
        //        me = this,
        var objClientes = Ext.create('FRT.model.clientes.Cliente', {
            codCliente: data.codCliente, razonSocialClie: data.razonSocialClie, rfcClie: data.rfcClie,
            calleClie: data.calleClie, numExtClie: data.numExtClie, numIntClie: data.numIntClie, coloniaClie: data.coloniaClie, codMunicipio: data.codMunicipio,
            codEstado: data.codEstado, localidadClie: data.localidadClie, paisClie: data.paisClie, cpClie: data.cpClie
        });
        objClientes.setUsuario(Ext.create('FRT.model.usuarios.Usuario', {
            codUsuario: data.codUsuario, loginUsuario: data.loginUsuario, passwordUsuario: data.passwordUsuario, nombreUsuario: data.nombreUsuario, apePaterUsuario: data.apePaterUsuario,
            apeMaterUsuario: data.apeMaterUsuario, mailUsuario: data.mailUsuario, telefonoUsuario: data.telefonoUsuario
        }));
        objClientes.getUsuario().setPerfil(Ext.create('FRT.model.perfiles.Perfil', { codPerfil: 1 }))

        Ext.Ajax.request({
            url: FRT.Routes.root + '/Clientes/Guardar',
            encode: true,
            params: Ext.encode(objClientes.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                view.unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    if (this.editar == false) {
                        // 
                        this.iniciarSession()
                      
                        return;
                    } else {
                        this.limpiarForm();
                        form.getForm().setValues(objClientes.getData(true));
                        form.getForm().setValues(objClientes.getUsuario().getData(true));
                    }
                    this.mnj('Info', 'Datos Actualizados.', Ext.MessageBox.INFO);
                    
              
                 }
                
               
            },
            failure: function (o, r, n) {
               
                this.limpiarForm();
                view.unmask();
            }
           
        });
        
    },
    limpiarForm: function (me, e, eOpts) {
        var view = this.getView(),
        form = view.down('#FormCliente');
        form.reset();
    },
    iniciarSession: function (me, e, eOpts) {
         
        var form = this.getView().down('#FormCliente').getForm();
        data = form.getValues();
        this.getView().mask("Procesando...");
        var objUsuario = Ext.create('FRT.model.usuarios.Usuario', data);
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Usuarios/Login',
            encode: true,
            params: Ext.encode(objUsuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                this.getView().unmask();
                var resp = Ext.decode(response.responseText);
                if (resp.success) {
                    
                   Ext.ComponentQuery.query('registro')[0] ? Ext.ComponentQuery.query('registro')[0].destroy() : null;
                    FRT.Session.usuario = resp.user;
                    FRT.Session.menu = resp.menu;
                    var main = Ext.ComponentQuery.query('main')[0];
                    main ? main.destroy() : null;
                    main = Ext.ComponentQuery.query('main')[0];
                    main ? main : Ext.create('FRT.view.main.Main');
                    Ext.ComponentQuery.query('main #menuPanel')[0].show();
                }

            },
            failure: function (o, r, n) {
                form.reset();
                this.getView().unmask();
            }

        });
    },
    cancelarRegistro: function (item, e, eOpts) {
       
        if (this.editar == false) {
            Ext.ComponentQuery.query('registro')[0] ? Ext.ComponentQuery.query('registro')[0].destroy() : null;
            FRT.common.FnConnSeg.mostrarLogin();
        }
        if (this.editar == true) {
            this.limpiarForm();
        }
    },

  
  
    mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    
});

