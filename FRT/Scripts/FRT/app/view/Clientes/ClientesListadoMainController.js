
Ext.define('FRT.view.clientes.ClientesListadoMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.clienteslistadomain',

    agregarCliente: function () {
        var vista = Ext.create('FRT.view.clientes.ClienteDetalle');
        vista.show();
        vista.getController().cargarEstados();
    },
    accionCliente: function (me, record, item, index, e, eOpts) {
        
        if (item == 'actionEliminar')
            this.eliminarCliente(record);
    },
    editarCliente: function (me, record, item, index, e, eOpts) {
         
        var view = this.getView(),
        vista = Ext.create('FRT.view.clientes.ClienteDetalle');
        vista.show();
        vista.getController().mostrarDetalleCliente(record);
        
    },
    eliminarCliente: function (record) {
         
        this.getView().mask();
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Clientes/Eliminar',
            encode: true,
            params: Ext.encode({ codCliente: record.get('codCliente') }),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                this.getView().unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {

                    var store = view.down('#gridClientes').getStore();
                    store.remove(record);
                    if (store.getCount() === 0 && store.currentPage > 1)
                        store.currentPage = store.currentPage - 1;
                    this.cargarGridClientes();
                }
            },
            failure: function (o, r, n) {
                this.mnj('Error', 'El registro no fue eliminado', Ext.MessageBox.ERROR)
               
            }
        });

    },
    cargarStores: function (me, eOpts) {
         this.cargarGridClientes();
     },
    cargarGridClientes: function (url) {
         
         var view = this.getView()
         var gridDocumentos = view.down('#gridClientes');
         if (view.down('#paginadorClientes').getStore().currentPage > 1)
             view.down('#paginadorClientes').moveFirst();
         else
             gridDocumentos.getStore().load({
                 url: url,
                 params: {
                     busqvalor: view.down('#Busqvalor').getValue()
                 },
                 scope: this,
                 callback: function (records, operation, success) {
                 }
             });
     },
     buscarClientes: function (me, e, eOpts) {
         if (e.getCharCode() == Ext.EventObject.ENTER)
             this.cargarGridClientes();
     },
     cargarClientesToolBar: function (me, page, eOpts) {

         var view = this.getView()
         var p = me.getStore().getProxy();
         p.setExtraParam('busqvalor', view.down('#Busqvalor').getValue());
     },
     changeBuscarClientes: function (me, newValue, oldValue, eOpts) {
         if (newValue.length === 0)
             this.cargarGridClientes();
     },
     cargarClientes: function (params) {
         var store = Ext.getStore('clientes.Clientes');
         store.load({
             params: params ? params : {},
             scope: this,
             callback: function (records, operation, success) {
             }
         });
     },

});
