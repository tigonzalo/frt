Ext.define('FRT.view.clientes.ClientesListadoMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.clienteslistadomain',
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    requires: [
        'FRT.view.clientes.ClientesListadoMainController',
         'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Spacer'
    ],

    controller: 'clienteslistadomain',
    //se crea funcion para 
    initComponent: function () {
        var me = this,
           storeClientes = Ext.create('FRT.store.clientes.Clientes');
        Ext.applyIf(me, {
            items: [
        {
            xtype: 'gridpanel',
            itemId: 'gridClientes',
            flex: 1,
            style: {
                'background-color': 'transparent',
                'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                'background-size': '100% 100%',
                'background-repeat': 'no-repeat',
                'border-radius': '7px 7px 0px 0px',
                'color': 'white'
            },
            title: 'Listado de Clientes',
            titleAlign: 'center',
            cls: 'pMain',
            store: storeClientes,
            dockedItems: [
                            {
                                xtype: 'toolbar',
                                style: {
                                    'background-color': 'transparent'
                                },
                                dock: 'top',
                                height:60,
                                items: [
                                  
                                    {
                                        xtype: 'textfield',
                                        flex: 1,
                                        labelAlign: 'top',
                                        cls: 'rounded',
                                        width: 400,
                                        fieldLabel: 'Buscar',
                                        //labelAlign: 'top',
                                        labelWidth: 50,
                                        emptyText: 'Criterio de Busqueda',
                                        itemId: 'Busqvalor',
                                        enableKeyEvents: true,
                                        listeners: {
                                            keypress: 'buscarClientes',
                                            change: 'changeBuscarClientes'

                                        }
                                    },
                                    {
                                        xtype: 'tbspacer',
                                        flex: 2

                                    },
                                    {
                                        text: 'Agregar',
                                        itemId: 'BtnGuardar',
                                        icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/file-add.png',
                                        // disabled: !FRT.common.FnConnSeg.isVisible('Empresas', 'Guardar'),
                                        listeners: {
                                            click: 'agregarCliente'
                                        }
                                    }
                                    
                                ]
                            },
        {
            xtype: 'pagingtoolbar',
            itemId: 'paginadorClientes',
            dock: 'bottom',
            displayInfo: true,
            store: storeClientes,
            listeners: {
                 beforechange: 'cargarClientesToolBar'
               }
        }
            ],
            columns: [
                 {
                     xtype: 'rownumberer',
                     width: 35
                 },
                 {
                     xtype: 'actioncolumn',
                     width: 40,
                     align: "center",
                     items: [
                         {
                             icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Delete.png',
                             tooltip: 'Eliminar Cliente',
                             handler: function (grid, rowIndex, colIndex) {
                                 var rec = grid.getStore().getAt(rowIndex);
                                 this.up('grid').fireEvent('itemclick', grid, rec, 'actionEliminar', rowIndex, colIndex);

                             }

                         }
                     ]
                 },
                {

                    xtype: 'gridcolumn',
                    dataIndex: 'razonSocialClie',
                    flex: 2,
                    text: 'Razon Social'
                },
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'rfcClie',
                    text: 'RFC'
                },
                {
                    renderer: function (value, metaData, record, rInx, cInx, store, view) {

                        return record.getUsuario().get('mailUsuario');
                    },
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'codUsuario',
                    text: 'Email'
                }

            ],
            listeners: {
                itemclick: 'accionCliente',
                itemdblclick: 'editarCliente'

            }
        
        }
            ]
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }

});