

Ext.define('FRT.view.clientes.DocumentoInfo', {
    extend: 'Ext.container.Container',
    alias: 'widget.documentoInfo',

    requires: [
        'FRT.view.clientes.ClienteDetalleController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View'
    ],


    controller: 'clientedetalle',
    autoScroll: true,
    initComponent: function () {
        var me = this,
           storeUsosCFDI = Ext.create('FRT.store.usosCFDI.UsosCFDI'),
           storeEmpresas = Ext.create('FRT.store.empresas.Empresas');
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    itemId: 'FormDocumento',
                    scrollable: true,
                    items: [
                        {
                            xtype: 'container',
                            items: [
                                {
                                    xtype: 'container',
                                    itemId: 'facturaFielset',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        margin: '5 5 5 5',
                                    },
                                    items: [
                                     {
                                         xtype: 'tbspacer',
                                         flex: .5
                                     },
                                        {
                                            xtype: 'fieldset',
                                            title: '',
                                            flex: 2,
                                            width: 550,
                                            layout: {
                                                type: 'auto'
                                            },
                                            padding: '5 0 10 0',
                                            defaults: {
                                                margin: '5 5 5 5'
                                            },
                                            items: [
                                                {
                                                    xtype: 'hiddenfield',
                                                    anchor: '100%',
                                                    itemId: 'codDocumento',
                                                    fieldLabel: 'Label',
                                                    name: 'codDocumento'
                                                },
                                                {
                                                    xtype: 'combobox'
                                                    , listConfig: {
                                                        loadingText: 'Cargando...'
                                                        , emptyText: 'No se encontraron registros coincidentes '
                                                        , tpl: Ext.create('Ext.XTemplate',
                                                        '<table Id="comboTable"  width="100%">',
                                                            '<th width="100%">Empresa/Restaurant</th>',
                                                        '</table>',
                                                            '<tpl for=".">',
                                                                '<div class="x-boundlist-item" margin="0">',
                                                                    '<table Id="comboTable" width="100%">',
                                                                        '<td width="100%">{nombreEmp}</td>',
                                                                         //'<td width="33%">{IdDptmto}</td>',
                                                                         // '<td width="33%">{IdArea}</td>',
                                                                    '</table>',
                                                                '</div>',
                                                            '</tpl>'

                                                    ),
                                                        displayTpl: Ext.create('Ext.XTemplate',
                                                            '<tpl for=".">',
                                                            '{nombreEmp}',
                                                            '</tpl>'
                                                            ),
                                                    }
                                                    , flex: 0.65
                                                    //, labelAlign: 'top'
                                                    , itemId: 'CmbEmpresa'
                                                    , width: 400
                                                    , enableKeyEvents: true
                                                    , displayField: 'nombreEmp'
                                                    , pageSize: 10
                                                    , store: storeEmpresas
                                                    , queryMode: 'local'
                                                    , valueField: 'codEmpresa'
                                                    , autoSelect: false
                                                    , anyMatch: true
                                                    , fieldLabel: 'Empresa/Restaurant'
                                                    , labelWidth: 180
                                                    , forceSelection: true
                                                    , allowBlank: false
                                                    , name: 'codEmpresa'
                                                    //, hidden: JSON.parse(TUA.common.Seguridad.verificarVarConfig('S').toLowerCase())
                                                },
                                                {
                                                    xtype: 'combobox'
                                                    , flex: 0.65
                                                    , name: 'codUsoCFDI'
                                                    , itemId: 'cmbUsosCFDI'
                                                    , width: "80%"
                                                    , margin: '0 20 5 10'
                                                    , maxWidth: 600
                                                    , enableKeyEvents: true
                                                    , displayField: 'claveDescripcion'
                                                    , store: storeUsosCFDI
                                                    , queryMode: 'local'
                                                    , valueField: 'codUsoCFDI'
                                                    , autoSelect: false
                                                    , anyMatch: true
                                                    , fieldLabel: 'Uso del CFDI'
                                                    , labelWidth: 180
                                                    , forceSelection: true
                                                    , msgTarget: 'side'
                                                    , allowBlank: false
                                                },
                                               {
                                                   xtype: 'container',
                                                   layout: {
                                                       type: 'hbox',
                                                       align: 'center'
                                                   },

                                                   items: [
                                                       {
                                                           xtype: 'textfield',
                                                           width: 400,
                                                           //labelAlign: 'top',
                                                           itemId: 'folio',
                                                           name: 'folio',
                                                           fieldLabel: 'Número Folio del Ticket',
                                                           labelWidth: 180,
                                                           allowBlank: false
                                                       },
                                                        {
                                                            xtype: 'component',
                                                            width: 16,
                                                            height: 16,
                                                            margin: '20 5 20 5',
                                                            autoEl: {
                                                                tag: 'img',
                                                                src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                            },
                                                            listeners: {
                                                                render: function (p) {
                                                                    //ticketFecha.png
                                                                    FRT.common.FnConnSeg.toolTip('<img src="Scripts/FRT/Imagenes/ticketFolio.png">', p, '63 0 0 60');
                                                                }
                                                            }
                                                        }

                                                   ]
                                               },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },

                                                    items: [
                                                       {
                                                           xtype: 'datefield',
                                                           //anchor: '100%',
                                                           //labelAlign: 'top',
                                                           width: 400,
                                                           itemId: 'fechaConsumo',
                                                           name: 'fechaConsumo',
                                                           fieldLabel: 'Fecha Consumo',
                                                           labelWidth: 180,
                                                           format: 'd/m/Y',
                                                           padding: '1 0 5 0',
                                                           allowBlank: false,
                                                       },
                                                         {
                                                             xtype: 'component',
                                                             width: 16,
                                                             margin: '20 5 20 5',
                                                             height: 16,
                                                             autoEl: {
                                                                 tag: 'img',
                                                                 src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                             },
                                                             listeners: {
                                                                 render: function (p) {
                                                                     FRT.common.FnConnSeg.toolTip('<img src="Scripts/FRT/Imagenes/ticketFecha.png">', p, '63 0 0 60');
                                                                 }
                                                             }
                                                         }

                                                    ]
                                                },

                                                {
                                                    xtype: 'textfield',
                                                    itemId: 'total',
                                                    //labelAlign: 'top',
                                                    width: 400,
                                                    name: 'total',
                                                    //anchor: '100%',
                                                    fieldLabel: 'Total Consumo',
                                                    labelWidth: 180
                                                    , allowBlank: false
                                                    // , vtype: 'mayorcerop'CodStatusDoc
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },

                                                    items: [
                                                      {
                                                          xtype: 'checkboxfield',
                                                          itemId: 'Checkfraccion',
                                                          labelWidth: 180,
                                                          fieldLabel: '!! ¿Fraccionar Ticket?',
                                                          name: 'fraccionado',
                                                          inputValue: 'true',
                                                          listeners: {
                                                              change: 'crearFraccionTicket'
                                                          }
                                                      },
                                                         {
                                                             xtype: 'component',
                                                             width: 16,
                                                             margin: '20 5 20 5',
                                                             height: 16,
                                                             autoEl: {
                                                                 tag: 'img',
                                                                 src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                             },
                                                             listeners: {
                                                                 render: function (p) {
                                                                     FRT.common.FnConnSeg.toolTip('Marcar si desea obtener varias facturas o <br> un monto menor al ticket', p, '63 0 0 60');
                                                                 }
                                                             }
                                                         }

                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldset',
                                                    itemId: 'fielFraccion',
                                                    collapsed: true,
                                                    collapsible: true,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },
                                                    defaults: {
                                                        margin: '10,10,10,10'
                                                    },
                                                    padding: '10,10,10,10',
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'center'
                                                            },

                                                            items: [
                                                               {
                                                                   xtype: 'numberfield',
                                                                   width: 130,
                                                                   fieldLabel: 'Número Facciones',
                                                                   name: 'nFracciones',
                                                                   itemId: 'nFracciones',
                                                                   labelAlign: 'top',
                                                                   minValue: 2,
                                                                   maxValue: 30,
                                                                   listeners: {
                                                                       change: 'calcularMontoFraccion'
                                                                   }
                                                               },
                                                                 {
                                                                     xtype: 'component',
                                                                     width: 16,
                                                                     margin: '5 5 5 5',
                                                                     height: 16,
                                                                     autoEl: {
                                                                         tag: 'img',
                                                                         src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                                     },
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             FRT.common.FnConnSeg.toolTip('Ayuda a fraccionar en montos iguales la cantidad del Ticket', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }

                                                            ]
                                                        },

                                                        {
                                                            xtype: 'tbspacer',
                                                            flex: .5
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'center'
                                                            },

                                                            items: [
                                                              {
                                                                  xtype: 'textfield',
                                                                  width: 130,
                                                                  name: 'totalFraccion',
                                                                  itemId: 'totalFraccion',
                                                                  fieldLabel: 'Total Fraccion',
                                                                  labelAlign: 'top'
                                                              },
                                                                 {
                                                                     xtype: 'component',
                                                                     width: 16,
                                                                     margin: '5 5 5 5',
                                                                     height: 16,
                                                                     autoEl: {
                                                                         tag: 'img',
                                                                         src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                                     },
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             FRT.common.FnConnSeg.toolTip('Escribir o indicar la cantidad a facturar', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }

                                                            ]
                                                        }

                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxfield',
                                                    itemId: 'hold',
                                                    checked: true,
                                                    labelWidth: 180,
                                                    fieldLabel: '!! Enviar Factura al Correo',
                                                    name: 'hold',
                                                    inputValue: 'true',
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        }

                                    ]
                                },
                        {
                            xtype: 'container',
                            itemId: 'botonestb',
                            height: 35,
                            padding: 1,
                            //width: 700,
                            layout: {
                                type: 'absolute'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    padding: '10 10 10 10',
                                    style: {
                                        'background-color': 'transparent'
                                    },
                                    items: [
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1,
                                            height: 20,
                                            //width: 400
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            // flex: 1,
                                            text: 'Facturar Ticket',
                                            // hidden: true,
                                            width: 150,
                                            itemId: 'BtnGuardarF',
                                            listeners: {
                                                click: 'guardarDocumento'
                                            }

                                        },

                                        {
                                            xtype: 'tbseparator',
                                            width: 50
                                        },
                                        {
                                            xtype: 'button',
                                            // flex: 1,
                                            width: 150,
                                            text: 'Cancelar',
                                            //hidden: true,
                                            itemId: 'BtnCancelar',
                                            listeners: {
                                                click: 'cancelarRegistro'
                                            }

                                        },
                                         {
                                             xtype: 'tbseparator'
                                         },
                                           {
                                               xtype: 'tbspacer',
                                               flex: 1,
                                               height: 20,
                                               //width: 400
                                           },
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'tbspacer',
                            flex: .25
                        }




                            ]
                        }
                    ]
                }
            ]
            //    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarEmpresasCmb'

    }

});