﻿Ext.define('FRT.view.clientes.ClienteDetalle', {
    alias: 'widget.clientesdetalle'
    , controller: 'clientedetalle'
    , extend: 'Ext.window.Window'
    , requires: [
        'FRT.view.clientes.ClienteInfo',
        'FRT.view.clientes.DocumentoInfo',
        'Ext.toolbar.Spacer',
        'Ext.grid.column.RowNumberer',
        'Ext.grid.column.Action',
        'Ext.grid.column.Check',
        'FRT.view.clientes.ClienteDetalleController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.grid.column.Boolean',
        'Ext.grid.View'
    ]
    //, title: 'Datos de la Horario'
    , titleAlign: 'center'

    // , resizable: false
    , height: 600
    , width: 900
    , layout: {
        type: 'fit'
    }
    //, cls: 'pWin'
    , modal: true
    , initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            title: 'Cliente Info',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    width: 100,
                                    itemId: 'BtnLimpiar',
                                    listeners: {
                                        click: 'cancelarRegistro'
                                    }
                                },
                                    {
                                        text: 'Guardar',
                                        width: 100,
                                        itemId: 'BtnGuardar',
                                        listeners: {
                                            click: 'guardarCliente'
                                        }
                                    }
                                    
                            ],
                            items: [
                                {
                                    xtype: 'clienteinfo'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            title: 'Facturar',
                            //buttons: [
                            //        {
                            //            text: 'Cancelar',
                            //            flex: 1,
                            //            itemId: 'BtnLimpiar',
                            //            listeners: {
                            //                click: 'cancelarRegistro'
                            //            }
                            //        },
                                    
                            //],
                            items: [
                                {
                                    xtype: 'documentoInfo'
                                }
                            ]
                        }
                    ]
                }
            ]

        });

        me.callParent(arguments);
    }

  

});