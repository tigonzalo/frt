Ext.define('FRT.view.clientes.ClienteInfo', {
    extend: 'Ext.container.Container',
    alias: 'widget.clienteinfo',
       
    requires: [
        'FRT.view.clientes.ClienteDetalleController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.ComboBox',
        'Ext.form.*',

    ],

    controller: 'clientedetalle',
    constrain: true,
    bodyPadding: 10,
    frame: true,
    modal: true
    , height: 430
     , layout: {
         type: 'hbox',
         align: 'stretch'
     }
     , initComponent: function () {
          var me = this,
             storePerfiles = Ext.create('FRT.store.clientes.Clientes'),
             storeEstados = Ext.create('FRT.store.estados.Estados'),
             storeMunicipios = Ext.create('FRT.store.municipios.Municipios');
        Ext.applyIf(me, {
                    
            items: [
                {
                    xtype: 'form',
                    //cls: 'pMain',
                    itemId: 'FormCliente',
                    height: 400,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    padding: '15 10 15 10',
                    items: [
                        {
                            xtype: 'panel',
                            //height: 100,
                            title: 'DATOS Usuario',
                            style: {
                                'background-color': 'transparent',
                                'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                'background-size': '100% 100%',
                                'background-repeat': 'no-repeat'
                            },
                            autoScroll: true,
                            defaults: {
                                margin: '5 0 0 60',
                            },
                            items: [
                                 {
                                     xtype: 'hiddenfield',
                                     fieldLabel: 'codUsuario',
                                     name: 'codUsuario'
                                 },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Usuario',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'loginUsuario',
                                    maxLength: 20,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',

                                },
                                {
                                    xtype: 'textfield',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    fieldLabel: 'Contraseña',
                                    inputType: 'password',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'passwordUsuario'
                                },
                                {
                                    xtype: 'textfield',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    fieldLabel: 'Confirmar Contraseña',
                                    inputType: 'password',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    name: 'cpasswordUsuario'

                                },
                                {
                                    xtype: 'textfield',
                                    labelAlign: 'top',
                                    anchor: '100%',
                                    fieldLabel: 'Correo Electronico',
                                    allowBlank: false,
                                    allowOnlyWhitespace: false,
                                    maxLength: 50,
                                    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                    emptyText: 'usuario@mail.com',
                                    name: 'mailUsuario',
                                    vtype: 'email'
                                },
                                 {
                                     xtype: 'hiddenfield',
                                     fieldLabel: ' ',
                                     name: 'codCliente'
                                 },
                                //{
                                //    xtype: 'textfield',
                                //    labelAlign: 'top',
                                //    anchor: '100%',
                                //    fieldLabel: 'Nombre',
                                //    allowBlank: false,
                                //    allowOnlyWhitespace: false,
                                //    name: 'nombreUsuario',
                                //    maxLength: 20,
                                //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'

                                //},

                                //{
                                //    xtype: 'textfield',
                                //    labelAlign: 'top',
                                //    fieldLabel: 'Apellido Paterno',
                                //    anchor: '100%',
                                //    allowBlank: false,
                                //    allowOnlyWhitespace: false,
                                //    name: 'apePaterUsuario',
                                //    maxLength: 20,
                                //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',

                                //},
                                //{
                                //    xtype: 'textfield',
                                //    labelAlign: 'top',
                                //    fieldLabel: 'Apellido Materno',
                                //    anchor: '100%',
                                //    name: 'apeMaterUsuario',
                                //    maxLength: 20,
                                //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',


                                //},
                                //{
                                //    xtype: 'textfield',
                                //    labelAlign: 'top',
                                //    anchor: '100%',
                                //    fieldLabel: 'Telefono',
                                //    name: 'telefonoUsuario',
                                //    regex: /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/,
                                //    regexText: 'Se requiere un teléfono válido ejemplo: 123-456-7890.',
                                //    maxLength: 20,
                                //    maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.'


                                //},
                            ]
                        },
                            {
                                xtype: 'tbspacer',
                                width: 40,
                            },
                            {
                                xtype: 'panel',
                                title: 'DATOS FISCALES',
                                autoScroll: true,
                                style: {
                                    'background-color': 'transparent',
                                    'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                    'background-size': '100% 100%',
                                    'background-repeat': 'no-repeat'
                                },
                                width: 600,
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                defaults: {
                                    margin: '10 10 10 40',
                                },

                                items: [
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                maxLength: 200,
                                                fieldLabel: 'Razón Social',
                                                anchor: '100%',
                                                msgTarget: 'under',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'razonSocialClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'RFC',
                                                labelAlign: 'top',
                                                msgTarget: 'under',
                                                maxLength: 14,
                                                minLength: 12,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                anchor: '100%',
                                                regex: /^[A-Z&]{3,4}\d{6}\w{3}/,
                                                regexText: 'Se requiere un RFC válido',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'rfcClie',
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        field.setValue(newValue.toUpperCase());
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Calle',
                                                msgTarget: 'under',
                                                maxLength: 70,
                                                anchor: '100%',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'calleClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Número Exterior',
                                                msgTarget: 'under',
                                                maxLength: 12,
                                                anchor: '100%',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'numExtClie'
                                            },
                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                fieldLabel: 'Número Interior',
                                                msgTarget: 'under',
                                                maxLength: 12,
                                                anchor: '100%',
                                                name: 'numIntClie'
                                            },

                                            {
                                                xtype: 'textfield',
                                                labelAlign: 'top',
                                                anchor: '100%',
                                                msgTarget: 'under',
                                                fieldLabel: 'Código Postal',
                                                maxLength: 6,
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'cpClie',
                                                regex: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/,
                                                regexText: 'Se requiere un código postal válido',

                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,

                                        items: [
                                          {
                                              xtype: 'textfield',
                                              fieldLabel: 'Colonia',
                                              labelAlign: 'top',
                                              msgTarget: 'under',
                                              maxLength: 70,
                                              anchor: '100%',
                                              allowBlank: false,
                                              allowOnlyWhitespace: false,
                                              name: 'coloniaClie'
                                          },
                                        {
                                            xtype: 'combobox',
                                            labelAlign: 'top',
                                            labelAlign: 'top',
                                            fieldLabel: 'Estado',
                                            anchor: '100%',
                                            name: 'codEstado',
                                            itemId: 'cmbEstados',
                                            store: storeEstados,
                                            displayField: 'nombreEstado',
                                            valueField: 'codEstado',
                                            queryMode: 'local',
                                            emptyText: 'Seleccionar Estado',
                                            allowBlank: false,
                                            forceSelection: true,
                                            msgTarget: 'under',
                                            allowOnlyWhitespace: false,
                                            listeners: {
                                                change: 'cargarMunicipios'
                                            },
                                        },
                                        {
                                            xtype: 'combobox',
                                            labelAlign: 'top',
                                            fieldLabel: 'Municipio',
                                            anchor: '100%',
                                            name: 'codMunicipio',
                                            itemId: 'cmbMunicipios',
                                            msgTarget: 'under',
                                            store: storeMunicipios,
                                            displayField: 'nombreMunicipio',
                                            valueField: 'codMunicipio',
                                            queryMode: 'local',
                                            emptyText: 'Seleccionar Municipio',
                                            forceSelection: true,
                                            allowBlank: false,
                                            allowOnlyWhitespace: false,
                                        },
                                        {
                                            xtype: 'textfield',
                                            labelAlign: 'top',
                                            anchor: '100%',
                                            fieldLabel: 'Pais',
                                            maxLength: 50,
                                            name: 'paisClie',
                                            msgTarget: 'under',
                                            value: 'México',
                                            editable: false,
                                            allowBlank: false,
                                            allowOnlyWhitespace: false

                                        },
                                        {
                                            xtype: 'textfield',
                                            labelAlign: 'top',
                                            anchor: '100%',
                                            fieldLabel: 'Localidad',
                                            msgTarget: 'under',
                                            maxLength: 150,
                                            name: 'localidadClie'
                                        }


                                        ]
                                    }

                                ]
                            },
                            {
                                xtype: 'panel',
                                flex: 1
                            }
                    ]
                },
                             
                            

            ]
        
        });
        me.callParent(arguments);
    }
   
 
});