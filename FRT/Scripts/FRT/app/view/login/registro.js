﻿Ext.define('FRT.view.login.registro', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.registro',

    requires: [
        'FRT.view.login.LoginController',
        'FRT.view.clientes.ClientesMain'
    ],

    layout: 'border',
    titleAlign: 'center',
    modal: true,
    constrain: true,
    controller: 'Login',
    frame: true,
    style: {
        'background-color': '#17425f',
        'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondo.jpg' + ')',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-position': 'center'
    },
    initComponent: function () {
        var me = this;

        clientesMain = Ext.create('FRT.view.clientes.ClientesMain');
        clientesMain.getController().editar = false;

        Ext.applyIf(me, {
            items: [

                {
                xtype: 'container',
                region: 'center',
                autoScroll: true,
                items: [
                     {

                         xtype: 'container',
                         height: 50,
                         style: {
                             'background-color': 'transparent',
                             'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/NuevoUsuario.png' + ')',
                             'background-size': 'auto',
                             'background-repeat': 'no-repeat',
                             'background-position': 'top center'
                         },
                        // padding: '10 40 40 40',
                        // layout: 'fit',
                        
                     },
                    {

                        xtype: 'container',
                        style: {
                            'background-color': 'transparent',
                            'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                            'background-size': '100% 100%',
                            'background-repeat': 'no-repeat'
                        },
                        itemId: 'contentPanel',
                        autoScroll: true,
                        //padding: '10 40 40 40',
                        //layout: 'fit',
                       
                        items: [
                          clientesMain,
                        ]
                    }

                ]
            },
                 {
                     xtype: 'container',
                     region: 'north',
                     height: 160,
                     style: {
                         'background-color': 'transparent',
                         'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/encabezado1.png' + ')',
                         'background-size': 'auto',
                         'background-repeat': 'no-repeat',
                         'background-attachment': 'fixed',
                         'background-position': '10% 0%'
                     }
                 }

            ]

        });

        me.callParent(arguments);
    }

});