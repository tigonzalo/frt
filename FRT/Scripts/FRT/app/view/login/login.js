﻿Ext.define('FRT.view.login.Login', {
    extend: 'Ext.window.Window',
    alias: 'widget.login',

    requires: [
        'FRT.view.login.LoginController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Text'
    ],


    height: 264,
    width: 400,
    layout: 'fit',
    title: 'Inicio',
    controller: 'login',
    modal: true,
    buttons: [
        {
            text: 'Cancelar',
            tabIndex: "4",
            listeners: {
                click: 'destruirVisata'
            }
        },
        {
            text: 'Aceptar',
            anchor: '100%',
            itemId: 'BtnRegistro',
            tabIndex: 3,
            listeners: {
                click: 'iniciarSession'
            }
        }
    ],
    initComponent: function () {
        var me = this;
        //var storePerfiles = Ext.create('CGJ.store.perfiles.Perfiles');
        //var storeSucursales = Ext.create('CGJ.store.sucursales.Sucursales');
        Ext.applyIf(me, {
            items: [
         {
             xtype: 'form',
             itemId: 'FormLogin',
             bodyPadding: 10,
             title: '',
             items: [
                 {
                     xtype: 'fieldset',
                     height: 142,
                     padding: '10,10,10,10',
                     title: '',
                     items: [
                         {
                             xtype: 'textfield',
                             anchor: '100%',
                             fieldLabel: 'Usuario',
                             name: 'loginUsuario',
                             labelAlign: 'top',
                             allowBlank: false,
                             allowOnlyWhitespace: false,
                             enableKeyEvents: true,
                             listeners: {
                                 keypress: 'loginEnter'
                             }
                         },
                         {
                             xtype: 'textfield',
                             anchor: '100%',
                             fieldLabel: 'Contraseña',
                             name: 'passwordUsuario',
                             labelAlign: 'top',
                             inputType: 'password',
                             allowBlank: false,
                             allowOnlyWhitespace: false,
                             enableKeyEvents: true,
                             listeners: {
                                 keypress: 'loginEnter'
                             }
                         }
                     ]
                 }
             ]
         }
            ]

        });

        me.callParent(arguments);
    },
    //listeners: {
    //    afterrender: 'cargarStores'

    //}

});