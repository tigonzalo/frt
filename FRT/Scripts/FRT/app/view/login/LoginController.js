﻿Ext.define('FRT.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',


    loginEnter: function (me, e, eOpts) {
        if (e.getCharCode() == Ext.EventObject.ENTER) {
            this.iniciarSession();
        }
    },
    iniciarSession: function (me, e, eOpts) {
        var form = this.getView().down('#FormLogin').getForm(),
            valido = form.isValid(),
            data = form.getValues();
        if (!valido) {
            this.mnjs('Error', 'Favor de llenar correctamente el formulario.');
            return;
        }
        this.getView().mask("Procesando...");
        var objUsuario = Ext.create('FRT.model.usuarios.Usuario', data);
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Usuarios/Login',
            encode: true,
            params: Ext.encode(objUsuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                debugger
                this.getView().unmask();
                var resp = Ext.decode(response.responseText);
                if (resp.success) {
                    this.getView().destroy();
                    var main = Ext.ComponentQuery.query('main')[0];
                    main.down("#contentPanel").removeAll();
                    FRT.Session.usuario = resp.user;
                    FRT.Session.menu = resp.menu;
                    var menu = main.down('#menuPanel');
                    menu.setVisible(true);
                }

            },
            failure: function (o, r, n) {
                form.reset();
                this.getView().unmask();
            }

        });
    },
    recuperarContrasenia: function (me, e, eOpts) {

        this.abilitarElemento(true, '#ContraFS');
        this.abilitarElemento(false, '#loginFS');
        this.getView().down('#FormLogin').setTitle('RECUPERAR CONTRASEÑA');


    },
    cancelarEnvioContrasena: function (me, e, eOpts) {
        this.abilitarElemento(false, '#ContraFS');
        this.abilitarElemento(true, '#loginFS');
        this.getView().down('#FormLogin').setTitle('ACCESO');
    },
    enviarCorreo: function (me, e, eOpts) {

        var form = this.getView().down('#FormLogin').getForm();
        data = form.getValues();

        if (!data.mailUsuario) {
            this.mnjs('Error', 'Favor de llenar el campo del Correo Electrónico.');
            return;
        }
        this.getView().mask("Procesando...");
        var objUsuario = Ext.create('FRT.model.usuarios.Usuario', data);
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Sistemas/OptenerContrasenia',
            encode: true,
            params: Ext.encode(objUsuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                this.getView().unmask();
                var resp = Ext.decode(response.responseText);
                if (resp.success) {
                    this.abilitarElemento(false, '#ContraFS');
                    this.abilitarElemento(true, '#loginFS');
                    var view = this.getView();
                    var elementoV = view.down("#BtnRecContrasenia");
                    elementoV.setText("¿Olvidaste tu Contraseña?");

                }

            },
            failure: function (o, r, n) {
                form.reset();
                this.getView().unmask();
            }

        });
    },
    abilitarElemento: function (valor, elemento) {
        var view = this.getView();
        var elementoV = view.down(elemento);
        elementoV.setVisible(valor);
    },
    destruirVisata: function () {
        this.getView().destroy();
    },
    loginEnter: function (me, e, eOpts) {
        if (e.getCharCode() == Ext.EventObject.ENTER) {
            this.iniciarSession();
        }
    },
    mostrarClientesMain: function (item, e, eOpts) {
        Ext.ComponentQuery.query('login')[0] ? Ext.ComponentQuery.query('login')[0].destroy() : null;
        var clientesMain = Ext.create('FRT.view.login.registro');
        clientesMain.show();
        //var main = new Ext.container.Viewport();
        //
        //var clientesMain = Ext.create('FRT.view.clientes.ClientesMain');
        //clientesMain.getController().editar = false;
        //main.layout = 'border';
        //main.itemId='mainregistro',

        //main.add({
        //    xtype: 'container',
        //    region: 'center',
        //    itemId: 'contentPanel',
        //    style: { "background-color": "white" },
        //    layout: 'fit',
        //    items: [
        //      clientesMain,
        //    ]
        //});
        //main.show;
    },
    mnjs: function (titulo, mnj) {
        Ext.MessageBox.show({
            title: titulo,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: Ext.MessageBox.ERROR
        });
    }


});
