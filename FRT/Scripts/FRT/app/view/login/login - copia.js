﻿Ext.define('FRT.view.login.Login', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.login',

    requires: [
        'FRT.view.login.LoginController',
            'Ext.container.Container'
    ],

    style: {
        'background-color': '#424757',
        //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondo-01.jpg' + ')',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-position': 'center'
    },
    layout: 'border',
    controller: 'Login',
    items: [
        {
            xtype: 'container',
            //style: {
            //    'background-color': 'transparent',
            //    'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/login-03.png' + ')',
            //    'background-size': 'auto',
            //    'background-repeat': 'no-repeat',
            //    'background-attachment':'fixed',
            //    'background-position': '10% 5%'
            //},
            region: 'center',
            flex:1,
            //autoScroll: true,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [


                {
                    xtype: 'container',
                    flex: .75,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    padding: '30 70 50 100',
                    items: [
                        {
                            xtype: 'container',
                            height: 150,
                        },
                        {
                            xtype: 'container',
                            style: {
                                'background-color': 'transparent',
                                //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/login-02.png' + ')',
                                'background-size': 'auto',
                                'background-repeat': 'no-repeat',
                                'background-attachment': 'fixed',
                                'background-position': '20% 5%'
                            },
                               html: '<h1 style="font-style: arial;"><span style="font-family:arial,helvetica,sans-serif;">' +
                                      '<span style="color:#ffffff;"><span style="font-size:30px;">Factura Global&nbsp;</span></span></span></h1><p><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#ffffff;"></span></span></span></p>',

                        }
                    ]

                },
                {
                    xtype: 'container',
                    flex: 1,
                    padding: '30 70 20 100',
                    autoScroll: true,
                    items: [
                         {
                             xtype: 'container',
                             height: 150
                         },
                         {
                             xtype: 'container',
                             height: 100,
                             style: {
                                 'background-color': 'transparent',
                                 //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/titulos/Bienvenido.png' + ')',
                                 'background-size': '80%',
                                 'background-repeat': 'no-repeat',
                                 'background-position': 'left top'
                                 
                             }
                             //   html: '<h1 style="font-style: arial;"><span style="font-family:arial,helvetica,sans-serif;">' +
                             //'<span style="color:#ffffff;"><span style="font-size:30px;">Facturaci&oacute;n Global&nbsp;</span></span></span></h1><p><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="color:#ffffff;">cdadcadcad</span></span></span></p>',

                         },
                            {
                                xtype: 'form',
                                cls: 'formLogin',
                                //width: 400,
                                flex: .5,
                                maxWidth: 400,
                                title: 'ACCESO',
                                bodyStyle: {
                                    'background-color': 'transparent'
                                },
                                itemId: 'FormLogin',
                                border: false,
                                padding: '20 20 20 20',

                                items: [
                                    {
                                        xtype: 'fieldset',
                                        style: {
                                            'background-color': 'transparent',
                                            //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                            'background-size': '100%, 100%',
                                            'background-repeat': 'no-repeat'
                                        },
                                        itemId: 'loginFS',
                                        defaults: {
                                            margin: '10,0,0,10',
                                        },
                                        //title: 'Acceso',
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                anchor: '100%',
                                                labelAlign: 'top',
                                                cls: 'rounded',
                                                fieldLabel: 'Usuario',
                                                fieldStyle: 'border:0px solid white;',
                                                border: false,
                                                fieldStyle: 'backcolor:green;',
                                                maxLength: 20,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                name: 'loginUsuario',
                                                enableKeyEvents: true,
                                                msgTarget: 'under',
                                                listeners: {
                                                    keypress: 'loginEnter',
                                                    render: function (p) {
                                                        //FRT.common.FnConnSeg.toolTip('<img src="Scripts/FRT/Imagenes/pampas.gif">', p, '63 0 0 60');
                                                        //FRT.common.FnConnSeg.toolTip('Nombre del Usuario al momento de Registrarse.', p, '63 0 0 60');
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'textfield',
                                                anchor: '100%',
                                                labelAlign: 'top',
                                                // cls: 'rounded',
                                                fieldLabel: 'Contraseña',
                                                fieldStyle: 'border:0px solid white;',
                                                border: false,
                                                maxLength: 20,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                inputType: 'password',
                                                allowBlank: false,
                                                name: 'passwordUsuario',
                                                enableKeyEvents: true,
                                                msgTarget: 'under',
                                                listeners: {
                                                    keypress: 'loginEnter',
                                                    render: function (p) {
                                                        //FRT.common.FnConnSeg.toolTip('<img src="Scripts/FRT/Imagenes/pampas.gif">', p, '63 0 0 60');
                                                        // FRT.common.FnConnSeg.toolTip('Contraseña del Usuario al momento de Registrarse.', p, '63 0 0 60');
                                                    }
                                                }

                                            },
                                                 {
                                                     xtype: 'container',
                                                     flex: 1,
                                                     layout: {
                                                         type: 'vbox',
                                                         align: 'stretch'
                                                     },
                                                     items: [
                                                         {
                                                             xtype: 'container',
                                                             layout: {
                                                                 type: 'hbox',
                                                                 align: 'stretch'
                                                             },
                                                             items: [
                                                                 {
                                                                     xtype: 'label',
                                                                     style: { 'color': 'white', 'font-weight': 'bold' },
                                                                     text: '¿Olvidaste tu contraseña?'
                                                                 },
                                                                 {
                                                                     xtype: 'label',
                                                                     html: '<a href="#" style="color:rgb(23, 67, 96)">Haz click aqui</a>',
                                                                     style: { 'font-weight': 'bold' },
                                                                     flex: 1,
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             p.getEl().on('click', 'recuperarContrasenia');
                                                                             //FRT.common.FnConnSeg.toolTip('Se enviará el Usuario y Contraseña al correo Registrado.', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }
                                                             ]
                                                         },
                                                         {
                                                             xtype: 'container',
                                                             layout: {
                                                                 type: 'hbox',
                                                                 align: 'stretch'
                                                             },
                                                             items: [
                                                                 {
                                                                     xtype: 'label',
                                                                     style: { 'color': 'white', 'font-weight': 'bold' },
                                                                     text: '¿Eres nuevo usuario?'
                                                                 },
                                                                 {
                                                                     xtype: 'label',
                                                                     html: '<a href="#" style="color:rgb(23, 67, 96)">Registrarse aqui</a>',
                                                                     style: { 'font-weight': 'bold' },
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             p.getEl().on('click', 'mostrarClientesMain');
                                                                            // FRT.common.FnConnSeg.toolTip('Podrás obtener tu Usuario y contraseña.', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }
                                                             ]
                                                         }
                                                     ]
                                                 },
                                                 {
                                                     xtype: 'container',
                                                     layout: {
                                                         type: 'hbox',
                                                         align: 'stretch',
                                                         pack: 'center'
                                                     },
                                                     items: [
                                                         {
                                                             xtype: 'tbspacer',
                                                             flex: 1
                                                         },
                                                         {
                                                             xtype: 'button',
                                                             text: 'Entrar',
                                                             width: 100,
                                                             itemId: 'BtnRegistro',
                                                             listeners: {
                                                                 click: 'iniciarSession'
                                                             }
                                                         },
                                                         {
                                                             xtype: 'tbspacer',
                                                             flex: 1
                                                         }
                                                     ]
                                                 }


                                        ]
                                    },
                                    {
                                        xtype: 'fieldset',
                                        //style: {
                                        //    'background-color': 'transparent',
                                        //    'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                                        //    'background-size': '100% 100%',
                                        //    'background-repeat': 'no-repeat'
                                        //},
                                        padding: 10,
                                        itemId: 'ContraFS',
                                        hidden: true,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                style: { 'border-radius': '7px' },
                                                anchor: '100%',
                                                fieldLabel: 'Correo Electrónico Registrado',
                                                labelAlign: 'top',
                                                labelWidth: 150,
                                                maxLength: 60,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                //allowBlank: false,
                                                vtype: 'email',
                                                //allowOnlyWhitespace: false,
                                                name: 'mailUsuario'
                                            },
                                            {
                                                xtype: 'tbspacer',
                                                height: 10
                                            },
                                            {
                                                xtype: 'container',
                                                layout: {
                                                    type: 'hbox',
                                                    align: 'stretch',
                                                    pack: 'center'
                                                },
                                                items: [
                                                    {
                                                        xtype: 'tbspacer',
                                                        flex: 1
                                                    },
                                                   {
                                                       xtype: 'button',
                                                       text: 'Enviar',
                                                       width: 100,
                                                       itemId: 'BtnEnvioCorreo',
                                                       listeners: {
                                                           click: 'enviarCorreo'
                                                       }
                                                   },
                                                    {
                                                        xtype: 'tbspacer',
                                                        flex: 1
                                                    },
                                                   {
                                                       xtype: 'button',
                                                       text: 'Cancelar',
                                                       width: 100,
                                                       itemId: 'BtnCancelar',
                                                       listeners: {
                                                           click: 'cancelarEnvioContrasena'
                                                       }
                                                   },
                                                    {
                                                        xtype: 'tbspacer',
                                                        flex: 1
                                                    }
                                                ]
                                            }

                                        ]
                                    }
                                ]
                            }
                    ]
                }
            ]
        }
    ]

});