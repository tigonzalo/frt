
Ext.define('FRT.view.documentos.DocumentosCliente', {
    extend: 'Ext.container.Container',
    alias: 'widget.documentoscliente',

    requires: [
        'FRT.view.clientes.ClientesMain',
        'FRT.view.documentos.DocumentosClienteController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View'
    ],
    controller: 'documentoscliente',
    autoScroll: true,
    initComponent: function () {
        var me = this;
            //storeUsosCFDI = Ext.create('FRT.store.usosCFDI.UsosCFDI'),
            //storeEmpresas = Ext.create('FRT.store.empresas.Empresas');
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    region: 'north',
                    height: 100,
                    itemId: 'headerPanel',
                    title: 'FACTURA REST'
                },
                {
                    xtype: 'tabpanel',
                    region: 'center',
                    itemId: 'contentPanel',
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            itemId: 'tab1',
                            layout: {
                                type: 'fit'
                            },
                            title: 'INFORMACION TICKET',
                            items: [
                                {
                                    xtype: 'form',
                                    margin: 10,
                                    autoScroll: true,
                                    defaults: {
                                        labelWidth: 180,
                                        maxWidth: 300,
                                        minWidth: 500,
                                        width: 500,

                                    },
                                    layout: {
                                        align: 'center',
                                        padding: 10,
                                        type: 'vbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            maxWidth: 300,
                                            minWidth: 500,
                                            width: 500,
                                            fieldLabel: '*Retaurant/empresa',
                                            labelWidth: 180
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Número de folio del Tikect'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: '*Fecha de consumo'
                                        },
                                        {
                                            xtype: 'numberfield',
                                            fieldLabel: '*Total consumo'
                                        },
                                        {
                                            xtype: 'numberfield',
                                            fieldLabel: '*Cantidad a facturar'
                                        }
                                    ]
                                }
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'bottom',
                                    items: [
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'button',
                                            width: 200,
                                            text: 'Siguiente'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            itemId: 'tab2',
                            title: 'Datos del cliente',
                            items: [
                                {
                                    xtype: 'form',
                                    margin: 10,
                                    autoScroll: true,
                                    defaults: {
                                        labelWidth: 180,
                                        maxWidth: 300,
                                        minWidth: 500,
                                        width: 500,

                                    },
                                    layout: {
                                        align: 'center',
                                        padding: 10,
                                        type: 'vbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Rfc del cliente'
                                        },
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            fieldLabel: '*Uso del CFDI'
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: '*Correo del cliente',
                                            vtype: 'email'
                                        }
                                    ]
                                }
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'bottom',
                                    items: [
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'button',
                                            width: 200,
                                            text: 'Siguiente'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            itemId: 'tab3',
                            title: 'Optener'
                        }
                    ]
                }
            ]
            //    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    //listeners: {
    //    afterrender: 'cargarEmpresasCmb'

    //}

});