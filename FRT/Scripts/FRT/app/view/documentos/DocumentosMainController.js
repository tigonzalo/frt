

Ext.define('FRT.view.documentos.DocumentosMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.documentosMain',
    calcularMontoFraccion: function (me, newValue, oldValue, eOpts) {
        
        var view = this.getView();
        var montoTicket = view.down('#total').getValue();
        var nFracciones = view.down('#nFracciones').getValue();

        var totalFraccion = montoTicket / nFracciones;

        view.down('#totalFraccion').setValue(totalFraccion.toFixed(2));


    },
    crearFraccionTicket: function (me, newValue, oldValue, eOpts) {
        
        var view = this.getView();
        this.expandirFraccion(newValue);


    },
    expandirFraccion: function (value) {
        var view = this.getView(),
            fielFraccion = view.down('#fielFraccion'),
            totalFraccion = view.down('#totalFraccion'),
            nFracciones = view.down('#nFracciones');

        if (value) {
            fielFraccion.setExpanded(true);
            nFracciones.setValue(2);
            //totalFraccion.setValue(0);
        } else {
            fielFraccion.setExpanded(false);
            //fielFraccion.setDisabled(true);
        }

    },
    cargarDocumentosConsulta: function (me, eOpts) {
        this.cargarGridDocumentos();
    },
//     cargarStores: function (me, eOpts) {
//         var storeDocumentos = Ext.getStore('Documentos.Documentos');
//         storeDocumentos.load({
//             //  params: params
//             scope: this,
//             callback: function (records, operation, success) {
//             }
//         });
//     }
    //     ,
    crearReporteDocumentos: function () {
        
        //this.open('POST', url, record.get('codDocumento'), '_blank');
        var view = this.getView();
        var form = document.createElement("form");
        form.action = FRT.Routes.root + 'Documentos/RepDocumentos';
        form.method = 'POST';
        form.style = "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400"
        form.target = '_blank' || "_self";
        
            
            var input = document.createElement("INPUT");
            input.name = 'busqvalor';
            input.value = view.down('#txtBuscar').getValue();
            form.appendChild(input);
           
        
        form.style.display = 'none';
        document.body.appendChild(form);
        form.submit();
    },
    cargarGridDocumentos: function (url) {
         
        var view = this.getView()
        var gridDocumentos = view.down('#gridDocumentos');
         if (view.down('#paginadorDocumentos').getStore().currentPage > 1)
             view.down('#paginadorDocumentos').moveFirst();
        else
        gridDocumentos.getStore().load({
            url: url,
            params: {
                busqvalor: view.down('#txtBuscar').getValue()
            },
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },
    buscarDocumentos: function (me, e, eOpts) {
         
        if (e.getCharCode() == Ext.EventObject.ENTER) 
            this.cargarGridDocumentos();
        
    },
    cargarDocumentosToolBar: function (me, page, eOpts) {
         
        var view = this.getView()
        var p = me.getStore().getProxy();
        p.setExtraParam('busqvalor', view.down('#txtBuscar').getValue());
    },
    changeBuscarDocumentos: function (me, newValue, oldValue, eOpts) {
        if (newValue.length === 0) 
            this.cargarGridDocumentos();
    },
    guardarDocumento: function (me, e, eOpts) {
        
        var view = this.getView();
        cmbUsosCFDI = view.down('#cmbUsosCFDI'),
        cmbEmpresa = view.down('#CmbEmpresa'),
            form = view.down('#FormDocumento'),
            data = form.getForm().getValues();
        if (!form.getForm().isValid()) {
            this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
            return;
        }
        //if (data.total == 00) {
        //    this.mnj('Error', 'El Monto del total no puede ser 0.', Ext.MessageBox.ERROR)
        //    return;
        //}
        view.mask('Procesando Factura...');
        var objDocumentos = Ext.create('FRT.model.documentos.Documento', data);
        objDocumentos.setUsoCFDI(cmbUsosCFDI.getSelection());
        objDocumentos.setEmpresa(cmbEmpresa.getSelection());

        Ext.Ajax.request({
            url: FRT.Routes.root + 'Documentos/Guardar',
            encode: true,
            params: Ext.encode(objDocumentos.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.mnj('Aviso', 'El Sistema le enviara su factura y documento XML al correo registrado.')
                    this.consultarFacturas();
                }

                //this.limpiarForm();
                view.unmask();
                //this.cargarStores();
            },
            failure: function (o, r, n) {
                me.mnj('Error', 'El registro no se registro', Ext.MessageBox.ERROR)
                this.limpiarForm();
               view.unmask();
            }
        });

    }
    , limpiarForm: function () {
        var view = this.getView();
        view.reset();

    }
    , mostrarDetalleCliente: function () {
         
        var view = this.getView();
        var vistaCliente = view.down('#datosCliente');
        vistaCliente.disable();
       // var objUsuario = Ext.create('FRT.model.usuarios.Usuario', FRT.Session.usuario);
        //Ext.Ajax.request({
        //                        url: FRT.Routes.root + 'Clientes/Listado',
        //                        encode: true,
        //                        params: Ext.encode(objUsuario.getData(true)),
        //                        headers: { 'Content-Type': 'application/json' },
        //                        method: 'post',
        //                        scope: this,
        //                        success: function (response) {
        //                            var resp = Ext.JSON.decode(response.responseText);
        //                            if (resp.success == true) {
        //                                 
        //                               // me.mnj('Aviso', 'El registro fue eliminado')

        //                            }
        //                            me.cargarStores();
        //                            me.getView().unmask();
        //                        },
        //                        failure: function (o, r, n) {
        //                            me.mnj('Error', 'El registro no fue eliminado', Ext.MessageBox.ERROR)
        //                            me.getView().unmask();
        //                        }
        //                    });


    },
    //cargar los ususos  de los CFDI
    cargarUsosCFDI: function () {
        var view = this.getView(),
        cmbUsosCFDI = view.down('#cmbUsosCFDI');
        cmbUsosCFDI.getStore().load();
    }
    , cargarEmpresasCmb: function (inicial) {
        
        this.informacionCliente();
        this.cargarUsosCFDI();
        var me = this;
        var view = this.getView();
        var comboEmpresa = view.down('#CmbEmpresa');
        var proxy = comboEmpresa.getStore().getProxy();
        comboEmpresa.getStore().pageSize = 10;
        proxy.setExtraParam('start', 0);
        proxy.setExtraParam('limit', 10);
        comboEmpresa.getStore().load({
            params: { campo: 'Nombre', valor: comboEmpresa.getRawValue() },
            scope: this,
            url: FRT.Routes.root + 'Empresas/Listado',
            pageSize: 10,
            callback: function (records, operation, success) {
                if (success == true) {
                    if (!inicial == true) {
                         
                       // TUA.common.Common.prepareExpadPickerEvent(me.getCmbPuestos());
                    }
                }
            }
        });
    }
   , informacionCliente: function () {
        
        var view = this.getView();
        var botonGuardar = view.down('#FormDocumento #clienteMain #BtnGuardar');
        var botonCancelar = view.down('#FormDocumento #clienteMain #BtnLimpiar');
        var vistaCliente = view.down('#FormDocumento #clienteMain #FormCliente');
        //vistaCliente.setTitle('Datos de Facturación del Cliente')
        botonGuardar.setVisible(false);
        botonCancelar.setVisible(false);
       // this.setReadOnlyForAll(true, vistaCliente)

    }
   , setReadOnlyForAll: function (readOnly, vista) {
        Ext.suspendLayouts();
        vista.getForm().getFields().each(function (field) {
            field.setReadOnly(readOnly);
        });
        Ext.resumeLayouts();
   }
   , siguiente: function () {
       
       var view = this.getView();
       var vistaCliente = view.down('#FormDocumento #clienteMain #FormCliente');
       this.guardarCliente(vistaCliente)
   }
  , guardarCliente: function (vistaCliente) {
       
    var view  = this.getView();
    form = vistaCliente;
    //btnGuardar = view.down('#BtnGuardar'),
    data = form.getForm().getValues();
      
    if (!form.getForm().isValid()) {
        this.mnj('Error', 'Datos no validos en el formulario.<BR> Favor de verificarlos', Ext.MessageBox.ERROR)
        return;
    }
    if (data.passwordUsuario !== data.cpasswordUsuario) {
        this.mnj('Error', 'Las contraseñas no coinciden.', Ext.MessageBox.ERROR);
        return;
    }
    view.mask('Procesando...');
        //         
      //        me = this,
    
    var codEstado = view.down('#cmbEstados').getValue();
    var objClientes = Ext.create('FRT.model.clientes.Cliente', {
        codCliente: data.codCliente, razonSocialClie: data.razonSocialClie, rfcClie: data.rfcClie,
        calleClie: data.calleClie, numExtClie: data.numExtClie, numIntClie: data.numIntClie, coloniaClie: data.coloniaClie, codMunicipio: data.codMunicipio,
        codEstado: codEstado, localidadClie: data.localidadClie, paisClie: data.paisClie, cpClie: data.cpClie
    });
    objClientes.setUsuario(Ext.create('FRT.model.usuarios.Usuario', {
        codUsuario: data.codUsuario, loginUsuario: data.loginUsuario, passwordUsuario: data.passwordUsuario, nombreUsuario: data.nombreUsuario, apePaterUsuario: data.apePaterUsuario,
        apeMaterUsuario: data.apeMaterUsuario, mailUsuario: data.mailUsuario, telefonoUsuario: data.telefonoUsuario
    }));
    objClientes.getUsuario().setPerfil(Ext.create('FRT.model.perfiles.Perfil', { codPerfil: 1 }))

    Ext.Ajax.request({
        url: FRT.Routes.root + 'Clientes/Guardar',
        encode: true,
        params: Ext.encode(objClientes.getData(true)),
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        scope: this,
        success: function (response) {
            view.unmask();
            var resp = Ext.JSON.decode(response.responseText);
            if (resp.success == true) {
                if (this.editar == false) {
                    this.iniciarSession()
                    return;
                }
                var view2 = this.getView();
                var vistaCliente = view2.down('#FormDocumento #datosCliente');
                vistaCliente.setVisible(false);
                var vistaFactura = view2.down('#FormDocumento #facturaFielset');
                var documentoDescarga = view2.down('#FormDocumento #documentoDescarga');
                var botonliente = view2.down('#FormDocumento #BtnDatosCliente');
                //var botonFacturar = view2.down('#FormDocumento #BtnGuardarF');
                //var BtnConsultarF = view2.down('#FormDocumento #BtnConsultarF');
                vistaFactura.setVisible(true);
                documentoDescarga.setVisible(true);
                botonliente.setVisible(false);
                Ext.ComponentQuery.query('#FormDocumento #botonestb')[0].setVisible(true)
                //botonFacturar.setVisible(true);
                //BtnConsultarF.setVisible(true); 
                
              
            }
                
               
        },
        failure: function (o, r, n) {
               
            this.limpiarForm();
            view.unmask();
        }
           
    });
        
}
  , consultarFacturas: function () {
      Ext.ComponentQuery.query('main #contentPanel')[0].removeAll();
      var documentosConsultaMain = Ext.create('FRT.view.documentos.DocumentosConsultaMain');
      Ext.ComponentQuery.query('main #contentPanel')[0].add(documentosConsultaMain);
  }
  , visualizarPdf: function (me, record, item, index, e, eOpts) {
      //this.descargarXML(34)
      var view = this.getView();
      var codDocumento = view.down('#FormDocumento #codDocumento');
      codDocumento.setValue(34)
      var file = codDocumento.getValue() + '.pdf';
    
      if (file) {
          var dir = FRT.Routes.root + 'Documentos/pdf/' + file;
          window.open(dir, '_blank', 'left=10, top=10,width=600,height=600');
      }
      else
          this.mnj('Aviso', 'No se puede mostar documento PDF! debe generar una Factura.');
  },
  retimbrarCFDI: function (me, record, item, index, e, eOpts) { 
      var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
      this.getView().mask("Procesando..");
      Ext.Ajax.request({
          url: FRT.Routes.root + 'Documentos/RetimbrarCFDI',
          encode: true,
          params: Ext.encode(objDocumento.getData(true)),
          headers: { 'Content-Type': 'application/json' },
          method: 'post',
          scope: this,
          success: function (response) {

              var resp = Ext.JSON.decode(response.responseText);
              if (resp.success == true) {
                  this.cargarGridDocumentos();
                  url = FRT.Routes.root + 'Documentos/RepImpresaCFDI';
                  this.open('POST', url, record.get('codDocumento'), '_blank');
              }
              //else {
              //    this.mnj('Error', 'Folio no Facturado, </br> Favor de reportarlo al administrador', Ext.MessageBox.ERROR);
              //    this.getView().unmask();
              //    return;

              //}

              this.getView().unmask();

          },
          failure: function (o, r, n) {
              this.getView().unmask();
          }

      });
  },

  descargarArchivoMain: function (me, record, item, index, e, eOpts) {
       
      
      //vericar documento
      
      if (item ==="actionPdf") {
          if (record.get('StatusDocumento') == 'En Proceso') {
              var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
              this.vericarCFDI(objDocumento);
              return;
          }
         if (record.get('StatusDocumento') == 'Cancelado') {
             this.mnj('Aviso', 'Esta Factura esta Cancelada.');
             return;
         }
      }
     if (item === "actionCancelarCFDI") {
        this.cancelarCFDI(me, record, item, index, e, eOpts);

     }
     if (item === "actionReHacerXML") {
         if (record.get('StatusDocumento') == 'Facturado')
             return;
         this.reHacerXML(me, record, item, index, e, eOpts);
		 return;

     }

     
      //
    //var uuid = record.get("uuid");
    //if (uuid.length !== 36) {
    //    this.retimbrarCFDI(me, record, item, index, e, eOpts);
    //    return;
        
    //    //this.mnj('Error', 'Folio no Facturado, </br> Favor de reportarlo al administrador', Ext.MessageBox.ERROR);
    //    //return;
    //}
     if (item === "actionPdf" && record.get('StatusDocumento') == 'Facturado') {
         
        url = FRT.Routes.root + 'Documentos/RepImpresaCFDI';
        this.open('POST', url, record.get('codDocumento'), '_blank');
       
    }
     if (item === "actionXml" && record.get('StatusDocumento') == 'Facturado') {
        this.descargarXML(me, record, item, index, e, eOpts);

    }
     if (item === "actionEnviarCorreo" && record.get('StatusDocumento') == 'Facturado') {
        this.enviarCorreoCFDI(me, record, item, index, e, eOpts);

    }

  },
  vericarCFDI: function (objDocumento) {
       
      this.getView().mask("Procesando..");
      Ext.Ajax.request({
          url: FRT.Routes.root + 'Documentos/Listado',
          encode: true,
          params: Ext.encode({ codDocumento: objDocumento.get('codDocumento')}),
          headers: { 'Content-Type': 'application/json' },
          method: 'post',
          scope: this,
          success: function (response) {
              var resp = Ext.JSON.decode(response.responseText);
              if (resp.success == true) {
                  if (resp.List[0].StatusDocumento.Descripcion == 'Facturado') {
                      this.cargarGridDocumentos();
                      url = FRT.Routes.root + 'Documentos/RepImpresaCFDI';
                      this.open('POST', url, objDocumento.get('codDocumento'), '_blank');
                      
                  } else {
                       
                      if (resp.List[0].nError == 166197) {
                          this.mnj("Error", "La Factura no se genero.<br> Error en captura de datos del Cliente RFC", Ext.MessageBox.ERROR);
                      } else {
                          this.mnj('Aviso', "El sistema ya capturo la informacion del ticket:" + objDocumento.get('folio') + "<br> en breve se enviaran la factura y el documento XML al correo registrado.");
                      }
                     
                      
                  }
                  this.getView().unmask();
                  return;
                  
              }
              //else {
              //    this.mnj('Error', 'Folio no Facturado, </br> Favor de reportarlo al administrador', Ext.MessageBox.ERROR);
              //    this.getView().unmask();
              //    return;

              //}

              this.getView().unmask();

          },
          failure: function (o, r, n) {
              this.getView().unmask();
          }

      });
  },

 enviarCorreoCFDI: function (me, record, item, index, e, eOpts) {
 
     var objDocumento=Ext.create('FRT.model.documentos.Documento',record.getData());
     this.getView().mask("Procesando..");
     Ext.Ajax.request({
         url: FRT.Routes.root + 'Documentos/RecuperarCFDI',
         encode: true,
         params: Ext.encode(objDocumento.getData(true)),
         headers: { 'Content-Type': 'application/json' },
         method: 'post',
         scope: this,
         success: function (response) {

             var resp = Ext.JSON.decode(response.responseText);
             if (resp.success == true) {
                 this.mnj('Aviso', 'Los Archivos se han enviado al correo Registrado.');
             }

             this.getView().unmask();

         },
         failure: function (o, r, n) {
             this.getView().unmask();
         }

     });
 },
 reHacerXML: function (me, record, item, index, e, eOpts) {
     me = this;
     Ext.MessageBox.show({
         title:'Cancelar Re Hacer XML?',
         msg: 'Desea Re Hacer el XML?',
         buttons: Ext.Msg.YESNO,
         icon: Ext.Msg.QUESTION,
         fn: function(btn){                    
             if (btn == "no"){
                 return;
             }
             if (btn == "yes"){
                 var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
                 me.getView().mask("Procesando..");
                 Ext.Ajax.request({
                     url: FRT.Routes.root + 'Documentos/ReHacerXML',
                     encode: true,
                     params: Ext.encode(objDocumento.getData(true)),
                     headers: { 'Content-Type': 'application/json' },
                     method: 'post',
                     scope: this,
                     success: function (response) {

                         var resp = Ext.JSON.decode(response.responseText);
                         if (resp.success == true) {
                             me.cargarGridDocumentos();
                         }

                         me.getView().unmask();

                     },
                     failure: function (o, r, n) {
                         me.getView().unmask();
                     }

                 });
             }
         }                
     });
    
 },
cancelarCFDI: function (me, record, item, index, e, eOpts) {
    me = this;
    Ext.MessageBox.show({
        title:'Cancelar Documento?',
        msg: 'Desea Cancelar el Documento?',
        buttons: Ext.Msg.YESNO,
        icon: Ext.Msg.QUESTION,
        fn: function(btn){                    
            if (btn == "no"){
                return;
            }
            if (btn == "yes"){
                var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
                me.getView().mask("Procesando..");
                Ext.Ajax.request({
                    url: FRT.Routes.root + 'Documentos/CancelarCFDI',
                    encode: true,
                    params: Ext.encode(objDocumento.getData(true)),
                    headers: { 'Content-Type': 'application/json' },
                    method: 'post',
                    scope: this,
                    success: function (response) {

                        var resp = Ext.JSON.decode(response.responseText);
                        if (resp.success == true) {
                            me.cargarGridDocumentos();
                        }

                        me.getView().unmask();

                    },
                    failure: function (o, r, n) {
                        me.getView().unmask();
                    }

                });
            }
        }                
});
    
    }
    , descargarXML: function (me, record, item, index, e, eOpts) {
        var view = this.getView();
        var codDocumento = record.get('codDocumento')

        var empresa = record.getEmpresa();
        var fileXML ='33'+ empresa.get('rfcEmp') + 'Resultado/' + record.get('uuid') + '.xml';// codDocumento + '.xml';

        var Path = record.get('rutaPdf');

        if ( fileXML) {
            //si es esa ruta? bueno lo dejamos ahi
            var dir = FRT.Routes.root + 'Models/' + 'DocumentosPac/' + fileXML;
            var link = document.createElement("a");
            link.download = fileXML;
            link.href = dir;
            link.click();

        } else
            this.mnj('Aviso', 'No se puede realizar la descarga! debe generar una Factura.');
    }
,  open: function (verb, url, data, target) {
         
    var form = document.createElement("form");
    form.action = url;
    form.method = verb;
    form.style = "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400"
    form.target = target || "_self";
    if (data) {
        // for (var key in data) {
        var input = document.createElement("INPUT");
        input.name = 'codDocumento';
        // input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
        input.value = data
        form.appendChild(input);
        //}
    }
    form.style.display = 'none';
    document.body.appendChild(form);
    form.submit();
}
 , descargarArchivos: function (me, record, item, index, e, eOpts)
 {
     var view = this.getView();
     var codDocumento = record.get('codDocumento')

     var filePDF = record.get('rutaXml') + '.pdf';// codDocumento + '.pdf';
     var fileXML = record.get('rutaXml') + '.xml';// codDocumento + '.xml';

     var Path = record.get('rutaPdf');

    if (filePDF && fileXML) {
        //si es esa ruta? bueno lo dejamos ahi
        var dir = FRT.Routes.root + 'Documentos/' + fileXML;
            var link = document.createElement("a");
            link.download = fileXML;
            link.href = dir;
            link.click();

            var dir2 = FRT.Routes.root + 'Documentos/' + filePDF;
            var link2 = document.createElement("a");
            link2.download = filePDF;
            link2.href = dir2;
            link2.click();

     }else
         this.mnj('Aviso', 'No se puede realizar la descarga! debe generar una Factura.');
    }

    ,ObtenerPdfXml: function (me, record, item, index, e, eOpts) {
        var view = this.getView();
        var objDocumento = Ext.create('FRT.model.documentos.Documento', record.getData());
        // no hace la peticion al servidor params: Ext.encode(objClientes.getData(true)),
         Ext.Ajax.request({
             url: FRT.Routes.root + 'Documentos/Copiar',
            encode: true,
            params: Ext.encode(objDocumento.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,

                 
            success: function (response) {

                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
                    this.descargarArchivos(me, record, item, index, e, eOpts);
                    //this.mnj('Mensaje', ' Proceso Exitoso', Ext.MessageBox.INFO);
                    //no hace nada
                }
            },
            failure: function (o, r, n) {
                this.mnj('Error', 'No se copiaron los archivos PDF y XML', Ext.MessageBox.ERROR);
            }
        });

}    

    ,mnj: function (title, mnj, ico) {

        Ext.MessageBox.show({
            title: title,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: (ico ? ico : Ext.MessageBox.INFO)
        });
    },

    buscarDoc: function (me, e, eOpts) {
         var busqcampo = this.getView().down('#Busqcampo').getValue();
         var busqvalor = this.getView().down('#Busqvalor').getValue();

         if ((e.keyCode === Ext.EventObject.ENTER) || ((e.keyCode === Ext.EventObject.BACKSPACE || e.keyCode === Ext.EventObject.DELETE) && me.getValue() == "")) {

             this.cargarDoc({ busqcampo: busqcampo, busqvalor: busqvalor });
         } else if (me.getItemId() === 'Busqcampo') {
             this.cargarDoc({ busqcampo: busqcampo, busqvalor: busqvalor });
         }
     },

     cargarDoc: function (params) {
         var store = Ext.getStore('Documentos.Documentos');
         store.load({
             params: params ? params : {},
             scope: this,
             callback: function (records, operation, success) {
             }
         });
     },
    
});
