

Ext.define('FRT.view.documentos.DocumentosConsultaMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.DocumentosConsultaMain',
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    requires: [
    //        'FRT.view.documentos.documentosMainViewModel',
        'FRT.view.documentos.DocumentosMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View',
        'Ext.toolbar.Paging',
        'Ext.toolbar.Spacer'
        
    ],
    controller: 'documentosMain',
 
    initComponent: function () {
        var me = this;
        storeDocumentos = Ext.create('FRT.store.documentos.Documentos');
        Ext.applyIf(me, {
            dockedItems: [
              {
                  xtype: 'toolbar',
                  dock: 'top',
                  items: [
                      {
                          xtype: 'textfield',
                          fieldLabel: 'Label'
                      }
                  ]
              }
            ],
            items: [
                   {
                    xtype: 'gridpanel',
                    itemId: 'gridDocumentos',
                    style: {
                        'background-color': 'transparent',
                        'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                        'background-size': '100% 100%',
                        'background-repeat': 'no-repeat',
                        'border-radius': '7px 7px 0px 0px',
                        'color':'white'
                    },
                    flex: 1,
                    dockedItems: [
                        {
                            dock: 'top',
                            height : 70,
                            xtype: 'toolbar',
                            style: {
                                'background-color': 'transparent'
                            },
                            items: [
                                {
                                    xtype: 'tbspacer',
                                    flex:1
                                },
                                {
                                    xtype: 'textfield',
                                    cls: 'rounded',
                                    itemId: 'txtBuscar',
                                    width: 400,
                                    fieldLabel: 'Buscar',
                                    labelAlign:'top',
                                    labelWidth: 50,
                                    emptyText :'Criterio de Busqueda',
                                    enableKeyEvents: true,
                                    listeners: {
                                        keypress: 'buscarDocumentos',
                                        change: 'changeBuscarDocumentos'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Reporte',
                                    itemId: 'btnReporte',
                                    hidden: !FRT.common.FnConnSeg.isVisible('Documentos', 'RepDocumentos'),
                                    listeners: {
                                        click: 'crearReporteDocumentos'
                                    }

                                }
                            ]
                        },
                        {
                            xtype: 'pagingtoolbar',
                            itemId: 'paginadorDocumentos',
                            cls: 'go-paging-tb',
                            dock: 'bottom',
                            displayInfo: true,
                            //style: {
                            //    'background-color': '#174360',
                            //    //'background-image': 'url(' + FRT.Routes.root + 'Scripts/FRT/Imagenes/fondoContainer.png' + ')',
                            //    'background-size': '100% 100%',
                            //    'background-repeat': 'no-repeat',
                            //   // 'border-radius': '0px 0px 7px 7px',
                            //    'color': 'red',
                                
                            //},
                            store: storeDocumentos,
                            listeners: {
                                beforechange: 'cargarDocumentosToolBar'
                            }
                        }
                    ],
                    store: storeDocumentos,
                    columns: [
                        {
                            xtype: 'rownumberer',
                            width: 35
                        },
                        {
                            xtype: 'actioncolumn',
                            width: 40,
                            align :"center",
                            hidden: !FRT.common.FnConnSeg.isVisible('Documentos', 'CancelacionCFDI'),
                            items: [
                                {
                                    icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/Delete.png',
                                    tooltip: 'Cancelar CFDI',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('grid').fireEvent('itemclick', grid, rec, 'actionCancelarCFDI', rowIndex, colIndex);

                                    }

                                }
                            ]
                        },
                        {
                            xtype: 'actioncolumn',
                            width: 40,
                            align: "center",
                            //hidden: !FRT.common.FnConnSeg.isVisible('Documentos', 'CancelacionCFDI'),
                            items: [
                                {
                                    icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/report.png',
                                    tooltip: 'Re hacer XML',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('grid').fireEvent('itemclick', grid, rec, 'actionReHacerXML', rowIndex, colIndex);

                                    }

                                }
                            ]
                        },
                        {
                            xtype: 'actioncolumn',
                            width: 40,
                            align: "center",
                           // hidden: !FRT.common.FnConnSeg.isVisible('Documentos', 'CancelacionCFDI'),
                            items: [
                                {
                                    icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/mail.png',
                                    tooltip: 'Enviar Docuemntos a Correo',
                                    handler: function (grid, rowIndex, colIndex) {
                                        var rec = grid.getStore().getAt(rowIndex);
                                        this.up('grid').fireEvent('itemclick', grid, rec, 'actionEnviarCorreo', rowIndex, colIndex);

                                    }

                                }
                            ]
                        },
                         {
                             xtype: 'actioncolumn',
                             width: 40,
                             align: "center",
                             text: 'PDF',
                             items: [
                                 {
                                     icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/file-downloadpdf.png',
                                     tooltip: 'Descarga PDF',
                                     handler: function (grid, rowIndex, colIndex) {
                                         var rec = grid.getStore().getAt(rowIndex);
                                         this.up('grid').fireEvent('itemclick', grid, rec, 'actionPdf', rowIndex, colIndex);

                                     }

                                 }
                             ]
                         },
                           {
                               xtype: 'actioncolumn',
                               width: 40,
                               align: "center",
                               text: 'XML',
                               items: [
                                   {
                                       icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16x16/xml.png',
                                       tooltip: 'Descarga XML',
                                       handler: function (grid, rowIndex, colIndex) {
                                           var rec = grid.getStore().getAt(rowIndex);
                                           this.up('grid').fireEvent('itemclick', grid, rec, 'actionXml', rowIndex, colIndex);

                                       }

                                   }
                               ]
                           },
                           {
                               xtype: 'gridcolumn',
                               dataIndex: 'cliente',
                               text: 'RFC Cliente',
                               flex: 2,
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   var cliente = record.getCliente();
                                   return cliente.get('rfcClie');
                               }
                           },

                           {
                               xtype: 'gridcolumn',
                               dataIndex: 'usuario',
                               text: 'Correo',
                               flex: 2,
                               renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                   var usuario = record.getUsuario()
                                   return usuario.get('mailUsuario');
                               }
                           },

                        {
                            xtype: 'gridcolumn',
                            width: 110,
                            dataIndex: 'fechaConsumo',
                            text: 'Fecha Consumo',
                            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                return Ext.Date.format(value, 'd/m/Y');
                            }

                        },
                        {
                            xtype: 'gridcolumn',
                            width: 110,
                            dataIndex: 'fecha',
                            text: 'Fecha Factura',
                            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                return Ext.Date.format(value, 'd/m/Y');
                            }

                        },
                        {
                            xtype: 'gridcolumn',
                            flex: 1,
                            dataIndex: 'folio',
                            text: 'Folio'
                        },
                        

                        {
                            xtype: 'gridcolumn',
                            flex: 1,
                            dataIndex: 'folioFactura',
                            text: 'Factura',
                        },

                        //{
                        //    xtype: 'gridcolumn',
                        //    dataIndex: 'empresa',
                        //    text: 'Restaurante',
                        //    flex: 2,
                        //    renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                        //        var empresa = record.getEmpresa();
                        //        return empresa.get('nombreEmp');
                        //    }
                        //},

                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'StatusDocumento',
                            text: 'Estatus',
                            flex: 1,
                            //renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                            //    
                            //    var uuid = record.get("uuid");
                            //    if (uuid.length == 36)
                            //        return "Facturada";
                            //    else
                            //        return "en Proceso";
                            //}
                        },
                        {
                            xtype: 'gridcolumn',
                            width: 127,
                            align: 'right',
                            dataIndex: 'total',
                            text: 'Total',
                            flex: 1,
                            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                                return Ext.util.Format.usMoney(value);
                            }
                        }
                    ],
                    

                    listeners: {
                        itemclick: 'descargarArchivoMain'

                    }
                }
            ]
            //    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarDocumentosConsulta'

    }

});