

Ext.define('FRT.view.documentos.DocumentosMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.documentosmain',

    requires: [
        'FRT.view.clientes.ClientesMain',
        'FRT.view.documentos.DocumentosMainController',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Action',
        'Ext.grid.View'
    ],
    controller: 'documentosMain',
    autoScroll: true,
    initComponent: function () {
        var me = this,
            storeUsosCFDI = Ext.create('FRT.store.usosCFDI.UsosCFDI'),
            storeEmpresas = Ext.create('FRT.store.empresas.Empresas');
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    itemId: 'FormDocumento',
                    scrollable: true,
                    items: [
                        {
                            xtype: 'container',
                            items: [

                                {
                                    xtype: 'container',
                                    hidden: true,
                                    itemId: 'facturaFielset',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    defaults: {
                                        margin: '5 5 5 5',
                                    },
                                    items: [
                                     {
                                         xtype: 'tbspacer',
                                         flex: .5
                                     },
                                        {
                                            xtype: 'fieldset',
                                            title: '',
                                            flex: 2,
                                            width: 500,
                                            layout: {
                                                type: 'auto'
                                            },
                                            padding: '5 0 10 0',
                                            defaults: {
                                                margin: '2 2 2 2'
                                            },
                                            items: [
                                                {
                                                    xtype: 'hiddenfield',
                                                    anchor: '100%',
                                                    itemId: 'codDocumento',
                                                    fieldLabel: 'Label',
                                                    name: 'codDocumento'
                                                },
                                                {
                                                    xtype: 'combobox'
                                                    , listConfig: {
                                                        loadingText: 'Cargando...'
                                                        , emptyText: 'No se encontraron registros coincidentes '
                                                        , tpl: Ext.create('Ext.XTemplate',
                                                        '<table Id="comboTable"  width="100%">',
                                                            '<th width="100%">Empresa/Restaurant</th>',
                                                        '</table>',
                                                            '<tpl for=".">',
                                                                '<div class="x-boundlist-item" margin="0">',
                                                                    '<table Id="comboTable" width="100%">',
                                                                        '<td width="100%">{nombreEmp}</td>',
                                                                         //'<td width="33%">{IdDptmto}</td>',
                                                                         // '<td width="33%">{IdArea}</td>',
                                                                    '</table>',
                                                                '</div>',
                                                            '</tpl>'

                                                    ),
                                                        displayTpl: Ext.create('Ext.XTemplate',
                                                            '<tpl for=".">',
                                                            '{nombreEmp}',
                                                            '</tpl>'
                                                            ),
                                                    }
                                                    , flex: 0.65
                                                    //, labelAlign: 'top'
                                                    //top, right, bottom, left
                                                    , margin: '5 20 10 10'
                                                    , itemId: 'CmbEmpresa'
                                                    , width: "80%"
                                                    , maxWidth :600
                                                    , enableKeyEvents: true
                                                    , displayField: 'nombreEmp'
                                                    , pageSize: 10
                                                    , store: storeEmpresas
                                                    , queryMode: 'local'
                                                    , valueField: 'codEmpresa'
                                                    , autoSelect: false
                                                    , anyMatch: true
                                                    , fieldLabel: 'Empresa/Restaurant'
                                                    , labelWidth: 180
                                                    , forceSelection: true
                                                    , msgTarget: 'side'
                                                    , allowBlank: false
                                                    , name: 'codEmpresa'
                                                    //, hidden: JSON.parse(TUA.common.Seguridad.verificarVarConfig('S').toLowerCase())
                                                },
                                                {
                                                    xtype: 'combobox'
                                                    , flex: 0.65
                                                    , name: 'codUsoCFDI'
                                                    , itemId: 'cmbUsosCFDI'
                                                    , width: "80%"
                                                    , margin: '0 20 5 10'
                                                    , maxWidth: 600
                                                    , enableKeyEvents: true
                                                    , displayField: 'claveDescripcion'
                                                    , store: storeUsosCFDI
                                                    , queryMode: 'local'
                                                    , valueField: 'codUsoCFDI'
                                                    , autoSelect: false
                                                    , anyMatch: true
                                                    , fieldLabel: 'Uso del CFDI'
                                                    , labelWidth: 180
                                                    , forceSelection: true
                                                    , msgTarget: 'side'
                                                    , allowBlank: false
                                                },
                                               {
                                                   xtype: 'container',
                                                   layout: {
                                                       type: 'hbox',
                                                       align: 'center'
                                                   },

                                                   items: [
                                                       {
                                                           xtype: 'textfield',
                                                           width: "80%"
                                                           , maxWidth :600
                                                           //labelAlign: 'top',
                                                           , margin: '0 20 0 10',
                                                           itemId: 'folio',
                                                           name: 'folio',
                                                           fieldLabel: 'Número Folio del Ticket',
                                                           msgTarget: 'side',
                                                           labelWidth: 180,
                                                           allowBlank: false
                                                       },
                                                        {
                                                            xtype: 'component',
                                                            width: 16,
                                                            height: 16,
                                                            margin: '20 2 0 2',
                                                            autoEl: {
                                                                tag: 'img',
                                                                src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                            },
                                                            listeners: {
                                                                render: function (p) {
                                                                    //ticketFecha.png
                                                                    FRT.common.FnConnSeg.toolTip('<img src="' + FRT.Routes.root + 'Scripts/FRT/Imagenes/ticketFolio.png">', p, '63 0 0 60');
                                                                }
                                                            }
                                                        }

                                                   ]
                                               },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },

                                                    items: [
                                                       {
                                                           xtype: 'datefield',
                                                           //anchor: '100%',
                                                           //labelAlign: 'top',
                                                           width: "80%"
                                                           , margin: '0 20 0 10'
                                                           , maxWidth :600,
                                                           itemId: 'fechaConsumo',
                                                           name: 'fechaConsumo',
                                                           fieldLabel: 'Fecha Consumo',
                                                           msgTarget: 'side',
                                                           labelWidth: 180,
                                                           format: 'd/m/Y',
                                                           //padding: '1 0 5 0',
                                                           allowBlank: false,
                                                       },
                                                         {
                                                             xtype: 'component',
                                                             width: 16,
                                                             margin: '20 2 0 2',
                                                             height: 16,
                                                             autoEl: {
                                                                 tag: 'img',
                                                                 src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                             },
                                                             listeners: {
                                                                 render: function (p) {
                                                                     FRT.common.FnConnSeg.toolTip('<img src="' + FRT.Routes.root + 'Scripts/FRT/Imagenes/ticketFecha.png">', p, '63 0 0 60');
                                                                 }
                                                             }
                                                         }

                                                    ]
                                                },

                                                {
                                                    xtype: 'textfield',
                                                    itemId: 'total'
                                                    //labelAlign: 'top',
                                                    , margin: '0 20 0 10'
                                                    , width: "80%"
                                                    , maxWidth :600,
                                                    name: 'total',
                                                    fieldLabel: 'Total Consumo',
                                                    labelWidth: 180
                                                    , allowBlank: false
                                                    , msgTarget: 'side'
                                                    // , vtype: 'mayorcerop'CodStatusDoc
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },

                                                    items: [
                                                      {
                                                          xtype: 'checkboxfield',
                                                          itemId: 'Checkfraccion',
                                                          labelWidth: 180,
                                                          fieldLabel: '!! ¿Fraccionar Ticket?',
                                                          name: 'fraccionado',
                                                          inputValue: 'true',
                                                          listeners: {
                                                              change: 'crearFraccionTicket'
                                                          }
                                                      },
                                                         {
                                                             xtype: 'component',
                                                             width: 16,
                                                             margin: '20 2 0 2s',
                                                             height: 16,
                                                             autoEl: {
                                                                 tag: 'img',
                                                                 src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                             },
                                                             listeners: {
                                                                 render: function (p) {
                                                                     FRT.common.FnConnSeg.toolTip('Marcar si desea obtener varias facturas o <br> un monto menor al ticket', p, '63 0 0 60');
                                                                 }
                                                             }
                                                         }

                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldset',
                                                    itemId: 'fielFraccion',
                                                    collapsed: true,
                                                    collapsible: true,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'center'
                                                    },
                                                    defaults: {
                                                        margin: '10,10,10,10'
                                                    },
                                                    padding: '10,10,10,10',
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'center'
                                                            },

                                                            items: [
                                                               {
                                                                   xtype: 'numberfield',
                                                                   width: 130,
                                                                   fieldLabel: 'Número Facciones',
                                                                   name: 'nFracciones',
                                                                   itemId: 'nFracciones',
                                                                   labelAlign: 'top',
                                                                   minValue: 2,
                                                                   maxValue: 30,
                                                                   listeners: {
                                                                       change: 'calcularMontoFraccion'
                                                                   }
                                                               },
                                                                 {
                                                                     xtype: 'component',
                                                                     width: 16,
                                                                     margin: '5 5 5 5',
                                                                     height: 16,
                                                                     autoEl: {
                                                                         tag: 'img',
                                                                         src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                                     },
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             FRT.common.FnConnSeg.toolTip('Ayuda a fraccionar en montos iguales la cantidad del Ticket', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }

                                                            ]
                                                        },

                                                        {
                                                            xtype: 'tbspacer',
                                                            flex: .5
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'center'
                                                            },

                                                            items: [
                                                              {
                                                                  xtype: 'textfield',
                                                                  width: 130,
                                                                  name: 'totalFraccion',
                                                                  itemId: 'totalFraccion',
                                                                  fieldLabel: 'Total Fraccion',
                                                                  labelAlign: 'top'
                                                              },
                                                                 {
                                                                     xtype: 'component',
                                                                     width: 16,
                                                                     margin: '5 5 5 5',
                                                                     height: 16,
                                                                     autoEl: {
                                                                         tag: 'img',
                                                                         src: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/ayuda.png',
                                                                     },
                                                                     listeners: {
                                                                         render: function (p) {
                                                                             FRT.common.FnConnSeg.toolTip('Escribir o indicar la cantidad a facturar', p, '63 0 0 60');
                                                                         }
                                                                     }
                                                                 }

                                                            ]
                                                        }

                                                    ]
                                                },
                                                {
                                                    xtype: 'checkboxfield',
                                                    itemId: 'hold',
                                                    checked: true,
                                                    labelWidth: 180,
                                                    fieldLabel: '!! Enviar Factura al Correo',
                                                    name: 'hold',
                                                    inputValue: 'true',
                                                }

                                            ]
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: .5
                                        }

                                    ]
                                },
                        {
                            xtype: 'container',
                            itemId: 'botonestb',
                            hidden: true,
                            height: 25,
                            padding: 1,
                            layout: {
                                type: 'absolute'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    padding: '10 10 10 10',
                                    style: {
                                        'background-color': 'transparent'
                                    },
                                    items: [
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1,
                                            height: 20,
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Facturar Ticket',
                                            width: 150,
                                            itemId: 'BtnGuardarF',
                                            listeners: {
                                                click: 'guardarDocumento'
                                            }

                                        },

                                        {
                                            xtype: 'tbseparator',
                                            width: 50
                                        },
                                        {
                                            xtype: 'button',
                                            width: 150,
                                            text: 'Consultar Facturas Emitidas',
                                            itemId: 'BtnConsultarF',
                                            listeners: {
                                                click: 'consultarFacturas'
                                            }

                                        },
                                         {
                                             xtype: 'tbseparator'
                                         },
                                           {
                                               xtype: 'tbspacer',
                                               flex: 1,
                                               height: 20,
                                               //width: 400
                                           },
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'tbspacer',
                            flex: 1
                        },
                                {
                                    xtype: 'container',
                                    name: 'datosCliente',
                                    itemId: 'datosCliente',
                                    flex: 4,
                                    //collapsible: true,
                                    //title: 'Datos Fiscales del Cliente',
                                    items: [
                                        {
                                            xtype: 'clientesmain',
                                            itemId: 'clienteMain'
                                        }
                                    ]
                                },
                                 {
                                     xtype: 'tbspacer',
                                     height: 20
                                 },
                                {
                                    xtype: 'tbspacer',
                                    height: 20
                                },
                                  {
                                      xtype: 'container',
                                      itemId: 'documentoDescarga',
                                      hidden: true,
                                      flex: 1,
                                      layout: {
                                          align: 'stretch',
                                          type: 'hbox'
                                      },
                                      title: 'Descargas',
                                      items: [
                                            {
                                                xtype: 'tbspacer',
                                                flex: 1
                                            },
                                          {
                                              xtype: 'tbspacer',
                                              flex: 1
                                          },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 2
                                        }

                                      ]
                                  }



                            ]
                        }
                    ]
                }
            ]
            //    agrega cierre de la funciom 
        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarEmpresasCmb'

    }

});