﻿Ext.define('FRT.view.usuarios.UsuariosMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.usuariosmain',

    cargarStores: function (){
        this.cargarUsuarios();
        this.cargarEmpresas();
        this.cargarPerfiles();
    },

    cargarPerfiles: function (params) {
        var store = Ext.getStore('Perfiles.Perfiles');
        store.load({
            params: {codPerfil:1},
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },

    cargarEmpresas:function(params){
        var store = Ext.getStore('empresas.Empresas');
        store.load({
           // params:params,
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },

    cargarUsuarios: function (params) {
        var store = Ext.getStore('Usuarios.Usuarios');
        store.load({
            params: params ? params : {codPerfil:1},
            scope: this,
            callback: function (records, operation, success) {
            }
        });
    },

    guardarUsuario: function (me, e, eOpts) {
        var form = this.getView().down('#FormUsuario').getForm(),
          valido = form.isValid(),
          data = form.getValues();

        if (!valido) {
            this.mnjs('Error', 'Favor de llenar correctamente el formulario.', Ext.MessageBox.ERROR);
            return;
        }
        if (data.cpasswordUsuario !== data.passwordUsuario) {
            this.mnjs('Error', 'Las contraseñas no coinciden.', Ext.MessageBox.ERROR);
            return;
        }
     

        this.getView().mask("Procesando...");
        var objUsuario = Ext.create('FRT.model.usuarios.Usuario', data);
        objUsuario.setEmpresa(Ext.create('FRT.model.empresas.Empresa', { codEmpresa: data.codEmpresa, nombreEmp: form.findField('codEmpresa').rawValue }))
        objUsuario.setPerfil(Ext.create('FRT.model.perfiles.Perfil', { codPerfil: data.codPerfil, nombrePerfil: form.findField('codPerfil').rawValue }));
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Usuarios/Guardar',
            encode: true,
            params: Ext.encode(objUsuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                this.getView().unmask();
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success == true) {
              
                    this.limpiarForm();
                    if (!Ext.isNumber(objUsuario.get('codUsuario')))
                        this.cargarUsuarios();
                    else
                        Ext.getStore('Usuarios.Usuarios').getById(objUsuario.get('codUsuario')).set(objUsuario.getData(true));
                }
                },
            failure: function (o, r, n) {
                this.getView().unmask();
                this.limpiarForm();
            }

        });
    },

    limpiarForm: function (me, e, eOpts) {
        var form = this.getView().down('#FormUsuario').getForm();
        form.reset();
        var fields = form.getFields().items;
        Ext.Array.each(fields, function (field, indx) {
            field.setReadOnly(false);
        });
        this.getView().down('#BtnGuardar').show();
        this.getView().down('#BtnGuardar').setText('Agregar');
    },

    accionUsuario: function (me, record, item, index, e, eOpts) {

        var form = this.getView().down('#FormUsuario').getForm(),
            pform = this.getView().down('#FormUsuario'),
            view = this.getView();
        if (item === 'actionEditar') {
            this.editar = true;
            view.down('#BtnGuardar').show();
            view.down('#BtnGuardar').setText('Guardar');
            pform.setCollapsed(false);
            var fields = form.getFields().items;
            Ext.Array.each(fields, function (field, indx) {
                field.setReadOnly(false);
            });
            form.loadRecord(record);

        }
        else if (item === 'actionEliminar') {
            Ext.MessageBox.confirm('Alerta', 'Confirmar eliminación', function (button) {
                if (button === 'yes') {
                    view.mask("Procesando...");
                    Ext.Ajax.request({
                        url: FRT.Routes.root + 'Usuarios/Eliminar',
                        encode: true,
                        params: Ext.encode({ codUsuario: record.get('codUsuario') }),
                        headers: { 'Content-Type': 'application/json' },
                        method: 'post',
                        scope: this,
                        success: function (response) {
                            view.unmask();
                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success == true) {
                            
                                this.limpiarForm();
                                if (Ext.getStore('Usuarios.Usuarios').getCount() == 1)
                                    this.cargarUsuarios();

                                Ext.getStore('Usuarios.Usuarios').remove(record);
                                view.down('#GridUsuarios').getView().refresh();
                            }
                        },
                        failure: function (o, r, n) {
                            view.unmask();
                            this.limpiarForm();
                        }

                    });
                }
            }, this);
        } else {
            this.editar = false;
            view.down('#BtnGuardar').hide();
            pform.setCollapsed(false);
            var fields = form.getFields().items;
            Ext.Array.each(fields, function (field, indx) {
                field.setReadOnly(true);
            });
            form.loadRecord(record);

        }
       
    },

    buscarUsuarios: function (me, e, eOpts) {
        var busqcampo = this.getView().down('#Busqcampo').getValue();
        var busqvalor = this.getView().down('#Busqvalor').getValue();

        if ((e.keyCode === Ext.EventObject.ENTER) || ((e.keyCode === Ext.EventObject.BACKSPACE || e.keyCode === Ext.EventObject.DELETE) && me.getValue() == "")) {
         
            this.cargarUsuarios({ busqcampo: busqcampo, busqvalor: busqvalor,codPerfil:1 });
        } else if (me.getItemId() === 'Busqcampo')
        {
            this.cargarUsuarios({ busqcampo: busqcampo, busqvalor: busqvalor, codPerfil: 1 });
        }
    },
    
    asignarParametrosExtras:function (me, page, eOpts){
        var store = Ext.getStore('Usuarios.Usuarios');
        store.getProxy().setExtraParam('codPerfil', 1)
   
},

    mnjs: function (titulo, mnj,ico) {
        Ext.MessageBox.show({
            title: titulo,
            msg: mnj,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon:ico?ico: Ext.MessageBox.INFO
        });
    }
});


