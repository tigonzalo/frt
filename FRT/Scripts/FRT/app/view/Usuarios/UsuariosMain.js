﻿Ext.define('FRT.view.usuarios.UsuariosMain', {
    extend: 'Ext.container.Container',
    alias: 'widget.usuariosmain',
    requires: [
           'Ext.toolbar.Spacer',
           'Ext.grid.column.RowNumberer',
           'Ext.grid.column.Action',
           'Ext.grid.column.Check',
           'FRT.view.usuarios.UsuariosMainController',
           'Ext.form.Panel',
           'Ext.form.FieldSet',
           'Ext.form.field.ComboBox',
           'Ext.form.field.Checkbox',
           'Ext.grid.Panel',
           'Ext.grid.column.Number',
           'Ext.grid.column.Date',
           'Ext.grid.column.Boolean',
           'Ext.grid.View'
    ],


    controller: 'usuariosmain',
    layout: 'border',
    initComponent: function () {
        var me = this;
        storeU = Ext.create('FRT.store.usuarios.Usuarios'),
        storeE = Ext.create('FRT.store.empresas.Empresas'),
        storeP = Ext.create('FRT.store.perfiles.Perfiles');
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    region: 'north',
                    collapsible: true,
                    collapseMode: 'mini',
                    bodyPadding: 10,
                    itemId: 'FormUsuario',
                    title: 'Usuarios',
                    titleAlign: 'center',
                    layout: 'fit',
                    buttons: [
                        {
                            text: 'Agregar',
                            itemId: 'BtnGuardar',
                            disabled: !FRT.common.FnConnSeg.isVisible('Usuarios', 'Guardar'),
                            listeners: {
                                click: 'guardarUsuario'
                            }
                        }, {
                            text: 'Limpiar',
                            itemId: 'BtnLimpiar',
                            listeners: {
                                click: 'limpiarForm'
                            }
                        }

                    ],
                    items: [
                           {
                               xtype: 'fieldset',
                               title: 'Datos del Usuario',
                               layout: {
                                   type: 'hbox',
                                   align: 'stretch'
                               },
                               items: [
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        defaults: {
                                            labelWidth: 130
                                        },
                                        layout: 'anchor',
                                        items: [
                                            {
                                                xtype: 'hiddenfield',
                                                name: 'codUsuario'

                                            },
                                            {
                                                xtype: 'combobox',
                                                anchor: '100%',
                                                fieldLabel: 'Empresa',
                                                store: storeE,
                                                editable: false,
                                                allowBlank: false,
                                                allowOnlyWhitespace: false,
                                                displayField: 'nombreEmp',
                                                valueField: 'codEmpresa',
                                                queryMode: 'local',
                                                name: 'codEmpresa'
                                            },
                                              {
                                                  xtype: 'combobox',
                                                  anchor: '100%',
                                                  fieldLabel: 'Perfil',
                                                  store: storeP,
                                                  editable: false,
                                                  allowBlank: false,
                                                  allowOnlyWhitespace: false,
                                                  displayField: 'nombrePerfil',
                                                  valueField: 'codPerfil',
                                                  queryMode: 'local',
                                                  name: 'codPerfil'
                                              }, {
                                                  xtype: 'textfield',
                                                  anchor: '100%',
                                                  allowBlank: false,
                                                  allowOnlyWhitespace: false,
                                                  fieldLabel: 'Nombre',
                                                  maxLength: 20,
                                                  maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                  name: 'nombreUsuario'
                                              },
                                             {
                                                 xtype: 'textfield',
                                                 anchor: '100%',
                                                 allowBlank: false,
                                                 allowOnlyWhitespace: false,
                                                 fieldLabel: 'Apellido Paterno',
                                                 maxLength: 20,
                                                 maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                 name: 'apePaterUsuario'
                                             },
                                               {
                                                   xtype: 'textfield',
                                                   anchor: '100%',
                                                   fieldLabel: 'Apellido Materno',
                                                   maxLength: 20,
                                                   maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                   name: 'apeMaterUsuario'
                                               }
                                        ]
                                    },
                                      {
                                          xtype: 'tbspacer',
                                          flex: 0.1
                                      },
                                     {
                                         xtype: 'container',
                                         defaults: {
                                             labelWidth: 130
                                         },
                                         flex: 1,
                                         layout: 'anchor',
                                         items: [
                                               {
                                                   xtype: 'textfield',
                                                   anchor: '100%',
                                                   fieldLabel: 'Email',
                                                   inputType: 'email',
                                                   emptyText: 'usuario@mail.com',
                                                   vtype:'email',
                                                   maxLength:50,
                                                   maxLengthText:'Se superó la cantidad maxima de caracteres {0}.',
                                                   allowBlank: false,
                                                   allowOnlyWhitespace: false,
                                                   name: 'mailUsuario'
                                               },
                                                 {
                                                     xtype: 'textfield',
                                                     anchor: '100%',
                                                     fieldLabel: 'Teléfono',
                                                     regex: /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/,
                                                     regexText: 'Se requiere un teléfono válido ejemplo: 123-456-7890.',
                                                     maxLength: 20,
                                                     maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                     name: 'telefonoUsuario'
                                                 },
                                              {
                                                  xtype: 'textfield',
                                                  anchor: '100%',
                                                  fieldLabel: 'Usuario',
                                                  allowBlank: false,
                                                  allowOnlyWhitespace: false,
                                                  maxLength: 20,
                                                  maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                  name: 'loginUsuario'
                                              },
                                            {
                                                xtype: 'textfield',
                                                anchor: '100%',
                                                fieldLabel: 'Contraseña',
                                                inputType: 'password',
                                                allowBlank: false,
                                                maxLength: 20,
                                                maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                name: 'passwordUsuario'
                                            },
                                             {
                                                 xtype: 'textfield',
                                                 anchor: '100%',
                                                 inputType: 'password',
                                                 fieldLabel: 'Confirmar Contraseña',
                                                 name: 'cpasswordUsuario',
                                                 maxLength: 20,
                                                 maxLengthText: 'Se superó la cantidad maxima de caracteres {0}.',
                                                 allowBlank: false
                                             }

                                         ]
                                     }



                               ]
                           }]
                },
                {
                    xtype: 'gridpanel',
                    region: 'center',
                    itemId: 'GridUsuarios',
                    title: 'Listado de Usuarios',
                    titleAlign: 'center',
                    flex: 2,
                    store: storeU,
                    columns: [
                         {
                             xtype: 'rownumberer',
                             width: 35
                         },
                          {
                              xtype: 'actioncolumn',
                              hidden: !FRT.common.FnConnSeg.isVisible('Usuarios', 'Guardar'),
                              width: 35,
                              align: 'center',
                              items: [
                                   {
                                       icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Edit.png',
                                       iconCls: 'actionEditar',
                                       tooltip: 'Editar registro',
                                       handler: function (grid, rowIndex, colIndex) {
                                           var rec = grid.getStore().getAt(rowIndex);
                                           this.up('grid').fireEvent('itemclick', grid, rec, 'actionEditar', rowIndex, colIndex);
                                       }
                                   }
                              ]
                          }, {
                              xtype: 'actioncolumn',
                              hidden: !FRT.common.FnConnSeg.isVisible('Usuarios', 'Eliminar'),
                              width: 35,
                              align: 'center',
                              items: [
                                  {
                                      icon: FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Delete.png',
                                      iconCls: 'actionEliminar',
                                      tooltip: 'Eliminar registro',
                                      handler: function (grid, rowIndex, colIndex) {
                                          var rec = grid.getStore().getAt(rowIndex);
                                          this.up('grid').fireEvent('itemclick', grid, rec, 'actionEliminar', rowIndex, colIndex);
                                      }
                                  }
                              ]
                          },
                            {
                                renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {

                                    return record.get('Perfil') ? record.get('Perfil').nombrePerfil : record.getPerfil().get('nombrePerfil');
                                },
                                xtype: 'gridcolumn',
                                dataIndex: 'codPerfil',
                                text: 'Perfil',
                                flex: 0.05
                            },
                               {
                                   renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                 
                                     return record.get('Empresa') ? record.get('Empresa').nombreEmp : record.getEmpresa().get('nombreEmp');
                                   },
                                   xtype: 'gridcolumn',
                                   dataIndex: 'codEmpresa',
                                   text: 'Empresa',
                                   flex: 0.1
                               },
                           
                     
                        {
                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                return value+' '+ record.get('apePaterUsuario')+' '+ record.get('apeMaterUsuario')
                            },
                            xtype: 'gridcolumn',
                            dataIndex: 'nombreUsuario',
                            text: 'Nombre Completo',
                            flex: 0.1
                        },
                          {
                              xtype: 'gridcolumn',
                              dataIndex: 'loginUsuario',
                              text: 'Usuario',
                              flex: 0.1
                          }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: '',
                                    itemId: 'Busqvalor',
                                    enableKeyEvents: true,
                                    listeners: {
                                        keyup: 'buscarUsuarios'

                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: '',
                                    value:'nombre',
                                    itemId: 'Busqcampo',
                                    editable: false,
                                    store: [['nombre', 'Nombre'], ['empresa', 'Empresa'], ['peril', 'Perfil']],
                                    listeners: {
                                        change: 'buscarUsuarios'

                                    }
                                },
                                {
                                    xtype:'label',
                                    html: '<img src="' + FRT.Routes.root + 'Scripts/FRT/Imagenes/16X16/Search.png">'
                                
                                    
                                }
                            ]
                        },
                        {
                            xtype: 'pagingtoolbar',
                            store: storeU,
                            dock: 'bottom',
                            displayInfo: true,
                            listeners: {
                                beforechange: 'asignarParametrosExtras'

                            }
                        }
                    ],
                    listeners: {
                        itemclick: 'accionUsuario'

                    }
                }
            ]



        });

        me.callParent(arguments);
    },
    listeners: {
        afterrender: 'cargarStores'

    }

});