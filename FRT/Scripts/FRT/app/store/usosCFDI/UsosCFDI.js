Ext.define('FRT.store.usosCFDI.UsosCFDI', {
    extend: 'Ext.data.Store',
    alias: 'store.usoscfdi',

    requires: [
        'FRT.model.usosCFDI.UsoCFDI',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'UsosCFDI',
            autoLoad: false,
            model: 'FRT.model.usosCFDI.UsoCFDI',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'UsosCFDI/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});