﻿Ext.define('FRT.store.municipios.Municipios', {
    extend: 'Ext.data.Store',
    alias: 'store.municipios',

    requires: [
        'FRT.model.municipios.Municipio',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'municipios.Municipios',
            autoLoad: false,
            model: 'FRT.model.municipios.Municipio',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'Municipios/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});