

Ext.define('FRT.store.estados.Estados', {
    extend: 'Ext.data.Store',
    alias: 'store.estados',

    requires: [
        'FRT.model.estados.Estado',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Array'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'estados.Estados',
            autoLoad: false,
            model: 'FRT.model.estados.Estado',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'estados/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});