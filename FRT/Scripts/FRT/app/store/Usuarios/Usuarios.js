﻿Ext.define('FRT.store.usuarios.Usuarios', {
    extend: 'Ext.data.Store',
    alias: 'store.usuarios',

    requires: [
        'FRT.model.usuarios.Usuario',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Usuarios.Usuarios',
            autoLoad: false,
            model: 'FRT.model.usuarios.Usuario',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'Usuarios/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});