Ext.define('FRT.store.clientes.Clientes', {
    extend: 'Ext.data.Store',
    alias: 'store.clientes',

    requires: [
        'FRT.model.clientes.Cliente',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'clientes.Clientes',
            autoLoad: false,
            model: 'FRT.model.clientes.Cliente',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'Clientes/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});