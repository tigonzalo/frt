﻿Ext.define('FRT.store.opciones.Opciones', {
    extend: 'Ext.data.Store',
    alias: 'store.opciones',
    requires: [
        'FRT.model.opciones.Opcion',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Opciones.Opciones',
            autoLoad: false,
            model: 'FRT.model.opciones.Opcion',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + '/Opciones/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});