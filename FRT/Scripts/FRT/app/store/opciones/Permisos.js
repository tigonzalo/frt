﻿Ext.define('FRT.store.opciones.Permisos', {
    extend: 'Ext.data.Store',
    alias: 'store.permisos',

    requires: [
        'FRT.model.opciones.Permiso',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Permisos.Permisos',
            autoLoad: false,
            model: 'FRT.model.opciones.Permiso',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + '/Opciones/ListadoPermisos',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});