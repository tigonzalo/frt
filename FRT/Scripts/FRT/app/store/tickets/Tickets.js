

Ext.define('FRT.store.tickets.Tickets', {
    extend: 'Ext.data.Store',
    alias: 'store.tickets',

    requires: [
        'FRT.model.tickets.Ticket',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Tickets.Tickets',
            autoLoad: false,
            model: 'FRT.model.tickets.Ticket',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root+'Tickets/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});