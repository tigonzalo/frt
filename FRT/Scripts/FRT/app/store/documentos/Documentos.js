

Ext.define('FRT.store.documentos.Documentos', {
    extend: 'Ext.data.Store',
    alias: 'store.documentos',

    requires: [
        'FRT.model.documentos.Documento',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Documentos.Documentos',
            autoLoad: false,
            model: 'FRT.model.documentos.Documento',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root+'Documentos/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});