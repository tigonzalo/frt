Ext.define('FRT.store.formasPagos.FormasPagos', {
    extend: 'Ext.data.Store',
    alias: 'store.formaspagos',
    requires: [
        'FRT.model.formasPagos.FormaPago',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],
    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'FormasPagos',
            autoLoad: false,
            model: 'FRT.model.formasPagos.FormaPago',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'FormasPagos/Listado_33',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});