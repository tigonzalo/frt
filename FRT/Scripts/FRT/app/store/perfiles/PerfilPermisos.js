﻿Ext.define('FRT.store.perfiles.PerfilPermisos', {
    extend: 'Ext.data.Store',
    alias: 'store.perfilpermisos',
    requires: [
        'FRT.model.perfiles.PerfilPermiso',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Perfiles.PerfilPermisos',
            autoLoad: false,
            model: 'FRT.model.perfiles.PerfilPermiso',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + '/PerfilPermisos/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});