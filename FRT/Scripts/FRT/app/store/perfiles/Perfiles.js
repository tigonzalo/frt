Ext.define('FRT.store.perfiles.Perfiles', {
    extend: 'Ext.data.Store',
    alias: 'store.perfiles',
    requires: [
        'FRT.model.perfiles.Perfil',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'Perfiles.Perfiles',
            autoLoad: false,
            model: 'FRT.model.perfiles.Perfil',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root+'/Perfiles/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});