
Ext.define('FRT.store.empresas.Empresas', {
    extend: 'Ext.data.Store',
    alias: 'store.empresas',

    requires: [
        'FRT.model.empresas.Empresa',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoDestroy: true,
            pageSize: 20,
            storeId: 'empresas.Empresas',
            autoLoad: false,
            model: 'FRT.model.empresas.Empresa',
            proxy: {
                type: 'ajax',
                url: FRT.Routes.root + 'Empresas/Listado',
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    rootProperty: 'List'
                }
            }
        }, cfg)]);
    }
});