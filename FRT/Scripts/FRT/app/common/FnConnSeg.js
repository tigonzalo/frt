﻿Ext.define('FRT.common.FnConnSeg', {
    singleton: true,

    ajaxP: null,
    mostrarInicio: function () {
        var main = Ext.ComponentQuery.query('main')[0];
        main.down("#contentPanel").removeAll();
        if (FRT.Session.usuario.Perfil.nombrePerfil == 'Cliente') {
            var documentosConsultaMain = Ext.create('FRT.view.documentos.DocumentosMain');
            main.down("#contentPanel").add(documentosConsultaMain);
            var btnSiguiente = Ext.ComponentQuery.query('clientesmain #BtnDatosCliente')[0];
            btnSiguiente.setVisible(true);
        }
        if (FRT.Session.usuario.Perfil.nombrePerfil == 'Empresa') {
            var clientesListado = Ext.create('FRT.view.clientes.ClientesListadoMain');
            main.down("#contentPanel").add(clientesListado);
            var icon = Ext.ComponentQuery.query('#tituloImage')[0];
            icon.getEl().dom.src = '';
        }

    },
    toolTip: function (texto, field, margin) {
        //if (!SIPCO.Session.usuario.ayuda)
        //    return;

        var theElem = field.getEl();
        var theTip = Ext.create('Ext.tip.Tip', {
            html: texto,
            margin: margin,
            shadow: false
        });
        field.getEl().on('mouseover', function () {
            theTip.showAt(theElem.getX(), theElem.getY());
        });
        field.getEl().on('mouseleave', function () {
            theTip.hide();
        });
    },
    mostrarLogin: function () {

        var main = Ext.ComponentQuery.query('main')[0];
        main ? main.destroy() : null;
        var login = Ext.ComponentQuery.query('login')[0];
        login ? login.destroy() : null;

        Ext.create('FRT.view.login.Login');
        //var main2 = Ext.ComponentQuery.query('main')[0];
        // main2 ? null : Ext.create('FRT.view.main.Main');
    },
    getMain: function () {
        var main = Ext.ComponentQuery.query('main')[0];
        main ? main : Ext.create('FRT.view.main.Main');
        
    },
    getSession: function () {
        
        Ext.Ajax.request({
            url: FRT.Routes.root + 'Usuarios/Session',
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success) {
                    debugger
                    FRT.Session.usuario = resp.user;
                    FRT.Session.menu = resp.menu;
                    var main = Ext.ComponentQuery.query('main')[0];
                    main ? main : Ext.create('FRT.view.main.Main');
                    //this.mostrarInicio();
                    //var main = Ext.ComponentQuery.query('main')[0];
                    //main.down("#contentPanel").removeAll();
                    //var documentosConsultaMain = Ext.create('FRT.view.documentos.DocumentosMain');
                    //main.down("#contentPanel").add(documentosConsultaMain);
                    //var btnSiguiente = Ext.ComponentQuery.query('clientesmain #BtnDatosCliente')[0];
                    //btnSiguiente.setVisible(true);
                }


            },
            failure: function (o, r, n) {
            }

        });
    },

    isVisible: function (nommenu, permiso) {

        var permisosUsuarios = FRT.Session.usuario ? FRT.Session.usuario.Perfil.perfilPermisos : null;
        if (permisosUsuarios == null)
            return;
        var menu = FRT.Session.menu;
        var visible = false,
            idMenu;

        for (var i = 0; i < menu.length; i++) {
            if (menu[i].desOpcion === nommenu) {
                idMenu = menu[i].codOpcion;
                for (var j = 0; j < permisosUsuarios.length; j++) {

                    if (permisosUsuarios[j].Permiso.desPermiso == permiso && permisosUsuarios[j].Permiso.codOpcion == idMenu) {
                        j = permisosUsuarios.length;
                        visible = true;
                    }
                }
                i = menu.length;
            }
        }
        return visible;

    },

    requestcomplete: function (conn, response, options) {

        var resp = Ext.JSON.decode(response.responseText);
        if (resp.success == false) {

            var msgError = resp.responseMessage ? resp.responseMessage.msgError : '',
                nError = resp.responseMessage.nError
            if (nError == 'AppFMC001') {
                return;
                //FRT.common.FnConnSeg.mostrarLogin();
            } else {
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: msgError,
                    buttons: Ext.MessageBox.OK,
                    animateTarget: 'mb9',
                    icon: Ext.MessageBox.ERROR
                });
            }

        }
    },

    requestexception: function (conn, response, options) {

        var resp = Ext.JSON.decode(response.responseText);
        var msgE = resp.responseMessage.msgError;
        Ext.MessageBox.show({
            title: 'Error',
            msg: msgE,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: Ext.MessageBox.ERROR
        });
    },
    parseDate: function (date, dateFormatServer, dateFormatToServer) {
        if (Ext.isDate(date))
            return date;
        else {
            var d = Ext.Date.parse(date, dateFormatServer, true);
            if (d == null)
                return Ext.Date.parse(date, dateFormatToServer, true);
            else
                return d;
        }
    },



});
Ext.Ajax.on({
    requestcomplete: FRT.common.FnConnSeg.requestcomplete,
    requestexception: FRT.common.FnConnSeg.requestexception
});
Ext.define('Ext.overrides.menu.Menu', {
    override: 'Ext.menu.Menu',


    hide: function (deep) {

        if (!this.floating)
            return
        if (this.el && this.isVisible()) {
            this.fireEvent("beforehide", this);
            if (this.activeItem) {
                this.activeItem.deactivate();
                this.activeItem = null;
            }
            this.el.hide();
            this.hidden = true;
            this.fireEvent("hide", this);
        }
        if (deep === true && this.parentMenu) {
            this.parentMenu.hide(true);
        }
    },


});