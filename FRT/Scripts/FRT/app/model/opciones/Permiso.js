﻿Ext.define('FRT.model.opciones.Permiso', {
    extend: 'Ext.data.Model',
    alias: 'model.permiso',
    requires: [
        'Ext.data.Field'
    ],

    idProperty: 'codPermiso',

    fields: [
        {
            name: 'codPermiso',
            type: 'int'
        },
         {
             name: 'codOpcion',
             type: 'int'
         },
        {
            name: 'desPermiso',
            type: 'string'
        }
    ]
});