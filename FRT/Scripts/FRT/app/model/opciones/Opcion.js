﻿Ext.define('FRT.model.opciones.Opcion', {
    extend: 'Ext.data.Model',
    alias: 'model.opcion',
    requires: [
        'Ext.data.Field'
    ],

    idProperty: 'codOpcion',

    fields: [
        {
            name: 'codOpcion',
            type: 'int'
        },
        {
           
            name: 'desOpcion',
            type: 'string'
        }
    ]
});