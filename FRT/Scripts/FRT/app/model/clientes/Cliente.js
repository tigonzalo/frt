Ext.define('FRT.model.clientes.Cliente', {
    extend: 'Ext.data.Model',
    alias: 'model.cliente',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],


    idProperty: 'codCliente',

    hasOne: {
        model: 'FRT.model.usuarios.Usuario',
        name: 'usuario'
    },
    fields: [
        {
            type: 'int',
            name: 'codCliente'
        },
        {
            type: 'int',
            name: 'codUsuario'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'razonSocialClie'
        },
        {
            type: 'string',
            name: 'rfcClie'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'calleClie'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'numExtClie'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'numIntClie'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'coloniaClie'
        },
        {
            type: 'int',
            name: 'codMunicipio'
        },
        {
            type: 'string',
            name: 'codEstado'
        },
        {
            type: 'string',
            name: 'estado'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'localidadClie'
        },
        {
            type: 'string',
            name: 'paisClie'
        },
        {
            type: 'string',
            name: 'cpClie'
        }
    ]
});