

Ext.define('FRT.model.documentos.Documento', {
    extend: 'Ext.data.Model',
    alias: 'model.documento',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
        //'Ext.data.association.BelongsTo'
    ],

    idProperty: 'codDocumento',

    fields: [
        {
            type: 'int',
            name: 'codDocumento'
        },
        {
            type: 'int',
            name: 'codEmpresa'
        },
        {
            type: 'int',
            name: 'codUsuario'
        },
        {
            type: 'int',
            name: 'codCliente'
        },
        {
            type: 'int',
            name: 'CodStatusDoc'
        },
        //{
        //    name: 'empresa',
        //    type: 'string',
        //    convert: function (v, rec) {
        //        if (Ext.isDefined(v))
        //            return v.nombreEmp;
        //        else
        //            return v;
        //    }
        //},

        {
            name: 'StatusDocumento',
            type: 'string',
            convert: function (v, rec) {
                if (Ext.isDefined(v))
                    return v.Descripcion;
                else
                    return v;
            }
        },
        {
            type: 'date',
            name: 'fecha',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y',
            convert: function (v, rec) {
                var fecha = FRT.common.FnConnSeg.parseDate(v, 'MS', 'd/m/Y');
                if (!Ext.isDefined(fecha))
                    return v;
                else
                    return fecha;
            }
        }
        ,{
            type: 'date',
            name: 'fechaConsumo',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y',
            convert: function (v, rec) {
                var fecha = FRT.common.FnConnSeg.parseDate(v, 'MS', 'd/m/Y');
                if (!Ext.isDefined(fecha))
                    return v;
                else
                    return fecha;
            }
        },
        {
            type: 'float',
            name: 'total'
        },
        {
            type: 'string',
            name: 'rutaPdf'
        },
        {
            type: 'string',
            name: 'rutaXml'
        },
        {
            type: 'string',
            name: 'folio'
        },
        {
            type: 'string',
            name: 'folioFactura'
        },
         {
             type: 'boolean',
             name: 'hold'
         },
         {
             type: 'int',
             name: 'nError'
         },
         {
             type: 'boolean',
             name: 'fraccionado'
         },
         {
             type: 'float',
             name: 'totalFraccion'
         },
         {
             type: 'int',
             name: 'nFracciones'
         },
         {
             type: 'string',
             name: 'rfcCliente'
         },
         {
             type: 'string',
             name: 'razonSocialCliente'
         },
         {
             type: 'bool',
             name: 'version33'
         },
         {
             type: 'string',
             name: 'correo'
         }

    ],
    hasOne: [
        {
            model: 'FRT.model.empresas.Empresa',
            name: 'empresa'
        },
        {
            model: 'FRT.model.clientes.Cliente',
            name: 'cliente'
        },
        {
            model: 'FRT.model.usuarios.Usuario',
            name: 'usuario'
        },
        {
            model: 'FRT.model.usosCFDI.UsoCFDI',
            name: 'usoCFDI'
        }
    ],
});

