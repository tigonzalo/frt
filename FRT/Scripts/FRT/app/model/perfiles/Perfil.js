Ext.define('FRT.model.perfiles.Perfil', {
    extend: 'Ext.data.Model',
    alias: 'model.perfil',
    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'codPerfil',

    hasMany: {
        model: 'FRT.model.perfiles.PerfilPermiso',
        name: 'perfilPermisos'
    },
    fields: [
        {
            type: 'int',
            name: 'codPerfil'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'nombrePerfil'
        }
    ]
});