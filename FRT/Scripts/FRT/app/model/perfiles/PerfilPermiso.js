Ext.define('FRT.model.perfiles.PerfilPermiso', {
    extend: 'Ext.data.Model',     
    alias: 'model.perfilpermiso',
    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'codPerfilPermiso',

    fields: [
        {
            type: 'int',
            name: 'codPerfilPermiso'
        },
        {
            type: 'int',
            name: 'codPerfil'
        },
        {
            type: 'int',
            name: 'codPermiso'
        }
    ]
});