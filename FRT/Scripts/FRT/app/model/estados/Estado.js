
Ext.define('FRT.model.estados.Estado', {
    extend: 'Ext.data.Model',
    alias: 'model.estado',

    requires: [
        'Ext.data.field.String'
    ],

    idProperty: 'codEstado',

    fields: [
        {
            type: 'string',
            name: 'codEstado'
        },
        {
            type: 'string',
            name: 'nombreEstado'
        }
    ]
});