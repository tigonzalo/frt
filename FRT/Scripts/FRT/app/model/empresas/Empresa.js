

Ext.define('FRT.model.empresas.Empresa', {
    extend: 'Ext.data.Model',
    alias: 'model.empresa',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'codEmpresa',

    fields: [
        {
            type: 'int',
            name: 'codEmpresa'
        },
        {

            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'nombreEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'rfcEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'telefonoEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'imgEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'mailFromEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'mailPasswordEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'mailHostEmp'
        },
        {
            //convert: function (v, rec) {
            //    return (v = null ? null : Ext.String.trim(v))
            //},
            type: 'int',
            name: 'mailPuertoEmp'
        },
        {
            //convert: function (v, rec) {
            //    return (v = null ? null : Ext.String.trim(v))
            //},
            type: 'bool',
            name: 'mailSslEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'calleEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'noExteriorEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'noInteriorEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'coloniaEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'localidadEmp'
        },
        {
            type: 'int',
            name: 'codMunicipio'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'codEstado'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'paisEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'cpEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'contactoEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'razonSocialEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'rutaEmp'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'passwordCFDI'
        }
    ]
});