

Ext.define('FRT.model.tickets.Ticket', {
    extend: 'Ext.data.Model',
    alias: 'model.ticket',
    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],
    idProperty: 'codTicket',
    fields: [
        {
            type: 'int',
            name: 'codTicket'
        },
        {
            type: 'int',
            name: 'codEmpresa'
        },
        {
            type: 'string',
            name: 'folio'
        },
         {
             type: 'float',
             name: 'total'
         },
         {
             type: 'float',
             name: 'propina'
         },
         {
            type: 'date',
            name: 'fechaCosumo',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y',
            convert: function (v, rec) {
                var fecha = FRT.common.FnConnSeg.parseDate(v, 'MS', 'd/m/Y');
                if (!Ext.isDefined(fecha))
                    return v;
                else
                    return fecha;
            }
        },
        {
            type: 'int',
            name: 'CodStatusDoc'
        }
        //{
        //    type: 'string',
        //    name: 'formaPago'
        //}
    ],
    hasOne: [
        {
            model: 'FRT.model.empresas.Empresa',
            name: 'empresa'
        },
        {
            model: 'FRT.model.statusDocumentos.StatusDocumento',
            name: 'statusDocumento'
        },
        {
            model: 'FRT.model.formasPagos.FormaPago',
            name: 'formaPago'
        }
    ],
});

