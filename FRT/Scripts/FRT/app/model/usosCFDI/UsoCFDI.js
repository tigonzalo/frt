Ext.define('FRT.model.usosCFDI.UsoCFDI', {
    extend: 'Ext.data.Model',
    alias: 'model.usocfdi',
    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],
    idProperty: 'codUsoCFDI',
    fields: [
        {
            type: 'int',
            name: 'codUsoCFDI'
        },
        {
            type: 'string',
            name: 'codigo'
        },
        {
            type: 'string',
            name: 'descripcion'
        },
        {
            type: 'boolean',
            name: 'aplicaFisica'
        },
        {
            type: 'boolean',
            name: 'aplicaMoral'
        },
        {
            type: 'string',
            name: 'claveDescripcion',
            convert: function (v, rec) {
                return rec.get('codigo') + " - " + rec.get('descripcion');
            }
        }
    ]
});