

Ext.define('FRT.model.statusDocumentos.StatusDocumento', {
    extend: 'Ext.data.Model',
    alias: 'model.StatusDocumento',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'CodStatusDoc',

    fields: [
        {
            type: 'int',
            name: 'CodStatusDoc'
        },
        {
            type: 'string',
            name: 'Descripcion'
        }
    ]
});