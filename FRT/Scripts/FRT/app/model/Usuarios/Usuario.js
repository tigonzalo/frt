﻿Ext.define('FRT.model.usuarios.Usuario', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'codUsuario',

    hasOne: [{
        model: 'FRT.model.empresas.Empresa',
        name: 'Empresa'
    },
   {
       model: 'FRT.model.perfiles.Perfil',
       name: 'Perfil'
   }],
    fields: [
        {
            type: 'int',
            name: 'codUsuario'

        },
        {
            convert: function (v, rec) {
                if (Ext.isNumber(v))
                    return v;
                return rec.get('Empresa') ? rec.get('Empresa').codEmpresa : null;
            },
            type: 'int',
            name: 'codEmpresa'
        },
        {
            convert: function (v, rec) {
                if (Ext.isNumber(v))
                    return v;
                return rec.get('Perfil')? rec.get('Perfil').codPerfil:null;
            },
            type: 'int',
            name: 'codPerfil'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'loginUsuario'
        },
        {
            type: 'string',
            name: 'passwordUsuario'
        },

         {
             convert: function (v, rec) {
                 return rec.get('passwordUsuario');
             },
             type: 'string',
             name: 'cpasswordUsuario'
         },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'nombreUsuario'
        },
        {
            convert: function (v, rec) {
                return (v = null ? null : Ext.String.trim(v))
            },
            type: 'string',
            name: 'apePaterUsuario'
        },
        {
            convert: function (v, rec) {
                return (v = null ? '' : Ext.String.trim(v))
            },
            type: 'string',
            name: 'apeMaterUsuario'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'mailUsuario'
        },
        {
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v))
            },
            type: 'string',
            name: 'telefonoUsuario'
        }
    ]
});