﻿Ext.define('FRT.model.municipios.Municipio', {
    extend: 'Ext.data.Model',
    alias: 'model.municipio',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],

    idProperty: 'codMunicipio',

    fields: [
        {
            type: 'int',
            name: 'codMunicipio'
        },
        {
            type: 'string',
            name: 'codEstado'
        },
        {
            type: 'string',
            name: 'nombreMunicipio'
        }
    ]
});