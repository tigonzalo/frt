Ext.define('FRT.model.formasPagos.FormaPago', {
    extend: 'Ext.data.Model',
    alias: 'model.formapago',
    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String'
    ],
    idProperty: 'codFormaPago',
    fields: [
        {
            type: 'int',
            name: 'codFormaPago'
        },
        {
            type: 'string',
            name: 'descripcion'
        },
        {
            type: 'string',
            name: 'clave'
        },
        {
            type: 'string',
            name: 'descripcionClave',
            convert: function (v, rec) {
                return '('+rec.get('clave') + ') ' + rec.get('descripcion');
            }
        }
    ]
});