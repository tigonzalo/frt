﻿
Ext.define('FMCM.store.empresas.Empresas', {
    extend: 'Ext.data.Store',

    requires: [
        'FMCM.model.empresas.Empresa',
         'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    config: {
        model: 'FMCM.model.empresas.Empresa',
        storeId: 'Empresas.Empresas',
        proxy: {
            type: 'ajax',
            actionMethods: {
                read: 'POST'
            },
            paramsAsJson: true,
            url: FMCM.Routes.root + '/Empresas/Listado',
            reader: {
                type: 'json',
                messageProperty: 'responseMessage',
                rootProperty: 'List'
            }
        }
    }
});