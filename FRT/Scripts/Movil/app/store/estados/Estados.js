﻿
Ext.define('FMCM.store.estados.Estados', {
    extend: 'Ext.data.Store',

    requires: [
        'FMCM.model.estados.Estado',
         'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    config: {
        model: 'FMCM.model.estados.Estado',
        storeId: 'Estados.Estados',
        proxy: {
            type: 'ajax',
            actionMethods: {
                read: 'POST'
            },
            paramsAsJson: true,
            url: FMCM.Routes.root + '/Estados/Listado',
            reader: {
                type: 'json',
                messageProperty: 'responseMessage',
                rootProperty: 'List'
            }
        }
    }
});