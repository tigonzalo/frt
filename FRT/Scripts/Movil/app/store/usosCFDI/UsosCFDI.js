﻿Ext.define('FMCM.store.usosCFDI.UsosCFDI', {
    extend: 'Ext.data.Store',
    requires: [
        'FMCM.model.usosCFDI.UsoCFDI',
         'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],
    config: {
        model: 'FMCM.model.usosCFDI.UsoCFDI',
        storeId: 'UsosCFDI',
        proxy: {
            type: 'ajax',
            actionMethods: {
                read: 'POST'
            },
            paramsAsJson: true,
            url: FMCM.Routes.root + '/UsosCFDI/Listado',
            reader: {
                type: 'json',
                messageProperty: 'responseMessage',
                rootProperty: 'List'
            }
        }
    }
});