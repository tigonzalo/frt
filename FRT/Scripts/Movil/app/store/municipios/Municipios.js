﻿
Ext.define('FMCM.store.municipios.Municipios', {
    extend: 'Ext.data.Store',

    requires: [
        'FMCM.model.municipios.Municipio',
         'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    config: {
        model: 'FMCM.model.municipios.Municipio',
        storeId: 'Municipios.Municipios',
        proxy: {
            type: 'ajax',
            actionMethods: {
                read: 'POST'
            },
            paramsAsJson: true,
            url: FMCM.Routes.root + '/Municipios/Listado',
            reader: {
                type: 'json',
                messageProperty: 'responseMessage',
                rootProperty: 'List'
            }
        }
    }
});