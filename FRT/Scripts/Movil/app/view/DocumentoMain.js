
Ext.Date.monthNames = [
    'Enero', 
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
];
Ext.override(Ext.Picker, {
    doneButton: 'Aceptar',
    cancelButton: 'Cancelar'
});
Ext.define('FMCM.view.DocumentoMain', {
    extend: 'Ext.form.Panel',
    alias: 'widget.DocumentoMain',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password',
        'Ext.field.Search',
        'Ext.Button',
        'Ext.field.Select',
        'Ext.dataview.NestedList',
        'Ext.field.Hidden',
        'Ext.field.DatePicker',
        'Ext.picker.Date'
    ],
  
    config: {
        layout: 'vbox',
        storeE:null,
        items: [
            {
              xtype: 'toolbar',
              docked: 'bottom',
              items: [
                        {
                            xtype: 'button',
                            iconCls: 'list',
                            ui: 'plain',
                            handler: function () {
                                if (Ext.Viewport.getMenus().left.isHidden()) {
                                    Ext.Viewport.showMenu('left');
                                }
                                else {
                                    Ext.Viewport.hideMenu('left');
                                }
                            }
                        },
                        {
                            xtype: 'spacer'
                        },
                        {
                            xtype: 'button',
                            iconAlign: 'right',
                            iconCls: 'action',
                            itemId:'btnGuardar',
                            text: 'Facturar'
                        },
                        {
                            xtype: 'spacer'
                        },
              ]
          },
            {
                xtype: 'container',
                flex: 1,
                scrollable: true,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        style: {
                            'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoPhone.jpg' + ')',
                            'background-size': '100% 100%',
                            'background-repeat': 'no-repeat',
                            'background-color': 'transparent'
                        },
                        items: [
                             {
                                 xtype: 'container',
                                 height: 80,
                                 style: {
                                     'background-color': 'transparent',
                                     'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/header.png' + ')',
                                     'background-size': 'auto',
                                     'background-repeat': 'no-repeat',
                                     'background-position': 'top center'
                                 }
                             },
                               {
                                   xtype: 'container',
                                   height: 50,
                                   style: {
                                       'background-color': 'transparent',
                                       'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/titulos/factura.png' + ')',
                                       'background-size': 'auto',
                                       'background-repeat': 'no-repeat',
                                       'background-position': 'top center'
                                   }
                               },
                            {
                                xtype: 'container',
                                style: {
                                    'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoContainer.png' + ')',
                                    'background-size': '100% 100%',
                                    'background-repeat': 'no-repeat',
                                    'background-color': 'transparent'
                                },
                                border: 'false',
                                padding: '40 30 40 30',
                                defaults: {
                                    margin: '10 10 10 10'
                                },
                                items: [
                                     {
                                         xtype: 'hiddenfield',
                                         itemId: 'codDocumento',
                                         name: 'codDocumento'

                                     },

                                    {
                                        xtype: 'selectfield',
                                        itemId: 'cmbEmpresa',
                                        label: 'Empresa/Restaurant',
                                        labelAlign: 'top',
                                        name: 'codEmpresa',
                                        valueField: 'codEmpresa',
                                        displayField: 'nombreEmp',
                                        placeHolder: 'Cargando..',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'selectfield',
                                        itemId: 'cmbUsosCFDI',
                                        label: 'Uso del CFDI',
                                        labelAlign: 'top',
                                        name: 'codUsoCFDI',
                                        valueField: 'codUsoCFDI',
                                        displayField: 'claveDescripcion',
                                        placeHolder: 'Cargando..',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Número del Folio del Ticket',
                                        labelAlign: 'top',
                                        name: 'folio',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'datepickerfield',
                                        label: 'Fecha de Consumo',
                                        labelAlign: 'top',
                                        placeHolder: 'mm/dd/yyyy',
                                        value: new Date(),
                                        name: 'fechaConsumo',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Total Consumo',
                                        labelAlign: 'top',
                                        name: 'total',
                                        itemId: 'total',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'checkboxfield',
                                        style: {
                                            'background-color': 'transparent'
                                        },
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        itemId: 'Checkfraccion',
                                        label: '¿Fraccionar Factura?',
                                        labelAlign: 'top',
                                        name: 'fraccionado'
                                    },
                                    {
                                        xtype: 'fieldset',
                                        height: 239,
                                        hidden: true,
                                        itemId: 'fielFraccion',
                                        title: '',
                                        style: {
                                            'background-color': 'transparent'
                                        },
                                        items: [
                                            {
                                                xtype: 'numberfield',
                                                itemId: 'nFracciones',
                                                label: 'Número de Fracciones',
                                                labelAlign: 'top',
                                                inputCls: 'PanelBio2',
                                                labelCls: 'LabelText',
                                                style: {
                                                    'background-color': 'transparent'
                                                },
                                                name: 'nFracciones'
                                            },
                                            {
                                                xtype: 'textfield',
                                                itemId: 'totalFraccion',
                                                label: 'Total Fracción',
                                                labelAlign: 'top',
                                                name: 'totalFraccion',
                                                inputCls: 'PanelBio2',
                                                labelCls: 'LabelText',
                                                style: {
                                                    'background-color': 'transparent'
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'checkboxfield',
                                        label: 'Enviar al Correo Registrado',
                                        labelAlign: 'top',
                                        name: 'hold',
                                        checked: true,
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    }
                                ]
                            },
                    
                        ]
                    }

                    
                ]
            }
        ]
    }
    

});