
Ext.define('FMCM.view.LoginForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.LoginForm',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Text',
        'Ext.field.Password',
        'Ext.Button'
    ],
   
    config: {
        //layout: {
        //    type: 'vbox',
        //    align: 'stretch'
        //},
        layout: 'vbox',
        scrollable: null,
        items: [
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'spacer',
                        flex:1
                    },
                    {
                        xtype: 'button',
                        iconCls: 'action',
                        itemId: 'BtnLogin',
                        text: 'Aceptar'
                    },
                    {
                        xtype: 'spacer',
                        flex: 1
                    }
                ]
            },
         {
             xtype: 'container',
              flex: 1,
              scrollable: true,
             items: [
                  {
                      xtype: 'container',
                      style: {
                          'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondo.jpg' + ')',
                          'background-size': '100% 100%',
                          'background-repeat': 'no-repeat',
                          'background-color': 'transparent'
                      },
                      items: [
                          {
                              xtype: 'container',
                              height: 80,
                              style: {
                                  'background-color': 'transparent',
                                  'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/header.png' + ')',
                                  'background-size': 'auto',
                                  'background-repeat': 'no-repeat',
                                  'background-position': 'top center'
                              }
                          },
                    {
                        xtype: 'container',
                        height: 50,
                        style: {
                            'background-color': 'transparent',
                            'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/titulos/bienvenido.png' + ')',
                            'background-size': 'auto',
                            'background-repeat': 'no-repeat',
                            'background-position': 'top center'
                        }
                    },
                 {
                     xtype: 'container',
                     style: {
                         'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoContainer.png' + ')',
                         'background-size': '100% 100%',
                         'background-repeat': 'no-repeat',
                         'background-color': 'transparent'
                     },
                    border: 'false',
                    padding: '40 30 40 30',
                     defaults: {
                         margin: '10 10 10 10'
                     },
                     height: 568,
                     layout: 'vbox',
                     items: [
                         
                         {
                             xtype: 'textfield',
                             labelAlign: 'top',
                             inputCls: 'PanelBio2',
                             labelCls: 'LabelText',
                             placeHolder: 'Usuario',
                             style: {
                                 'background-color': 'transparent'
                             },
                             name: 'loginUsuario',
                             label: 'Usuario',
                             required: true
                         },
                         {
                             xtype: 'passwordfield',
                             inputCls: 'PanelBio2',
                             labelCls: 'LabelText',
                             style: {
                                 'background-color': 'transparent'
                             },
                             labelAlign: 'top',
                             name: 'passwordUsuario',
                             label: 'Contraseña',
                             required: true
                         },
                         {
                             xtype: 'container',
                             height: 120,
                             defaults: {
                                 margin: '20 10 20 10'
                             },
                             items: [
                                 {
                                     xtype: 'label',
                                     itemId: 'contraseniaLab',
                                     html: '<a href="#" style="color:rgb(255, 255, 255)">¿Olvidaste tu contraseña? Haz click aqui</a>',
                                   
                                 },
                                 {
                                     xtype: 'label',
                                     itemId: 'nuevoUsuarioLab',
                                     html: '<a href="#" style="color:rgb(255, 255, 255)">¿Eres nuevo usuario? Registrarse aqui</a>',
                                  

                                 }
                             ]
                         }
                     ]
                 }
                      ]
                  }
                 
             ]
         }
        ]
    },
    initialize: function() {
    this.callParent();
    this.down('#contraseniaLab').element.on({
        scope: this, tap: function (e, t) {
            FMCM.app.getController('Main').cargarCorreoMain();
    }
    });
    this.down('#nuevoUsuarioLab').element.on({
        scope: this, tap: function (e, t) {
            FMCM.app.getController('Login').nuevoUsuario();
        }
    });
}
});