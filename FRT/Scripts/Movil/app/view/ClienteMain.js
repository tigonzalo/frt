
Ext.define('FMCM.view.ClienteMain', {
    extend: 'Ext.form.Panel',
    alias: 'widget.ClienteMain',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password',
        'Ext.field.Search',
        'Ext.Button',
        'Ext.field.Select',
        'Ext.dataview.NestedList',
       'Ext.field.Hidden',
       'FMCM.store.estados.Estados',
        'Ext.Label'
    ],
  
    config: {
        scrollable: null,
        layout: 'fit',
        storeE:null,
        items: [
            {
              xtype: 'toolbar',
              docked: 'bottom',
              items: [
                        {
                            xtype: 'button',
                            iconCls: 'list',
                            itemId:'listMenuCliente',
                            hidden: true,
                            ui: 'plain',
                            handler: function () {
                                if (Ext.Viewport.getMenus().left.isHidden()) {
                                    Ext.Viewport.showMenu('left');
                                }
                                else {
                                    Ext.Viewport.hideMenu('left');
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            hidden: true,
                            iconCls: 'arrow_left',
                            itemId:'btnCancelar',
                            text: 'Cancelar',
                            handler: function () {
                                FMCM.app.getController('Main').cerrarSesion();
                            }
                        },
                        {
                            xtype: 'spacer'
                        },
                   
                    {
                        xtype: 'button',
                        iconCls: 'action',
                        itemId:'btnGuardar',
                        text: 'Siguiente'
                    }
              ]
          },
            {
                xtype: 'container',
                flex: 1,
                scrollable: true,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        style: {
                            'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoPhone.jpg' + ')',
                            'background-size': '100% 100%',
                            'background-repeat': 'no-repeat',
                            'background-color': 'transparent'
                        },
                        items: [
                             {
                                 xtype: 'container',
                                 height: 80,
                                 style: {
                                     'background-color': 'transparent',
                                     'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/header.png' + ')',
                                     'background-size': 'auto',
                                     'background-repeat': 'no-repeat',
                                     'background-position': 'top center'
                                 }
                             },
                               {
                                   xtype: 'container',
                                   height: 50,
                                   style: {
                                       'background-color': 'transparent',
                                       'background-image': 'url(' +  FMCM.Routes.root + '/Scripts/Movil/Images/titulos/modificar.png'  + ')',
                                       'background-size': 'auto',
                                       'background-repeat': 'no-repeat',
                                       'background-position': 'top center'
                                   }
                               },
                            {
                                 xtype: 'container',
                                style: {
                                    'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoContainer.png' + ')',
                                    'background-size': '100% 100%',
                                    'background-repeat': 'no-repeat',
                                    'background-color': 'transparent'
                                },
                                border: 'false',
                                padding: '40 30 40 30',
                                defaults: {
                                    margin: '10 10 10 10'
                                },
                                items: [
                                     {
                                         xtype: 'hiddenfield',
                                         itemId: 'codUsuario',
                                         name: 'codUsuario'

                                     },

                                    {
                                        xtype: 'textfield',
                                        label: 'Usuario',
                                        labelAlign: 'top',
                                        name: 'loginUsuario',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'passwordfield',
                                        label: 'Contraseña',
                                        labelAlign: 'top',
                                        name: 'passwordUsuario',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'passwordfield',
                                        label: 'Confirmar Contraseña',
                                        labelAlign: 'top',
                                        name: 'cpasswordUsuario',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Correo',
                                        labelAlign: 'top',
                                        name: 'mailUsuario',
                                        inputCls: 'PanelBio2',
                                        labelCls: 'LabelText',
                                        placeHolder: 'Usuaio',
                                        style: {
                                            'background-color': 'transparent'
                                        }
                                    }
                                ]
                            },
                    {
                        xtype: 'container',
                        style: {
                            'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoContainer.png' + ')',
                            'background-size': '100% 100%',
                            'background-repeat': 'no-repeat',
                            'background-color': 'transparent'
                        },
                        border: 'false',
                        padding: '40 30 40 30',
                        defaults: {
                            margin: '10 10 10 10'
                        },
                        items: [
                            {
                                xtype: 'hiddenfield',
                                itemId: 'codCliente',
                                name: 'codCliente'

                            },
                            {
                                xtype: 'textfield',
                                label: 'Razón Social',
                                labelAlign: 'top',
                                name: 'razonSocialClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Rfc',
                                labelAlign: 'top',
                                name: 'rfcClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Código Postal',
                                labelAlign: 'top',
                                name: 'cpClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Calle',
                                labelAlign: 'top',
                                name: 'calleClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Número Exterior',
                                labelAlign: 'top',
                                name: 'numExtClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Número Interior',
                                labelAlign: 'top',
                                name: 'numIntClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Colonia',
                                labelAlign: 'top',
                                name: 'coloniaClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'selectfield',
                                label: 'Estado',
                                labelAlign: 'top',
                                name: 'codEstado',
                                itemId: 'cmbEstados',
                                valueField: 'codEstado',
                                displayField: 'nombreEstado',
                                placeHolder: 'Cargando..',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'selectfield',
                                label: 'Municipio',
                                labelAlign: 'top',
                                name: 'codMunicipio',
                                itemId: 'cmbMunicipios',
                                displayField: 'nombreMunicipio',
                                valueField: 'codMunicipio',
                                placeHolder: 'Cargando..',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Localidad',
                                labelAlign: 'top',
                                name: 'localidadClie',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            },
                            {
                                xtype: 'textfield',
                                label: 'Pais',
                                labelAlign: 'top',
                                name: 'paisClie',
                                value: 'Mexico',
                                inputCls: 'PanelBio2',
                                labelCls: 'LabelText',
                                style: {
                                    'background-color': 'transparent'
                                }
                            }
                        ]
                    }
                  
                        ]
                    }

                    
                ]
            }
        ]
    }
    

});