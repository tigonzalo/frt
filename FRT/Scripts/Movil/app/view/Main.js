Ext.define('FMCM.view.Main', {
    extend: 'Ext.form.Panel',
    xtype: 'main',
    requires: [
        'Ext.List'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            },
							{
                                xtype: 'fieldset',
                                title: 'MyFieldSet',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    },
                                    {
                                        xtype: 'textfield',
                                        label: 'Field'
                                    }
                                ]
                            }
							
            
        ]
    }
});
