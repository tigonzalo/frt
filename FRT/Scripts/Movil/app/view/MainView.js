
Ext.define('FMCM.view.MainView', {
    extend: 'Ext.Panel',
    alias: 'widget.MainView',

    requires: [
        'FMCM.view.LoginForm',
        'Ext.Menu',
        'Ext.Label'
    ],
    
    config: {
        layout: 'fit',
        scrollable: null,
        items: [
          {
              xtype: 'container',
              layout: 'fit',
              itemId: 'contentPanel',
              flex: 1
          }
        ],
        listeners: {
            initialize: function () {
                Ext.Viewport.setMenu(this.createMenu(), {
                    side: 'left',
                    reveal: true
                });
            }
        },

    },
    createMenu: function () {

        var items = [
            {
                xtype: 'button',
                text: 'Factura',
                iconCls: 'user',
                handler: function () {
                    FMCM.app.getController('Main').cargarClienteMain();
                    Ext.Viewport.hideMenu('left');
                }
            },
            {
                xtype: 'button',
                text: 'Datos',
                iconCls: 'user',
                handler: function () {
                    FMCM.app.getController('Main').cargarClienteMain();
                    Ext.Viewport.hideMenu('left');
                }
            },
            {
                xtype: 'button',
                text: 'Salir',
                itemId:'btnSalir',
                iconCls: 'arrow_left',
                handler: function () {
                    FMCM.app.getController('Main').cerrarSesion();
                    Ext.Viewport.hideMenu('left');
                }
            }
        ];

        return Ext.create('Ext.Menu', {
            style: 'padding: 0',
            id: 'menu',
            width: 200,
            scrollable: 'vertical',
            items: items
        });
    }
});