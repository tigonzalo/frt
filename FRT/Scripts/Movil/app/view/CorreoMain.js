
Ext.define('FMCM.view.CorreoMain', {
    extend: 'Ext.form.Panel',
    alias: 'widget.CorreoMain',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Text',
        'Ext.field.Password',
        'Ext.Button',
        'Ext.Label'
    ],
   
    config: {
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'spacer',
                        flex: 1
                    },
                     {
                         xtype: 'button',
                         iconCls: 'arrow_left',
                         itemId: 'btnCancelar',
                         text: 'Cancelar',
                         handler: function () {
                             FMCM.app.getController('Main').cerrarSesion();
                         }
                     },
                    {
                        xtype: 'spacer',
                        flex:1
                    },
                    {
                        xtype: 'button',
                        iconCls: 'action',
                        itemId: 'BtnCorreo',
                        text: 'Enviar'
                    },
                    {
                        xtype: 'spacer',
                        flex: 1
                    }
                ]
            },
         {
             xtype: 'container',
             flex: 1,
             scrollable: true,
             items: [
                  {
                      xtype: 'container',
                      style: {
                          'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondo.jpg' + ')',
                          'background-size': '100% 100%',
                          'background-repeat': 'no-repeat',
                          'background-color': 'transparent'
                      },
                      items: [
                          {
                              xtype: 'container',
                              height: 80,
                              style: {
                                  'background-color': 'transparent',
                                  'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/header.png' + ')',
                                  'background-size': 'auto',
                                  'background-repeat': 'no-repeat',
                                  'background-position': 'top center'
                              }
                          },
                    {
                        xtype: 'container',
                        height: 50,
                        style: {
                            'background-color': 'transparent',
                            'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/titulos/bienvenido.png' + ')',
                            'background-size': 'auto',
                            'background-repeat': 'no-repeat',
                            'background-position': 'top center'
                        }
                    },
                 {
                     xtype: 'container',
                     style: {
                         'background-image': 'url(' + FMCM.Routes.root + '/Scripts/Movil/Images/fondoContainer.png' + ')',
                         'background-size': '100% 100%',
                         'background-repeat': 'no-repeat',
                         'background-color': 'transparent'
                     },
                    border: 'false',
                    padding: '40 30 40 30',
                    height: 568,
                     defaults: {
                         margin: '10 10 10 10'
                     },
                     

                     items: [
                         {
                             xtype: 'emailfield',
                             labelAlign: 'top',
                             inputCls: 'PanelBio2',
                             labelCls: 'LabelText',
                             name: 'mailUsuario',
                             label: 'Correo',
                             placeHolder: 'email@example.com',
                             required: true,
                             style: {
                                 'background-color': 'transparent'
                             }
                         },
                         
                     ]
                 }
                      ]
                  }
                 
             ]
         }
        ]
    }
    
});