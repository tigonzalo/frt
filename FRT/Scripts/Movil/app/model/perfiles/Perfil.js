﻿Ext.define('FMCM.model.perfiles.Perfil', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    config: {
        idProperty: 'codPerfil',
        fields: [
      {
          name: 'codPerfil',
          type: 'int'
      },
       {
           type: 'string',
           name: 'nombrePerfil'
       }
        
        ],
        validations: [
               { type: 'presence', name: 'nombrePerfil', message: "El nombre del Perfil." }
        ]
      

    }
});