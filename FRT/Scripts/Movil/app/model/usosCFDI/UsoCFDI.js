﻿Ext.define('FMCM.model.usosCFDI.UsoCFDI', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.Field',
        'Ext.data.association.HasOne'
    ],

    config: {
        
        idProperty: 'codUsoCFDI',
        fields: [
      {
          name: 'codUsoCFDI',
          type: 'int'
      },
       {
           name: 'codigo',
           type: 'string'
       },
        {
            name: 'descripcion',
            type: 'string'
        },
        {
            name: 'aplicaFisica',
            type: 'boolean'
        },
        {
            name: 'aplicaMoral',
            type: 'boolean'
        },
        {
            name: 'claveDescripcion',
            type: 'string',
            convert: function (v, rec) {
                return rec.get('codigo') + " - " + rec.get('descripcion');
            }

        }
        
        ]     

    }
});