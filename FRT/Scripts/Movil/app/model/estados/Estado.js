﻿
Ext.define('FMCM.model.estados.Estado', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    config: {
        idProperty: 'codEstado',
        fields: [
            {
                name: 'codEstado',
                type: 'string'
            },
            {
                type: 'string',
                name: 'nombreEstado'
            }
        ]
    }
});