﻿Ext.define('FMCM.model.municipios.Municipio', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    config: {
        idProperty: 'codMunicipio',
        fields: [
            {
                name: 'codMunicipio',
                type: 'int'
            },
            {
                type: 'string',
                name: 'codEstado'
            },
            {
                type: 'string',
                name: 'nombreMunicipio'
            }
        ]
    }
});