﻿Ext.define('FMCM.model.usuarios.Usuario', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field',
        'Ext.data.association.HasOne',
        'FMCM.model.perfiles.Perfil'
    ],

    config: {
        hasOne: {
            associatedName: 'perfil',
            model: 'FMCM.model.perfiles.Perfil',
            primaryKey: 'codPerfil',
            foreignKey: 'codPerfil'
        },
        idProperty: 'codUsuario',
        fields: [
      {
          name: 'codUsuario',
          type: 'int'
      },
       {
           name: 'codEmpresa',
           type: 'int',
           useNull: true
       },
        {
            name: 'codPerfil',
            type: 'int',
            useNull: true
        },
        {
            name: 'loginUsuario',
            type: 'string'
        },
        {
            name: 'passwordUsuario',
            type: 'string'
        },
         {
             convert: function (v, rec) {
                 return rec.get('passwordUsuario');
             },
             type: 'string',
             name: 'cpasswordUsuario'
         },
        {
            name: 'mailUsuario',
            type: 'string'
        }
        ],
        validations: [
               { type: 'presence', name: 'loginUsuario', message: "El Nombre de Usuario." },
               { type: 'length', name: 'loginUsuario', message: "El nombre del Usuario debe ser maximo de 20 caracteres", max: 20 },
               { type: 'presence', name: 'passwordUsuario', message: "La Contraseña." },
               { type: 'length', name: 'passwordUsuario', message: "La contraseña del Usuario debe ser maximo de 20 caracteres", max: 20 },
               { type: 'email', name: 'mailUsuario', message: " El Correo con un formato valido." },
               { type: 'length', name: 'mailUsuario', message: "El Correo del Usuario debe ser maximo de 50 caracteres", max: 50 }
        ]

    }
});