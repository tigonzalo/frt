﻿Ext.define('FMCM.model.empresas.Empresa', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    config: {
        idProperty: 'codEmpresa',
        fields: [
      {
          name: 'codEmpresa',
          type: 'int'
      },
       {
           type: 'string',
           name: 'nombreEmp'
       },
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'rfcEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'telefonoEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'imgEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'mailFromEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'mailPasswordEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'mailHostEmp'
        //},
        //{
        //    //convert: function (v, rec) {
        //    //    return (v = null ? null : Ext.String.trim(v))
        //    //},
        //    type: 'int',
        //    name: 'mailPuertoEmp'
        //},
        //{
        //    //convert: function (v, rec) {
        //    //    return (v = null ? null : Ext.String.trim(v))
        //    //},
        //    type: 'bool',
        //    name: 'mailSslEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'calleEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'noExteriorEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'noInteriorEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'coloniaEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'localidadEmp'
        //},
        //{
        //    type: 'int',
        //    name: 'codMunicipio'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'codEstado'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'paisEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'cpEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'contactoEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'razonSocialEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'rutaEmp'
        //},
        //{
        //    convert: function (v, rec) {
        //        return (v = null ? null : Ext.String.trim(v))
        //    },
        //    type: 'string',
        //    name: 'passwordCFDI'
        //}
        ],
        //validations: [
        //       { type: 'presence', name: 'razonSocialClie', message: "El campo Razón Social." },
        //       { type: 'length', name: 'razonSocialClie', message: "El campo Razón Social debe ser maximo de 200 caracteres", max: 200 },
        //       { type: 'presence', name: 'rfcClie', message: "El campo RFC." },
        //       //{ type: 'length', name: 'rfcClie', message: "El RFC debe ser mínimo de 12 caracteres", min: 12 },
        //       { type: 'length', name: 'rfcClie', message: "El campo RFC debe ser maximo de 14 caracteres", max: 14 },
        //       { type: 'format', name: 'rfcClie', message: "El campo RFC es incorrecto", matcher: /^[A-Z&]{3,4}\d{6}\w{3}/ },
        //       { type: 'presence', name: 'numExtClie', message: "El campo Número Exterior." },
        //       { type: 'length', name: 'numExtClie', message: "El campo Número Exterior debe ser maximo de 12 caracteres", max: 12 },

        //       { type: 'presence', name: 'coloniaClie', message: "El campo Colonia." },
        //       { type: 'length', name: 'coloniaClie', message: "El campo Colonia debe ser maximo de 70 caracteres", max: 70 },
        //       { type: 'presence', name: 'codMunicipio', message: "El campo Municipio." },
        //       { type: 'presence', name: 'codEstado', message: "El campo Estado." },
        //       { type: 'presence', name: 'paisClie', message: "El campo País." },
        //       { type: 'presence', name: 'cpClie', message: "El campo Código Postal." },
        //       { type: 'format', name: 'cpClie', message: "El campo Código Postal es incorrecto.", matcher: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/ },
        //       { type: 'length', name: 'cpClie', message: "El campo Código debe ser maximo de 6 caracteres", max: 6 },
        //       { type: 'presence', name: 'calleClie', message: "El campo Calle" },
        //       { type: 'length', name: 'calleClie', message: "El campo Calle debe ser maximo de 70 caracteres", max: 70 },
        //]
      

    }
});