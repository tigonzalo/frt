﻿Ext.define('FMCM.model.clientes.Cliente', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field',
        'Ext.data.association.HasOne',
        'FMCM.model.usuarios.Usuario'
    ],

    config: {
        hasOne: {
            associatedName: 'usuario',
            model: 'FMCM.model.usuarios.Usuario',
            primaryKey: 'codUsuario',
            foreignKey: 'codUsuario'
        },
        idProperty: 'codCliente',
        fields: [
      {
          name: 'codCliente',
          type: 'int'
      },
       {
           name: 'codUsuario',
           type: 'int'
       },
        {
            name: 'razonSocialClie',
            type: 'string',
            useNull: true
        },
        {
            name: 'rfcClie',
            type: 'string',
            useNull: true,
            convert: function (v, rec) {
                return (v = null ? v : Ext.String.trim(v.toUpperCase()))
            }
        },
        {
            name: 'calleClie',
            type: 'string',
            useNull: true
        },
        {
            name: 'numExtClie',
            type: 'string',
            useNull: true

        },
        {
            name: 'numIntClie',
            type: 'string',
            useNull: true

        },
          
        {
            name: 'coloniaClie',
            type: 'string',
            useNull: true

        },
        {
            name: 'codMunicipio',
            type: 'int',
            useNull: true

        },
        {
            name: 'codEstado',
            type: 'int',
            useNull: true

        },
        {
            name: 'localidadClie',
            type: 'string',
            useNull: true

        },
        {
            name: 'paisClie',
            type: 'string',
            useNull: true

        },
        {
            name: 'cpClie',
            type: 'string',
            useNull: true

        }
        ],
        validations: [
               { type: 'presence', name: 'razonSocialClie', message: "El campo Razón Social." },
               { type: 'length', name: 'razonSocialClie', message: "El campo Razón Social debe ser maximo de 200 caracteres", max: 200 },
               { type: 'presence', name: 'rfcClie', message: "El campo RFC." },
               //{ type: 'length', name: 'rfcClie', message: "El RFC debe ser mínimo de 12 caracteres", min: 12 },
               { type: 'length', name: 'rfcClie', message: "El campo RFC debe ser maximo de 14 caracteres", max: 14 },
               { type: 'format', name: 'rfcClie', message: "El campo RFC es incorrecto", matcher: /^[A-Z&]{3,4}\d{6}\w{3}/ },
               { type: 'presence', name: 'numExtClie', message: "El campo Número Exterior." },
               { type: 'length', name: 'numExtClie', message: "El campo Número Exterior debe ser maximo de 12 caracteres", max: 12 },

               { type: 'presence', name: 'coloniaClie', message: "El campo Colonia." },
               { type: 'length', name: 'coloniaClie', message: "El campo Colonia debe ser maximo de 70 caracteres", max: 70 },
               { type: 'presence', name: 'codMunicipio', message: "El campo Municipio." },
               { type: 'presence', name: 'codEstado', message: "El campo Estado." },
               { type: 'presence', name: 'paisClie', message: "El campo País." },
               { type: 'presence', name: 'cpClie', message: "El campo Código Postal." },
               { type: 'format', name: 'cpClie', message: "El campo Código Postal es incorrecto.", matcher: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/ },
               { type: 'length', name: 'cpClie', message: "El campo Código debe ser maximo de 6 caracteres", max: 6 },
               { type: 'presence', name: 'calleClie', message: "El campo Calle" },
               { type: 'length', name: 'calleClie', message: "El campo Calle debe ser maximo de 70 caracteres", max: 70 },
        ]
      

    }
});