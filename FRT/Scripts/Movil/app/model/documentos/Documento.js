﻿Ext.define('FMCM.model.documentos.Documento', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.Field'
    ],

    config: {
        hasOne: {
            associatedName: 'usoCFDI',
            model: 'FMCM.model.usosCFDI.UsoCFDI',
            primaryKey: 'codUsoCFDI',
            foreignKey: 'codUsoCFDI'
        },
        idProperty: 'codDocumento',
        fields: [
      {
          name: 'codDocumento',
          type: 'int'
      },
      {
          name: 'codEmpresa',
          type: 'int'
      },
      {
          name: 'codUsoCFDI',
          type: 'int'
      },
       {
           name: 'codUsuario',
           type: 'int'
       },
        {
            name: 'codCliente',
            type: 'int'
        },
        {
            name: 'CodStatusDoc',
            type: 'int'
        },
        {
            name: 'StatusDocumento',
            type: 'string',
            convert: function (v, rec) {
                if (Ext.isDefined(v))
                    return v.Descripcion;
                else
                    return v;
            }
        },
       {
           type: 'date',
           name: 'fecha',
           format: 'd/m/Y',
           submitFormat: 'd/m/Y',
           convert: function (v, rec) {
               var fecha = FMCM.common.FnConnSeg.parseDate(v, 'MS', 'd/m/Y');
               if (!Ext.isDefined(fecha))
                   return v;
               else
                   return fecha;
           }
       },
         {
            type: 'date',
            name: 'fechaConsumo',
            format: 'd/m/Y',
            submitFormat: 'd/m/Y',
            convert: function (v, rec) {
                var fecha = FMCM.common.FnConnSeg.parseDate(v, 'MS', 'd/m/Y');
                if (!Ext.isDefined(fecha))
                    return v;
                else
                    return fecha;
            }
        },
        {
            type: 'float',
            name: 'total'
        },
        {
            type: 'string',
            name: 'rutaPdf'
        },
        {
            type: 'string',
            name: 'rutaXml'
        },
        {
            type: 'string',
            name: 'folio'
        },
        {
            type: 'string',
            name: 'folioFactura'
        },
        {
            type: 'boolean',
            name: 'hold'
        },
        {
            type: 'boolean',
            name: 'fraccionado'
        },
        {
            type: 'float',
            name: 'totalFraccion'
        },
        {
            type: 'int',
            name: 'nFracciones'
        }
        ],
        validations: [
               { type: 'format', name: 'codEmpresa', message: "El campo Empresa", matcher: /^[1-9][0-9]*$/ },
               { type: 'format', name: 'codUsoCFDI', message: "El campo Uso CFDI", matcher: /^[1-9][0-9]*$/ },
               { type: 'presence', name: 'folio', message: "El número del Folio del Ticket" },
               { type: 'presence', name: 'fechaConsumo', message: "El campo Fecha de Consumo." },
               { type: 'presence', name: 'total', message: "El campo Total Consumo" },
               
              
        ]
      

    }
});