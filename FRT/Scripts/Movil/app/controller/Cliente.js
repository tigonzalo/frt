
Ext.define('FMCM.controller.Cliente', {
    extend: 'Ext.app.Controller',
    requires: [
    'FMCM.model.estados.Estado',
    'FMCM.store.estados.Estados'
    ],
    config: {
        objActualizarAJAX: null,
        objValidaVista:null,
        refs: {
            MainView: 'MainView',
            ClienteMain: 'ClienteMain',
            cmbEstados: 'ClienteMain #cmbEstados',
            cmbMunicipios: 'ClienteMain #cmbMunicipios'
        },

        control: {

            "ClienteMain #btnGuardar": {
                tap: 'guardarCliente'
            },
            "ClienteMain #btnCancelar": {
                tap: 'cancelar'
            },
            "ClienteMain #cmbEstados": {
                change: 'selectEstado'
            }
        }
    },
    selectEstado: function (Cmb) {
         
        if (Cmb.getValue() == null || Cmb.getValue() =='0')
            return;
        this.cargarMunicipios(Cmb.getValue());
    },
    cargarMunicipios: function (id) {
        store = Ext.getStore('Municipios.Municipios');
        store.removeAll();
        store.load({
            encode: true,
            params: { codEstado: id },
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (records, operation, success, res) {
                if (success) {
                    if (records.length > 0) {
                         
                        store.insert(0, { codEstado: '0', nombreEstado: 'Seleccione el Municipio' });
                        this.getCmbMunicipios().setStore(store);
                        this.getCmbMunicipios().setValue(this.objActualizarAJAX.Municipio.codMunicipio);
                        this.objValidaVista = Ext.encode(this.getClienteMain().getValues());
                       
                    }
                }
               
            }
        });
    },
    cargarEstados: function () {
        store = Ext.getStore('Estados.Estados');
        store.removeAll();
        store.load({
            encode: true,
            // params: { validado: true, eliminado: false, start: 0, limit: 20, tipoBusqueda: campoBuscar, valorBusqueda: valorBuscar },
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (records, operation, success, res) {
                if (success) {
                    if (records.length > 0) {
                         
                        store.insert(0, { codEstado: '0', nombreEstado: 'Seleccione el Estado' });
                        this.getCmbEstados().setStore(store);
                    }
                }
            }
        });
    },

    cancelar: function (button, e, eOpts) {
        this.getClienteMain().destroy();
        var mainView = Ext.ComponentQuery.query('mainView')[0];
        if (!Ext.isDefined(mainView)) {
            FMCM.common.FnConnSeg.getSession();
            return;
        }
            
    },
    guardarCliente: function (button, e, eOpts) {
         
        
        var view = this.getClienteMain(),
            data = this.getClienteMain().getValues();
            objCliente = Ext.create('FMCM.model.clientes.Cliente', this.getClienteMain().getValues());
        objCliente.setUsuario(Ext.create('FMCM.model.usuarios.Usuario', this.getClienteMain().getValues()))
        objCliente.getUsuario().setPerfil(Ext.create('FMCM.model.perfiles.Perfil', { codPerfil: 1 }))
        
        if (data.passwordUsuario !== data.cpasswordUsuario) {
            Ext.Msg.alert("Error: ", 'Las contraseņas no coinciden.', function () { });
            return;
        }
        var errores = objCliente.validate(), erroresU = objCliente.getUsuario().validate(), mnj = "";

        if (!erroresU.isValid()) {

            Ext.each(erroresU.items, function (rec, i) {
                mnj += rec.getMessage() + "<br>";
            });
            Ext.Msg.alert("Favor de Escribir: ", mnj, function () { });
            return;

        }

        if (!errores.isValid()) {
            
            Ext.each(errores.items, function (rec, i) {
                mnj += rec.getMessage() + "<br>";
            });
            Ext.Msg.alert("Favor de Escribir: ", mnj, function () { });
            return ;

        }
        if (this.objValidaVista == Ext.encode(view.getValues())) {
            this.crearDocumentoMain(objCliente.getData(true))
            return;
        }
        view.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });

        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Clientes/Guardar',
            encode: true,
            params: Ext.encode(objCliente.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {
                 
                view.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                     
                    Ext.Msg.alert("Info !!!", 'Datos Actualizados', function () { });
                    this.crearDocumentoMain(objCliente.getData(true))
                  
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });

    },
    crearDocumentoMain: function (objC) {
         
        var documentoMain = Ext.create('widget.DocumentoMain'),
            mainView = this.getMainView();
        mainView.down('#contentPanel').removeAll();
        mainView.down('#contentPanel').add(documentoMain);
        //FMCM.app.getController('Documento').cargarUsosCFDI();
        FMCM.app.getController('Documento').cargarEmpresas();
        FMCM.app.getController('Documento').setObjCliente(objC);
        //FMCM.app.getController('Documento').cargarUsosCFDI();

    },
    enviarLogin: function (button, e, eOpts) {
         
        var loginForm = this.getLoginForm();
        var usuario = Ext.create('FMCM.model.usuarios.Usuario', loginForm.getValues());
         
        var errores = usuario.validate(), mnj = "";
        if (!errores.isValid()) {

            Ext.each(errores.items, function (rec, i) {
                if (rec.getField() == 'loginUsuario' || rec.getField() == 'passwordUsuario')
                    mnj += rec.getMessage() + "<br>";
            });
            if (mnj.length > 0){
                Ext.Msg.alert("Favor de:", mnj, function () { });
                return ;
            }

        }
        loginForm.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });

        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Usuarios/Login',
            encode: true,
            params: Ext.encode(usuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {
                 
                loginForm.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                    this.getLoginForm().destroy();
                    FMCM.Session.usuario = resp.user;
                    FMCM.Session.menu = resp.menu;

                    var clienteMain = Ext.create('widget.ClienteMain'),
                        mainView = this.getMainView();
                    mainView.down('#contentPanel').removeAll();
                    mainView.down('#contentPanel').add(clienteMain);
                    FMCM.app.getController('Cliente').cargarEstados();
                    FMCM.app.getController('Cliente').cargarDetalleCliente();
                    Ext.ComponentQuery.query('ClienteMain #listMenuCliente')[0].setHidden(false)
                    Ext.ComponentQuery.query('ClienteMain #btnCancelar')[0].setHidden(true)
                   
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });

    },
    cargarDetalleCliente: function (me, e, eOpts) {
        
        var objUsuario = Ext.create(FMCM.model.usuarios.Usuario, FMCM.Session.usuario),
        view = this.getClienteMain();
        view.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });
        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Clientes/Listado',
            encode: true,
            params: Ext.encode({codUsuario:objUsuario.get('codUsuario')}),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {

                view.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                     
                    this.objActualizarAJAX = resp.List[0];
                    var objCliente = Ext.create('FMCM.model.clientes.Cliente', resp.List[0]).getData(true),
                        objUsuario = Ext.create('FMCM.model.usuarios.Usuario', resp.List[0].usuario).getData(true);
                    view.setValues(objCliente);
                    view.setValues(objUsuario);
                  
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });
    }

  

});