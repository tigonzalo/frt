
Ext.define('FMCM.controller.Documento', {
    extend: 'Ext.app.Controller',
    requires: [
    'FMCM.model.empresas.Empresa',
    'FMCM.store.empresas.Empresas',
    'FMCM.model.usosCFDI.UsoCFDI',
    'FMCM.store.usosCFDI.UsosCFDI'
    ],
    config: {
        objActualizarAJAX: null,
        objValidaVista: null,
        objCliente:null,
        refs: {
            MainView: 'MainView',
            DocumentoMain: 'DocumentoMain',
            cmbEmpresa: 'DocumentoMain #cmbEmpresa',
            cmbUsosCFDI: 'DocumentoMain #cmbUsosCFDI',
            Checkfraccion: 'DocumentoMain #Checkfraccion',
            fielFraccion: 'DocumentoMain #fielFraccion',
            totalFraccion: 'DocumentoMain #totalFraccion',
            nFracciones: 'DocumentoMain #nFracciones',
            total: 'DocumentoMain #total'

        },

        control: {

            "DocumentoMain #btnGuardar": {
                tap: 'guardarDocumento'
            },
            "DocumentoMain #cmbEmpresa": {
                change: 'selectEmpresa'
            },
            "DocumentoMain #btnCancelar": {
                tap: 'cancelar'
            },
            "DocumentoMain #Checkfraccion": {
                change: 'crearFraccionTicket'
            },
            "DocumentoMain #nFracciones": {
                change: 'calcularMontoFraccion'
            }
            
        },
        //init: function () {
        //    this.cargarUsosCFDI();
        //},
        
    },
    //launch: function() {
    //    console.log('launch login');
    //},
    //init: function () {
    //    this.cargarUsosCFDI();
    //},
    selectEmpresa: function () {
        this.cargarUsosCFDI();
    },
    calcularMontoFraccion: function (me, newValue, oldValue, eOpts) {


        var montoTicket = this.getTotal().getValue(),
            nFracciones = this.getNFracciones().getValue();

        var totalFraccion = montoTicket / nFracciones;
        this.getTotalFraccion().setValue(totalFraccion.toFixed(2));


    },
    crearFraccionTicket: function (me, newValue, oldValue, eOpts) {
        this.expandirFraccion(newValue);
    },
    expandirFraccion: function (value) {

        var fielFraccion = this.getFielFraccion(),
            totalFraccion = this.getTotalFraccion().getValue(),
            nFracciones = this.getNFracciones().getValue();

        if (value) {
            fielFraccion.setHidden(false)
            this.getNFracciones().setValue(2);
        } else {
            fielFraccion.setHidden(true)
        }

    },
    cargarUsosCFDI: function () {
        store = Ext.getStore('UsosCFDI');
        store.removeAll();
        store.load({
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (records, operation, success, res) {
                if (success) {
                    if (records.length > 0) {
                        store.insert(0, { codUsoCFDI: 0, descripcion: 'Seleccionar..', clave: '' });
                        this.getCmbUsosCFDI().setStore(store);

                    }
                }

            }
        });
    },
    cargarEmpresas: function () {
        store = Ext.getStore('Empresas.Empresas');
        store.removeAll();
        store.load({
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (records, operation, success, res) {
                if (success) {
                    //this.cargarUsosCFDI();
                    if (records.length > 0) {
                        store.insert(0, { codEmpresa: 0, nombreEmp: 'Seleccionar..' });
                        this.getCmbEmpresa().setStore(store);
                        
                    }
                }

            }
        });
    },

    cancelar: function (button, e, eOpts) {
        this.getDocumentoMain().destroy();
        var mainView = Ext.ComponentQuery.query('mainView')[0];
        if (!Ext.isDefined(mainView)) {
            FMCM.common.FnConnSeg.getSession();
            return;
        }
            
    },
    guardarDocumento: function (button, e, eOpts) {
        var view = this.getDocumentoMain(),
            data = this.getDocumentoMain().getValues();
            objDocumento = Ext.create('FMCM.model.documentos.Documento', data);
            objDocumento.set('codCliente', objCliente.data.codCliente);
            objDocumento.set('codUsuario', objCliente.data.usuario.codUsuario);
            
        debugger
        var errores = objDocumento.validate(), mnj = "";


        if (!errores.isValid()) {
            
            Ext.each(errores.items, function (rec, i) {
                mnj += rec.getMessage() + "<br>";
            });
            Ext.Msg.alert("Favor de Escribir: ", mnj, function () { });
            return ;

        }
        objDocumento.setUsoCFDI(this.getCmbUsosCFDI().getRecord());
       
        view.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });

        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Documentos/Guardar',
            encode: true,
            params: Ext.encode({ objDocumento: objDocumento.getData(true), helper: data.enviarCorreo==null ? true:data.enviarCorreo }),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {
                 
                view.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                    Ext.Msg.alert("información: ", 'El sistema le enviará en breve al correo registrado su factura.', function () { });
                    this.getDocumentoMain().reset()
                   
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });

    },
    crearDocumentoMain: function () {
        var documentoMain = Ext.create('widget.DocumentoMain'),
            mainView = this.getMainView();
        mainView.down('#contentPanel').removeAll();
        mainView.add(documentoMain);

    },
    cargarDetalleDocumento: function (me, e, eOpts) {
        var objUsuario = Ext.create(FMCM.model.usuarios.Usuario, FMCM.Session.usuario),
        view = this.getDocumentoMain();
        view.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });
        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Documentos/Listado',
            encode: true,
            params: Ext.encode({codUsuario:objUsuario.get('codUsuario')}),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {

                view.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                     
                    this.objActualizarAJAX = resp.List[0];
                    var objDocumento = Ext.create('FMCM.model.clientes.Documento', resp.List[0]).getData(true),
                        objUsuario = Ext.create('FMCM.model.usuarios.Usuario', resp.List[0].usuario).getData(true);
                    view.setValues(objDocumento);
                    view.setValues(objUsuario);  

                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });
        
    }

  

});