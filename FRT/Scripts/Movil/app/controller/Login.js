
Ext.define('FMCM.controller.Login', {
    extend: 'Ext.app.Controller',
    requires: [
    'FMCM.model.usuarios.Usuario'
    ],
    config: {
        refs: {
            MainView: 'MainView',
            LoginForm: 'LoginForm',
            ClienteMain: 'ClienteMain',
            listMenuCliente: 'ClienteMain #listMenuCliente'
        },

        control: {
            
            "LoginForm #BtnLogin": {
                tap: 'enviarLogin'
            }

        }
    },

    
    nuevoUsuario: function (button, e, eOpts) {
         
        var mainView = this.getMainView();
        mainView.down('#contentPanel').removeAll(),
        clienteMain = Ext.create('widget.ClienteMain');
        mainView.down('#contentPanel').add(clienteMain);
        FMCM.app.getController('Cliente').cargarEstados();
        Ext.ComponentQuery.query('ClienteMain #btnCancelar')[0].setHidden(false)
    },
    enviarLogin: function (button, e, eOpts) {
         
        var loginForm = this.getLoginForm();
        var usuario = Ext.create('FMCM.model.usuarios.Usuario', loginForm.getValues());
         
        var errores = usuario.validate(), mnj = "";
        if (!errores.isValid()) {

            Ext.each(errores.items, function (rec, i) {
                if (rec.getField() == 'loginUsuario' || rec.getField() == 'passwordUsuario')
                mnj += rec.getMessage() + "<br>";
            });
            if (mnj.length > 0){
                Ext.Msg.alert("Favor de:", mnj, function () { });
                return ;
            }

        }
        loginForm.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });

        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Usuarios/Login',
            encode: true,
            params: Ext.encode(usuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {
                 
                loginForm.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                    this.getLoginForm().destroy();
                    FMCM.Session.usuario = resp.user;
                    FMCM.Session.menu = resp.menu;

                    var clienteMain = Ext.create('widget.ClienteMain'),
                        mainView = this.getMainView();
                    mainView.down('#contentPanel').removeAll();
                    mainView.down('#contentPanel').add(clienteMain);
                    FMCM.app.getController('Cliente').cargarEstados();
                    FMCM.app.getController('Cliente').cargarDetalleCliente();
                    Ext.ComponentQuery.query('ClienteMain #listMenuCliente')[0].setHidden(false)
                    Ext.ComponentQuery.query('ClienteMain #btnCancelar')[0].setHidden(true)
                   
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });

    }

  

});