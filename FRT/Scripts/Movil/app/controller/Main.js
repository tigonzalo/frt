
Ext.define('FMCM.controller.Main', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            MainView: 'MainView'
        }
    },
    cerrarSesion: function () {
        
        var view = this.getMainView();
        view.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });
        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Usuarios/Logout',
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                 
                
            },
            failure: function (o, r, n) {
               

            }

        });
    },
    cargarVistaMain: function () {

        if( this.getMainView()){
            FMCM.Session.usuario = null;
            this.getMainView().destroy();
            
        }
        var mainView = Ext.create('widget.MainView');
        mainView.down('#contentPanel').removeAll();

        if (FMCM.Session.usuario == null) {
           var loginForm = Ext.create('widget.LoginForm');
           mainView.down('#contentPanel').add(loginForm);
        }

        Ext.Viewport.add(mainView);

    },
    cargarClienteMain: function () {

    var mainView = this.getMainView();
    mainView.down('#contentPanel').removeAll();
    var ClienteMain = Ext.create('widget.ClienteMain');
    mainView.down('#contentPanel').add(ClienteMain);
    FMCM.app.getController('Cliente').cargarEstados();
    FMCM.app.getController('Cliente').cargarDetalleCliente();
    Ext.ComponentQuery.query('ClienteMain #listMenuCliente')[0].setHidden(false)
   

    },
    cargarDocumentoMain: function (objC) {
        debugger
         
        var mainView = Ext.ComponentQuery.query('MainView')[0];
        mainView.down('#contentPanel').removeAll();
        var DocumentoMain = Ext.create('widget.DocumentoMain');
        mainView.down('#contentPanel').add(DocumentoMain);
        FMCM.app.getController('Documento').cargarEmpresas();
        FMCM.app.getController('Documento').cargarUsosCFDI();
        FMCM.app.getController('Documento').setObjCliente(objC)


    },
    cargarCorreoMain: function () {
        var mainView = Ext.ComponentQuery.query('MainView')[0];
        mainView.down('#contentPanel').removeAll();
        var CorreoMain = Ext.create('widget.CorreoMain');
        mainView.down('#contentPanel').add(CorreoMain);



    }

});