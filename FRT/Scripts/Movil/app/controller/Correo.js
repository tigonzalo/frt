
Ext.define('FMCM.controller.Correo', {
    extend: 'Ext.app.Controller',
    requires: [
    'FMCM.model.usuarios.Usuario'
    ],
    config: {
        refs: {
            MainView: 'MainView',
            CorreoMain: 'CorreoMain'
        },

        control: {
            
            "MainView #BtnCorreo": {
                tap: 'enviarContrasenia'
            }

        }
    },

    enviarContrasenia: function (button, e, eOpts) {
         
        var correoMain = this.getCorreoMain();
        var usuario = Ext.create('FMCM.model.usuarios.Usuario', correoMain.getValues());
        
        var errores = usuario.validate(), mnj = "";
        if (!errores.isValid()) {

            Ext.each(errores.items, function (rec, i) {
                if (rec.getField() == 'mailUsuario' )
                mnj += rec.getMessage() + "<br>";
            });
            if (mnj.length > 0){
                Ext.Msg.alert("Favor de:", mnj, function () { });
                return ;
            }

        }
        correoMain.mask({
            xtype: 'loadmask',
            message: 'Procesando...'
        });

        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Sistemas/OptenerContrasenia',
            encode: true,
            params: Ext.encode(usuario.getData(true)),
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            callback: function (operation, success, res) {
                correoMain.unmask();
                var resp = Ext.decode(res.responseText);
                if (resp.success === true) {
                    Ext.Msg.alert("Info !!!", 'El sistema Envi� su Usuario y contrase�a', function () { });
                    FMCM.app.getController('Main').cerrarSesion();
           
                   // FMCM.common.FnConnSeg.mostrarInicio();
                } else {

                    var mnj = resp.responseMessage.msgError;
                    Ext.Msg.alert("Error !!!", mnj, function () { });
                }
            }
        });

    }

  

});