﻿Ext.define('FMCM.common.FnConnSeg', {
    singleton: true,

    ajaxP: null,
    mostrarInicio: function () {
        
        var mainView = Ext.ComponentQuery.query('mainView')[0];
        mainView ? mainView.destroy() : mainView = Ext.create('widget.MainView');

        if (FMCM.Session.usuario.Perfil.nombrePerfil == 'Cliente') {
            var ClienteMain = Ext.ComponentQuery.query('ClienteMain')[0];
            ClienteMain ? ClienteMain.destroy() : ClienteMain = Ext.create('widget.ClienteMain');
            mainView.down('#contentPanel').add(ClienteMain);
            FMCM.app.getController('Cliente').cargarEstados();
            FMCM.app.getController('Cliente').cargarDetalleCliente();
            Ext.ComponentQuery.query('ClienteMain #listMenuCliente')[0].setHidden(false)
           
        }
        Ext.Viewport.add(mainView);

    },
    toolTip: function (texto, field, margin) {
        //if (!SIPCO.Session.usuario.ayuda)
        //    return;

        var theElem = field.getEl();
        var theTip = Ext.create('Ext.tip.Tip', {
            html: texto,
            margin: margin,
            shadow: false
        });
        field.getEl().on('mouseover', function () {
            theTip.showAt(theElem.getX(), theElem.getY());
        });
        field.getEl().on('mouseleave', function () {
            theTip.hide();
        });
    },
    mostrarLogin: function () {
        FMCM.app.getController('Main').cargarVistaMain();
    },

    getSession: function () {
         
        Ext.Ajax.request({
            url: FMCM.Routes.root + 'Usuarios/Session',
            encode: true,
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            scope: this,
            success: function (response) {
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.success) {
                     
                    FMCM.Session.usuario = resp.user;
                    FMCM.Session.menu = resp.menu;
                    this.mostrarInicio();
                   
                    //var main = Ext.ComponentQuery.query('main')[0];
                    //main ? main : Ext.create('FMCM.view.main.Main');

                    //
                    //var main = Ext.ComponentQuery.query('main')[0];
                    //main.down("#contentPanel").removeAll();
                    //var documentosConsultaMain = Ext.create('FMCM.view.documentos.DocumentosMain');
                    //main.down("#contentPanel").add(documentosConsultaMain);
                    //var btnSiguiente = Ext.ComponentQuery.query('clientesmain #BtnDatosCliente')[0];
                    //btnSiguiente.setVisible(true);
                }


            },
            failure: function (o, r, n) {
            }

        });
    },

    isVisible: function (nommenu, permiso) {

        var permisosUsuarios = FMCM.Session.usuario ? FMCM.Session.usuario.Perfil.perfilPermisos : null;
        if (permisosUsuarios == null)
            return;
        var menu = FMCM.Session.menu;
        var visible = false,
            idMenu;

        for (var i = 0; i < menu.length; i++) {
            if (menu[i].desOpcion === nommenu) {
                idMenu = menu[i].codOpcion;
                for (var j = 0; j < permisosUsuarios.length; j++) {

                    if (permisosUsuarios[j].Permiso.desPermiso == permiso && permisosUsuarios[j].Permiso.codOpcion == idMenu) {
                        j = permisosUsuarios.length;
                        visible = true;
                    }
                }
                i = menu.length;
            }
        }
        return visible;

    },

    requestcomplete: function (conn, response, options) {
         
        var resp = Ext.JSON.decode(response.responseText);
        if (resp.success == false) {

            var msgError = resp.responseMessage ? resp.responseMessage.msgError : '',
                nError = resp.responseMessage.nError
            if (nError == 'AppFMC001') {
                FMCM.common.FnConnSeg.mostrarLogin();
            } else {
                
                Ext.Msg.alert("Error !!!", msgError, function () { });

            }

        }
    },

    requestexception: function (conn, response, options) {
         
        var resp = Ext.JSON.decode(response.responseText);
        var msgE = resp.responseMessage.msgError;
        Ext.MessageBox.show({
            title: 'Error',
            msg: msgE,
            buttons: Ext.MessageBox.OK,
            animateTarget: 'mb9',
            icon: Ext.MessageBox.ERROR
        });
    },
    parseDate: function (date, dateFormatServer, dateFormatToServer) {
        if (Ext.isDate(date))
            return date;
        else {
            var d = Ext.Date.parse(date, dateFormatServer, true);
            if (d == null)
                return Ext.Date.parse(date, dateFormatToServer, true);
            else
                return d;
        }
    },



});

Ext.define('Ext.overrides.menu.Menu', {
    override: 'Ext.menu.Menu',


    hide: function (deep) {

        if (!this.floating)
            return
        if (this.el && this.isVisible()) {
            this.fireEvent("beforehide", this);
            if (this.activeItem) {
                this.activeItem.deactivate();
                this.activeItem = null;
            }
            this.el.hide();
            this.hidden = true;
            this.fireEvent("hide", this);
        }
        if (deep === true && this.parentMenu) {
            this.parentMenu.hide(true);
        }
    },


});