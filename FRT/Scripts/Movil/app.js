/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/
Ext.define('FMCM.Routes', { statics: { root: document.FMCUrl } });
Ext.define('FMCM.Session', { statics: { usuario: null, menu: null } });

Ext.application({
    name: 'FMCM',
    requires: [
        'Ext.MessageBox',
        'FMCM.util.SizeMonitor', //TEMP fix, Chrome 43 bug
        'FMCM.util.PaintMonitor', //TEMP fix, Chrome 43 bug
        'FMCM.common.FnConnSeg'
    ],

    models: [
       'usuarios.Usuario',
       'clientes.Cliente',
       'municipios.Municipio',
       'estados.Estado',
       'perfiles.Perfil',
       'empresas.Empresa',
       'documentos.Documento',
       'usosCFDI.UsoCFDI'
    ],
    views: [
        'MainView',
        'LoginForm',
        'ClienteMain',
        //'DocumentoMain',
        'CorreoMain'
    ],
    controllers: [
        'Main',
        'Login',
        'Cliente',
        'Documento',
        'Correo'
    ],
    stores: [
       'municipios.Municipios',
       'estados.Estados',
       'empresas.Empresas',
       'usosCFDI.UsosCFDI'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },
    appFolder: FMCM.Routes.appRoot + 'Scripts/43/app',
    launch: function () {
        
        document.FMCUrl = {};
        Ext.Ajax.on({
            requestcomplete: FMCM.common.FnConnSeg.requestcomplete,
            requestexception: FMCM.common.FnConnSeg.requestexception
        });
        FMCM.common.FnConnSeg.getSession();
        if (Ext.get('page-loader')) {
            Ext.get('page-loader').destroy();
        }
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
